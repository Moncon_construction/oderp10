# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2009-2014 Monos Group (<http://monos.mn>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models
from odoo.exceptions import UserError, ValidationError
from odoo.tools.translate import _
# from bzrlib.transport import readonly
from odoo.addons.l10n_mn_extra_cost_item.models.extra_cost_item import SPLIT_METHOD_EC as ec_split_method


class StockPickingExtraCost(models.Model):
    _name = 'stock.extra.cost'
    _description = 'Stock Extra Cost'

    picking_id = fields.Many2one('stock.picking',
                                 'Picking',
                                 required=True)
    purchase_id = fields.Integer('Purchase',
                                 readonly=True,
                                 default=False)
    name = fields.Char('Name',
                       size=128,
                       required=True)
    item_id = fields.Many2one('extra.cost.item',
                              'Item id',
                              required=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('completed', 'Completed')],
                             string='State',
                             default='draft',
                             required=True)
    amount = fields.Float('Amount',
                          required=True)
    currency_id = fields.Many2one('res.currency',
                                  'Currency',
                                  required=True)
    account_id = fields.Many2one('account.account',
                                 'Account',
                                 required=True)
    allocmeth = fields.Selection(ec_split_method,
                                 string='Allocation Method',
                                 default='by_subtotal',
                                 required=True)
    partner_id = fields.Many2one('res.partner',
                                 'Related Partner',
                                 required=True)
    account_analytic_id = fields.Many2one('account.analytic.account',
                                          'Analytic Account')
    invoice_id = fields.Many2one('account.invoice',
                                 'Invoice')
    date_invoice = fields.Date('Invoice Date',
                               required=True)
    adjustment_lines = fields.One2many('stock.extra.cost.line',
                                       'extra_cost_id',
                                       'Cost Adjustment Lines',
                                       readonly=True,
                                       ondelete='cascade')

    # Additional Field Displaying the used currency
    currency_rate = fields.Float('Extra Cost Currency Rate',
                                 readonly=True,
                                 compute='_get_ec_ratebydate',
                                 store=True)
    currency_amount = fields.Float('Extra Cost Currency Amount',
                                   readonly=True,
                                   compute='_get_ec_ramountbydate',
                                   store=True)

    @api.multi
    @api.depends('date_invoice',
                 'currency_id')
    def _get_ec_ratebydate(self):
        for this in self:
            this.ensure_one()
            this.currency_rate = \
                this.currency_id.with_context(
                    date=this.date_invoice
                    ). \
                compute(
                    1,
                    this.picking_id.company_id.currency_id
                    )

    @api.multi
    @api.depends('date_invoice',
                 'currency_id',
                 'amount')
    def _get_ec_ramountbydate(self):
        for this in self:
            this.currency_amount = \
                this.currency_id.with_context(
                    date=this.date_invoice
                    ). \
                compute(
                    this.amount,
                    this.picking_id.company_id.currency_id
                    )

    @api.onchange('item_id')
    def onchange_picking_id(self):
        if self.env['purchase.order'].search([
                ('name', '=', self.picking_id.origin)]):
            self.purchase_id = self.env['purchase.order'].search([
                ('name', '=', self.picking_id.origin)])[0]
        self.currency_id = self.item_id.currency.id
        self.allocmeth = self.item_id.allocmeth
        self.name = self.item_id.name
        self.partner_id = self.picking_id.partner_id
        self.date_invoice = self.picking_id.min_date
        self.account_id = self.item_id.account_id

    # Нэхэмжлэх үүсгэх функц 2017.10.13
    @api.multi
    def make_invoice(self):
        product_uom_unit = self.env['product.uom']
        inv_obj = self.env['account.invoice']
        for extra in self:
            journal_ids = self.env['account.journal'].search(
                [('type', '=', 'purchase'),
                 ('company_id', '=', self.picking_id.company_id.id)],
                limit=1)
            if not journal_ids:
                raise Exception.exceptions.except_orm(
                    _('Error!'),
                    _('''Define purchase journal
                      for this company: "%s" (id:%d).''') % (
                          self.company_id.name, self.company_id.id))
            account = self.partner_id.property_account_receivable_id
            inv_vals = {
                'name': self.name,
                'reference': self.name,
                'account_id': account.id,
                'type': 'in_invoice',
                'partner_id': extra.partner_id.id,
                'currency_id': extra.currency_id.id,
                'journal_id': len(journal_ids) and journal_ids[0].id or False,
                'invoice_line_ids': [(0, 0, {
                        'name': extra.name,
                        'account_id': extra.account_id.id,
                        'price_unit': extra.amount or 0.0,
                        'quantity': 1.0,
                        'product_id': False,
                        'uom_id': product_uom_unit,
                        'account_analytic_id': extra.account_analytic_id.id
                                             or False})],
                'origin': self.name,
                'fiscal_position_id': False,
                'payment_term_id': False,
                'company_id': self.picking_id.company_id.id,
                'date_invoice': extra.date_invoice or self.picking_id.min_date
            }

            inv_id = inv_obj.create(inv_vals)
            extra.write({'invoice_id': inv_id.id})
        return True

    @api.multi
    def calculate_cost(self):
        pack_cost_obj = self.env['stock.extra.cost.line']
        self.mapped('adjustment_lines').unlink()

        total_cost_plus = 0

        cost_line = self

        total_qty = 0.0
        total_amount = 0.0
        total_price = 0.0
        total_weight = 0.0
        total_volume = 0.0
        total_line = 0.0

        for move in self.picking_id.mapped('move_lines'):
            if move.product_id:
                total_weight += move.product_id.weight * move.product_qty
                total_volume += move.product_id.volume * move.product_qty
            total_amount += move.price_unit * move.product_qty
            total_price += move.price_unit
            total_qty += move.product_qty
            total_line += 1

        per_unit = 0
        costlineamount = cost_line.amount
        if cost_line.allocmeth == 'by_quantity':
            per_unit = (costlineamount / total_qty)
        elif cost_line.allocmeth == 'by_weight' and total_weight:
            per_unit = (costlineamount / total_weight)
        elif cost_line.allocmeth == 'by_volume' and total_volume:
            per_unit = (costlineamount / total_volume)
        elif cost_line.allocmeth == 'equal':
            per_unit = (costlineamount / total_line)
        elif cost_line.allocmeth == 'by_subtotal' and total_amount:
            per_unit = (costlineamount / total_amount)

        for move in self.picking_id.mapped('move_lines'):
            _cost_plus = 0
            if cost_line.allocmeth == 'by_quantity':
                _cost_plus = per_unit
            elif cost_line.allocmeth == 'by_weight':
                _cost_plus = per_unit * move.product_id.weight
            elif cost_line.allocmeth == 'by_volume':
                _cost_plus = per_unit * move.product_id.volume
            elif cost_line.allocmeth == 'equal' and per_unit:
                _cost_plus = per_unit / move.product_qty
            elif cost_line.allocmeth == 'by_subtotal':
                _cost_plus = per_unit * move.price_unit

            total_cost_plus += _cost_plus

            pack_cost_obj.create({
                        'move_line_id': move.id,
                        'extra_cost_id': cost_line.id,
                        'unit_cost': _cost_plus,
                        'total_cost': _cost_plus * move.product_qty})

        self.write({'state': 'completed'})
        return True


class StockPicking(models.Model):
    _inherit = ['stock.picking']

    pack_approve_ok = fields.Boolean('Pack Approved',
                                     readonly=True,
                                     copy=False)
    cost_ok = fields.Boolean('Cost Approved', readonly=True, copy=False)

    extra_cost_status = fields.Selection(
        [('no_cost',
          'No Extra Cost Recorded'),
         ('new_cost',
          'Extra Cost Recorded'),
         ('pack_approved',
          'Pack Operations Approved'),
         ('cost_calculated',
          'Extra Cost Allocated'),
         ('cost_ok',
          'Cost Approved')],
        string='Stock Extra Cost Status',
        compute='_compute_ec_state',
        copy=False, index=True,
        readonly=True,
        store=True,
        track_visibility='onchange',
        help=" * No Extra Cost Recorded: \n"
             " * Extra Cost Recorded: Pack Operations Approval pending\n"
             " * Pack Operations Approved: Extra Cost Allocation pending\n"
             " * Extra Cost Allocated: Cost Approval Pending\n"
             " * Cost Approved: Stock Picking Approval Pending")

    extra_cost_ids = fields.One2many('stock.extra.cost',
                                     'picking_id',
                                     'Extra Costs',
                                     ondelete='cascade')

    extra_cost_visible = fields.Boolean(compute='_check_extra_cost_visibility',
                                        string='Extra Cost Visible')

    total_ec_amount_cost = fields.Float(compute='_compute_total_ec_amount',
                                        string='Total Extra Cost Amount Cost',
                                        store=True,
                                        digits=0)

    total_ec_amount_noncost = fields.Float(compute='_compute_total_ec_amount',
                                           string='Total Extra Cost'
                                                   ' Amount Non Cost',
                                           store=True,
                                           digits=0)

    total_ec_amount = fields.Float(compute='_compute_total_ec_amount',
                                   string='Total Extra Cost Amount',
                                   store=True,
                                   digits=0)

    @api.depends('extra_cost_ids')
    def _compute_total_ec_amount(self):
        for order in self:
            extra_cost = 0
            extra_cost_noncalc = 0
            # BEGIN: Calculate total price
            for extra in order.extra_cost_ids:
                if not extra.item_id.non_cost_calc:
                    extra_cost += extra.currency_amount
                else:
                    extra_cost_noncalc += extra.currency_amount
            # END: Calculate total price
            order.update({
                'total_ec_amount_cost': extra_cost,
                'total_ec_amount_noncost': extra_cost_noncalc,
                'total_ec_amount': extra_cost_noncalc + extra_cost
            })

    @api.multi
    def _check_extra_cost_visibility(self):
        ec_visibility = False
        for this in self:
            if this.picking_type_id.code == 'incoming':
                ec_visibility = True
            this.extra_cost_visible = ec_visibility

    @api.depends('extra_cost_ids')
    @api.one
    def _compute_ec_state(self):
        ''' Extra Cost State of a picking depends
         on the state of its related stock.move
        '''
        if self.cost_ok:
            self.extra_cost_status = 'cost_ok'
        else:
            prev_status = self.extra_cost_status
            self.nocost_approve_ok = False
            if not self.extra_cost_ids:
                self.extra_cost_status = 'no_cost'
                self.pack_approve_ok = False
            elif prev_status == 'no_cost':
                self.extra_cost_status = 'new_cost'
            elif self.pack_approve_ok:
                self.extra_cost_status = 'pack_approved'
            elif not prev_status:
                self.extra_cost_status = 'new_cost'

    @api.multi
    def button_confirm_cost(self):
        for pick in self:
            if (pick.extra_cost_status):
                ec_status = pick.extra_cost_status
                if (ec_status == 'new_cost'):
                    # BEGIN: Handle new_cost Extra Cost Status Warning
                    raise ValidationError(_("Please check and approve" +
                                            " pack operations by" +
                                            " clicking " +
                                            " [Confirm Pack Operations]" +
                                            " button!"))
                    # END: Handle new_cost Extra Cost Status Warning
                if (ec_status == 'pack_approved'):
                    # BEGIN: Handle pack_approved Extra Cost Status Warning
                    raise ValidationError(_("Please click " +
                                            " [Calculate Cost Adjustment]" +
                                            " button!"))
                    # END: Handle pack_approved Extra Cost Status Warning
                if (pick.extra_cost_status == 'no_cost'):
                    # BEGIN: Handle no_cost Extra Cost Status Warning
                    view = pick.env.ref(
                        'l10n_mn_stock_extra_cost.view_ec_nocost_approval')
                    wiz = pick.env['stock.nocost.approval']. \
                        create({'picking': pick.id})
                    return {
                        'name': _('Extra Cost Has No Records!'),
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'stock.nocost.approval',
                        'views': [(view.id, 'form')],
                        'view_id': view.id,
                        'target': 'new',
                        'res_id': wiz.id,
                        'context': pick.env.context
                    }
                    # END: Handle no_cost Extra Cost Status Warning
                if (pick.extra_cost_status == 'cost_calculated'):
                    # BEGIN: Handle Extra Cost Status Approval
                    view = pick.env.ref(
                        'l10n_mn_stock_extra_cost.view_ec_cost_approval2')
                    wiz = pick.env['stock.cost.approval']. \
                        create({'picking': pick.id})
                    return {
                        'name': _('Extra Cost Approval'),
                        'type': 'ir.actions.act_window',
                        'view_type': 'form',
                        'view_mode': 'form',
                        'res_model': 'stock.cost.approval',
                        'views': [(view.id, 'form')],
                        'view_id': view.id,
                        'target': 'new',
                        'res_id': wiz.id,
                        'context': pick.env.context
                    }
                    # END: Handle Extra Cost Status Approval

    @api.multi
    def button_compute_extra_cost(self):
        for picking in self:
            for cost_line in picking.extra_cost_ids:
                cost_line.calculate_cost()
                cost_line.make_invoice()
        if self.extra_cost_status == 'pack_approved':
            self.extra_cost_status = 'cost_calculated'
            self.message_post(body=_('Stock total extra cost' +
                                     ' calculated for adjustment.'))

    @api.multi
    def do_pretransfer_split(self):
        for pick in self:
            if pick.state == 'done':
                raise UserError(_('The pick is already validated'))
            pack_operations_delete = self.env['stock.pack.operation']
            if not pick.move_lines and not pick.pack_operation_ids:
                raise UserError(_('Please create some Initial Demand'
                                  ' or Mark as Todo and create'
                                  ' some Operations. '))
            # In draft or with no pack operations edited yet,
            # ask if we can just do everything
            if pick.state == 'draft' or \
                all([x.qty_done == 0.0
                    for x in pick.pack_operation_ids]
                    ) or all([x.qty_done == x.product_qty
                              for x in pick.pack_operation_ids]):
                # If no lots when needed, raise error
                picking_type = pick.picking_type_id
                if (picking_type.use_create_lots or
                        picking_type.use_existing_lots):
                    for pack in pick.pack_operation_ids:
                        if pack.product_id and \
                                pack.product_id.tracking != 'none':
                            raise UserError(_('Some products require'
                                              ' lots/serial numbers,'
                                              ' so you need to specify'
                                              ' those first!'))
                view = self.env. \
                    ref('l10n_mn_stock_extra_cost.view_ec_complete_approval')
                wiz = self. \
                    env['stock.complete.approval'].create({'pick_id': pick.id})
                # TDE FIXME: a return in a loop, what a good idea. Really.
                return {
                    'name': _('Complete Approval?'),
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'stock.complete.approval',
                    'views': [(view.id, 'form')],
                    'view_id': view.id,
                    'target': 'new',
                    'res_id': wiz.id,
                    'context': self.env.context,
                }
            # Process only backorder prior to extra cost processing
            if pick.check_backorder():
                view = self.env.ref(
                    'l10n_mn_stock_extra_cost.view_ec_backorder_confirmation')
                wiz = self.env['stock.ec.backorder.confirmation']. \
                    create({'pick_id': pick.id})
                return {
                    'name': _('Create Backorder?'),
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'stock.ec.backorder.confirmation',
                    'views': [(view.id, 'form')],
                    'view_id': view.id,
                    'target': 'new',
                    'res_id': wiz.id,
                    'context': self.env.context
                }
            for operation in pick.pack_operation_ids:
                if operation.qty_done < 0:
                    raise UserError(_('No negative quantities allowed'))
                if operation.qty_done > operation.product_qty:
                    raise UserError(_('Quantity done amount larger'
                                      ' than initial demand!'))
                if operation.qty_done > 0:
                    operation.write({'product_qty': operation.qty_done})
                else:
                    pack_operations_delete |= operation

            if pack_operations_delete:
                pack_operations_delete.unlink()
        return

    @api.multi
    def do_new_transfer(self):
        for pick in self:
            if pick.picking_type_id.code == 'incoming':
                # BEGIN: check extra cost status
                if (pick.extra_cost_status != 'no_cost' and pick.extra_cost_status != 'cost_ok'):
                    raise ValidationError(_("You cannot process incoming" +
                                            " shipment until stock picking" +
                                            " cost approved!"))
            # END: check extra cost status
        return super(StockPicking, self).do_new_transfer()


class StockMove(models.Model):
    _inherit = ['stock.move']

    @api.multi
    def _get_stock_price_unit_with_calculated_extra_cost(self, product_qty):
        """ created For the use of Stock_Move
        model within the stock extra
        cost module 2017.11.09"""
        self.ensure_one()
        line = self.purchase_line_id[0]
        cost = line.taxes_id.compute_all(line.price_unit,
                                         line.currency_id,
                                         product_qty)['total_excluded']
        # Handle Stock Move simulation
        if product_qty:
            cost /= product_qty
        if self.picking_id.extra_cost_ids:
            for extra in self.picking_id.extra_cost_ids:
                curr_cost = 0
                current_extra_cost_line = \
                    extra.adjustment_lines.search(
                        [('move_line_id', '=', self.id),
                         ('extra_cost_id', '=', extra.id)])[0]
                if not extra.item_id.non_cost_calc:
                    curr_cost = current_extra_cost_line.unit_cost
                    if self.picking_id.company_id.currency_id.id != \
                            extra.currency_id.id:
                        curr_cost = extra.currency_id. \
                                with_context(date=extra.date_invoice). \
                                compute(
                                    current_extra_cost_line.unit_cost,
                                    self.picking_id.company_id.currency_id,
                                    round=False)
                    cost += curr_cost
        """ End: """
        price_unit = cost
        return price_unit

    @api.multi
    def get_price_unit(self):
        if self.picking_id.extra_cost_status == 'cost_ok':
            if self.purchase_line_id:
                price_unit = self. \
                    _get_stock_price_unit_with_calculated_extra_cost(
                        self.product_qty)
                self.write({'price_unit': price_unit})
                return self.price_unit
        return super(StockMove, self).get_price_unit()
