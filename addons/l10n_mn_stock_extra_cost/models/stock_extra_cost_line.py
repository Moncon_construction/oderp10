# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2009-2014 Monos Group (<http://monos.mn>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import fields, models
from odoo.tools.translate import _
import odoo.addons.decimal_precision as dp


class StockExtraCostLine(models.Model):
    _name = 'stock.extra.cost.line'
    _description = 'Stock Extra Cost Line'

    move_line_id = fields.Many2one('stock.move',
                                   'Move Line',
                                   required=True,
                                   readonly=True)
    extra_cost_id = fields.Many2one('stock.extra.cost',
                                    'Extra Cost',
                                    readonly=True,
                                    ondelete='cascade')
    unit_cost = fields.Float('+ unit cost',
                             readonly=True,
                             digits=dp.get_precision('Extra Cost Unit')
                             )
    total_cost = fields.Float('+ total cost',
                              readonly=True,
                              digits=dp.get_precision('Extra Cost Unit')
                              )
