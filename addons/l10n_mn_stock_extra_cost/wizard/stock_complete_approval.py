# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class StockCompleteApproval(models.TransientModel):
    _name = 'stock.complete.approval'
    _description = 'Complete Approval'

    pick_id = fields.Many2one('stock.picking')

    @api.model
    def default_get(self, fields):
        res = super(StockCompleteApproval, self).default_get(fields)
        if not res.get('pick_id') and self._context.get('active_id'):
            res['pick_id'] = self._context['active_id']
        return res

    @api.multi
    def process(self):
        self.ensure_one()
        for pack in self.pick_id.pack_operation_ids:
            pack.qty_done = 0
        self.pick_id.pack_approve_ok = True
        self.pick_id.extra_cost_status = 'pack_approved'
        self.pick_id.message_post(body=_('Stock Pack Operations approved.'))
