# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class StockCostApproval(models.TransientModel):
    _name = 'stock.cost.approval'
    _description = 'Extra Cost Approval'

    picking = fields.Many2one('stock.picking')

    @api.model
    def default_get(self, fields):
        res = super(StockCostApproval, self).default_get(fields)
        if not res.get('picking') and self._context.get('active_id'):
            res['picking'] = self._context['active_id']
        return res

    @api.multi
    def process(self):
        for obj in self:
            obj.picking.message_post(body=_('Stock Picking Cost approved.'))
            obj.picking.write({'cost_ok': True})
            obj.picking.write({'extra_cost_status': 'cost_ok'})
