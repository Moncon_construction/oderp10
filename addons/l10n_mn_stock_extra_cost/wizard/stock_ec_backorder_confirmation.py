# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class StockECBackorderConfirmation(models.TransientModel):
    _name = 'stock.ec.backorder.confirmation'
    _description = 'Extra Cost PreApproval Backorder Confirmation'

    pick_id = fields.Many2one('stock.picking')

    @api.model
    def default_get(self, fields):
        res = super(StockECBackorderConfirmation, self).default_get(fields)
        if 'pick_id' in fields and \
                self._context.get('active_id') and not res.get('pick_id'):
            res = {'pick_id': self._context['active_id']}
        return res

    @api.one
    def _process(self):
        operations_to_delete = self.pick_id. \
            pack_operation_ids.filtered(lambda o: o.qty_done <= 0)
        for pack in \
                self.pick_id.pack_operation_ids - operations_to_delete:
            pack.product_qty = pack.qty_done
        operations_to_delete.unlink()
        self.pick_id.with_context(do_only_split=True).do_transfer()
        self.pick_id.pack_approve_ok = True
        self.pick_id.extra_cost_status = 'pack_approved'
        for pack in self.pick_id.pack_operation_ids:
            pack.qty_done = 0
        self.pick_id.message_post(body=_('Stock Pack Operations'
                                         ' approved. Backorder created.'))

    @api.multi
    def process(self):
        self._process()
