# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2009-2014 Monos Group (<http://monos.mn>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import api, fields, models
from odoo.tools.translate import _
import xlsxwriter
from io import BytesIO
import base64
import time


class StockExtraCostPrint(models.TransientModel):
    _name = "stock.extra.cost.print"
    _description = "Stock Cost Sheet"

    picking_id = fields.Many2one(
        'stock.picking',
        'Stock Picking',
        required=True,
        readonly=True,
        default=lambda self: self._context.get('active_id', False)
        )

    total_ec_amount_cost = fields.Float('Total Extra Cost Amount Cost',
                                        compute='_get_picking_totals',
                                        readonly=True,
                                        store=False)
    total_ec_amount_noncost = fields.Float('Total Extra Cost Amount Non Cost',
                                           compute='_get_picking_totals',
                                           readonly=True,
                                           store=False)
    total_ec_amount = fields.Float('Total Extra Cost Amount',
                                   compute='_get_picking_totals',
                                   readonly=True,
                                   store=False)

    @api.depends('picking_id')
    @api.multi
    def _get_picking_totals(self):
        self.ensure_one()
        self.total_ec_amount_cost = self.picking_id.total_ec_amount_cost
        self.total_ec_amount_noncost = self.picking_id.total_ec_amount_noncost
        self.total_ec_amount = self.picking_id.total_ec_amount

    @api.multi
    def export(self):
        picking = self['picking_id'][0]
        # Begin: CALCULATE Extra Cost
        picking.button_compute_extra_cost()
        # End: CALCULATE Extra Cost
        sheetname_1 = 'Adjusted_Costs_%s' % picking.id

        now = time.strftime('%Y-%m-%d')
        output = BytesIO()

        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet(sheetname_1)

        # Begin: Copied from hour_balance_print
        title = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 12,
            'font_name': 'Calibri',
            })
        subtitle = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 9,
            'font_name': 'Calibri',
            })
        sub_title1 = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 9,
            'bg_color': '#99ccff',
            'font_name': 'Calibri',
            'text_wrap': 1,
            'num_format': '#,##0.00'
            })
        cell_format_center = workbook.add_format({
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 7,
            'font_name': 'Calibri',
            'text_wrap': 1,
            'num_format': '#,##0'
            })
        cell_format_currency = workbook.add_format({
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 7,
            'font_name': 'Calibri',
            'text_wrap': 1,
            'num_format': '#,##0.0000'
            })
        cell_format_footer = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 7,
            'bg_color': '#99ccff',
            'font_name': 'Calibri',
            'text_wrap': 1,
            'num_format': '#,##0.00'
            })
        signature = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 7,
            'font_name': 'Calibri',
            })

        row = 1
        col = 0
        # Flag for crosscurrency
        fromcurrency = picking.company_id.currency_id
        tocurrency = fromcurrency

        # Begin: Set Title above list data header
        worksheet.write(row,
                        col+1,
                        _('(%s)Adjustment Line Document') % picking.name,
                        title)

        title_text = [_('Supplier : %s') % picking.partner_id.name,
                      _('Picking ID : %s') % picking.name,
                      _('Picking Date : %s') % picking.min_date,
                      _('Currency : %s') % picking.company_id.currency_id.name]

        # Begin: WRITE TITLE data into Excel File
        for atitle in title_text:
            worksheet.write(row+1, col+1, atitle, subtitle)
            row += 1
        # End: WRITE TITLE data into Excel File

        # Remember to clear title_text array right over here
        del title_text
        # ...
        # End: Set Title above list data header

        # Begin: Setting Header for list data
        # List subcategory
        subheader_list = [fromcurrency.name]

        header_list = [{_('OrderNum'): None},
                       {_('Barcode'): None},
                       {_('Product'): None},
                       {_('Unit Price'): subheader_list},
                       {_('Quantity'): None},
                       {_('Total Price'): subheader_list}]

        data_dict = {}
        data_dict[_('OrderNum')] = {}
        data_dict[_('Barcode')] = {}
        data_dict[_('Product')] = {}
        data_dict[_('Quantity')] = {}

        for asub in subheader_list:
            data_dict[_('Unit Price%s') % asub] = {}
            data_dict[_('Total Price%s') % asub] = {}

        # Remember to clear subheader_list array right over here
        del subheader_list
        # ----

        # List subcategory
        subheader_list = []
        subheader_list = [_('Unit Extra Cost'),
                          _('Total Extra Cost')]

        line_record_number = 0
        # BEGIN: LOOP THROUGH ORDER LINE
        headerrec = True
        ttl_NonCostCalc = _('Non Cost Calculated (%s)') % fromcurrency.name
        ttl_NonCostCalc0 = _('%s%s') % (ttl_NonCostCalc, subheader_list[0])
        ttl_NonCostCalc1 = _('%s%s') % (ttl_NonCostCalc, subheader_list[1])

        data_dict[ttl_NonCostCalc0] = {}
        data_dict[ttl_NonCostCalc1] = {}
        data_dict[_('Cost%s') % subheader_list[0]] = {}
        data_dict[_('Cost%s') % subheader_list[1]] = {}

        for move in picking.move_lines:
            line_record_number += 1
            product_qty = move.product_qty
            product_curr = move.purchase_line_id.currency_id
            # BEGIN: Tax Calculation
            curr_taxes = move.purchase_line_id. \
                taxes_id.compute_all(move.price_unit,
                                     product_curr,
                                     product_qty
                                     )
            price_unit = (product_qty > 0
                          and curr_taxes['total_excluded'] / product_qty
                          ) or 0
            price_total = price_unit * product_qty
            # ----------
            taxed = curr_taxes['total_included'] / product_qty
            tax = taxed - price_unit
            # END: Tax Calculation

            # BEGIN: setting blue header data
            data_dict[_('OrderNum')][line_record_number] = line_record_number
            data_dict[_('Barcode')][line_record_number] = \
                move.product_id.barcode
            data_dict[_('Product')][line_record_number] = move.name
            data_dict[_('Quantity')][line_record_number] = product_qty
            data_dict[_('Unit Price%s') %
                      fromcurrency.name][line_record_number] = price_unit
            data_dict[_('Total Price%s') %
                      fromcurrency.name][line_record_number] = price_total
            # END: setting blue header data
            cost_unit = move. \
                _get_stock_price_unit_with_calculated_extra_cost(product_qty)
            data_dict[_('Cost%s') % subheader_list[0]][line_record_number] = \
                cost_unit
            data_dict[_('Cost%s') % subheader_list[1]][line_record_number] = \
                cost_unit*product_qty
            data_dict[ttl_NonCostCalc0][line_record_number] = tax
            data_dict[ttl_NonCostCalc1][line_record_number] = \
                tax*product_qty

            # Begin: Set Pivot data Header for each type of extra cost recorded
            ec_titlenum = 6
            for extra in picking.extra_cost_ids:
                current_extra_cost_line = \
                    extra.adjustment_lines.search(
                        [('move_line_id', '=', move.id),
                         ('extra_cost_id', '=', extra.id)])[0]
                if not extra.item_id.non_cost_calc:
                    ec_titlenum += 1
                    currextralineunitcost = current_extra_cost_line.unit_cost
                    currextralinetotalcost = current_extra_cost_line.total_cost
                    header_fieldname = \
                        _('%s:%s (%s)') % (extra.name,
                                           ec_titlenum,
                                           extra.currency_id.name
                                           )
                    if headerrec:
                        header_list.append({header_fieldname: subheader_list})
                        data_dict[_('%s%s') %
                                  (header_fieldname, subheader_list[0])] = {}
                        data_dict[_('%s%s') %
                                  (header_fieldname, subheader_list[1])] = {}
                    # Begin: Calculate Data for this Column
                    data_dict[_('%s%s') %
                              (header_fieldname,
                               subheader_list[0])][line_record_number] = \
                        current_extra_cost_line.unit_cost
                    data_dict[_('%s%s') %
                              (header_fieldname,
                               subheader_list[1])][line_record_number] = \
                        current_extra_cost_line.total_cost
                    # End: Calculate Data for this Column
                    if tocurrency.id != extra.currency_id.id:
                        header_fieldname = _('%s (%s:%s Rate for %s: %s)') % (
                                                    extra.name,
                                                    tocurrency.name,
                                                    ec_titlenum,
                                                    extra.date_invoice,
                                                    extra.currency_rate
                                                    )
                        if headerrec:
                            header_list.append({header_fieldname:
                                                subheader_list})
                            data_dict[_('%s%s') % (header_fieldname,
                                                   subheader_list[0])] = {}
                            data_dict[_('%s%s') % (header_fieldname,
                                                   subheader_list[1])] = {}
                        # Begin: Calculate Data for this Column
                        currextralineunitcost = extra.currency_id. \
                            with_context(date=extra.date_invoice).compute(
                                current_extra_cost_line.unit_cost,
                                tocurrency
                                )
                        currextralinetotalcost = extra.currency_id. \
                            with_context(date=extra.date_invoice).compute(
                                current_extra_cost_line.total_cost,
                                tocurrency
                                )
                        data_dict[_('%s%s') % (header_fieldname,
                                               subheader_list[0]
                                               )][line_record_number] = \
                            currextralineunitcost
                        data_dict[_('%s%s') % (header_fieldname,
                                               subheader_list[1]
                                               )][line_record_number] = \
                            currextralinetotalcost
                        # End: Calculate Data for this Column
                else:
                    data_dict[ttl_NonCostCalc0][line_record_number] += \
                        current_extra_cost_line.unit_cost
                    data_dict[ttl_NonCostCalc1][line_record_number] += \
                        current_extra_cost_line.total_cost
            # End: Set Pivot data Header for each type of extra cost recorded

            if headerrec:
                header_list.append({ttl_NonCostCalc: subheader_list})
                header_list.append({_('Cost'): subheader_list})
            headerrec = False
        # END: LOOP THROUGH ORDER LINE

        # Begin: WRITE DATA INTO EXCEL FILE
        row += 3
        totrow = 0
        for headeritem in header_list:
            for keyheader, aheader in headeritem.iteritems():
                subcol = 0
                if aheader is None:
                    # Print single header with merge row
                    worksheet.merge_range(row,
                                          col,
                                          row+1,
                                          col,
                                          keyheader,
                                          sub_title1)
                    # BEGIN: PRINT OUT DATA
                    # Further Notice: See towards elimination
                    currheader_name = keyheader
                    for currrownum, currrowdata in \
                            data_dict[currheader_name].iteritems():
                        row_for_data = row+1+currrownum
                        worksheet.write(row_for_data,
                                        col,
                                        currrowdata,
                                        cell_format_center)
                        totrow += 1
                    footer_data = _('-')
                    if currheader_name == _('Product'):
                        footer_data = _('Total')
                    worksheet.write(row_for_data+1,
                                    col,
                                    footer_data,
                                    cell_format_footer)
                    # END: PRINT OUT DATA
                else:
                    for subcatitem in aheader:
                        # Print header
                        worksheet.write(row+1,
                                        col+subcol,
                                        subcatitem,
                                        sub_title1)
                        subcol += 1
                        column_total = 0
                        # BEGIN: PRINT OUT DATA
                        currheader_name = _('%s%s' % (keyheader,
                                                      subcatitem))
                        for currrownum, currrowdata in \
                                data_dict[currheader_name].iteritems():
                            row_for_data = row+1+currrownum
                            col_for_data = col+subcol-1
                            worksheet.write(row_for_data,
                                            col_for_data,
                                            currrowdata,
                                            cell_format_currency)
                            column_total += currrowdata
                        # -------Set Footer-----------
                        worksheet.write(row_for_data+1,
                                        col_for_data,
                                        column_total,
                                        cell_format_footer)
                        # END: PRINT OUT DATA
                    subcol = subcol-1
                    if subcol > 0:
                        # Print header
                        worksheet.merge_range(row,
                                              col,
                                              row,
                                              col+subcol,
                                              keyheader,
                                              sub_title1)
                    else:
                        # Print header
                        worksheet.write(row,
                                        col,
                                        keyheader,
                                        sub_title1)
                col += subcol+1
        # ----Set Signature
        worksheet.write(row+totrow,
                        1,
                        _("Cost Adjusted By: Chief Accountant:" +
                          "......................................." +
                          "................."),
                        signature)
        worksheet.write(row+totrow,
                        6,
                        _("Cost Supervised By: Chief of IAD:" +
                          "......................................." +
                          "..................."),
                        signature)
        # End: WRITE DATA INTO EXCEL FILE
        # Formatting: Enlarge Header rows
        worksheet.set_row(row, 30)
        worksheet.set_row(row+1, 30)
        # Remember to clear subheader_list and
        # header_list array right over here
        del subheader_list
        del header_list
        del data_dict
        # ...
        # End: Setting Header for list data

        # Section Amended towards Workbook Export Change
        workbook.close()
        out = base64.encodestring(output.getvalue())
        file_name = 'stock_extra_cost_print_%s' % now

        excel_id = self.env['oderp.report.excel.output'].create(
            {'filedata': out,
             'filename': file_name+'.xlsx'})

        return {
            'name': 'Export Result',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'oderp.report.excel.output',
            'res_id': excel_id.id,
            'view_id': False,
            'context': self._context,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'nodestroy': True,
        }
