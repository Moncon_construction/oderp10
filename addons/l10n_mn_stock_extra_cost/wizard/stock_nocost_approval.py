# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class StockNoCostApproval(models.TransientModel):
    _name = 'stock.nocost.approval'
    _description = 'No Extra Cost Approval'

    picking = fields.Many2one('stock.picking')

    @api.model
    def default_get(self, fields):
        res = super(StockNoCostApproval, self).default_get(fields)
        if not res.get('picking') and self._context.get('active_id'):
            res['picking'] = self._context['active_id']
        return res

    @api.multi
    def process(self):
        for obj in self:
            obj.picking.message_post(body=_('Stock Picking Cost'
                                            ' approved with no'
                                            ' extra cost record.'))
            obj.picking.write({'cost_ok': True})
            obj.picking.write({'extra_cost_status': 'cost_ok'})
