# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock Minus Balance",
    'version': '1.0',
    'depends': ['l10n_mn_stock'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Modules',
    'description': """
        """,
    'data': [
        'report/report_stock_minus_balance_pdf_view.xml',
        'wizard/report_stock_minus_balance_view.xml',
    ],
    'installable': True,
    'auto_install': False
}
