# -*- coding: utf-8 -*-

from odoo import models
from odoo.addons.l10n_mn_web.models.time_helper import *


class ReportStockMinusBalancePDF(models.AbstractModel):
    """
        Хасах үлдэгдэл рүү орсон барааны тайлан PDF
    """
    
    _name = 'report.l10n_mn_stock_minus_balance_report.pdf_report'

    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('l10n_mn_stock_minus_balance_report.pdf_report')
        wizard_id = self.env[report.model].browse(docids)
        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': wizard_id,
            'printed_date': str(get_day_like_display(fields.Datetime.now(), self.env.user)),
            'lines': wizard_id.get_pdf_lines(),
            
        }
        return report_obj.render('l10n_mn_stock_minus_balance_report.pdf_report', docargs)