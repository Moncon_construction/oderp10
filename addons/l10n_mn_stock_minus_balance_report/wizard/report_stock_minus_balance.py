# -*- coding: utf-8 -*-

import base64
from dateutil.relativedelta import relativedelta
from io import BytesIO
import logging
import time
import xlsxwriter

from odoo import models, fields, api, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.exceptions import ValidationError


_logger = logging.getLogger('stock.report')


class ReportStockMinusBalance(models.TransientModel):
    """
        Хасах үлдэгдэл рүү орсон барааны тайлан
    """

    _name = 'report.stock.minus.balance'

    def _domain_warehouse(self):
        _warehouse_ids = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouse_ids.append(warehouse.id)
        if _warehouse_ids:
            return [('id', 'in', _warehouse_ids), ('company_id', '=', self.env.user.company_id.id)]
        else:
            return False

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    date_from = fields.Date("Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'))
    date_to = fields.Date("End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    warehouse_ids = fields.Many2many('stock.warehouse', string="Warehouses", required=True, domain=_domain_warehouse)
    product_ids = fields.Many2many('product.product', string="Products")
    report_type = fields.Selection([('end', 'Show lines with minus residuals at end of the period'),
                                    ('middle_n_end_p', 'Show lines with minus between period but positive at end of the period'),
                                    ('middle', 'Show lines with minus residuals during period')], default='end', required=True, string='Report Type')

    def get_sheet_format(self, sheet):
        sheet.set_portrait()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        sheet.hide_gridlines(2)
        return sheet

    def get_header(self, sheet, header_formats, rowx, report_name):
        sheet.merge_range(rowx, 0, rowx, 2, '%s' % self.company_id.name, header_formats['format_filter'])
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 4, report_name.upper(), header_formats['format_name'])
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 2, '  %s: %s - %s' % (_('Duration'), self.date_from, self.date_to), header_formats['format_filter'])
        rowx += 1
        sheet.merge_range(rowx, 0, rowx, 2, '  %s: %s' % (_('Printed Date'), fields.Datetime.now()), header_formats['format_filter'])
        rowx += 1
        return sheet, rowx

    def get_data(self, location_id, date_from, date_to, sub_where):
        # @Note: Тайлант хугацааны хүрээнд хэзээ хэдий хэмжээний хөдөлгөөн хийгдсэн тухай түүхийг нэгж огнооны хувьд олж авч буцаана. 
        # Тайлант хугацааны эхний үлдэгдлийг тусад нь авдаг тул эхний үлдэгдлийг давхар тооцохоос сэргийлэв.
        date_from = date_from + relativedelta(microseconds=1)
        qry = """
                SELECT pid, code, product, date, SUM(qty) AS qty
                FROM
                (

                    SELECT p.id AS pid, m.id AS mid, p.default_code AS code, pt.name AS product, m.date AS date,
                        CASE
                            WHEN m.location_id != %s THEN
                                COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor, 0)
                            ELSE
                                -COALESCE(m.product_uom_qty/uom_m.factor*uom_t.factor, 0) END AS qty
                    FROM stock_move m
                    LEFT JOIN product_product p ON (m.product_id = p.id)
                    LEFT JOIN product_template pt ON (pt.id = p.product_tmpl_id)
                    LEFT JOIN product_uom uom_m ON (uom_m.id = m.product_uom)
                    LEFT JOIN product_uom uom_t ON (uom_t.id = pt.uom_id)
                    WHERE m.location_dest_id != m.location_id AND (m.location_id = %s OR m.location_dest_id = %s) AND m.company_id = %s AND m.state = 'done' AND m.date between '%s' AND '%s' %s

                ) sub_table
                GROUP BY pid, code, product, date
                ORDER BY product ASC, pid ASC, date ASC, qty DESC
            """ % (location_id, location_id, location_id, self.company_id.id, str(date_from), str(date_to), sub_where)
        return qry

    def get_value(self, sheet, content_line_formats, rowx, lines, location_id, date_from, date_to, qty_dp_digit):
        product_qty, product_id, sequence, cannot_draw_product_ids = 0, False, 1, []
        for line in lines:
            can_draw = True
            if product_id != line['pid']:
                product_qty = self.env['product.product'].sudo().browse(line['pid']).with_context({'calculate_from_move': True}).get_qty_availability([location_id], date_from)
            product_qty += line['qty']
            
            if round(product_qty, qty_dp_digit) < 0 and line['pid'] not in cannot_draw_product_ids:
                if self.report_type == 'end' or self.report_type == 'middle_n_end_p':
                    end_qty = self.env['product.product'].sudo().browse(line['pid']).with_context({'calculate_from_move': True}).get_qty_availability([location_id], date_to)
                    if round(end_qty, qty_dp_digit) >= 0:
                        if self.report_type == 'end':
                            cannot_draw_product_ids.append(line['pid'])
                    else:
                        if self.report_type == 'middle_n_end_p':
                            cannot_draw_product_ids.append(line['pid'])
                if line['pid'] not in cannot_draw_product_ids:
                    sheet.write(rowx, 0, sequence, content_line_formats['format_content_center'])
                    sheet.write(rowx, 1, line['code'], content_line_formats['format_content_center'])
                    sheet.write(rowx, 2, line['product'], content_line_formats['format_content_left'])
                    sheet.write(rowx, 3, str(get_day_like_display(line['date'], self.env.user)), content_line_formats['format_content_center'])
                    sheet.write(rowx, 4, product_qty, content_line_formats['format_content_float'])
                    sequence += 1
                    rowx += 1
            product_id = line['pid']
        return sheet, rowx

    @api.multi
    def export_report(self):
        if not self.warehouse_ids:
            raise ValidationError(_("Please select warehouse."))

        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Stock Minus Balance')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_content_header = book.add_format(ReportExcelCellStyles.format_content_header)
        format_group_left = book.add_format(ReportExcelCellStyles.format_group_left)
        format_content_center = book.add_format(ReportExcelCellStyles.format_content_center)
        format_content_left = book.add_format(ReportExcelCellStyles.format_content_left)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)

        header_formats = {
            'format_filter': format_filter,
            'format_name': format_name,
        }
        content_line_formats = {
            'format_content_center': format_content_center,
            'format_content_left': format_content_left,
            'format_content_float': format_content_float,
        }

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('report_stock_minus_balance'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet = self.get_sheet_format(sheet)

        # compute column
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 20)

        # create header
        rowx = 0
        sheet, rowx = self.get_header(sheet, header_formats, rowx, report_name)

        # create content header
        sheet.write(rowx, 0, _('№'), format_content_header)
        sheet.write(rowx, 1, _('Product Code'), format_content_header)
        sheet.write(rowx, 2, _('Product Name'), format_content_header)
        sheet.write(rowx, 3, _('Date'), format_content_header)
        sheet.write(rowx, 4, _('Residual'), format_content_header)
        sheet.set_row(rowx, 30)
        rowx += 1

        # Уг тайлангийн query-г нэг query-гээр авах боломжгүй байсан тул query болгонд ашиглах нийтлэг атрибутуудыг бэлдэв
        date_from = get_display_day_to_user_day(self.date_from + ' 00:00:00', self.env.user)
        date_to = get_display_day_to_user_day(self.date_to + ' 23:59:59', self.env.user)
        qty_dp = self.env['decimal.precision'].sudo().search([('name', '=', 'Product Unit of Measure')])
        qty_dp_digit = qty_dp.sudo().digits if qty_dp else 6
        sub_where = " AND p.id IN (" + ','.join(map(str, self.product_ids.ids)) + ") " if self.product_ids else ""

        # create content
        for warehouse in self.warehouse_ids:
            # Груп дах мөрүүдийн өгөгдлийг бэлтгэх
            self.env.cr.execute(self.get_data(warehouse.lot_stock_id.id, date_from, date_to, sub_where))
            lines = self.env.cr.dictfetchall()
            # Груп дах мөрүүдийг зурах/Групын толгойн rowx байршлыг хадгалж авах
            group_rowx, rowx = rowx, (rowx + 1)
            sheet, rowx = self.get_value(sheet, content_line_formats, rowx, lines, warehouse.lot_stock_id.id, date_from, date_to, qty_dp_digit)
            # Ядаж нэг мөр зурагдсан бол групын толгойг зурах
            if group_rowx != (rowx - 1):
                sheet.merge_range(group_rowx, 0, group_rowx, 4, warehouse.name, format_group_left)
            else:
                rowx -= 1

        # close workbook and export report
        book.close()
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        return report_excel_output_obj.export_report()

    def get_pdf_lines(self):
        # PDF тайлангийн хүснэгтийн өгөгдлийг бэлдэж буцаана.
        # Уг тайлангийн query-г нэг query-гээр авах боломжгүй байсан тул query болгонд ашиглах нийтлэг атрибутуудыг бэлдэв
        date_from = get_display_day_to_user_day(self.date_from + ' 00:00:00', self.env.user)
        date_to = get_display_day_to_user_day(self.date_to + ' 23:59:59', self.env.user)
        qty_dp = self.env['decimal.precision'].sudo().search([('name', '=', 'Product Unit of Measure')])
        qty_dp_digit = qty_dp.sudo().digits if qty_dp else 6
        sub_where = " AND p.id IN (" + ','.join(map(str, self.product_ids.ids)) + ") " if self.product_ids else ""
        total_lines = []
        
        for warehouse in self.warehouse_ids:
            # Агуулахад хөдөлгөөн хийгдсэн гүйлгээнүүдийг цуглуулах
            self.env.cr.execute(self.get_data(warehouse.lot_stock_id.id, date_from, date_to, sub_where))
            lines = self.env.cr.dictfetchall()
            
            wh_product_lines = []
            product_qty, product_id, cannot_draw_product_ids = 0, False, []
            for line in lines:
                can_draw = True
                if product_id != line['pid']:
                    # Тайлант хугацаагаарх эхний үлдэгдлийг авах
                    product_qty = self.env['product.product'].sudo().browse(line['pid']).with_context({'calculate_from_move': True}).get_qty_availability([warehouse.lot_stock_id.id], date_from)
                # Тухайн огноонд хасах руу орсон эсэхийг шалгах / хасах руу орсон бол тайланд тусгах
                product_qty += line['qty']
                if round(product_qty, qty_dp_digit) < 0 and line['pid'] not in cannot_draw_product_ids:
                    if self.report_type == 'end' or self.report_type == 'middle_n_end_p':
                        end_qty = self.env['product.product'].sudo().browse(line['pid']).with_context({'calculate_from_move': True}).get_qty_availability([warehouse.lot_stock_id.id], date_to)
                        if round(end_qty, qty_dp_digit) >= 0:
                            if self.report_type == 'end':
                                cannot_draw_product_ids.append(line['pid'])
                        else:
                            if self.report_type == 'middle_n_end_p':
                                cannot_draw_product_ids.append(line['pid'])
                    if line['pid'] not in cannot_draw_product_ids:
                        wh_product_lines.append({
                            'code': line['code'],
                            'product': line['product'],
                            'product_id': line['pid'],
                            'date': str(get_day_like_display(line['date'], self.env.user)),
                            'product_qty': product_qty
                        })
                product_id = line['pid']
            
            if len(wh_product_lines) > 0:
                total_lines.append({
                    'wh_id': warehouse.sudo().id,
                    'wh': warehouse.name,
                    'minus_lines': wh_product_lines
                })
        
        return total_lines
    
    @api.multi
    def export_to_pdf(self):
        # PDF тайлан
        return self.env.get('report').get_action(self.ids, 'l10n_mn_stock_minus_balance_report.pdf_report')
