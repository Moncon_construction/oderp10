# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-Today Asterisk Technologies LLC Co.,ltd
#    (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

{
    'name': "Mongolian Customizations of Stock Warehouse Orderpoint",
    'version': '1.0',
    'depends': [
        'l10n_mn_stock',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Stock Warehouse Orderpoint Additional Features
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/product_category_view.xml',
        'views/procurement_view.xml',
        'views/purchase_order_view.xml',
        'views/run_procurement_view.xml',
        'views/stock_transit_order_view.xml',
        'views/stock_warehouse_orderpoint.xml',
        'wizard/procurement_orderpoint_compute_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
