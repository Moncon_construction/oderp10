# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-Today Asterisk Technologies LLC Co.,ltd
#    (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import api, fields, models, _ 

import odoo.addons.decimal_precision as dp
from odoo.exceptions import UserError

class ProductDeliveryCategory(models.Model):
    _name = "product.delivery.category"

    name = fields.Char('Name', required=True, translate=True)


class ProductCategory(models.Model):
    _inherit = "product.category"
    
    delivery_categ_id = fields.Many2one('product.delivery.category', 'Product Delivery Category')