# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'
    
    is_reordering = fields.Boolean('Reordering', default=False)
    pro_order_run_id = fields.Many2one('procurement.order.run', 'Procurement Order Run')

    @api.multi
    def write(self, vals):
        '''
            Худалдан авалтын захиалгын төлөв өөрчлөгдөхөд 
            татан авалтын захиалгын баримтын төлөв дагаж өөрчлөгдөнө
        '''
        res = super(PurchaseOrder, self).write(vals)
        if 'state' in vals.keys():
            for obj in self:
                if obj.is_reordering == True:
                    for line in self.order_line:
                        proc = self.env['procurement.order'].search([('purchase_line_id', '=', line.id)])
                        if proc:
                            proc.write({'order_state':vals['state']})
        return res