# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, _, models
from odoo.exceptions import UserError
import logging
import threading

_logger = logging.getLogger(__name__)

class ProcurementOrderRun(models.Model):
    _name = 'procurement.order.run'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    @api.depends('pro_order_ids','stock_picking_ids','transit_order_ids','purchase_order_ids')
    @api.multi
    def _count_all(self):
        ''' Дахин захиалгын дүрмүүдээс үүссэн татан авалт, 
            бэлтгэх баримт, нөхөн дүүргэлтийн захиалгуудын тоог харуулна
        '''
        procurement_count = picking_count = transit_count = procurements_count = purchase_count = 0
        purchase = ''
        for order in self.pro_order_ids:
            if order.purchase_id and purchase != order.purchase_id:
                purchase_count += 1
                purchase = order.purchase_id
        for order in self.pro_order_ids:
            procurement_count += 1
        for order in self.stock_picking_ids:
            picking_count += 1
        for order in self.transit_order_ids:
            transit_count += 1
        self.transit_count = transit_count
        self.picking_count = picking_count
        self.procurements_count = procurement_count
        self.purchase_count = purchase_count
        
    procurements_count = fields.Integer(
        string='Procurements', compute='_count_all')
    transit_count = fields.Integer(
        string='Transit',  compute='_count_all')
    picking_count = fields.Integer(
        string='Picking', compute='_count_all')
    purchase_count = fields.Integer(
        string='Purchase', store=True, readonly=True, compute='_count_all')
    purchase_order_ids = fields.One2many('purchase.order', 'pro_order_run_id', 'Purchase Order')
    run_date = fields.Date(default=fields.Date.today)
    created_user_id = fields.Many2one('res.users', string='Responsible',default=lambda self: self.env.user)
    warehouse_ids = fields.Many2many('stock.warehouse', 'procurement_order_run_warehouses_rel', 'run_id', 'warehouse_id', 'Warehouses', required=True)
    product_category_ids = fields.Many2many('product.category', 'procurement_order_run_product_category_rel', 'run_id', 'category_id', 'Category')
    pro_order_ids = fields.One2many('procurement.order', 'pro_order_run_id', 'Procurement Order')
    transit_order_ids = fields.One2many('stock.transit.order', 'pro_order_run_id', 'Transit Order')
    stock_picking_ids = fields.One2many('stock.picking', 'pro_order_run_id', 'Stock Picking')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('computed stock', 'Computed Stock'),
        ('runned', 'Runned Procurement Order')
        ], string='Status', readonly=True, index=True, default='draft', copy=False)
    
    def _procure_calculation_orderpoint(self):
        with api.Environment.manage():
            # As this function is in a new thread, I need to open a new cursor, because the old one may be closed
            new_cr = self.pool.cursor()
            self = self.with_env(self.env(cr=new_cr))
            
            '''
            scheduler_cron = self.sudo().env.ref('procurement.ir_cron_scheduler_action')
            # Avoid to run the scheduler multiple times in the same time
            try:
                print 'try'
                with tools.mute_logger('odoo.sql_db'):
                    self._cr.execute("SELECT id FROM ir_cron WHERE id = %s FOR UPDATE NOWAIT", (scheduler_cron.id,))
            except Exception:
                print 'except'
                _logger.info('Attempt to run procurement scheduler aborted, as already running')
                self._cr.rollback()
                self._cr.close()
                return {}
            '''
            product_ids = []
            if self.product_category_ids:
                product_ids = self.env['product.product'].search([('categ_id','in',self.product_category_ids.ids)])
            self.env['procurement.order']._procure_orderpoint_confirm(
                use_new_cursor=new_cr.dbname,
                company_id=self.env.user.company_id.id, product_ids=product_ids, warehouse_ids=self.warehouse_ids,pro_order_run_id=self.id)
            new_cr.close()
            return {}

    @api.multi
    def procure_calculation(self):
        threaded_calculation = threading.Thread(target=self._procure_calculation_orderpoint, args=())
        threaded_calculation.start()
        self.write({'state':'computed stock'})

    @api.multi
    def run_procurement(self):
        ''' Дахин захиалах дүрмээр үүссэн татан авалтын захиалгуудыг ажиллуулж
            Бэлтгэх баримт,Нөхөн дүүргэлт, Худалдан авалтын захиалга үүсгэнэ.
        '''
        Picking = self.env['stock.picking']
        for order in self.pro_order_ids:
            order.run()
        transit_order = self.env['stock.transit.order']
        checked_pickings = []
        move_dest = ''
        create_ok = False
        for picking in self.stock_picking_ids:
            is_picking = False  
            line_vals = []  
            vals = []   
            for move in picking.move_lines:
                wh_id = move.picking_type_id.warehouse_id
                move_dest  = self.env['stock.move'].search([('move_dest_id', '=', move.id)])
                if move_dest:
                    recompute = True
                    if is_picking == False:
                        in_picking = Picking.create(move_dest._get_new_picking_values())
                        is_picking = True
                    move_dest.write({'picking_id': in_picking.id})
                    supply_wh_id = move_dest.picking_type_id.warehouse_id
                line_vals.append({
                    'name': move.product_id.name, 
                    'company_id': move.company_id.id,
                    'date_planned': self.run_date,
                    'product_id': move.product_id.id,
                    'move_id': move.id,
                    'product_qty': move.product_uom_qty,
                    'product_uom': move.product_uom.id,
                    'state': 'draft',
                })
                line = [(0, 0, l)for l in line_vals]
            vals = {
                'supply_warehouse_id': supply_wh_id.id, 
                'warehouse_id': wh_id.id,
                'supply_picking_type_id': supply_wh_id.out_type_id.id,
                'receive_picking_type_id': wh_id.in_type_id.id,
                'date_order': self.run_date,
                'company_id': picking.company_id.id,
                'is_reordering': True,
                'pro_order_run_id': self.id,
                'order_line':line
            }
            transit_order = self.env['stock.transit.order'].sudo().create(vals)
            
            transit_order.signal_workflow('action_send')
            picking.write({'transit_order_id':transit_order.id})
            in_picking.write({'transit_order_id':transit_order.id})
            in_picking.write({'pro_order_run_id': self.id})
            
            for move in picking.move_lines:
                move.procurement_id.write({'order_number':transit_order.name,
                                           'order_state':transit_order.state
                                           })
                move_dest  = self.env['stock.move'].search([('move_dest_id', '=', move.id)])
                if move_dest:
                    move_dest.procurement_id.write({'order_number':transit_order.name,
                                                    'order_state':transit_order.state
                                                    })
        self.write({'state':'runned'})
        
    @api.multi
    def open_procurements(self):
        ''' Холбоотой татан авалтын захиалгуудыг харуулна
        '''
        res = self.env['ir.actions.act_window'].for_xml_id('l10n_mn_stock_orderpoint', 'procurement_order_action_exceptions_inherit')
        res['domain'] = [('pro_order_run_id', '=', self.id)]
        res['context'] = {}
        return res
    
    @api.multi
    def open_pickings(self):
        '''Холбоотой бэлтгэх баримтуудыг харуулна
        '''
        res = self.env['ir.actions.act_window'].for_xml_id('stock', 'stock_picking_action_picking_type')
        res['domain'] = [('pro_order_run_id', '=', self.id)]
        res['context'] = {}
        return res
    
    @api.multi
    def open_transit_order(self):
        '''Холбоотой нөхөн дүүргэлтийн захиалгуудыг харуулна
        '''
        res = self.env['ir.actions.act_window'].for_xml_id('l10n_mn_stock', 'action_stock_transit_order')
        res['domain'] = [('pro_order_run_id', '=', self.id)]
        res['context'] = {}
        return res
        
    @api.multi
    def open_purchase_order(self):
        res = self.env['ir.actions.act_window'].for_xml_id('purchase', 'purchase_rfq')
        res['domain'] = [('pro_order_run_id', '=', self.id)]
        res['context'] = {}
        return res

    def unlink(self):
        for order in self:
            if order.state not in ('draft'):
                raise UserError(
                    _('You can not delete this procurement order. You can only be deleted during the drafts!'))
        return super(ProcurementOrderRun, self).unlink()

    def cancel(self):
        for order in self:
            if order.pro_order_ids:
                for p_order in order.pro_order_ids:
                    p_order.unlink()
            if order.stock_picking_ids:
                for picking in order.stock_picking_ids:
                    picking.unlink()
            if order.transit_order_ids:
                for tr_order in order.transit_order_ids:
                    tr_order.action_cancel()
            if order.purchase_order_ids:
                for pur_order in order.purchase_order_ids:
                    pur_order.button_cancel()
            if order.pro_order_ids:
                for p_order in order.pro_order_ids:
                    p_order.unlink()
            order.write({'state':'draft'})
        return True
