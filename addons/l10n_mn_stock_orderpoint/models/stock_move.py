# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models


class StockMove(models.Model):
    _inherit = 'stock.move'
    
    def _prepare_procurement_from_move(self):
        res = super(StockMove, self)._prepare_procurement_from_move()
        res.update({'pro_order_run_id': self.procurement_id.pro_order_run_id.id})
        return res

    @api.multi
    def assign_picking(self):
        """ Try to assign the moves to an existing picking that has not been
        reserved yet and has the same procurement group, locations and picking
        type (moves should already have them identical). Otherwise, create a new
        picking to assign them to. """
        Picking = self.env['stock.picking']
        for move in self:
            recompute = False
            '''
            Дахин захиалгын дүрмээс орлогын баримтуудыг үүсгэж 
            дахин захиалах дүрэм ажиллуулах баримттай холбоно. 
            '''
            if move.location_id.usage == 'transit' or (move.location_id.usage == 'internal' and move.location_dest_id.usage == 'customer'):
                picking = Picking.search([
                    ('group_id', '=', move.group_id.id),
                    ('location_id', '=', move.location_id.id),
                    ('location_dest_id', '=', move.location_dest_id.id),
                    ('picking_type_id', '=', move.picking_type_id.id),
                    ('printed', '=', False),
                    ('state', 'in', ['draft', 'confirmed', 'waiting', 'partially_available', 'assigned'])], limit=1)
                if not picking:
                    recompute = True
                    picking = Picking.create(move._get_new_picking_values())
                move.write({'picking_id': picking.id})
            if move.picking_id and move.picking_id.group_id:
                picking = move.picking_id
                order = self.env['sale.order'].sudo().search([('procurement_group_id', '=', picking.group_id.id)])
                picking.message_post_with_view(
                    'mail.message_origin_link',
                    values={'self': picking, 'origin': order},
                    subtype_id=self.env.ref('mail.mt_note').id)
                
            if recompute:
                move.recompute()
        for move in self:
            move.picking_id.write({'pro_order_run_id': move.procurement_id.pro_order_run_id.id})
        return True
    _picking_assign = assign_picking