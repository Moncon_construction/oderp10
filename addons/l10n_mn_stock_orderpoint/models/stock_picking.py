# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class StockPicking(models.Model):
    _inherit = 'stock.picking'
    
    pro_order_run_id = fields.Many2one('procurement.order.run', 'Procurement Order Run')