# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class StockTransitOrder(models.Model):
    _inherit = 'stock.transit.order'
    
    pro_order_run_id = fields.Many2one('procurement.order.run', 'Procurement Order Run')
    is_reordering = fields.Boolean('Reordering', default=False)
    
    @api.multi
    def wkf_confirm_transit(self):
        '''Нөхөн дүүргэлтийн захиалга батлах үед татан авалтын захиалгаас үүссэн бол
            агуулахын орлого зарлагын баримтыг давхар үүсгэхгүй болгосон. 
        '''
        if self.is_reordering == False:
            self._create_picking()
            self._create_out_picking()
        self.write({'state': 'confirmed'})

    @api.multi
    def write(self, vals):
        '''
            Нөхөн дүүргэлтийн захиалгын төлөв өөрчлөгдөхөд 
            татан авалтын захиалгын баримтын төлөв дагаж өөрчлөгдөнө
        '''
        res = super(StockTransitOrder, self).write(vals)
        if 'state' in vals.keys():
            for obj in self:
                state = ''
                if obj.is_reordering == True:
                    for line in self.order_line:
                        if line.move_id:
                            if line.move_id.procurement_id:
                                if vals['state'] == 'done':
                                    state = 'tdone'
                                else:
                                    state = vals['state']
                                line.move_id.procurement_id.write({'order_state':state})
                                move_dest  = self.env['stock.move'].search([('move_dest_id', '=', line.move_id.id)])
                                if move_dest:
                                    move_dest.procurement_id.write({'order_state':state})
        return res
    
class StockTransitOrderLine(models.Model):
    _inherit = 'stock.transit.order.line'
    
    move_id = fields.Many2one('stock.move', string='Stock Move')
    