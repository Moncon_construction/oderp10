# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import threading

from odoo import api, fields, models, _ 
_logger = logging.getLogger(__name__)

class ProcurementOrderpointConfirm(models.TransientModel):
    _inherit = 'procurement.orderpoint.compute'
    
    delivery_categ_id = fields.Many2one('product.delivery.category', 'Product Delivery Category', required=True)
    warehouse_ids = fields.Many2many('stock.warehouse', 'orderpoint_wizard_warehouses_rel', 'orderpoint_wizard_id', 'warehouse_id', string='Warehouses', required=True)

    def _procure_calculation_orderpoint(self):
        with api.Environment.manage():
            # As this function is in a new thread, I need to open a new cursor, because the old one may be closed
            new_cr = self.pool.cursor()
            self = self.with_env(self.env(cr=new_cr))
            
            '''
            scheduler_cron = self.sudo().env.ref('procurement.ir_cron_scheduler_action')
            # Avoid to run the scheduler multiple times in the same time
            try:
                print 'try'
                with tools.mute_logger('odoo.sql_db'):
                    self._cr.execute("SELECT id FROM ir_cron WHERE id = %s FOR UPDATE NOWAIT", (scheduler_cron.id,))
            except Exception:
                print 'except'
                _logger.info('Attempt to run procurement scheduler aborted, as already running')
                self._cr.rollback()
                self._cr.close()
                return {}
            '''
            product_ids = []
            if self.delivery_categ_id.id:
                product_ids = self.env['product.product'].search([('categ_id.delivery_categ_id.id','=',self.delivery_categ_id.id)])
            
            self.env['procurement.order']._procure_orderpoint_confirm(
                use_new_cursor=new_cr.dbname,
                company_id=self.env.user.company_id.id, product_ids=product_ids, warehouse_ids=self.warehouse_ids)
            new_cr.close()
            return {}