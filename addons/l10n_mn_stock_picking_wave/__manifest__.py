# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock 'Warehouse Management: Waves'",
    'version': '1.0',
    'depends': [
        'stock_picking_wave',
        'hr',
        'l10n_mn_stock',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Stock Additional Features
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/stock_picking_wave_view.xml',
        'views/vehicle_view.xml',
        'report/report_view.xml',
        'report/report_picking_view.xml',
    ],
    'license': 'GPL-3',
}