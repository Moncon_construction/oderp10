# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError

class StockPickingWave(models.Model):
    _inherit = "stock.picking.wave"
    
    user_id = fields.Many2one(
        'res.users', string='Create User', track_visibility='onchange',
        help='Person responsible for this wave', required=True, default=lambda self:self.env.user.id)
    create_date = fields.Date('Create Date', readonly=True, default=fields.Date.context_today)
    shipment_date = fields.Date('Shipment Date', default=fields.Date.context_today)
    vehicle_id = fields.Many2one('vehicle.vehicle', string="Vehicle")
    driver_name = fields.Char("Driver Name", required=True)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self:self.env.user.company_id.id, required=True, readonly=True)
    warehouse_id = fields.Many2one('stock.warehouse', string="Warehouse")
    picking_type_id = fields.Many2one('stock.picking.type', string="Picking Type", domain="[('code', '=', 'outgoing'),('warehouse_id','=',warehouse_id)]")
    picking_ids = fields.One2many(
        'stock.picking', 'wave_id', string='Pickings',
        help='List of picking associated to this wave')
    
    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(StockPickingWave, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if view_type == 'form':
            if 'warehouse_id' in res['fields']:
                res['fields']['warehouse_id']['domain'] = [
                    ('id', 'in', map(lambda x:x.id, self.env.user.allowed_warehouses))]
        return res
    
    @api.onchange('vehicle_id')
    def onchange_vehicle_id(self):
        for wave in self:
            wave.driver_name = wave.vehicle_id.employee_id.name

    @api.multi
    def print_picking_all(self):
        pickings = self.mapped('picking_ids')
        if not pickings:
            raise UserError(_('Nothing to print.'))
        return self.env["report"].with_context(active_ids=pickings.ids, active_model='stock.picking').get_action([], 'l10n_mn_stock.report_picking')

    @api.onchange('warehouse_id')
    def onchange_warehouse(self):
        for wave in self:
            if self.warehouse_id:
                stock_picking_type = self.env['stock.picking.type'].search([('warehouse_id', '=', self.warehouse_id.id),('code','=','outgoing')], limit=1)
                if stock_picking_type:
                    self.picking_type_id = stock_picking_type.id
    
    @api.onchange('picking_type_id')
    def onchange_picking_type(self):
        domain = {}
        domain['picking_ids'] = [('id', 'in', [])]
        picking_ids = False
        domain = {}
        picking_ids = False
        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_sale_stock_picking_wave'),('state', 'in', ('installed', 'to upgrade'))])
        module2 = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_distribution_team_stock_picking_wave'),('state', 'in', ('installed', 'to upgrade'))])
        if module and module2:
            if self.team_id and self.distribution_id:
                picking_ids = self.env['stock.picking'].search([('team_id', '=', self.team_id.id),('distribution_section_id', '=', self.distribution_id.id),('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
            elif self.team_id:
                picking_ids = self.env['stock.picking'].search([('team_id', '=', self.team_id.id),('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
            elif self.distribution_id:
                picking_ids = self.env['stock.picking'].search([('distribution_section_id', '=', self.distribution_id.id),('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
            else:
                picking_ids = self.env['stock.picking'].search([('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
        elif module:
            if self.team_id:
                picking_ids = self.env['stock.picking'].search([('team_id', '=', self.team_id.id),('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
            else:
                picking_ids = self.env['stock.picking'].search([('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
        elif module2:
            if self.distribution_id:
                picking_ids = self.env['stock.picking'].search([('distribution_section_id', '=', self.distribution_id.id),('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
            else:
                picking_ids = self.env['stock.picking'].search([('state', 'not in', ('cancel', 'done')), ('company_id', '=', self.company_id.id),('picking_type_id', '=', self.picking_type_id.id)])
        else:
            picking_ids = self.env['stock.picking'].search([('state', 'not in', ('cancel', 'done')), ('company_id', '=', wave.company_id.id),('picking_type_id', '=', wave.picking_type_id.id)])
        domain['picking_ids'] = [('id', 'in', picking_ids.ids)]
        return {'domain': domain}