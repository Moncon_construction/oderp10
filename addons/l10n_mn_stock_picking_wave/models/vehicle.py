# -*- coding: utf-8 -*-

from odoo import fields, models

class VehicleVehicle(models.Model):
    _name = 'vehicle.vehicle'
    _description = 'Vehicle'
    _inherit = ['mail.thread']
    
    name = fields.Char('Vehicle Number')
    employee_id = fields.Many2one('hr.employee', 'Driver Name')
    vehicle_mark = fields.Char('Vehicle Mark')
