# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, models, _
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, convert_curr
from odoo.exceptions import UserError
from datetime import datetime

class ReportStockPickingWave(models.AbstractModel):
    _name = 'report.l10n_mn_stock_picking_wave.report_picking_wave'

    @api.multi
    def render_html(self, docids, data=None):
        #Бэлтгэх давалгааг хэвлэдэг болгов.
        report_obj = self.env['report']
        picking_obj = self.env['stock.picking']
        picks = self.env['stock.picking']
        product_obj = self.env['product.product']
        waves = self.env['stock.picking.wave'].search([('id', 'in', docids)])
        pickings = []
        for wave in waves:
            picks |= picking_obj.browse(wave.picking_ids.ids)
        report = report_obj._get_report_from_name('l10n_mn_stock_picking_wave.report_picking_wave')
        product_lines_big = {}
        product_lines = []
        product_line = {}
        products = {}
        i = 0
        print_date = datetime.today().strftime('%Y-%m-%d')
        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_sale_stock_picking_wave'),('state', 'in', ('installed', 'to upgrade'))])
        module2 = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_distribution_team_stock_picking_wave'),('state', 'in', ('installed', 'to upgrade'))])
        if module and module2:
            sale_team = waves.team_id.name
            distribution_team = waves.distribution_id.name
            receiver_name = waves.distribution_id.user_id.name
        elif module:
            sale_team = waves.team_id.name
            for member in waves.team_id.member_ids:
                if member.has_group('l10n_mn_sale.sale_delivery_worker'):
                    receiver_name = member.name
                    break
        elif module2:
            distribution_team = waves.distribution_id.name
            receiver_name = waves.distribution_id.user_id.name
        else:
            sale_team = ''
            distribution_team = ''
            receiver_name = waves.driver_name
        for picking in picks:
            for line in picking.move_lines:
                if not line.product_id in product_line:
                    i += 1
                    product_obj |= line.product_id
                    box_total = line.product_uom_qty / line.product_id.product_quantity_in_box if line.product_id.product_quantity_in_box != 0 else 0
                    product_line[line.product_id] = {
                        'number': i,
                        'default_code': line.product_id.default_code,
                        'barcode': line.product_id.barcode,
                        'name': line.product_id.name,
                        'uom': line.product_id.product_tmpl_id.uom_id.name,
                        'qty': line.product_uom_qty,
                        'box_qty': round(box_total, 2)
                   }
                else:
                    qty = product_line[line.product_id]['qty'] + line.product_uom_qty
                    product_line[line.product_id].update({'qty': qty})
        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': waves,
            'lines': product_line,
            'products': product_obj,
            'print_date': print_date,
            'sale_team': sale_team,
            'distribution_team': distribution_team,
            'driver_name': receiver_name
        }
        return report_obj.render('l10n_mn_stock_picking_wave.report_picking_wave', docargs)
