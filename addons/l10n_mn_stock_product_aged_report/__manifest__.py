# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock Product Aged Report",
    'version': '1.0',
    'depends': [
        'l10n_mn_stock_account_cost_for_each_wh',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       * Бараа материалын насжилтын тайлан
    """,
    'data': [
        'security/ir.model.access.csv',
        'report/product_aged_report_view.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
}
