# -*- encoding: utf-8 -*-
#########################

import time
import base64
from io import BytesIO

import xlsxwriter
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.addons.l10n_mn_stock_base_report.tools.report_excel_cell_styles import \
    ProductLedgerReportExcelCellStyles  # @UnresolvedImport
from odoo.exceptions import UserError, ValidationError
from datetime import datetime
from odoo.addons.l10n_mn_report.tools.tools import str_tuple
import pytz
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

class ProductAgedReport(models.TransientModel):
    """
       БАРАА МАТЕРИАЛЫН НАСЖИЛТЫН ТАЙЛАН
    """
    _name = 'product.aged.report'
    _inherit = 'oderp.report.html.output'
    _description = "Product Aged Report"

    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env.user.company_id.id)
    date_from = fields.Date(string="Start Date", required=True, default=lambda *a: time.strftime('%Y-%m-01'), translate=True)
    date_to = fields.Date(string="End Date", required=True, default=lambda *a: time.strftime('%Y-%m-%d'), translate=True)
    warehouse_ids = fields.Many2many('stock.warehouse', string="Warehouses", required=True)
    category_ids = fields.Many2many('product.category', string="Categories")
    product_ids = fields.Many2many('product.product', string="Products")
    location_ids = fields.Many2many('stock.location', string='Locations')
    
    @api.onchange("company_id")
    def _get_domains(self):
        domain = {}
        domain['warehouse_ids'] = [('company_id','=', self.company_id.id)]
        _warehouses = []
        for warehouse in self.env.user.allowed_warehouses:
            _warehouses.append(warehouse.id)
        if _warehouses:
            domain['warehouse_ids'] = [('id', 'in', _warehouses), ('company_id','=', self.company_id.id)]
        domain['category_ids'] = [('company_id','=', self.company_id.id)]
        domain['product_ids'] = [('product_tmpl_id.company_id','=', self.company_id.id)]
        return {'domain': domain}

    # Тухайн сонгосон агуулахад харьяалагдах байралалууд харуулах
    @api.onchange("warehouse_ids")
    def _get_location_domain(self):
        domain = {}
        location_ids = []
        if self.warehouse_ids:
            for wh in self.warehouse_ids:
                loc_ids = self.env['stock.location'].search(
                    [('usage', '=', 'internal'), ('location_id', 'child_of', [wh.view_location_id.id])]).ids
                location_ids += loc_ids
            domain['location_ids'] = [('id', 'in', location_ids)]
        return {'domain': domain}
    
    def _get_lines(self):
        locations = []
        where = ''
        # by default timezone = UTC
        user_time_zone = pytz.UTC
        if self.env.user.partner_id.tz:
            # change the timezone to the timezone of the user
            user_time_zone = pytz.timezone(self.env.user.partner_id.tz)
        else:
            raise ValidationError(_('Warning !\n Please. You set your time zone. Settings go to the User menu!'))
        st_date = str(self.date_from) + ' 00:00:00'
        end_date = str(self.date_to) + ' 23:59:59'
        date_from = user_time_zone.localize(datetime.strptime(st_date, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(pytz.utc).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        date_to = user_time_zone.localize(datetime.strptime(end_date, DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(pytz.utc).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        if self.warehouse_ids:
            for wh in self.warehouse_ids:
                loc_id = self.env['stock.location'].search([('usage', '=', 'internal'),('location_id', 'child_of', [wh.view_location_id.id])]).ids
                locations += loc_id
        else:
            loc_id = self.env['stock.location'].search([('usage', '=', 'internal')]).ids
            locations += loc_id
        if self.location_ids:
            location_ids = self.location_ids.ids
            where += ' AND sl.id in (' + ','.join(map(str, location_ids)) + ') '
        if len(locations) > 0:
            locations = tuple(locations)
        else:
            raise ValidationError(_('There is no stock location!'))
        if self.product_ids:
            product_ids = self.product_ids.ids
            where += ' AND pp.id in (' + ','.join(map(str, product_ids)) + ') '
        elif self.category_ids:
            category_ids = self.category_ids.ids
            where += ' AND pc.id in (' + ','.join(map(str, category_ids)) + ') '
        self.env.cr.execute("SELECT l.account_name as account_name, l.account_code as account_code, l.default_code as default_code, "
                                "l.ptid as ptid, l.purchase_date as purchase_date, l.product_qty as product_qty, l.total_cost as total_cost "
                            "FROM "
                            "(SELECT  "
                                "CASE WHEN aa1.name is null THEN  aa.name ElSE aa1.name END AS account_name, "
                                "CASE WHEN aa1.code is null THEN  aa.code ElSE aa1.code END AS account_code, "
                            "pt.default_code as default_code, pt.id as ptid, sq.in_date as purchase_date, sum(sq.qty) as product_qty, sum(sq.qty*pwsp.standard_price) as total_cost "
                            "FROM stock_quant sq "
                            "LEFT JOIN product_product pp on sq.product_id = pp.id "
                            "LEFT JOIN product_template pt on pp.product_tmpl_id = pt.id "
                            "LEFT JOIN product_category pc on pt.categ_id = pc.id "
                            "LEFT JOIN stock_location sl ON sq.location_id = sl.id "
                            "LEFT JOIN stock_location sl1 ON sl.location_id = sl1.id "
                            "LEFT JOIN stock_warehouse sw ON sw.view_location_id = sl.id "
                            "LEFT JOIN stock_warehouse sw1 ON sw1.view_location_id = sl1.id "
                            "LEFT JOIN product_warehouse_standard_price pwsp on pt.id = pwsp.product_id and sw.id = pwsp.warehouse_id "
                            "LEFT JOIN account_account aa on sw.stock_valuation_account_id = aa.id "
                            "LEFT JOIN account_account aa1 on sw1.stock_valuation_account_id = aa1.id "
                            "WHERE sq.in_date BETWEEN '" + date_from + "' AND '" + date_to + "' " + where + " "
                            "AND sq.location_id in " + str_tuple(locations) + " "
                            "GROUP BY sq.product_id, aa.name, aa.code,aa1.name, aa1.code, pt.default_code, pt.id, sq.in_date, sw.stock_valuation_account_id) AS l "
                            "ORDER BY l.account_code asc, l.default_code,l.purchase_date desc" )
        results = self._cr.dictfetchall()
        return results , len(results) if results else False

    @api.multi
    def export_report(self):
        # create workbook
        report_obj = self
        user = self.env.user
        company = user.company_id
        
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)
        
        # create name
        report_name = _(u'Бараа материалын насжилтын тайлан')
        file_name = "%s_%s.xlsx" % (report_name, time.strftime('%Y%m%d_%H%M'),)
        
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('product_aged_report'), form_title=file_name).create({})
        
        format_name = book.add_format(ProductLedgerReportExcelCellStyles.format_name)
        format_filter = book.add_format(ProductLedgerReportExcelCellStyles.format_filter)
        format_filter_right = book.add_format(ProductLedgerReportExcelCellStyles.format_filter_right)
        format_title = book.add_format(ProductLedgerReportExcelCellStyles.format_title)
        format_title_float = book.add_format(ProductLedgerReportExcelCellStyles.format_title_float)
        format_group_left = book.add_format(ProductLedgerReportExcelCellStyles.format_group_left)
        format_filter_bold = book.add_format(ProductLedgerReportExcelCellStyles.format_filter_bold)
        
        format_content_center = book.add_format(ProductLedgerReportExcelCellStyles.format_content_center)
        format_content_text = book.add_format(ProductLedgerReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ProductLedgerReportExcelCellStyles.format_content_number)
        format_content_float = book.add_format(ProductLedgerReportExcelCellStyles.format_content_float)
        
        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1c    ￼    Companym, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
        
        # compute column
        sheet.set_column('A:A', 15)
        sheet.set_column('B:B', 25)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 20)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 15)
        sheet.set_row(7, 20)
        
        rowx = 0
        len = 0
        # create contents
        sheet.merge_range(rowx+1, 2, rowx+1, 3, '%s' % report_name, format_filter)
        sheet.write(rowx+2, 0, _(u'Company name:'), format_filter)
        sheet.merge_range(rowx+2, 1, rowx+2, 2, u'%s' % (company.name), format_filter)
        sheet.write(rowx+2, 5, _(u'Duration:'), format_filter)
        sheet.write(rowx+2, 6, u'%s~%s' % (self.date_from, self.date_to), format_filter)
        
        lines_list, len = self._get_lines()
                             
        rowx += 4
        sheet.write(rowx, 0, _(u"Дансны код"), format_title)
        sheet.write(rowx, 1, _(u"Дансны нэр"), format_title)
        sheet.write(rowx, 2, _(u"Барааны код"), format_title)
        sheet.write(rowx, 3, _(u"Барааны нэр"), format_title)
        sheet.write(rowx, 4, _(u"Худалдаж авсан огноо"), format_title)
        sheet.write(rowx, 5, _(u"Тоо"), format_title)
        sheet.write(rowx, 6, _(u"Нийт өртөг"), format_title)
         
        rowx += 1
        accounts = []
        product =  account_code = account_name = default_code = purchase_date = ''
        total_cost = product_qty = i = 0
        
        is_printed = False
        if lines_list:
            l=1
            for line in lines_list:
                #Тухайн нэг агуулах дээрх дансыг авч байна.
                if line['account_name'] not in accounts:
                    if l!=1: #Өмнөх дансны хамгийн сүүлийн барааг зурж байна.
                        sheet.write(rowx, 0, account_code, format_content_center)
                        sheet.write(rowx, 1, account_name, format_content_center)
                        sheet.write(rowx, 2, default_code, format_content_center)
                        sheet.write(rowx, 3, pt_with_user_lang.name, format_content_center)
                        sheet.write(rowx, 4, purchase_date, format_content_center)
                        sheet.write(rowx, 5, product_qty, format_content_center)
                        sheet.write(rowx, 6, total_cost, format_content_center)
                        rowx += 1
                        is_printed = True
                    accounts.append(line['account_name'])
                    total_cost = product_qty =  0
                    i = rowx
                    if line['total_cost'] == None:
                        total_cost += 0
                    else:
                        total_cost += line['total_cost']
                    product_qty += line['product_qty']
                    account_code = line['account_code']
                    account_name = line['account_name']
                    default_code = line['default_code']
                    purchase_date = line['purchase_date']
                    pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                    if not pt_with_user_lang:
                        pt_with_user_lang =  line['ptid']
                    if is_printed == True or l==1:
                        product = line['ptid']
                        purchase_date = line['purchase_date']
                        total_cost = product_qty =  0
                if line['ptid'] == product and line['purchase_date'] == purchase_date:
                    if line['total_cost'] == None:
                        total_cost += 0
                    else:
                        total_cost += line['total_cost']
                    product_qty += line['product_qty']
                    is_printed = False
                if line['ptid'] != product or line['purchase_date'] != purchase_date:
                    sheet.write(rowx, 0, account_code, format_content_center)
                    sheet.write(rowx, 1, account_name, format_content_center)
                    sheet.write(rowx, 2, default_code, format_content_center)
                    sheet.write(rowx, 3, pt_with_user_lang.name, format_content_center)
                    sheet.write(rowx, 4, purchase_date, format_content_center)
                    sheet.write(rowx, 5, product_qty, format_content_center)
                    sheet.write(rowx, 6, total_cost, format_content_center)
                    product = line['ptid']
                    total_cost = product_qty = 0
                    account_code = line['account_code']
                    account_name = line['account_name']
                    default_code = line['default_code']
                    purchase_date = line['purchase_date']
                    pt_with_user_lang = self.env['product.template'].with_context(lang=self.env.user.lang).browse(line['ptid'])
                    if not pt_with_user_lang:
                        pt_with_user_lang =  line['ptid']
                    if line['total_cost'] == None:
                        total_cost += 0
                    else:
                        total_cost += line['total_cost']
                    product_qty += line['product_qty']
                    if not i == rowx:
                        #Тухайн нэг агуулахын дансыг багцалж байна.
                        sheet.merge_range(i, 0, rowx+1, 0, line['account_code'], format_content_center)
                        sheet.merge_range(i, 1, rowx+1, 1, line['account_name'], format_content_center)
                    rowx += 1
                    is_printed = True
                #Тайлангийн хамгийн сүүлийн барааны мөрийг зурж байна
                if l == len:
                    sheet.write(rowx, 0, account_code, format_content_center)
                    sheet.write(rowx, 1, account_name, format_content_center)
                    sheet.write(rowx, 2, default_code, format_content_center)
                    sheet.write(rowx, 3, pt_with_user_lang.name, format_content_center)
                    sheet.write(rowx, 4, purchase_date, format_content_center)
                    sheet.write(rowx, 5, product_qty, format_content_center)
                    sheet.write(rowx, 6, total_cost, format_content_center)
                    if not i == rowx:
                        #Тухайн нэг агуулахын дансыг багцалж байна.
                        sheet.merge_range(i, 0, rowx, 0, line['account_code'], format_content_center)
                        sheet.merge_range(i, 1, rowx, 1, line['account_name'], format_content_center)
                l += 1
             
        sheet.set_zoom(70)
        book.close()
        
        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()