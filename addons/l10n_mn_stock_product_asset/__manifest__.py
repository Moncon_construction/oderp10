# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock Product Asset",
    'version': '1.0',
    'depends': ['l10n_mn_stock', 'l10n_mn_account_asset', 'l10n_mn_purchase_asset'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Modules',
    'description': """
    Description text
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/product_asset_view.xml',
        'views/purchase_asset_picking_view.xml',
        'views/stock_location_view.xml',
        'data/data_stock_picking_type.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
