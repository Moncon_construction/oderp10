# -*- coding: utf-8 -*-
##############################################################################

from odoo import api, fields, models
from datetime import datetime


class InheritReceiveAssetsWizardLine(models.TransientModel):
    _inherit = 'receive.assets.wizard.line'

    product_asset_line = fields.Many2one('product.asset.line', 'Product Asset Line')

class InheritedReceiveAssetsWizard(models.TransientModel):
    _inherit = 'receive.assets.wizard'
    @api.model
    def default_get(self, fields):
        context = dict(self._context or {})
        res = super(InheritedReceiveAssetsWizard, self).default_get(fields)

        picking_id = self._context.get('active_id')
        active_model = self._context.get('active_model')

        assert active_model in ('purchase.asset.picking'), 'Bad context propagation'

        picking = self.env['purchase.asset.picking'].search([('id', '=', picking_id)])
        res.update({'picking_id': picking_id})
        items = []
        self._cr.execute(
            "SELECT id FROM ir_module_module WHERE name = 'l10n_mn_account_asset_code_sequencer' AND state IN ('installed', 'to upgrade')")
        results = self._cr.dictfetchall()
        results = True if results and len(results) > 0 else False
        if context.get('active_model') == 'purchase.asset.picking' and context.get('active_ids'):
            purchases = self.env[context.get('active_model')].browse(context.get('active_ids'))
            for purchase in purchases:
                for line in purchase.line_ids:
                    for i in range(line.qty):
                        item = [0, False, {
                            'code': line.code,
                            'name': line.name,
                            'asset_categ_id': line.asset_categ_id.id,
                            'owner_id': line.owner_id.id,
                            'location_id': line.location_id.id,
                            'po_line_id': line.po_line_id.id,
                            'is_code_auto': results,
                            'buy_date': str(datetime.now().date()),
                            'product_asset_line': line.product_asset_line.id if line.product_asset_line else False
                        }
                                ]
                        items.append(item)
                res.update({'line_ids': items})
                return res

class ReceiveAssetsWizard(models.TransientModel):
    _inherit = 'receive.assets.wizard'

    def get_asset_data(self, item):
        asset_datas = super(ReceiveAssetsWizard, self).get_asset_data(item)
        if item.product_asset_line:
            asset_datas['value'] = item.product_asset_line.standard_price
        return asset_datas


