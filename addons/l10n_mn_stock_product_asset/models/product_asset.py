# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
import time

class ProductAsset(models.Model):
    _name = 'product.asset'
    _rec_name = 'warehouse_id'

    name = fields.Char('Name')
    state = fields.Selection([('draft', 'Draft'),
                             ('confirm', 'Confirmed')], string='State', track_visibility='onchange', readonly=True, default="draft")
    date = fields.Date(string='Date')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id, required=True)
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse', required=True)
    picking_type_id = fields.Many2one('stock.picking.type', string="Location", domain="[('code', '=', 'outgoing'), ('warehouse_id','=',warehouse_id)]",
                                      required=True)
    line_ids = fields.One2many('product.asset.line', 'product_asset_id', string="Line")
    count_out_stock_picking = fields.Integer(compute='_count_out_stock_picking')
    count_purchase_asset_picking = fields.Integer(compute='_count_purchase_asset_picking')

    def _count_out_stock_picking(self):
        stock_picking = self.env['stock.picking'].search([('product_asset', '=', self.id)])
        self.count_out_stock_picking = len(stock_picking)

    def _count_purchase_asset_picking(self):
        stock_picking = self.env['purchase.asset.picking'].search([('product_asset_id', '=', self.id)])
        self.count_purchase_asset_picking = len(stock_picking)

    @api.multi
    def confirm(self):
        self.create_out_picking()
        self.create_purchase_asset_picking()
        self.state = 'confirm'

    @api.multi
    def draft(self):
        purchase_asset_picking_ids = self.env['purchase.asset.picking'].search([('product_asset_id', '=', self.id)])
        for purchase_asset_picking in purchase_asset_picking_ids:
            self.env['account.asset.asset'].search([('picking_id', '=', purchase_asset_picking.id), ('is_supply_asset', '=', False)]).unlink()
            purchase_asset_picking.unlink()
        stock_picking = self.env['stock.picking'].search([('product_asset', '=', self.id)])
        for picking in stock_picking:
            picking.action_force_cancel()
            picking.unlink()
        self.state = 'draft'

    @api.multi
    def create_out_picking(self):
        StockPicking = self.env['stock.picking']
        StockMove = self.env['stock.move']
        des_location = self.env['stock.location'].search([('asset_location', '=', True)])
        for record in self:
            values = {'product_asset': record.id,
                      'company_id': record.company_id.id,
                      'picking_type_id': self.picking_type_id.id,
                      'location_dest_id': des_location[0].id,
                      'location_id': self.picking_type_id.default_location_src_id.id}
            stock_picking_object = StockPicking.create(values)
            if record.line_ids:
                for line in record.line_ids:
                    move_lines_values = {'name': line.product_id.name,
                                         'picking_id': stock_picking_object.id,
                                         'product_id': line.product_id.id,
                                         'product_uom': line.product_id.product_tmpl_id.uom_id.id,
                                         'location_id': self.picking_type_id.default_location_src_id.id,
                                         'location_dest_id': des_location[0].id,
                                         'product_uom_qty': line.qty,
                                         'company_id': record.company_id.id}
                    StockMove.create(move_lines_values)
            stock_picking_object.action_confirm()
            stock_picking_object.action_assign()
            if not stock_picking_object.pack_operation_product_ids and not stock_picking_object.pack_operation_pack_ids and not stock_picking_object.pack_operation_ids:
                stock_picking_object.do_prepare_partial()
            stock_picking_object.do_new_transfer()
            wiz = self.env['stock.immediate.transfer'].create({'pick_id': stock_picking_object.id})
            stock_picking_object.write({'min_date': self.date})
            wiz.force_date = self.date
            wiz.process()

    @api.multi
    def create_purchase_asset_picking(self):
        product_asset_line = self.env['product.asset.line']
        for product_asset_id in self:
            asset_picking_vals = {
                'product_asset_id': self.id,
                'origin_type': 'product_asset',
                'create_date': product_asset_id.date,
                'user_id': product_asset_id._uid,
                'state': 'ready',
                'company_id': product_asset_id.company_id.id
            }
            asset_picking = self.env['purchase.asset.picking'].create(asset_picking_vals)
            self._create_asset_moves(product_asset_line.search([('product_asset_id', '=', product_asset_id.id)]), asset_picking.id)
        return asset_picking.id

    def _create_asset_moves(self, product_asset_lines, asset_picking_id):
        for line in product_asset_lines:
            if line.asset_categ_id:
                employee = self.env['hr.employee'].search([('user_id', '=', line.user_id.id)])
                val = {
                    'name': line.product_id.name,
                    'asset_categ_id': line.asset_categ_id.id,
                    'origin_type': 'product_asset',
                    'product_id': line.product_id.id,
                    'qty': line.qty,
                    'asset_picking_id': asset_picking_id,
                    'owner_id': employee.id if employee else False,
                    'product_asset_line': line.id
                }
                new_line_id = self.env['purchase.asset.picking.line'].create(val)
        return True


class ProductAssetLine(models.Model):
    _name = 'product.asset.line'

    product_asset_id = fields.Many2one('product.asset')
    product_id = fields.Many2one('product.product', string='Product', required=True)
    qty = fields.Char('Qty', required=True)
    standard_price = fields.Float(readonly=True, compute='_compute_standard_price')
    available_qty = fields.Float(compute='_compute_available_qty', string='Available quantity', digits=dp.get_precision('Product asset Qty'))
    asset_categ_id = fields.Many2one('account.asset.category', 'Asset Category', required=True)
    user_id = fields.Many2one('res.users', 'User', default=lambda self: self.env.uid)

    @api.one
    @api.depends('product_id')
    def _compute_standard_price(self):
        module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_stock_account_cost_for_each_wh'), ('state', 'in', ('installed', 'to upgrade'))])
        if module:
            # Агуулах бүрээр өртөг тооцоолдог үед тухайн барааны агуулахаарх өртөг шаардах хуудасын
            # мөрүүдэд өртгөө авна
            if self.product_id and self.product_asset_id.company_id and self.product_asset_id.warehouse_id:
                standard_price = self.env['product.warehouse.standard.price'].search(
                    [('company_id', '=', self.product_asset_id.company_id.id),
                     ('warehouse_id', '=', self.product_asset_id.warehouse_id.id),
                     ('product_id', '=', self.product_id.id)], limit=1).standard_price
                self.standard_price = standard_price
        else:
            self.standard_price = self.product_id.standard_price

    @api.depends('product_id')
    def _compute_available_qty(self):
        move_qty = 0
        pro_qty = 0
        for obj in self:
            warehouse = obj.product_asset_id.warehouse_id
            if not warehouse:
                raise UserError(_('Warning!\nYou must select supply warehouse before add product asset line!'))
            if obj.product_id:
                obj.available_qty = obj.product_id.get_qty_availability(
                    [obj.product_asset_id.picking_type_id.default_location_src_id.id], obj.product_asset_id.date,
                    qty_dp_digit=self.env['decimal.precision'].precision_get('Product asset Qty'))
                obj.name = obj.product_id.name_get()[0][1]
            else:
                obj.available_qty = 0

