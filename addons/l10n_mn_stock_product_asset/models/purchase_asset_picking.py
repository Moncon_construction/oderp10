# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import fields, models, api, _

class PurchaseAssetPicking(models.Model):
    _inherit = 'purchase.asset.picking'

    origin_type = fields.Selection([('purchase', 'Purchase'),
                                    ('product_asset', 'Product Asset')], default='purchase', string='Origin Type')
    purchase_id = fields.Many2one('purchase.order', 'Purchase Order', required=False)
    supplier_id = fields.Many2one('res.partner', 'Supplier', required=False)
    product_asset_id = fields.Many2one('product.asset', 'Expense')
    count_out_stock_picking = fields.Integer(compute='_count_out_stock_picking')

    def _count_out_stock_picking(self):
        stock_picking = self.env['stock.picking'].search([('product_asset', '=', self.product_asset_id.id)])
        self.count_out_stock_picking = len(stock_picking)

    # Smart button дарахад Агуулахын хөдөлгөөн-рүү үсрэнэ
    @api.multi
    def out_stock_picking_purchase_asset_action(self):
        for obj in self:
            action = self.env.ref('stock.action_picking_tree')
            result = action.read()[0]
            result.pop('id', None)
            result['context'] = {}
            stock_picking_ids = self.env['stock.picking'].search([('product_asset', '=', self.product_asset_id.id)])
            if stock_picking_ids:
                result['domain'] = [('id', 'in', stock_picking_ids.ids)]
            return result


class PurchaseAssetPickingLIne(models.Model):
    _inherit = 'purchase.asset.picking.line'

    origin_type = fields.Selection(related='asset_picking_id.origin_type', readonly=True)
    po_line_id = fields.Many2one('purchase.order.line', 'Purchase Order Line', required=False)
    product_asset_line = fields.Many2one('product.asset.line', 'Product Asset Line')