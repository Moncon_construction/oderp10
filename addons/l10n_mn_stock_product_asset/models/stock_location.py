# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import fields, models, api, _
from odoo.exceptions import ValidationError  # @UnresolvedImport

class Stock_location(models.Model):
    _inherit = 'stock.location'

    asset_location = fields.Boolean('Asset location')

    @api.constrains('asset_location')
    def _check_date(self):
        for obj in self:
            location = self.env['stock.location'].search([('id', '!=', obj.id), ('asset_location', '=', True), ('company_id', '=', obj.company_id.id)])
        if location:
            raise ValidationError(_("Asset location must be unique per company!"))