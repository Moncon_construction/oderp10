# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from collections import defaultdict

class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    def _get_accounting_data_for_valuation(self):
        journal_id, acc_src, acc_dest, acc_valuation = super(StockMove, self)._get_accounting_data_for_valuation()

        if self.location_id.usage == 'internal' and self.location_dest_id.usage in ['inventory'] and self.location_dest_id.asset_location:
            if self.location_dest_id.valuation_out_account_id:
                acc_dest = self.location_dest_id.valuation_out_account_id.id
            else:
                raise UserError(_('An inventory expense account has not been set up at the location of the asset'))
        return journal_id, acc_src, acc_dest, acc_valuation
