# -*- coding: utf-8 -*-
from odoo import models, fields, api

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    product_asset = fields.Many2one('product.asset', 'Expense')
