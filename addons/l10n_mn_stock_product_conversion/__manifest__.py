# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock Product Conversion",
    'version': '1.0',
    'depends': ['l10n_mn_stock'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Modules',
    'description': """
    Description text
    """,
    'data': [
        'views/product_conversion.xml',
        'report/stck_prod_report.xml',
        'report/stck_conversion.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}