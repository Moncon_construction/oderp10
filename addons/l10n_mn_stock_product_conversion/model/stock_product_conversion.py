# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models
from odoo.tools.translate import _
import odoo.addons.decimal_precision as dp  # @UnresolvedImport
from odoo.exceptions import UserError

import logging
from pydoc import locate
_logger = logging.getLogger(__name__)


class StockProductConversion(models.Model):
    _inherit = "stock.inventory"


    is_conversion = fields.Boolean()
     
    type_of_price = fields.Selection([('byPurchase','Price by Purchase'),('byCost','Price by Cost')], string='Type of Price',default='byPurchase')
    
    
    @api.multi
    def show_product_conversion(self):
         res = self.env['report'].get_action(self,'l10n_mn_stock_product_conversion.stck_conversion')
         return res