# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, convert_curr
from odoo.tools.translate import _
import odoo.addons.decimal_precision as dp  # @UnresolvedImport
from odoo.exceptions import UserError
import dateutil

class AccountInvoiceReportInherit(models.AbstractModel):
    _name = 'report.l10n_mn_stock_product_conversion.stck_conversion'   
    
    conversion_date = fields.Date()
    
    @api.multi
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name(
            'l10n_mn_stock_product_conversion.stck_conversion')
        docs = []
        document_obj = self.env['stock.inventory.line'].browse(docids)
        inventory = self.env['stock.inventory'].browse(docids)[0]
        inv_id = inventory.id
        conversion_date = dateutil.parser.parse(inventory.date).date()
        comp = self.env.user.company_id.name
        loc_id = inventory.location_id
        warehouse_stock= self.env['stock.warehouse'].search([('lot_stock_id', '=', loc_id.id)])
        # Орлого зарлага эсэхээс хамаарч зөрүүгээр тоо ширхэгийг гаргана
        line_in = inventory.line_ids.filtered(
            lambda line: line.product_qty > line.theoretical_qty) if inventory.line_ids else []
        line_ex = inventory.line_ids.filtered(
            lambda line: line.product_qty < line.theoretical_qty) if inventory.line_ids else []
        # Худалдах үнэ эсвэл өртгийн алийг нь сонгосноос хамаарч нэгж үнийг авч мөрийн дүн гарна
        sum_line_in = sum([(line1.lst_price * (line1.product_qty - line1.theoretical_qty))
                           if inventory.type_of_price == 'byPurchase'
                           else (line1.standard_price * (line1.product_qty - line1.theoretical_qty))
                           for line1 in line_in])
        sum_line_ex = sum([(line2.lst_price * (line2.theoretical_qty - line2.product_qty))
                           if inventory.type_of_price == 'byPurchase'
                           else (line2.standard_price * (line2.theoretical_qty - line2.product_qty))
                           for line2 in line_ex])
        docargs = {
            'doc_ids': docids,
            'inv_id':inv_id,
            'conversion_date': conversion_date,
            'comp' : comp,
            'warehouse_stock' : warehouse_stock,
            'doc_model': report.model,
            'data_report_margin_top': 5,
            'data_report_header_spacing': 5,
            'docs': document_obj,
            'type_of_price':inventory.type_of_price,
            'inventory': inventory,
            'stock_lines': {'line_in': line_in},
            'stock_lines1': {'line_ex': line_ex},
            'summ': {'sum_line_in': sum_line_in, 'sum_line_ex': sum_line_ex}
        }
        return report_obj.render('l10n_mn_stock_product_conversion.stck_conversion', docargs)
    