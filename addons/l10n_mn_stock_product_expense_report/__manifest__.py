# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2018 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

{
    "name" : "Mongolian Stock Product Expense Report",
    "version" : "1.0",
    "author" : "Asterisk Technologies LLC",
    "description": """Уг тайланд барааны зарлагыг тооллого, нөхөн дүүргэлт болон 
                        борлуулалтаар нь ялгаж харуулдаг.""",
    "website" : True,
    "category" : "purchase",
    	"depends" : ['l10n_mn_stock', 'l10n_mn_product_expense'],
    "init": [],
    "data" : [
        'security/ir.model.access.csv',
        'report/product_expensed_report.xml'
    ],
    "demo_xml": [
    ],
    "active": False,
    "installable": True,
}
