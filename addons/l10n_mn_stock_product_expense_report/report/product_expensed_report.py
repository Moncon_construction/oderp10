# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

#
# Please note that these reports are not multi-currency !!!
#

from odoo import api, fields, models, tools
from odoo.addons import decimal_precision as dp

class ProductExpensedReport(models.Model):
    _name = "product.expensed.report"
    _description = "Product Expensed Report"
    _auto = False
    _order = 'date asc'

    date = fields.Datetime('Expensed Date', readonly=True, help="Date on which this document has been created", oldname='date')
    product_uom_qty = fields.Float(
        'Quantity',
        digits=dp.get_precision('Product Unit of Measure'),
        default=1.0, required=True, states={'done': [('readonly', True)]})
    cost = fields.Float(string='Cost')
    product_id = fields.Many2one(
        'product.product', 'Product',
        domain=[('type', 'in', ['product', 'consu'])], index=True, required=True,
        states={'done': [('readonly', True)]})
    picking_id = fields.Many2one('stock.picking', 'Transfer Reference', index=True, readonly=True)
    categ_id = fields.Many2one(
        'product.category', 'Internal Category', domain="[('type','=','normal')]")
    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse')
    usage = fields.Selection([
        ('internal', 'Internal Location'),
        ('inventory', 'Inventory Loss'),
        ('expense', 'Expense')], string='Location Type')
    
    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'product_expensed_report')
        self._cr.execute("""
            create view product_expensed_report as (
                select
                    min(sm.id) as id,
                    sm.date as date,
                    sm.warehouse_id as warehouse_id,
                    sm.picking_id as picking_id,
                    sm.product_id as product_id,
                    (CASE When sl2.usage = 'inventory' and sp.expense is not null THEN 'expense' 
                     WHEN sl2.usage = 'inventory' and sp.expense is null THEN 'inventory' ELSE 'internal' END) as usage,
                    sum(sm.product_uom_qty) as product_uom_qty,
                    sum(sm.product_uom_qty*sm.price_unit) as cost,
                    pt.categ_id as categ_id
                from stock_move sm 
                left join stock_picking sp on sm.picking_id = sp.id 
                left join product_product pp on sm.product_id = pp.id 
                left join product_template pt on pp.product_tmpl_id = pt.id 
                left join product_category pc on pt.categ_id = pc.id 
                left join stock_location sl on sm.location_id = sl.id 
                left join stock_location sl2 on sm.location_dest_id = sl2.id 
                where sm.state = 'done' and sl.usage = 'internal' and sl2.usage in ('inventory', 'customer') and sm.company_id = %s
                group by
                    sm.date, sm.product_id, sm.picking_id, pt.categ_id, sm.warehouse_id, sl2.usage, sp.expense
            )""" % self.env.user.company_id.id)
