# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock Quant Fixer",
    'version': '1.0',
    'depends': ['l10n_mn_stock_account','l10n_mn_product_cost_calculation'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
       Stock Quant Fixer CRON
    """,
    'data': [
        'views/fix_stock_quant_view.xml',
        'views/fix_stock_quant_cron.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
}