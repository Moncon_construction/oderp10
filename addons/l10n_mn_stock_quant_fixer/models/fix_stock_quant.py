# -*- coding: utf-8 -*-
from odoo import api, fields, models, _, tools, registry
from odoo.exceptions import UserError
import logging
from datetime import datetime
import threading
from psycopg2 import OperationalError

_logger = logging.getLogger(__name__)


class FixStockQuant(models.Model):
    _inherit = 'fix.stock.quant'
    
    diff = fields.Float('quant move diff')

    def _check(self):
        location_obj = self.env['stock.location']

        sub_select = ''
        select = ''
        sub_join = ''
        join = ''
        where = ''
        account_where = ''
        group = ''
        order = ''
        locations = []
        date_from = datetime.now()
        date_to = datetime.now()
        having = "HAVING SUM(l.start_qty)::decimal(16,4) <> 0 OR SUM(l.start_cost)::decimal(16,4) <> 0 OR SUM(l.qty)::decimal(16,4) <> 0 \
            OR SUM(l.cost)::decimal(16,4) <> 0 OR SUM(l.ex_qty)::decimal(16,4) <> 0 OR SUM(l.ex_cost)::decimal(16,4) <> 0 "
        select = "sl.id as lid, sw.code AS wcode, sw.name AS wname, "
        join += "LEFT JOIN stock_location sl ON (l.lid = sl.id)"
        join += "LEFT JOIN stock_location sl1 ON (sl.location_id = sl1.id) "
        join += "LEFT JOIN stock_warehouse sw ON (sw.view_location_id = sl1.id) "
        group = ",sl.id, sw.code, sw.name "
        order += " sw.code, sw.name, "

        for obj in self:
            if obj.line_ids[0].product_id:
                if not locations:
                    warehouse_dic = {}
                    for wh in self.env['stock.warehouse'].search([('company_id', '=', 3)]):
                        loc_id = location_obj.search([('usage', '=', 'internal'), ('location_id', 'child_of', [wh.view_location_id.id])]).ids
                        if loc_id:
                            locations += loc_id
                            if loc_id[0] not in warehouse_dic.keys():
                                warehouse_dic[loc_id[0]] = wh.id
                        else:
                            raise UserError(_('Stock Location not found!'))
                    if len(locations) > 0:
                        locations = tuple(locations)
                        
                product_ids = obj.line_ids[0].product_id.ids
                where += ' AND pp.id in (' + ','.join(map(str, product_ids)) + ') '

                self.env.cr.execute("SELECT l.list_price,l.prod_id AS pid, l.ptid as ptid, l.name AS name, l.code AS code, l.barcode as barcode, l.uom_name AS uom_name, "
                            "l.cid AS cid, l.cname AS cname,l.size_name as size,l.size_id AS size_id, " + select + " "
                            "SUM(l.start_qty) AS start_qty, SUM(l.start_cost) AS start_cost, "
                            "SUM(l.qty) AS qty, SUM(l.cost) AS cost, "
                            "SUM(l.ex_qty) AS ex_qty, SUM(l.ex_cost) AS ex_cost, "
                            "SUM(l.start_qty + l.qty - l.ex_qty) AS end_qty,  "
                            "CASE WHEN SUM(l.start_qty) = 0 THEN SUM(l.cost - l.ex_cost) ELSE SUM(l.start_cost + l.cost - l.ex_cost) END AS end_cost "
                        "FROM "
                            "( "
                                "SELECT pt.list_price as list_price,ps.name AS size_name,ps.id AS size_id,m.product_id AS prod_id, pt.id as ptid, pt.name AS name, pp.default_code AS code, pp.barcode as barcode,  "
                                    " u2.name AS uom_name, pc.id AS cid, pc.name AS cname, " + sub_select + " "
                                    
                                    "CASE WHEN m.date < %s AND m.location_id NOT IN %s AND m.location_dest_id IN %s "
                                            "THEN COALESCE(m.product_uom_qty/u.factor*u2.factor,0) "
                                        "WHEN m.date < %s AND m.location_id IN %s AND m.location_dest_id NOT IN %s "
                                            "THEN -COALESCE(m.product_uom_qty/u.factor*u2.factor,0) ELSE 0 END AS start_qty, "
                                    "CASE WHEN m.date < %s AND m.location_id NOT IN %s AND m.location_dest_id IN %s "
                                            "THEN COALESCE(m.price_unit*m.product_uom_qty,0) "
                                        "WHEN m.date < %s AND m.location_id IN %s AND m.location_dest_id NOT IN %s "
                                            "THEN -COALESCE(m.price_unit*m.product_uom_qty,0) ELSE 0 END AS start_cost, "
                                            
                                    "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s AND m.location_dest_id IN %s "
                                            "THEN COALESCE(m.product_uom_qty/u.factor*u2.factor,0) ELSE 0 END AS qty, "
                                    "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id NOT IN %s AND m.location_dest_id IN %s "
                                            "THEN COALESCE(m.price_unit*m.product_uom_qty,0) ELSE 0 END AS cost, "
                                            
                                    "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s AND m.location_dest_id NOT IN %s "
                                            "THEN COALESCE(m.product_uom_qty/u.factor*u2.factor,0) ELSE 0 END AS ex_qty, "
                                    "CASE WHEN m.date BETWEEN %s AND %s AND m.location_id IN %s AND m.location_dest_id NOT IN %s "
                                            "THEN COALESCE(m.price_unit*m.product_uom_qty,0) ELSE 0 END AS ex_cost, "
                                            
                                    "CASE WHEN m.location_id NOT IN %s AND m.location_dest_id IN %s "
                                            "THEN m.location_dest_id "
                                        "WHEN m.location_id IN %s AND m.location_dest_id NOT IN %s "
                                            "THEN m.location_id ELSE 0 END AS lid "
                                "FROM stock_move m "
                                "LEFT JOIN product_product pp ON (pp.id=m.product_id) "
                                "LEFT JOIN product_template pt ON (pt.id=pp.product_tmpl_id) "
                                "LEFT JOIN product_size ps ON (ps.id = pt.size_id)"
                                "LEFT JOIN res_partner rp ON (pt.supplier_id = rp.id)"
                                "LEFT JOIN product_uom u ON (u.id=m.product_uom) "
                                "LEFT JOIN product_uom u2 ON (u2.id=pt.uom_id) "
                                "LEFT JOIN product_category pc ON (pt.categ_id = pc.id) " + sub_join + " "
                                "WHERE m.state = 'done' " + where + 
                            ") AS l " + join + account_where + " "
                        "GROUP BY l.prod_id, l.ptid, l.name, l.list_price, l.code, l.barcode, l.uom_name, l.cid, l.cname,l.size_id,l.size_name" + group + having + " "
                        "ORDER BY " + order + "l.cname, l.name, l.size_name, l.code, l.uom_name ", (date_from, locations, locations, date_from, locations, locations,
                                                                                                    date_from, locations, locations, date_from, locations, locations,
                                                                                                    date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                                                                                                    date_from, date_to, locations, locations, date_from, date_to, locations, locations,
                                                                                                    locations, locations, locations, locations))
                moves = self.env.cr.dictfetchall()
                
                where1 = ''
                where1 += ' AND product_id in (' + ','.join(str(p_id) for p_id in product_ids) + ') '
                self.env.cr.execute("select location_id as location_id, product_id as pid, sum(qty) as total_qty \
                                            from stock_quant where location_id in (" + ','.join(str(l_id) for l_id in locations) + ") " + where1 + " group by location_id, product_id")

                quant_lines = self.env.cr.dictfetchall()

                quant = {}
                for l in quant_lines:
                    if l['location_id'] not in quant.keys():
                        quant[l['location_id']] = {}
                        quant[l['location_id']][l['pid']] = l['total_qty']
                    else:
                        quant[l['location_id']][l['pid']] = l['total_qty']
                total_move_qty = 0
                total_quant_qty = 0
                
                for line in moves:
                    total_move_qty += line['end_qty']
                    total_quant_qty += quant[line['lid']][line['pid']] if (line['lid'] in quant.keys() and line['pid'] in quant[line['lid']].keys()) else 0
                obj.diff = int(total_move_qty) - int(total_quant_qty)
                if (int(total_move_qty) == int(total_quant_qty)) or -1 < (int(total_move_qty) - int(total_quant_qty)) < 1:
                    obj.state = 'calculated'
                    return True
                else:
                    obj.state = 'draft'
                    return False
            
#     def set_products(self):
#         fixed_quant = []
#         for obj in self.env['fix.stock.quant'].search([('state', '=', 'calculated')]):
#             for line in obj.line_ids[0]:
#                 if line.product_id:
#                     fixed_quant.append(line.product_id.id)
#         product_ids = self.env['product.product'].search([('id', 'not in', fixed_quant)], limit=100)
# 
#         for product in product_ids:
#             fix_quant_obj = self.env['fix.stock.quant']
#             fix_quant_line_obj = self.env['fix.stock.quant.line']
#             fixer_id = fix_quant_obj.create({
#                 'name':product.display_name,
#                 'line_ids': [],
#                 'state': 'draft'
#                 })
#             line_id = fix_quant_line_obj.create({'product_id':product.id})
#             fixer_id.write({'line_ids': [(4, line_id.id)]})

    def _calculating(self, thread):
        for obj in self:
            for product in obj.line_ids[0].product_id:
                if not product:
                    continue
                if not obj._check():
                    obj.line_ids[0].state = 'started'
                    fix_quant_id = False
                    _logger.info(' \n\n +++++++++++++++++++++++++++++++++++++++++ THREAD NUMBER: %s +++++++++++++++++++++++++++++++++++++++++> \n\n ' % thread)
                    try:
                        self.env.cr.execute("""DELETE FROM stock_quant where product_id = """ + str(product.id))
                        moves = self.env['stock.move'].search([('product_id', '=', product.id)], order='date ASC')
                        i = 0
                        _logger.info('\n !!!!!!!!!!!MOVE RANGE +++====================+++>%s' % len(moves))
                        if not len(moves) == 0:
                            for move in moves:
                                i += 1
                                _logger.info('\n *** FIXING MOVE NAME =======+>%s : %s \n' % (str(i), move.name))
                                if move.picking_id.state == 'done' or (not move.picking_id and move.state == 'done'):
                                    self.env.cr.execute("""DELETE FROM stock_quant_move_rel where move_id = """ + str(move.id))
                                    if move.picking_id and \
                                            (move.picking_id.picking_type_id.use_existing_lots or move.picking_id.picking_type_id.use_create_lots) and \
                                            move.product_id.tracking != 'none' and \
                                            not (move.restrict_lot_id):
                                        lot_id = self.get_lot_id(move.product_id.id, move)
                                        if lot_id:
                                            move.restrict_lot_id = lot_id[0]
                                    links = self.env['stock.move.operation.link'].search([('move_id', '=', move.id)])
                                    if links:
                                        # 1 stock.move-н ард олон stock.move.operation.link үүсэж байгаа тул тэд нарын нийлбэрээр шалгасан болно.
                                        # operation_id өөр өөр байх боломжтой тул давталтаар доорх байдлаар шийдэв
                                        link_qty = 0
                                        for link in links:
                                            link_qty += link.qty
                                        if move.product_qty > link_qty:
                                            link.qty = link.qty + move.product_qty - link_qty
                                        elif move.product_qty < link_qty:
                                            diff = link_qty - move.product_qty
                                            for link in links:
                                                if diff != 0:
                                                    if diff == link.qty:
                                                        self.env.cr.execute("""DELETE FROM stock_move_operation_link where id = """ + str(link.id))
                                                        diff = 0
                                                    elif diff < link.qty:
                                                        link.qty = link.qty - diff
                                                        diff = 0
                                                    elif diff > link.qty:
                                                        self.env.cr.execute("""DELETE FROM stock_move_operation_link where id = """ + str(link.id))
                                                        diff -= link.qty
                                    if not move.picking_id:
                                        move.picking_type_id = False
                                    account_moves = self.env['account.move.line'].search([('stock_move_id', '=', move.id)])
                                    for account_move in account_moves:
                                        if len(account_move.move_id.line_ids) > 1:
                                            self.env.cr.execute("""DELETE FROM account_move_line where id = """ + str(account_move.id))
                                        else:
                                            self.env.cr.execute("""DELETE FROM account_move_line where id = """ + str(account_move.id))
                                            account_move.move_id.button_cancel()
                                            account_move.move_id.unlink()
                                    if move.product_qty != move.product_uom_qty:
                                        self.env.cr.execute("""UPDATE stock_move set product_qty = product_uom_qty where id =  """ + str(move.id))
                                    move_date = move.date
                                    move.state = 'draft'
                                    move.action_confirm()
                                    move.force_assign()
                                    move.action_done_for_fix_quant()
                                    move.date = move_date
                                    if not move.account_line_ids:
                                        move.create_account_move_line()
                    except Exception:
                        _logger.info('\n<<<<<Something went wrong!>>>>>\n')
                        return False
                    finally:
                        return True
                elif obj._check():
                    obj.line_ids[0].state = 'calculated'

    @api.multi
    def run(self, autocommit=False, thread=False):
        # TDE FIXME: avoid browsing everything -> avoid prefetching ?
        for quant_fixers in self:
            # we intentionnaly do the browse under the for loop to avoid caching all ids which would be resource greedy
            # and useless as we'll make a refresh later that will invalidate all the cache (and thus the next iteration
            # will fetch all the ids again)
            if quant_fixers.state not in ("started", "calculated"):
                try:
                    if quant_fixers._calculating(thread=thread):
                        quant_fixers.write({'state': 'started'})
                    if autocommit:
                        self.env.cr.commit()
                except OperationalError:
                    if autocommit:
                        self.env.cr.rollback()
                        continue
                    else:
                        raise
        return True

    @api.multi
    @api.returns('self', lambda quant_fixers: [quant_fixer.id for quant_fixer in quant_fixers])
    def check(self, autocommit=False):
        # TDE FIXME: check should not do something, just check
        quant_fixer_calculated = self.env['fix.stock.quant']
        for quant_fixer in self:
            try:
                result = quant_fixer._check()
                if result:
                    quant_fixer_calculated += quant_fixer
                if autocommit:
                    self.env.cr.commit()
            except OperationalError:
                if autocommit:
                    self.env.cr.rollback()
                    continue
                else:
                    raise
        if quant_fixer_calculated:
            quant_fixer_calculated.write({'state': 'calculated'})
        return quant_fixer_calculated

    @api.model
    def run_quant_fixer(self, use_new_cursor=False, company_id=False, thread=False):
        '''
        Call the scheduler to check the procurement order. This is intented to be done for all existing companies at
        the same time, so we're running all the methods as SUPERUSER to avoid intercompany and access rights issues.

        @param use_new_cursor: if set, use a dedicated cursor and auto-commit after processing each procurement.
            This is appropriate for batch jobs only.
        @return:  Dictionary of values
        '''
        try:
            if use_new_cursor:
                cr = registry(self._cr.dbname).cursor()
                self = self.with_env(self.env(cr=cr))  # TDE FIXME
            FixStockQuantSudo = self.env['fix.stock.quant'].sudo()
            # Run draft fixers
            if thread == 'ONE':
                fix_quants = FixStockQuantSudo.search([('state', '!=', 'calculated')] + (company_id and [('company_id', '=', company_id)] or []), order='name asc', limit=1)
#                 [021559550150] [32018] Butcher Boy Coconut Oil 100%-444ml 
            elif thread == 'TWO':
                fix_quants = FixStockQuantSudo.search([('state', '!=', 'calculated')] + (company_id and [('company_id', '=', company_id)] or []), order='name desc', limit=1)
#             run_quants = []
#             while fix_quants:   
            fix_quants._calculating(thread=thread)
#             run_quants.extend(fix_quants.ids)
#             print run_quants, '<<<<<<<<<<<<<<<<<<<<<,,,,,,runquants'
#             if use_new_cursor:
#                 self.env.cr.commit()
#             fix_quants = FixStockQuantSudo.search([('id', 'not in', run_quants), ('state', '=', 'draft')] + (company_id and [('company_id', '=', company_id)] or []))

            # Check calculated quant fixers
#             fix_quants = FixStockQuantSudo.search([('state', '=', 'started')] + (company_id and [('company_id', '=', company_id)] or []))
#             fix_quants.check(autocommit=use_new_cursor)
#             if use_new_cursor:
#                 self.env.cr.commit()
#             self.env.cr.close()
  
        finally:
            if use_new_cursor:
                try:
                    self.env.cr.close()
                except Exception:
                    pass
        return {}

    @api.multi
    def _calculating_with_product(self, thread):
        with api.Environment.manage():
            # As this function is in a new thread, i need to open a new cursor, because the old one may be closed
            new_cr = registry(self._cr.dbname).cursor()
            self = self.with_env(self.env(cr=new_cr))  # TDE FIXME
            quant_fixer_cron = self.sudo().env.ref('l10n_mn_stock_quant_fixer.fix_stock_quant_cron')
            # Avoid to run the scheduler multiple times in the same time
            try:
                with tools.mute_logger('odoo.sql_db'):
                    self._cr.execute("SELECT id FROM ir_cron WHERE id = %s FOR UPDATE NOWAIT", (quant_fixer_cron.id,))
            except Exception:
                _logger.info('Attempt to run quant fixer aborted, as already running')
                self._cr.rollback()
                self._cr.close()
                return {}

            StockQuantFixer = self.env['fix.stock.quant']
            for company in self.env.user.company_ids:
                StockQuantFixer.run_quant_fixer(use_new_cursor=self._cr.dbname, company_id=company.id, thread=thread)
            # close the new cursor
            self._cr.close()
            return {}

    @api.multi
    def cron_fix_quant(self):
#         for obj in self.env['fix.stock.quant'].search([]):
#             obj._check()
#         uniq_product_id = []
#         for obj in self.env['fix.stock.quant'].search([]):
#             if obj.line_ids[0].product_id:
#                 if not obj.line_ids[0].product_id.id in uniq_product_id:
#                     uniq_product_id.append(obj.line_ids[0].product_id.id)
#                 else:
#                     obj.unlink()
#             else:
#                 obj.unlink()

#         fixed_quant = []
#         for obj in self.env['fix.stock.quant'].search([('state', '=', 'calculated')]):
#             for line in obj.line_ids[0]:
#                 if line.product_id:
#                     fixed_quant.append(line.product_id.id)
#         product_ids = self.env['product.product'].search([('id', 'not in', fixed_quant)], limit=100)
# 
#         for product in product_ids:
#             fix_quant_obj = self.env['fix.stock.quant']
# #             fix_quant_line_obj = self.env['fix.stock.quant.line']
# #             fixer_id = fix_quant_obj.create({
# #                 'name':product.display_name,
# #                 'line_ids': [],
# #                 'state': 'draft'
# #                 })
# #             line_id = fix_quant_line_obj.create({'product_id':product.id})
# #             fixer_id.write({'line_ids': [(4, line_id.id)]})
# 
#         _logger.info('\n\n\n<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>><<<<<<<<CRON START>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n\n')
# # 
# # #             fix_quant_obj = self.env['fix.stock.quant']
# # #             fix_quant_ids = fix_quant_obj.search([])
# # #             fixed_product_ids = []
# # #             if fix_quant_ids:
# # #                 for fixed in fix_quant_ids:
# # #                     for line in fixed.line_ids:
# # #                         fixed_product_ids.append(line.product_id.id)
# # #             product_ids1 = self.env['product.product'].search([('id', 'not in', fixed_product_ids)], order="id desc", limit=1)
# # #             fixed_product_ids = fixed_product_ids + product_ids1.ids
# # #             product_ids2 = self.env['product.product'].search([('id', 'not in', fixed_product_ids)], order="id asc", limit=1)
#   
        threaded_calculation_1 = threading.Thread(target=self._calculating_with_product('ONE'), args=(), name=('Thread ONE'))
        threaded_calculation_2 = threading.Thread(target=self._calculating_with_product('TWO'), args=(), name=('Thread TWO'))
#         threaded_calculation_3 = threading.Thread(target=self._calculating_with_product('THREAD--->THREE'), args=(), name=('Thread THREE'))
        threaded_calculation_1.start()
        threaded_calculation_2.start()
#         threaded_calculation_3.start()
