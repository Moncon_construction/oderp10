# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    "name": "Resource Report",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
Resource Reports
""",
    'depends': ['l10n_mn_stock'],
    "website": "http://www.asterisk-tech.mn/",
    "category": "Generic Modules/Others",
    "data": [
        'wizard/stock_resource_report_view.xml',
    ],
    "installable": True,
}