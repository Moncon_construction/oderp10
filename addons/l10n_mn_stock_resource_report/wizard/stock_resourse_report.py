# -*- coding: utf-8 -*-
from io import BytesIO
import base64
import time
import xlsxwriter
from odoo import api, fields, models, _
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class StockResourceReport(models.TransientModel):
    """
        Бараа материалын нөөцийн тайлан
    """

    @api.multi
    def _domain_setter(self):
        return [('id', '=', self.env.user.allowed_warehouses.ids)]

    _name = 'stock.resource.report'
    _description = "Stock Resource Report"

    show_reserved = fields.Boolean('Show reserved')
    show_residual = fields.Boolean('Show residual')
    date = fields.Date('Date', required=True, index=True, default=fields.Date.context_today)
    warehouse_ids = fields.Many2many('stock.warehouse', string="Warehouses", domain=_domain_setter)
    product_ids = fields.Many2many('product.product', string="Products")
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env['res.company']._company_default_get('report.sales'))

    def is_module_installed(self, module_name):
        self._cr.execute(
            "SELECT id FROM ir_module_module WHERE name = '%s' AND state IN ('installed', 'to upgrade')" % module_name)
        results = self._cr.dictfetchall()
        if results and len(results) > 0:
            return True
        else:
            return False

    @api.model
    def _get_unit_cost(self, product_id):
        if self.is_module_installed("l10n_mn_stock_account_cost_for_each_wh"):
            pwsps = self.env['product.warehouse.standard.price'].search([('product_id', '=', product_id)])
            if pwsps:
                return sum(pwsp.standard_price for pwsp in pwsps) / len(pwsps)
            else:
                return 0
        else:
            return self.env['product.product'].browse(product_id).product_tmpl_id.standard_price

    @api.model
    def get_lines(self, location_ids, product_ids, date, product_id=False):
        if product_ids:
            product_where = 'sm.product_id in (' + str(product_ids)[1:-1] + ') AND'
        else:
            product_where = ''
        query = '''
                    SELECT
                        sm.product_id as product_id,
                        pb.brand_name as brand,
                        pc.name as categ_name,
                        pt.default_code as default_code,
                        pu.name as uom_name,
                        pt.name as product_name,
                        sum(
                            pt.list_price * case
                                when sm.location_dest_id in %s
                                and sm.location_id not in %s then sm.product_qty
                                when sm.location_dest_id not in %s
                                and sm.location_id in %s then -sm.product_qty
                            end
                        ) as list_price,
                        COALESCE(sum(
                            case
                                when sm.location_dest_id in %s
                                and sm.location_id not in %s then sm.product_qty
                                when sm.location_dest_id not in %s
                                and sm.location_id in %s then - sm.product_qty
                            end
                        ), 0) as quantity
                    FROM
                        stock_move sm
                        LEFT JOIN product_product pp on sm.product_id = pp.id
                        LEFT JOIN product_template pt on pt.id = pp.product_tmpl_id
                        LEFT JOIN product_brand pb on pt.brand_name = pb.id
                        LEFT JOIN product_category pc on pt.categ_id = pc.id
                        LEFT JOIN product_uom pu on pt.uom_id = pu.id
                        LEFT JOIN stock_location sl1 on sm.location_id = sl1.id
                        LEFT JOIN stock_location sl2 on sm.location_dest_id = sl2.id
                    WHERE
                        sm.state = 'done' AND %s
                        sm.date <= '%s' ''' % ('(' + str(location_ids)[1:-1] + ')',
                                               '(' + str(location_ids)[1:-1] + ')',
                                               '(' + str(location_ids)[1:-1] + ')',
                                               '(' + str(location_ids)[1:-1] + ')',
                                               '(' + str(location_ids)[1:-1] + ')',
                                               '(' + str(location_ids)[1:-1] + ')',
                                               '(' + str(location_ids)[1:-1] + ')',
                                               '(' + str(location_ids)[1:-1] + ')',
                                               product_where,
                                               date) + ('AND sm.product_id = %s' % product_id if product_id else '') + '''
                    GROUP BY
                        sm.product_id,
                        pb.brand_name,
                        pc.name,
                        pt.default_code,
                        pu.name,
                        pt.name,
                        pt.list_price
                    '''
        self._cr.execute(query)
        return self.env.cr.dictfetchall()

    @api.model
    def _get_reserved_moves(self, product_id, location_ids):
        query = '''
        SELECT
            sum(sm.product_qty)
        FROM
            stock_move sm
            LEFT JOIN stock_location sl1 ON sm.location_id = sl1.id
            LEFT JOIN stock_location sl2 ON sm.location_dest_id = sl2.id
        WHERE
            sm.product_id = %s
            AND sm.state = 'assigned'
            AND sm.date <= '%s 23:59:59'
            AND sl1.id in %s
            AND sl2.id not in %s
        GROUP BY
            sm.product_id''' % (product_id,
                                self.date,
                                '(' + str(location_ids)[1:-1] + ')',
                                '(' + str(location_ids)[1:-1] + ')')
        self._cr.execute(query)
        result = self._cr.dictfetchall()
        return result[0]['sum'] if result else 0

    @api.multi
    def export_report(self):
        ''' Тайлангийн загварыг боловсруулж өгөгдлүүдийг
                    тооцоолж байрлуулна.
        '''
        StockLocation = self.env['stock.location']
        # Тайлан хэвлэх процесс
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # Нэр үүсгэх
        report_name = _('Stock resource report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'))
        # Тайлангийн формат хэсэг
        format_content_float_border = book.add_format(ReportExcelCellStyles.format_content_float_border)
        format_content_center_border = book.add_format(ReportExcelCellStyles.format_content_center_border)
        format_content_left_color = book.add_format(ReportExcelCellStyles.format_content_left_color)
        format_title_float_border = book.add_format(ReportExcelCellStyles.format_title_float_border)
        format_filter_right = book.add_format(ReportExcelCellStyles.format_filter_right)
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title_color = book.add_format(ReportExcelCellStyles.format_title_color)

        # тайлангийн обьект
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=report_name, form_title=file_name,).create({})
        # Тайлангийн хуудас
        sheet = book.add_worksheet(report_name)
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.39, 0.39, 0.39, 0.39)  # 1cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Arial"&9&P', {'margin': 0.1})
        sheet.hide_gridlines(2)
        sheet.set_landscape()
        rowx = 0

        # Тооцоолох багана
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 10)
        sheet.set_column('B:B', 10)
        sheet.set_column('C:C', 10)
        sheet.set_column('D:D', 27)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 10)
        sheet.set_column('G:G', 15)
        sheet.set_column('H:H', 15)
        sheet.set_column('I:I', 15)
        sheet.set_column('J:J', 20)
        sheet.set_column('K:K', 20)
        sheet.set_column('L:L', 12)
        sheet.set_column('M:M', 10)
        sheet.set_column('N:N', 10)
        sheet.set_column('O:O', 10)
        sheet.set_row(5, 30)
        sheet.set_row(6, 30)

        sheet.write(rowx, 0, _('Company name: %s') %
                    self.company_id.name, format_filter)
        rowx += 1
        report_name = _("Stock resource report")
        rowx += 1
        sheet.write(4, 0, _('Date: %s') % time.strftime('%Y-%m-%d'), format_filter)
        rowx += 3

        # тайлангийн(хүснэгт) толгой хэсэг
        colx = 0
        coly = 0
        sheet.merge_range(rowx, colx + 0, rowx + 1, coly, '№', format_title_color)
        sheet.merge_range(rowx, colx + 1, rowx + 1, coly + 1, _('Brand name'), format_title_color)
        sheet.merge_range(rowx, colx + 2, rowx + 1, coly + 2, _('Categories'), format_title_color)
        sheet.merge_range(rowx, colx + 3, rowx + 1, coly + 3, _('Internal code'), format_title_color)
        sheet.merge_range(rowx, colx + 4, rowx + 1, coly + 4, _('Product name'), format_title_color)
        sheet.merge_range(rowx, colx + 5, rowx + 1, coly + 5, _('Quantity'), format_title_color)
        sheet.merge_range(rowx, colx + 6, rowx + 1, coly + 6, _('Total'), format_title_color)
        sheet.merge_range(rowx, colx + 7, rowx + 1, coly + 7, _('Cost /Average cost/'), format_title_color)
        if self.show_residual:
            sheet.merge_range(rowx, colx + 8, rowx + 1, coly + 8, _('Зарагдах боломжтой тоо хэмжээ'), format_title_color)
        else:
            colx -= 1
            coly -= 1
        if self.show_reserved:
            sheet.merge_range(rowx, colx + 9, rowx + 1, coly + 9, _('Нөөцөлсөн тоо хэмжээ'), format_title_color)
        else:
            colx -= 1
            coly -= 1
        sheet.merge_range(rowx, colx + 10, rowx + 1, coly + 10, _('Total Number'), format_title_color)
        warehouse_and_location_usage_level = int(self.env.user.has_group('stock.group_stock_multi_locations')) + int(self.env.user.has_group('stock.group_stock_multi_warehouses'))
        if self.warehouse_ids:
            warehouse_ids = self.warehouse_ids
        else:
            warehouse_ids = self.env.user.allowed_warehouses
        location_ids = []
        if warehouse_and_location_usage_level < 2:
            for warehouse in warehouse_ids:
                sheet.merge_range(rowx, colx + 11, rowx + 1, coly + 11, warehouse.name, format_title_color)
                colx += 1
                coly += 1
                location_ids.append(warehouse.lot_stock_id.id)
        else:
            for warehouse in warehouse_ids:
                count_location = 0
                for location in StockLocation.search([]).filtered(lambda l: l.get_warehouse() == warehouse and l.usage == 'internal'):
                    sheet.write(rowx + 1, coly + 11, location.name, format_title_color)
                    coly += 1
                    count_location += 1
                    location_ids.append(location.id)
                if count_location == 1:
                    sheet.write(rowx, coly + 10, warehouse.name, format_title_color)
                else:
                    sheet.merge_range(rowx, coly - count_location + 11, rowx, coly + 11 - 1, warehouse.name, format_title_color)
        product_ids = []
        if self.product_ids:
            for product in self.product_ids:
                product_ids.append(product.id)
        sheet.merge_range(2, 0, 2, 8 + (1 if self.show_residual else 0) + (1 if self.show_reserved else 0) + len(location_ids), report_name, format_name)
        rowx += 2
        sequence = 1
        # Getting lines
        lines = self.get_lines(location_ids, product_ids, self.date)
        total_list_price = 0
        total_cost = 0
        total_quantity = 0
        total_residual = 0
        total_reserved = 0
        total_location_quantities = {}
        for line in lines:
            colx = 0
            sheet.write(rowx, colx + 0, sequence, format_content_center_border)
            sheet.write(rowx, colx + 1, line.get('brand'), format_content_center_border)
            sheet.write(rowx, colx + 2, line.get('categ_name'), format_content_center_border)
            sheet.write(rowx, colx + 3, line.get('default_code'), format_content_center_border)
            sheet.write(rowx, colx + 4, line.get('product_name'), format_content_left_color)
            sheet.write(rowx, colx + 5, line.get('uom_name'), format_content_center_border)
            sheet.write(rowx, colx + 6, line.get('list_price'), format_content_float_border)
            total_list_price += (line.get('list_price') or 0)
            sheet.write(rowx, colx + 7, self._get_unit_cost(line['product_id']) * line.get('quantity'), format_content_float_border)
            total_cost += self._get_unit_cost(line['product_id']) * line.get('quantity')
            if self.show_residual:
                sheet.write(rowx, colx + 8, line.get('quantity') - self._get_reserved_moves(line['product_id'], location_ids), format_content_float_border)
                total_residual += line.get('quantity') - self._get_reserved_moves(line['product_id'], location_ids)
            else:
                colx -= 1
            if self.show_reserved:
                sheet.write(rowx, colx + 9, self._get_reserved_moves(line['product_id'], location_ids), format_content_float_border)
                total_reserved += self._get_reserved_moves(line['product_id'], location_ids)
            else:
                colx -= 1
            sheet.write(rowx, colx + 10, line.get('quantity'), format_content_float_border)
            total_quantity += line.get('quantity')
            for location in location_ids:
                loc_line = self.get_lines([location], [], self.date, line.get('product_id'))
                sheet.write(rowx, colx + 11, loc_line[0].get('quantity'), format_content_float_border)
                if location in total_location_quantities:
                    total_location_quantities[location] += loc_line[0].get('quantity')
                else:
                    total_location_quantities[location] = loc_line[0].get('quantity')
                colx += 1
            rowx += 1
            sequence += 1
        colx = 6
        sheet.merge_range(rowx, 0, rowx, 5, _('TOTAL'), format_title_color)
        sheet.write(rowx, colx, total_list_price, format_title_float_border)
        colx += 1
        sheet.write(rowx, colx, total_cost, format_title_float_border)
        colx += 1
        if self.show_residual:
            sheet.write(rowx, colx, total_residual, format_title_float_border)
            colx += 1
        if self.show_reserved:
            sheet.write(rowx, colx, total_reserved, format_title_float_border)
            colx += 1
        sheet.write(rowx, colx, total_quantity, format_title_float_border)
        colx += 1
        for location in location_ids:
            sheet.write(rowx, colx, total_location_quantities[location], format_title_float_border)
            colx += 1

        rowx += 4
        sheet.write(rowx, 4, _('Product keeper:'), format_filter_right)
        sheet.write(rowx, 5, _('...................................................../'), format_filter)
        rowx += 1
        sheet.write(rowx, 4, _('Reviewed accountant:'), format_filter_right)
        sheet.write(rowx, 5, _('...................................................../'), format_filter)
        book.close()
        # Файлын өгөгдлийг тохируулах
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # Экспортын функц дуудаж байна
        return report_excel_output_obj.export_report()
