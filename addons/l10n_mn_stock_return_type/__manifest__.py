# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Stock Return Type",
    'version': '1.0',
    'depends': ['l10n_mn_stock'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Modules',
    'description': """
    Description text
    """,
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'wizard/stock_return_picking_view.xml',
        'views/stock_return_type_view.xml',
        'views/stock_picking_view.xml', 
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
