# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class StockReturnType(models.Model):
    _name = 'stock.return.type'
    _inherit = ['mail.thread']
    
    name = fields.Char(string='Name')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
