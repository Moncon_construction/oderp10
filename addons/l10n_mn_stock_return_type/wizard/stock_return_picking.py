# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class StockReturnPicking(models.TransientModel):
    _inherit = 'stock.return.picking'
    
    
    return_type_id = fields.Many2one('stock.return.type', string='Return Type')

    @api.multi
    def create_returns(self):
        res = super(StockReturnPicking, self).create_returns()
        if res['res_id']:
            new_picking_id = res['res_id']
            picking = self.env['stock.picking'].browse(new_picking_id)
            if picking:
                picking.write({'return_type_id':self.return_type_id.id})
            return res