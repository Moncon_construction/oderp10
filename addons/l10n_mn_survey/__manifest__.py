# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Survey Module Customizations",
    'version': '1.0',
    'depends': ['survey'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Survey Additional Features
    """,
    'data': [
        'security/ir.model.access.csv',
        'views/survey_views.xml'
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
