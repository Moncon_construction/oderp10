# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2018 Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.exceptions import UserError, ValidationError

class SurveyType(models.Model):
    _name = 'survey.type'
    _description = 'Survey Types'
    _rec_name = 'name'

    name = fields.Char('Name', required=True, translate=True)
    parent_type_id = fields.Many2one('survey.type', "Parent Type", domain=[('parent_type_id','=',False)])
    type = fields.Selection([
        ('lead_survey', 'Lead Survey'),
        ('employee_survey', 'Employee Survey'),
        ('other', 'Other')],
        default='other', string='Type')

class Survey(models.Model):
    _inherit = 'survey.survey'

    type = fields.Many2one('survey.type', "Survey Type")
    
class SurveyUserInput(models.Model):
    _inherit = "survey.user_input"
    
    user_id = fields.Many2one('res.users', "User", default=lambda self: self.env.uid)
