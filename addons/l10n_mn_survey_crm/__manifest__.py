# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Survey+CRM Module Customizations",
    'version': '1.0',
    'depends': ['l10n_mn_survey'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
        Survey+CRM Additional Features
    """,
    'data': [
        'views/survey_views.xml',
        'wizard/open_survey_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}