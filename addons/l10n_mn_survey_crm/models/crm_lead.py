# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-Today Asterisk Technologies Co.,ltd (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
##############################################################################

from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.exceptions import UserError, ValidationError
from odoo.addons.base.res.res_partner import FormatAddress

class Lead(FormatAddress, models.Model):

    _inherit = "crm.lead"

    @api.multi
    def _compute_survey_count(self):
        survey_data = self.env['survey.user_input'].read_group([('lead_id', 'in', self.ids)], ['lead_id'], ['lead_id'])
        mapped_data = {m['lead_id'][0]: m['lead_id_count'] for m in survey_data}
        for lead in self:
            lead.survey_input_count = mapped_data.get(lead.id, 0)
    
    survey_input_count = fields.Integer('# Surveys', compute='_compute_survey_count')
    
    @api.multi
    def action_survey_input(self):
        res = []
        for line in self:
            res = self.env['survey.user_input'].search([('lead_id', '=', line.id)])
            return {
                'type': 'ir.actions.act_window',
                'name': _('Survey'),
                'res_model': 'survey.user_input',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'view_id': False,
                'domain': [('id', 'in', res.ids)],
            }
    
    @api.multi
    def open_survey(self):
        for lead in self:
            view = self.env.ref('l10n_mn_survey_crm.view_open_crm_survey')
            return {
                'name': _('Open Survey'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'open.crm.survey',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'context': self.env.context,
            }
            
        return True