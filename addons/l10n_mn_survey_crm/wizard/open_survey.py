# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import re
import uuid
import urlparse
from urlparse import urljoin

from odoo.addons.website.models.website import slug
from odoo import api, fields, models, _  # @UnresolvedImport
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)

class OpenCRMSurvey(models.TransientModel):
    _name = 'open.crm.survey'
    _description = 'Open CRM Survey'

    survey_id = fields.Many2one('survey.survey', 'Survey', domain=[('type.type','=','lead_survey')], required=True)
    user_id = fields.Many2one('res.users', 'User')
    partner_id = fields.Many2one('res.partner', 'Partner')
    lead_id = fields.Many2one('crm.lead', 'Lead')

    @api.multi
    def start(self):
        #Start survey
        for obj in self:
            print 'active_id: ', self._context['active_id']
            ctx = dict(self.env.context)
            ctx['lead_id'] = self._context['active_id']
            ctx['survey_id'] = obj.survey_id.id
            if obj.partner_id:
                ctx['partner_id'] = obj.partner_id.id
            if obj.user_id:
                ctx['user_id'] = obj.user_id.id
            base_url = '/' if self.env.context.get('relative_url') else self.env['ir.config_parameter'].get_param('web.base.url')
            crm_lead = self.env['crm.lead'].browse(self._context['active_id'])
            if crm_lead:
                obj.public_url = urljoin(base_url, "survey/start/%s/%s" % (slug(obj.survey_id), slug(crm_lead)))
            return {
                'type': 'ir.actions.act_url',
                'name': "Start Survey",
                'target': 'self',
                'url': obj.public_url
            }

        return True
