# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Mongolian Swap Order",
    'version': '1.0',
    'depends': ['l10n_mn_sale', 'l10n_mn_stock'],
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """
   Product Additional Features
    """,
    'demo': [
        'data/swap_order_sequence_data.xml',
    ],
    'data': [
        'views/invoice_view.xml',
        'views/swap_order_view.xml',
        'security/ir.model.access.csv',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
