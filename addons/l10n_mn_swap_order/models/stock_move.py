# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class StockMove(models.Model):
    _inherit = 'stock.move'
    
    swap_order_line_out = fields.Many2one('swap.order.line', 'Swap order line out')
    swap_order_line_in = fields.Many2one('swap.order.line', 'Swap order line in')
    swap_order_line_id = fields.Many2one('swap.order.line', 'Swap order line')