# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    swap_order_id = fields.Many2one('swap.order', 'Swap order')

    @api.multi
    def do_new_transfer(self):
        # Агуулахын хөдөлгөөн дээр байх батлах товч дарахад холбоотой бараа солилцооны захиалга
        # дээр өөрчлөлт орж холбогдох нэхэмжлэлүүд үүснэ
        swap_order_in = []
        swap_order_out = []
        swap_order_ids = self.env['swap.order'].search([('id', '=', self.swap_order_id.id)])
        account_invoice = self.env['account.invoice']
        account_invoice_line = self.env['account.invoice.line']
        account_id = self.env['account.account'].search(
            [('company_id', '=', self.company_id.id), ('internal_type', '=', 'receivable'), ('deprecated', '=', False)],
            limit=1)
        account_id2 = self.env['account.account'].search(
            [('company_id', '=', self.company_id.id), ('internal_type', '=', 'income'), ('deprecated', '=', False)],
            limit=1)
        product = False
        for move_id in self.move_lines:
            if not product:
                product = move_id.product_id
            if move_id.swap_order_line_in:
                swap_order_in.append(self.env['swap.order.line'].search([('id', '=', move_id.swap_order_line_in.id)]))
            elif move_id.swap_order_line_out:
                swap_order_out.append(self.env['swap.order.line'].search([('id', '=', move_id.swap_order_line_out.id)]))
        for order in swap_order_ids:
            if order.state == 'confirmed':
                order.write({'picking_ids': [(6, 0, self.ids)],
                             'state': 'delivered',
                             'validator': self.env.user.id,
                             'minimum_planned_date_out': self.min_date,
                             'delivered': True})
                journal_id = self.get_account_journal(order.warehouse_id, product)
                invoice_id = account_invoice.create({'partner_id': order.partner_id.id,
                                                     'date_invoice': order.date_order,
                                                     'type': 'out_invoice',
                                                     'journal_id': journal_id,
                                                     'origin': order.id})
                for a in swap_order_in:
                    account_invoice_line.create({'invoice_id': invoice_id.id,
                                                 'product_id': a.product_id.id,
                                                 'quantity': a.product_qty,
                                                 'price_unit': a.list_price,
                                                 'account_id': account_id.id,
                                                 'name': '/'
                                                 })
            elif order.state == 'converted':
                journal_id = self.get_account_journal(order.warehouse_id, product)
                order.write({'picking_ids': [(6, 0, order.picking_ids.ids + self.ids)],
                             'state': 'done',
                             'minimum_planned_date_in': self.min_date,
                             'received': True})
                invoice_id2 = account_invoice.create({'partner_id': order.partner_id.id,
                                                      'date_invoice': order.date_order,
                                                      'type': 'in_invoice',
                                                      'journal_id': journal_id,
                                                      'origin': order.id})
                for a in swap_order_out:
                    account_invoice_line.create({'invoice_id': invoice_id2.id,
                                                 'product_id': a.product_id.id,
                                                 'quantity': a.product_qty,
                                                 'price_unit': a.list_price,
                                                 'account_id': account_id2.id,
                                                 'name': '/'})

            elif order.state == 'done':
                order.write({'picking_ids': [(6, 0, order.picking_ids.ids + self.ids)],
                             'state': 'done'})
        return super(StockPicking, self).do_new_transfer()

    def get_account_journal(self, warehouse, product):
        # Компани дундаа эсвэл агуулах бүрээс хамаарч журнал сонгоно
        is_cost_for_each_wh = self.sudo().env['ir.module.module'].search(
            [('name', '=', 'l10n_mn_stock_account_cost_for_each_wh')], limit=1)
        if is_cost_for_each_wh.state == 'installed':
            if not warehouse.stock_journal:
                raise UserError(_('You must set journal on the warehouse %s!') % warehouse.name)
            journal_id = warehouse.stock_journal.id
        else:
            if not product.categ_id.property_stock_journal:
                raise UserError(_('You must set journal on the product category %s!') % product.categ_id.name)
            journal_id = product.categ_id.property_stock_journal.id
        return journal_id
