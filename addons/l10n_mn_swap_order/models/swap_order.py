# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import pytz


class SwapOrder(models.Model):
    _name = 'swap.order'
    _inherit = ['mail.thread']

    @api.multi
    def _default_get_picking_type(self, code):
        # Default-оор агуулахын хөдөлгөөний төрлийг авах.
        type_obj = self.env['stock.picking.type']
        types = type_obj.search([('code', '=', code), ('warehouse_id.company_id', '=', self.env.user.company_id.id)])
        if not types:
            types = type_obj.search([('code', '=', code), ('warehouse_id', '=', False)])
            if not types:
                raise UserError(_("Make sure you have at least an incoming and outgoing picking type defined for product swap case"))
        return types[0]

    currency_id = fields.Many2one('res.currency', string='Currency',
                                  help="The optional other currency if it is a multi-currency entry.")
    name = fields.Char("Name", readonly=True, )
    picking_out_type_id = fields.Many2one('stock.picking.type', 'Picking out type', required=True, default=lambda self: self._default_get_picking_type('outgoing'))
    picking_in_type_id = fields.Many2one('stock.picking.type', 'Picking in type', required=True, default=lambda self: self._default_get_picking_type('incoming'))
    partner_id = fields.Many2one('res.partner', 'Partner', required=True)
    mode = fields.Selection([('only_cost', 'Only Cost')], default='only_cost', string='Mode', copy=False, required=True)
    date_order = fields.Date('Date order', required=True, default=lambda self: fields.datetime.now())
    origin = fields.Char('Origin')
    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse', required=True)
    order_line_out = fields.One2many('swap.order.line', 'order_out_id', 'Order Line Out')
    order_line_in = fields.One2many('swap.order.line', 'order_in_id', 'Order Line In')
    validator = fields.Many2one('res.users', 'Validator')
    date_approve = fields.Date('Date Approve')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)
    minimum_planned_date_out = fields.Date('Minimum planned date out')
    delivered = fields.Boolean('Delivered')
    minimum_planned_date_in = fields.Date('Minimum planned date in')
    received = fields.Boolean('Received')
    picking_ids = fields.One2many('stock.picking', 'swap_order_id', 'Pickings')
    notes = fields.Text('Note')
    price_out_total = fields.Monetary('Price out total', compute='_compute_total_money')
    price_in_total = fields.Monetary('Price in total', compute='_compute_total_money')
    price_diff = fields.Monetary('Price diff', compute='_compute_total_money')
    price_in_subtotal = fields.Monetary('Price in subtotal', compute='_compute_total_money')
    amount_total = fields.Monetary('Amount total', compute='_compute_total_money')
    sequence_id = fields.Many2one('ir.sequence', 'Reference Sequence', )
    state = fields.Selection([
        ('draft', 'Swap request'),
        ('confirmed', 'Confirmed'),
        ('delivered', 'Delivered'),
        ('converted', 'Converted'),
        ('done', 'Done'),
        ('cancel', 'Cancel'), ], string='States',
        copy=False, default='draft', required=True)

    @api.multi
    def unlink(self):
        if any(plan.state != 'draft' for plan in self):
            raise UserError(_('Cannot delete a swap order not in draft state'))
        return super(SwapOrder, self).unlink()

    @api.model
    def create(self, vals):
        sequence_id = self.env['ir.sequence'].get('swap.order')
        if sequence_id:
            vals['name'] = _('Swap order') + ' SWP.' + str(datetime.now().year) + str(sequence_id)
        else:
            vals['name'] = _('Swap order') + ' SWP.' + str(datetime.now().year)
        return super(SwapOrder, self).create(vals)

    @api.multi
    def confirm_order(self):
        # Батлах товч дарахад агуулахын хөдөлгөөн үүсгэн төлөвийг өөрчлөх
        self.ensure_one()
        if not self.order_line_in:
            raise UserError(_('You can not confirm swap order without Incoming Lines.'))
        if not self.order_line_out:
            raise UserError(_('You can not confirm swap order without Outgoing Lines.'))
        if not self.warehouse_id.out_type_id.default_location_dest_id:
            raise UserError(_('You must set out type inside basic delivery location on the warehouse %s!') % self.warehouse_id.name)
        locations = self.env['stock.location'].search([('usage', '=', 'internal'), ('location_id', 'child_of', [self.warehouse_id.view_location_id.id])])
        picking_id = self.env['stock.picking'].create({'partner_id': self.partner_id.id,
                                                       'min_date': self.minimum_planned_date_out,
                                                       'origin': self.name,
                                                       'location_id': self.warehouse_id.out_type_id.default_location_src_id.id,
                                                       'picking_type_id': self.warehouse_id.out_type_id.id,
                                                       'location_dest_id': self.warehouse_id.out_type_id.default_location_dest_id.id,
                                                       'swap_order_id': self.id,
                                                       'company_id': self.company_id.id})
        for line_out in self.order_line_out:
            self.env['stock.move'].create({'picking_id': picking_id.id,
                                           'product_id': line_out.product_id.id,
                                           'product_uom_qty': line_out.product_qty,
                                           'picking_type_id': self.warehouse_id.out_type_id.id,
                                           'location_id': self.warehouse_id.out_type_id.default_location_src_id.id,
                                           'location_dest_id': self.warehouse_id.out_type_id.default_location_dest_id.id,
                                           'product_uom': line_out.product_uom.id,
                                           'name': line_out.product_id.name,
                                           'swap_order_line_in': line_out.id})
        self.write({'state': 'confirmed'})

    @api.multi
    def converted_order(self):
        if not self.warehouse_id.in_type_id.default_location_dest_id:
            raise UserError(_('You must set in type inside basic delivery location on the warehouse %s!') % self.warehouse_id.name)
        picking_id = self.env['stock.picking'].create({'partner_id': self.partner_id.id,
                                                       'min_date': self.minimum_planned_date_in,
                                                       'origin': self.name,
                                                       'location_id': self.warehouse_id.in_type_id.default_location_src_id.id,
                                                       'picking_type_id': self.warehouse_id.in_type_id.id,
                                                       'location_dest_id': self.warehouse_id.in_type_id.default_location_dest_id.id,
                                                       'swap_order_id': self.id,
                                                       'company_id': self.company_id.id})
        for line_in in self.order_line_in:
            self.env['stock.move'].create({'picking_id': picking_id.id,
                                           'product_id': line_in.product_id.id,
                                           'product_uom_qty': line_in.product_qty,
                                           'picking_type_id': self.warehouse_id.in_type_id.id,
                                           'location_id': self.warehouse_id.in_type_id.default_location_src_id.id,
                                           'location_dest_id': self.warehouse_id.in_type_id.default_location_dest_id.id,
                                           'product_uom': line_in.product_uom.id,
                                           'name': line_in.product_id.name,
                                           'swap_order_line_out': line_in.id})
        self.write({'state': 'converted'})
        return True

    def action_cancel_draft(self):
        if not len(self.ids):
            return False
        self.write({'state': 'draft', 'received': False, 'delivered': False})
        return True

    @api.depends('order_line_out', 'order_line_in')
    def _compute_total_money(self):
        for a in self:
            for out_total in a.order_line_out:
                a.price_out_total += out_total.total_line
            for in_total in a.order_line_in:
                a.price_in_total += in_total.total_line
                a.price_in_subtotal += in_total.total_price
            a.price_diff = a.price_out_total - a.price_in_total


class SwapOrderLine(models.Model):
    _name = 'swap.order.line'

    @api.depends('product_id')
    def _compute_available_qty(self):
        move_qty = 0
        qty = 0
        for obj in self:
            warehouse = obj.order_out_id.warehouse_id
            if not warehouse:
                raise UserError(_('Warning!\nYou must select supply warehouse before add order line!'))
            if obj.product_id:
                product = obj.product_id
                locations = self.env['stock.location'].search([('usage', '=', 'internal'), ('location_id', 'child_of', [warehouse.view_location_id.id])])
                loc_ids = [loc.id for loc in locations]
                date = datetime.combine(datetime.strptime(obj.order_out_id.date_order, '%Y-%m-%d').date(), datetime.strptime('23:59:59', '%H:%M:%S').time())
                user = self.env['res.users'].browse(self._uid)
                user_time_zone = pytz.UTC
                if user.partner_id.tz:
                    user_time_zone = pytz.timezone(user.partner_id.tz)
                date_start = user_time_zone.localize(date).astimezone(pytz.utc).strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                # Тухайн барааны хувьд тухайн байрлал дээр хэдий хэмжээгээр нөөцлөгдсөн байгааг авч байна..
                if loc_ids:
                    self._cr.execute("SELECT sum(product_qty)::decimal(16,2) AS product_qty from stock_move "
                                     "where location_id in %s and product_id = %s and state = 'assigned'", (tuple(loc_ids), product.id))
                    moves = self._cr.fetchall()
                    move_qty = sum([qty[0] if qty[0] is not None else 0 for qty in moves if moves is not list])
                    self._cr.execute("SELECT sum(quantity)::decimal(16,2) AS product_qty from stock_history "
                                     "where date <= %s "
                                     "and location_id in %s and product_id = %s ", (date_start, tuple(loc_ids), product.id))
                    fetched = self._cr.fetchall()
                    qty = sum([qty[0] if qty[0] is not None else 0 for qty in fetched if fetched is not list])
                    obj.available_qty = qty - move_qty

    name = fields.Char("Name")
    order_in_id = fields.Many2one('swap.order', 'Order in')
    order_out_id = fields.Many2one('swap.order', 'Order oun')
    product_id = fields.Many2one('product.product', 'Product', required=True)
    product_qty = fields.Float('Product Quantity', required=True, default="1")
    product_uom = fields.Many2one('product.uom', 'Product Uom', required=True)
    date_planned = fields.Date('Date Planned', required=True)
    company_id = fields.Many2one('res.company', 'Company')
    cost_price_display = fields.Float('Cost price display', compute='_compute_money')
    cost_price = fields.Float('Cost price')
    list_price_display = fields.Float('List price display', compute='_compute_money')
    list_price = fields.Float('List price')
    total_price_display = fields.Float('Total price display', compute='_compute_money')
    total_price = fields.Float('Total price')
    total_line = fields.Float('Total line')
    total_line_display = fields.Float('Total line display', compute='_compute_money')
    name = fields.Char('Description', required=True)
    notes = fields.Text('Note')
    available_qty = fields.Float(compute='_compute_available_qty', string='Available quantity', readonly=True)
    move_ids = fields.One2many('stock.move', 'swap_order_line_id', 'Moves')
    state = fields.Selection([
        ('draft', 'Swap request'),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'), ], string='States',
        copy=False, default='draft', required=True)

    @api.onchange('product_id')
    def onchange_product_id(self):
        for order in self:
            order.list_price = order.product_id.list_price

    @api.onchange('list_price')
    def onchange_list_price(self):
        for order in self:
            order.total_line = order.product_qty * order.list_price
            order.total_line_display = order.product_qty * order.list_price
            order.total_price = order.product_qty * order.cost_price
            order.total_price_display = order.product_qty * order.cost_price

    @api.depends('product_id', 'product_qty')
    def _compute_money(self):
        for order in self:
            if order.product_id:
                if order.product_id.product_tmpl_id:
                    order.product_uom = order.product_id.uom_id.id
                    order.cost_price_display = order.product_id.product_tmpl_id.standard_price
                    order.cost_price = order.product_id.product_tmpl_id.standard_price
                    order.name = order.product_id.product_tmpl_id.name
                    order.total_line = order.product_qty * order.list_price
                    order.total_line_display = order.product_qty * order.list_price
                    order.total_price = order.product_qty * order.cost_price
                    order.total_price_display = order.product_qty * order.cost_price

    @api.onchange('product_qty')
    def onchange_product_qty(self):
        if self.product_qty > 0:
            self.total_price_display = self.product_qty * self.list_price
