# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    "name": "Mongolian Task",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
        Уг модуль нь ажилбарыг бүртгэнэ.
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": ['l10n_mn_hr',
                'l10n_mn_mining',],
    "data": [
        'security/ir.model.access.csv',
        'views/task_type_view.xml',
        'views/task_product_norm_view.xml',
        'views/task_salary_norm_view.xml',
        'views/task_task_view.xml',
        'wizard/mining_motohour_entry_cause_line_wizard.xml',
        'report/report_motohour_view.xml'
    ],
    'license': 'GPL-3',
    "installable": True,
    'auto_install': False
}