# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from odoo import models, fields
from odoo.addons import decimal_precision as dp  # @UnresolvedImport

class AccountAnalyticLine(models.Model):
    _inherit = 'account.analytic.line'

    perf_qty = fields.Float(string='Performance Qty', digits=dp.get_precision('Product Unit of Measure'), default=0)
    uom_id = fields.Many2one('product.uom', string='Unit of Measure')
    task_task_id = fields.Many2one('task.task', string='Task', ondelete='cascade', index=True, copy=False)