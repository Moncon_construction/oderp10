# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-today Asterisk Technologies LLC (<http://www.asterisk-tech.mn>)

#
##############################################################################

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo.exceptions import UserError
from odoo.exceptions import ValidationError, UserError

class mining_motohour_entry_cause_line(models.Model):
    _inherit = 'mining.motohour.entry.cause.line'
    
    task_type_id = fields.Many2one('task.type', string='Task Type')

class MiningDailyEntry(models.Model):
    _inherit = 'mining.daily.entry'

    # Батлах
    @api.multi
    def action_to_approved(self):
        task_object = self.env['task.task']
        task_line_technic_object = self.env['task.task.technic']
        timesheet_object = self.env['account.analytic.line']
        task_type_dict = {}
        for line in self.production_line_ids:
            user_id = False
            exca_user_id = False
            if line.dump_driver_id:
                if line.dump_driver_id.user_id:
                    user_id = line.dump_driver_id.user_id.id
                else:
                    raise UserError(_('%s ажилтанд холбоотой хэрэглэгч алга байна. Хэрэлгэч үүсгэнэ үү'%(line.dump_driver_id.name)))

            if line.excavator_driver_id:
                if line.excavator_driver_id.user_id:
                    exca_user_id = line.excavator_driver_id.user_id.id
                else:
                    raise UserError(_('%s ажилтанд холбоотой хэрэглэгч алга байна. Хэрэлгэч үүсгэнэ үү'%(line.excavator_driver_id.name)))
            if line.task_type_id not in task_type_dict:
                task_task_vals = {
                        'name': str(self.date) + ' ' + str(self.project_id.name) + ' ' + str(line.task_type_id.name),
                        'project_id': self.project_id.id,
                        'task_type_id': line.task_type_id.id,
                        'date_start': self.date,
                        'date_deadline': self.date,
                        'planned_hours': 1,
                        'mining_id': self.id
                    }
                task_task_id = task_object.create(task_task_vals)
                task_type_dict[line.task_type_id] = {
                        'task_type_id': line.task_type_id.id,
                        'task_task_id': task_task_id.id,
                        'product_uom_id': line.task_type_id.type_uom_id.id,
                        'date': self.date,
                        'tech_lines': {
                            line.dump_id.id: {
                                'technic_id': line.dump_id.id,
                                'employee_id': line.dump_driver_id.id if line.dump_driver_id else False,
                                'norm_uom_id': line.task_type_id.type_uom_id.id,
                                'perf_qty': line.sum_tn if not line.is_solo_technic else line.job_qty_tn,
                                'perf_hours': line.dump_driver_workhour,
                                'perf_km': line.land_avg_km * line.res_count,
                                'total_freight_tn_km': line.total_freight_tn_km
                                },
                            line.excavator_id.id: {
                                'technic_id': line.excavator_id.id,
                                'employee_id': line.excavator_driver_id.id if line.excavator_driver_id else False,
                                'norm_uom_id': line.task_type_id.type_uom_id.id,
                                'perf_qty': line.sum_m3 if not line.is_solo_technic else line.job_qty_m3,
                                'perf_hours': line.excavator_driver_workhour,
                                'perf_km': 0.0,
                                'total_freight_tn_km': 0.0
                            }
                        },
                        'timesheet_lines': {
                            user_id if user_id else 'null': {
                                'name': line.task_type_id.name,
                                'unit_amount': line.dump_driver_workhour,
                                'amount': 1,
                                'perf_qty': line.sum_tn if not line.is_solo_technic else line.job_qty_tn,
                                'account_id': self.project_id.analytic_account_id.id,
                                'company_id': self.env.user.company_id.id,
                                },
                            exca_user_id if exca_user_id else 'null': {
                                'name': line.task_type_id.name,
                                'unit_amount': line.excavator_driver_workhour,
                                'amount': 1,
                                'perf_qty': line.sum_m3 if not line.is_solo_technic else line.job_qty_m3,
                                'account_id': self.project_id.analytic_account_id.id,
                                'company_id': self.env.user.company_id.id,
                            }
                        }
                    }
            else:
                if line.dump_id.id not in task_type_dict[line.task_type_id]['tech_lines']:
                    task_type_dict[line.task_type_id]['tech_lines'][line.dump_id.id] = {
                                'technic_id': line.dump_id.id,
                                'employee_id': line.dump_driver_id.id if line.dump_driver_id else False,
                                'norm_uom_id': line.task_type_id.type_uom_id.id,
                                'perf_qty': line.sum_tn if not line.is_solo_technic else line.job_qty_tn,
                                'perf_hours': line.dump_driver_workhour,
                                'perf_km': line.land_avg_km * line.res_count,
                                'total_freight_tn_km': line.total_freight_tn_km
                                }
                else:
                    task_type_dict[line.task_type_id]['tech_lines'][line.dump_id.id]['perf_qty'] += line.sum_tn if not line.is_solo_technic else line.job_qty_tn
                    task_type_dict[line.task_type_id]['tech_lines'][line.dump_id.id]['perf_hours'] += line.dump_driver_workhour
                    task_type_dict[line.task_type_id]['tech_lines'][line.dump_id.id]['perf_km'] += line.land_avg_km * line.res_count
                    task_type_dict[line.task_type_id]['tech_lines'][line.dump_id.id]['total_freight_tn_km'] += line.total_freight_tn_km

                if line.excavator_id.id not in task_type_dict[line.task_type_id]['tech_lines']:
                    task_type_dict[line.task_type_id]['tech_lines'][line.excavator_id.id] = {
                                'technic_id': line.excavator_id.id,
                                'employee_id': line.excavator_driver_id.id if line.excavator_driver_id else False,
                                'norm_uom_id': line.task_type_id.type_uom_id.id,
                                'perf_qty': line.sum_m3 if not line.is_solo_technic else line.job_qty_m3,
                                'perf_hours': line.excavator_driver_workhour,
                                'perf_km': 0.0,
                                'total_freight_tn_km': 0.0
                            }
                else:
                    task_type_dict[line.task_type_id]['tech_lines'][line.excavator_id.id]['perf_qty'] += line.sum_m3 if not line.is_solo_technic else line.job_qty_m3
                    task_type_dict[line.task_type_id]['tech_lines'][line.excavator_id.id]['perf_hours'] += line.excavator_driver_workhour

                if user_id:
                    if user_id not in task_type_dict[line.task_type_id]['timesheet_lines']:
                        task_type_dict[line.task_type_id]['timesheet_lines'][user_id] = {
                                'name': line.task_type_id.name,
                                'unit_amount': line.dump_driver_workhour,
                                'amount': 1,
                                'perf_qty': line.sum_tn if not line.is_solo_technic else line.job_qty_tn,
                                'account_id': self.project_id.analytic_account_id.id,
                                'company_id': self.env.user.company_id.id,
                                }
                    else:
                        task_type_dict[line.task_type_id]['timesheet_lines'][user_id]['unit_amount'] += line.dump_driver_workhour
                        task_type_dict[line.task_type_id]['timesheet_lines'][user_id]['perf_qty'] += line.sum_tn if not line.is_solo_technic else line.job_qty_tn
                else:
                    if 'null' not in task_type_dict[line.task_type_id]['timesheet_lines']:
                        task_type_dict[line.task_type_id]['timesheet_lines']['null'] = {
                                'name': line.task_type_id.name,
                                'unit_amount': line.dump_driver_workhour,
                                'amount': 1,
                                'perf_qty': line.sum_tn if not line.is_solo_technic else line.job_qty_tn,
                                'account_id': self.project_id.analytic_account_id.id,
                                'company_id': self.env.user.company_id.id,
                                }
                    else:
                        task_type_dict[line.task_type_id]['timesheet_lines']['null']['unit_amount'] += line.dump_driver_workhour
                        task_type_dict[line.task_type_id]['timesheet_lines']['null']['perf_qty'] += line.sum_tn if not line.is_solo_technic else line.job_qty_tn
                if exca_user_id:
                    if exca_user_id not in task_type_dict[line.task_type_id]['timesheet_lines']:
                        task_type_dict[line.task_type_id]['timesheet_lines'][exca_user_id] = {
                                'name': line.task_type_id.name,
                                'unit_amount': line.excavator_driver_workhour,
                                'amount': 1,
                                'perf_qty': line.sum_m3 if not line.is_solo_technic else line.job_qty_m3,
                                'account_id': self.project_id.analytic_account_id.id,
                                'company_id': self.env.user.company_id.id,
                                }
                    else:
                        task_type_dict[line.task_type_id]['timesheet_lines'][exca_user_id]['unit_amount'] += line.excavator_driver_workhour
                        task_type_dict[line.task_type_id]['timesheet_lines'][exca_user_id]['perf_qty'] += line.sum_m3 if not line.is_solo_technic else line.job_qty_m3
                else:
                    if 'null' not in task_type_dict[line.task_type_id]['timesheet_lines']:
                        task_type_dict[line.task_type_id]['timesheet_lines']['null'] = {
                                'name': line.task_type_id.name,
                                'unit_amount': line.excavator_driver_workhour,
                                'amount': 1,
                                'perf_qty': line.sum_m3 if not line.is_solo_technic else line.job_qty_m3,
                                'account_id': self.project_id.analytic_account_id.id,
                                'company_id': self.env.user.company_id.id,
                                }
                    else:
                        task_type_dict[line.task_type_id]['timesheet_lines']['null']['unit_amount'] += line.excavator_driver_workhour
                        task_type_dict[line.task_type_id]['timesheet_lines']['null']['perf_qty'] += line.sum_m3 if not line.is_solo_technic else line.job_qty_m3

        for obj in task_type_dict:
            for tech_line in task_type_dict[obj]['tech_lines']:
                if task_type_dict[obj]['tech_lines'][tech_line]['technic_id']:
                    task_tech_vals = {
                        'task_id': task_type_dict[obj]['task_task_id'],
                        'date': task_type_dict[obj]['date'],
                        'technic_id': task_type_dict[obj]['tech_lines'][tech_line]['technic_id'],
                        'employee_id': task_type_dict[obj]['tech_lines'][tech_line]['employee_id'],
                        'task_type_id': task_type_dict[obj]['task_type_id'],
                        'norm_uom_id': task_type_dict[obj]['tech_lines'][tech_line]['norm_uom_id'],
                        'perf_qty': task_type_dict[obj]['tech_lines'][tech_line]['perf_qty'],
                        'perf_km': task_type_dict[obj]['tech_lines'][tech_line]['perf_km'],
                        'perf_hours': task_type_dict[obj]['tech_lines'][tech_line]['perf_hours'],
                        'total_freight_tn_km': task_type_dict[obj]['tech_lines'][tech_line]['total_freight_tn_km'],
                    }
                    task_line_technic_object.create(task_tech_vals)
            for sheet_line in task_type_dict[obj]['timesheet_lines']:
                if sheet_line != 'null':
                    sheet_line_vals = {
                        'task_task_id': task_type_dict[obj]['task_task_id'],
                        'date': task_type_dict[obj]['date'],
                        'user_id': sheet_line,
                        'uom_id': task_type_dict[obj]['product_uom_id'],
                        'name': task_type_dict[obj]['timesheet_lines'][sheet_line]['name'],
                        'unit_amount': task_type_dict[obj]['timesheet_lines'][sheet_line]['unit_amount'],
                        'amount': 1,
                        'perf_qty': task_type_dict[obj]['timesheet_lines'][sheet_line]['perf_qty'],
                        'account_id': self.env['task.task'].browse(task_type_dict[obj]['task_task_id']).project_id.analytic_account_id.id,
                        'company_id': self.env.user.company_id.id,
                    }
                    timesheet_object.create(sheet_line_vals)
        res = super(MiningDailyEntry, self).action_to_approved()
