# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
import time

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.addons import decimal_precision as dp  # @UnresolvedImport

class TaskProductNorm(models.Model):
    _name = 'task.product.norm'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Task Product Norm'

    name = fields.Char('Name', required=True,track_visibility='onchange', states={'approved': [('readonly', True)]})
    start_date = fields.Date('Start Date', required=True, default=lambda *a: time.strftime('%Y-%m-01'),track_visibility='onchange', states={'approved': [('readonly', True)]})
    end_date = fields.Date('End Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'), track_visibility='onchange', states={'approved': [('readonly', True)]})
    line_ids = fields.One2many('task.product.norm.line', 'product_norm_id', string='Lines', copy=False, states={'approved': [('readonly', True)]})
    user_id = fields.Many2one('res.users', string='Responsible', default=lambda self: self.env.user, readonly=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], string='State', default='draft', required=True, track_visibility='onchange')
    description = fields.Text('Description')

    @api.multi
    def unlink(self):
        for cost in self:
            if cost.state != 'draft':
                raise UserError(_('Delete only draft in state'))
        return super(TaskProductNorm, self).unlink()

    @api.multi
    def validate(self):
        # Батлах
        self.write({'state': 'approved'})

    @api.multi
    def action_to_draft(self):
        # Ноороглох
        self.write({'state': 'draft'})


class TaskProductNormLine(models.Model):
    _name = 'task.product.norm.line'
    _description = 'Task Product Norm Line'

    product_norm_id = fields.Many2one('task.product.norm', string='Product Norm', ondelete='cascade', index=True, copy=False)
    technic_id = fields.Many2one('technic', string='Technic')
    task_type_id = fields.Many2one('task.type', required=True, string='Task Type')
    production_uom = fields.Many2one('product.uom', string='Production UoM', required=True)
    production_qty = fields.Float(string='Production Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    product_id = fields.Many2one('product.product', string='Product', required=True)
    product_uom = fields.Many2one('product.uom', string='Product UoM', required=True)
    product_qty = fields.Float(string='Product Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)

    @api.onchange('product_id')
    def onchange_product_id(self):
        result = {}
        if not self.product_id:
            return result
        self.product_uom = self.product_id.uom_po_id or self.product_id.uom_id
        result['domain'] = {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
        return result