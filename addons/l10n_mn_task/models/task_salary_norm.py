# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
import time

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.addons import decimal_precision as dp  # @UnresolvedImport

class TaskSalaryNorm(models.Model):
    _name = 'task.salary.norm'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Task Salary Norm'

    name = fields.Char('Name', required=True,track_visibility='onchange', states={'approved': [('readonly', True)]})
    start_date = fields.Date('Start Date', required=True, default=lambda *a: time.strftime('%Y-%m-01'),track_visibility='onchange', states={'approved': [('readonly', True)]})
    end_date = fields.Date('End Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'), track_visibility='onchange', states={'approved': [('readonly', True)]})
    line_ids = fields.One2many('task.salary.norm.line', 'salary_norm_id', string='Lines', copy=False, states={'approved': [('readonly', True)]})
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    user_id = fields.Many2one('res.users', string='Responsible', default=lambda self: self.env.user, readonly=True)
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')], string='State', default='draft', required=True, track_visibility='onchange')
    description = fields.Text('Description')

    @api.multi
    def unlink(self):
        for cost in self:
            if cost.state != 'draft':
                raise UserError(_('Delete only draft in state'))
        return super(TaskSalaryNorm, self).unlink()

    @api.multi
    def validate(self):
        # Батлах
        self.write({'state': 'approved'})

    @api.multi
    def action_to_draft(self):
        # Ноороглох
        self.write({'state': 'draft'})


class TaskSalaryNormLine(models.Model):
    _name = 'task.salary.norm.line'
    _description = 'Task Salary Norm Line'

    salary_norm_id = fields.Many2one('task.salary.norm', string='Salary Norm', ondelete='cascade', index=True, copy=False)
    task_type_id = fields.Many2one('task.type', required=True, string='Task Type')
    employee_id = fields.Many2one('hr.employee', string="Employee", required=True)
    job_id = fields.Many2one('hr.job', string='Job', related="employee_id.job_id")
    production_uom = fields.Many2one('product.uom', string='Production UoM', required=True)
    production_qty = fields.Float(string='Production Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    norm_salary = fields.Float(string='Norm Salary', default=0)
    plan_hours = fields.Float(string='Plan Hours', default=0)
    salary_calculate_type = fields.Selection([('hour', 'Hour'),
                                              ('creation', 'Creation')], string='Salary Calculate Type', default='hour')