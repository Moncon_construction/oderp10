# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
import time
from odoo import models, fields, api, _
from odoo.addons import decimal_precision as dp  # @UnresolvedImport

class TaskTask(models.Model):
    _name = 'task.task'
    _inherit = 'project.task'

    task_type_id = fields.Many2one('task.type', string='Task Type', ondelete='restrict')
    task_uom_id = fields.Many2one('product.uom', related='task_type_id.type_uom_id', string="Task UOM")
    technic_line_ids = fields.One2many('task.task.technic', 'task_id', string='Technic', copy=False)
    product_line_ids = fields.One2many('task.task.product', 'task_id', string='Products', copy=False)
    timesheet_task_ids = fields.One2many('account.analytic.line', 'task_task_id', string='Products', copy=False)
    mining_id = fields.Many2one('mining.daily.entry', 'Mining')
   
class TaskTaskTechnic(models.Model):
    _name = 'task.task.technic'
    _description = 'Task Technic'
   
    task_id = fields.Many2one('task.task', string='Task', ondelete='cascade', index=True, copy=False)
    date = fields.Date('Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    technic_id = fields.Many2one('technic', string='Technic')
    employee_id = fields.Many2one('hr.employee', string='Employee')
    task_type_id = fields.Many2one('task.type', required=True, string='Task Type')
    perf_qty = fields.Float(string='Performance Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    perf_hours = fields.Float(string='Performance Hours', default=0)
    perf_km = fields.Float(string='Performance Km', default=0)
    total_freight_tn_km = fields.Float(string='Total freight tn/km')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    norm_uom_id = fields.Many2one('product.uom', string='Uom')


class TaskTaskProduct(models.Model):
    _name = 'task.task.product'
    _description = 'Task Product'
   
    task_id = fields.Many2one('task.task', string='Task', ondelete='cascade', index=True, copy=False)
    date = fields.Date('Date', required=True, default=lambda *a: time.strftime('%Y-%m-%d'))
    warehouse_id = fields.Many2one('stock.warehouse', string='Technic', required=True)
    product_id = fields.Many2one('product.product', string='Product', required=True, ondelete='restrict')
    product_qty = fields.Float(string='Product Quantity', digits=dp.get_precision('Product Unit of Measure'), default=0)
    product_uom = fields.Many2one('product.uom', string='Product UoM', required=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
   
    @api.onchange('product_id')
    def onchange_product_id(self):
        result = {}
        if not self.product_id:
            return result
        self.product_uom = self.product_id.uom_po_id or self.product_id.uom_id
        result['domain'] = {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
        return result