# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from odoo import models, fields

class TaskType(models.Model):
    _name = 'task.type'
    _description = 'Task Type'

    short_code = fields.Char('Short Code', required=True)
    name = fields.Char('Name', required=True)
    sequence = fields.Integer('Sequence', default=1)
    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic account')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    type_uom_id = fields.Many2one('product.uom', string="Task Type UOM")