# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

#
# Please note that these reports are not multi-currency !!!
#

from odoo import api, fields, models, tools
from odoo.addons import decimal_precision as dp

class ReportMotohour(models.Model):
    _inherit = "report.motohour"
    
    task_type_id = fields.Many2one('task.type', string='Task Type')
    
    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'report_motohour')
        self._cr.execute("""
            create or replace view report_motohour as (
                select
                    a.id as id,
                    a.operator_id as operator_id,
                    b.technic_id as technic_id,
                    a.carrier_id as carrier_id,
                    a.carrier_operator_id as carrier_operator_id,
                    a.cause_id as cause_id,
                    c.shift as shift,
                    c.date as date,
                    c.master_id as master_id,
                    c.create_uid as create_user_id,
                    c.project_id as project_id,
                    a.diff_time as diff_time,
                    a.performance_motohour as performance_motohour,
                    a.performance_km as performance_km,
                    a.task_type_id as task_type_id
                from mining_motohour_entry_cause_line a 
                left join mining_motohour_entry_line b on a.cause_id = b.id
                left join mining_daily_entry c on b.motohour_id = c.id 
                where a.company_id = %s
            )""" % self.env.user.company_id.id)
