# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016-today Asterisk Technologies LLC (<http://www.asterisk-tech.mn>)

#
##############################################################################

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import exceptions
from odoo.exceptions import ValidationError, UserError

class mining_motohour_entry_cause_line(models.TransientModel):
    _inherit = 'mining.motohour.entry.cause.line.wizard'

    task_type_id = fields.Many2one('task.type', string='Task Type')
    
    def _prepare_cause_line(self):
        res = super(mining_motohour_entry_cause_line, self)._prepare_cause_line()
        res.update({'task_type_id': self.task_type_id.id})
        return res
            
