# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    "name": "Техник",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",

    'summary': """
        Техникийн бүртгэл, норм, бичиг баримтын бүртгэл""",

    "description": """
        Уг модуль нь техникийн бүртгэл,норм, бичиг баримтын мэдээллийг бүртгэх боломжийг олгоно.
    """,

    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": [
        'product',
        'base',
        'project',
        'l10n_mn_hr',
        'l10n_mn_report',
    ],
    "data": [
        # security
        'security/technic_security.xml',
        'security/ir.model.access.csv',

        'views/project_views.xml',
        'email_templates/check_incoming_expiry_date.xml',
        'email_templates/check_outoftime_expiry_date.xml',
        'views/technic_default_view.xml',
        'views/hr_employee_views.xml',
        'data/hr_driver_license_category_data.xml',
        'data/technic_color_data.xml',
        'views/technic_norm_view.xml',
        'views/technic_transfer_view.xml',
        'views/technic_view.xml',
        'views/product_views.xml',
        'views/document_check_date.xml',
        'views/technic_document_view.xml',
        'views/menu_view.xml',
        'views/sequence.xml',

        # report
        'report/technic_list_wizard.xml',
        'report/technic_passport_wizard.xml',
        'report/downtime_wizard.xml',
        'report/technic.xml',

    ],
    "demo_xml": [
    ],
    "active": False,
    "installable": True,

    'contributors': ['Sugarsukh Sukhbaatar <sugarsukh@asterisk-tech.mn>'],

}
