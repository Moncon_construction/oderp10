# -*- coding: utf-8 -*-
from odoo import models, fields

class HrDriverLicenseCategory(models.Model):
    _name = "hr.driver.license.category"
    _description = "Driver license category"

    name = fields.Char('Driver license category')

class hr_employee(models.Model):
    _inherit = "hr.employee"

    driver_license_category_technic = fields.Many2many('hr.driver.license.category', 'employee_driver_license_rel', 'emp_id', 'license_id', 'License Category')
