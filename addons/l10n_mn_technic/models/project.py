# -*- coding: utf-8 -*-
from odoo import models, fields

class Project(models.Model):
    _inherit = 'project.project'

    use_technic = fields.Boolean(string='Use Technic Management', help='Specify if the project use technic management.', default=False)
