# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from datetime import datetime

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT  # @UnresolvedImport

class Technic(models.Model):
    _name = 'technic'
    _description = 'Technic'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _inherits = {'technic.norm': 'technic_norm_id'}
    _order = 'name'

    def _usage_history_count(self):
        for obj in self:
            obj_ids = self.env['technic.usage.history'].search([('technic_id', '=', obj.id)])
            obj.usage_history_count = len(obj_ids)

    def _ownership_history_count(self):
        for obj in self:
            obj_ids = self.env['technic.ownership.history'].search([('technic_id', '=', obj.id)])
            obj.ownership_history_count = len(obj_ids)

    def _state_history_count(self):
        for obj in self:
            obj_ids = self.env['technic.state.history'].search([('technic_id', '=', obj.id)])
            obj.state_history_count = len(obj_ids)

    @api.model
    def _default_project(self):
        project_ids = self.env['project.project'].search([('use_technic', '=', True)])
        if len(project_ids) == 1:
            return project_ids

    @api.multi
    def _compute_last_motohour_km(self):
        for obj in self:
            obj.last_motohour = sum(moto.usage_value for moto in obj.technic_usage_ids if moto.usage_uom_id.is_motohour)
            obj.last_km = sum(km.usage_value for km in obj.technic_usage_ids if km.usage_uom_id.is_kilometer)
            for m in obj.technic_extra_info_ids:
                if m.sum_work_motohour > 0:
                    obj.usage_percent_motohour = (obj.last_motohour / sum(extra.sum_work_motohour for extra in m)) * 100
                else:
                    obj.usage_percent_motohour = 0
                if m.sum_work_km > 0:
                    obj.usage_percent_km = (obj.last_km / sum(km.sum_work_km for km in m)) * 100
                else:
                    obj.usage_percent_km = 0

    state = fields.Selection([('draft', 'Draft'),
                               ('ready', 'Ready'),
                               ('in_repair', 'In repair'),
                               ('stop', 'Stop')], 'Status', Translate=True, default='draft')
    name = fields.Char(compute='technic_name_get', store=True)
    technic_type_id = fields.Many2one('technic.type', 'Technic type', readonly=True)
    manufacturer_id = fields.Many2one('res.partner', related='technic_norm_id.brand_id.manufacturer_id', string='Manufacturer', readonly=True)
    technic_brand_id = fields.Many2one('brand', 'Brand technic', readonly=True)
    technic_norm_id = fields.Many2one('technic.norm', 'Technic configuration', required=True, readonly=True, ondelete='cascade', states={'draft': [('readonly', False)]})
    serial_vin_number = fields.Char('Serial/VIN number', required=True)
    state_number = fields.Char('State number', readonly=True, states={'draft': [('readonly', False)]})
    garage_number = fields.Char('Garage Number', readonly=True, states={'draft': [('readonly', False)]})
    engine_number = fields.Char('Engine number', readonly=True, states={'draft': [('readonly', False)]})
    manufacture_date = fields.Integer('Manufacture date', readonly=True, states={'draft': [('readonly', False)]})
    registration_date = fields.Datetime('Registration date', readonly=True, states={'draft': [('readonly', False)]})
    project = fields.Many2one('project.project', 'Project', readonly=True, states={'draft': [('readonly', False)]}, domain="[('use_technic','=',True)]", default=_default_project)
    ownership_type = fields.Selection([('own', 'Own'),
                                    ('leasing', 'Leasing'),
                                    ('partner', 'Partner'),
                                    ('rental', 'Rental')], 'Ownership type', readonly=True, states={'draft': [('readonly', False)]})
    partner_id = fields.Many2one('res.partner', 'Partner', readonly=True, states={'draft': [('readonly', False)]})
    ownership_company_id = fields.Many2one('res.company', 'Ownership company', readonly=True, states={'draft': [('readonly', False)]})
    ownership_department_id = fields.Many2one('hr.department', 'Ownership department', readonly=True, states={'draft': [('readonly', False)]})
    respondent_id = fields.Many2one('hr.employee', 'Respondent', readonly=True, states={'draft': [('readonly', False)]})
    current_company_id = fields.Many2one('res.company', 'Current company', readonly=True, states={'draft': [('readonly', False)]})
    current_department_id = fields.Many2one('hr.department', 'Current department', readonly=True, states={'draft': [('readonly', False)]})
    current_respondent_id = fields.Many2one('hr.employee', 'Current respondent', readonly=True, states={'draft': [('readonly', False)]})
    current_owner_id = fields.Many2many('hr.employee','technic_owners_rel', 'technic_id', 'employee_id', 'Current Respondent',  states={'draft': [('readonly', False)]})
    location_id = fields.Many2one('res.country', 'Location', readonly=True, states={'draft': [('readonly', False)]})
    usage_state = fields.Selection([('in_usage', 'In usage'),
                                    ('in_reserve', 'In reserve'),
                                    ('in_rent', 'In rent'),
                                    ('in_pawn', 'In pawn')], 'Usage state', readonly=True, states={'draft': [('readonly', False)]})
    in_scrap = fields.Boolean('In scrap')
    technic_color = fields.Many2many('technic.color', 'technic_color_rel', 'technic_id', 'color_id', 'Technic color', readonly=True, states={'draft': [('readonly', False)]})
    technic_usage_ids = fields.One2many('technic.usage', 'technic_id', readonly=True, states={'draft': [('readonly', False)]})
    technic_document_ids = fields.One2many('technic.document', 'technic_id', readonly=True, states={'draft': [('readonly', False)]})
    technic_usage_history_ids = fields.One2many('technic.usage.history', 'technic_id', readonly=True, states={'draft': [('readonly', False)]})
    technic_state_history_ids = fields.One2many('technic.state.history', 'technic_id', readonly=True, states={'draft': [('readonly', False)]})
    technic_extra_info_ids = fields.One2many('technic.extra.info', 'technic_id', string='Extra info', readonly=True, states={'draft': [('readonly', False)]})
    usage_history_count = fields.Integer(compute=_usage_history_count, string='Usage history count')
    ownership_history_count = fields.Integer(compute=_ownership_history_count, string='Ownership history count')
    state_history_count = fields.Integer(compute=_state_history_count, string='State history count')
    number = fields.Char('Number')
    product_id = fields.Many2one('product.product', string="Product", domain="[('type','=','service')]")
    last_motohour = fields.Float('last motohour', digits=(16, 1), readonly=True, compute='_compute_last_motohour_km')
    last_km = fields.Float('last km', digits=(16, 1), readonly=True, compute='_compute_last_motohour_km')
    usage_percent_motohour = fields.Float('Usage motohour', readonly=True, compute='_compute_last_motohour_km')
    usage_percent_km = fields.Float('Usage km', readonly=True, compute='_compute_last_motohour_km')

    _sql_constraints = [
        ('technic_name_unique', 'UNIQUE(name)', 'Technic name must be unique!'),
    ]

    @api.depends('technic_norm_id', 'number')
    def technic_name_get(self):
        for record in self:
            brand_id = record.brand_id.name if record.brand_id else ''
            technic_model_id = record.technic_model_id.model_name if record.technic_model_id else ''
            number = record.number if record.number else ''
            record.name = brand_id + ' | ' + technic_model_id + ' | ' + number

    @api.multi
    def import_usage_oum(self):
        technic_usage_object = self.env['technic.usage']
        technic_usage_obj = technic_usage_object.search([('technic_id', '=', self.id)])
        for technic_usage in technic_usage_obj:
            technic_usage.unlink()
        if self.technic_usage_ids:
            return {}
        if self.technic_norm_id:
            for technic_norm_usage_uom in self.technic_norm_id.usage_uom_ids:
                data = {
                    'technic_id': self.id,
                    'usage_uom_id': technic_norm_usage_uom.usage_uom_id.id,
                    'product_uom_id': technic_norm_usage_uom.product_uom_id.id,
                }
                technic_usage_object.create(data)
        else:
            raise ValidationError(_('There is not usage information on technic!'))

    @api.model
    def create(self, vals):
        res = super(Technic, self).create(vals)
        res.import_usage_oum()
        return res


class hr_employee(models.Model):
    _inherit = "hr.employee"

    @api.multi
    def _count_technic(self):
        for record in self:
            self.env.cr.execute("SELECT technic_id "
                                "FROM technic_owners_rel "
                                "WHERE employee_id=%s " % record.id)
            technic_ids = self.env.cr.fetchall()
            record.technic_count = len(technic_ids) if technic_ids else 0

    technic_count = fields.Integer(compute='_count_technic', string='Technic Count')

    def view_technics(self):
        mod_obj = self.env['ir.model.data']
        dummy, action_id = tuple(mod_obj.get_object_reference('l10n_mn_technic', 'action_technic')[1])
        action = self.env['ir.actions.act_window'].read(action_id)
        technic_ids = []
        for employee in self:
            self.env.cr.execute("SELECT technic_id "
                       "FROM technic_owners_rel "
                       "WHERE employee_id=%s " % employee.id)
            technic_ids = self.env.cr.fetchall()
        action['context'] = {}
        action['domain'] = "[('id','in',[" + ','.join(map(str, technic_ids)) + "])]"
        return action


class TechnicUsage(models.Model):
    _name = 'technic.usage'
    _description = 'Technic usage'

    technic_id = fields.Many2one('technic', 'name')
    usage_uom_id = fields.Many2one('usage.uom', 'Usage measurement')
    product_uom_id = fields.Many2one('product.uom', 'Unit of measure', readonly=True)
    usage_value = fields.Float('Usage value')

    @api.model
    def create(self, vals):
        res = super(TechnicUsage, self).create(vals)
        # create history
        history_values = {
            'technic_id': vals.get('technic_id'),
            'change_measurement': vals.get('usage_uom_id'),
            'change_value': vals.get('usage_value'),
            'change_date': datetime.now(),
            'change_user_id': self.env.uid,
            'description': vals.get('description')
        }
        self.env['technic.usage.history'].create(history_values)
        return res

    @api.multi
    def write(self, vals):
        res = True
        usage_value = vals.get('usage_value')
        # calculate auto time measurement
        technic_usage_history_obj = self.env['technic.usage.history']
        for usage in self:
            if usage.usage_uom_id.auto_time:
                technic_usage_history = technic_usage_history_obj.search([('change_measurement', '=', vals.get('usage_uom_id')),
                                                                          ('technic_id', '=', vals.get('technic_id'))], order='change_date DESC', limit=1)
                if technic_usage_history:
                    start_date = datetime.strptime(technic_usage_history.change_date, DEFAULT_SERVER_DATETIME_FORMAT)
                    end_date = datetime.now()
                    delta = end_date - start_date
                    days = delta.total_seconds() / 86400.0

                    product_uom = usage.usage_uom_id.usage_uom_id
                    if product_uom.uom_type == 'reference':
                        usage_value = usage.usage_value + days
                    elif product_uom.uom_type == 'smaller':
                        usage_value = usage.usage_value + round((days * product_uom.factor), 2)
                    else:
                        usage_value = usage.usage_value + round((days / product_uom.factor_inv), 2)
                else:
                    usage_value = 0

        # check not 0
        if usage_value:
            for usage in self:
                # check value is raised
                if usage.usage_value < usage_value:
                    # create history
                    history_values = {
                        'technic_id': usage.technic_id.id,
                        'change_measurement': usage.usage_uom_id.id,
                        'change_value': usage_value,
                        'change_date': datetime.now(),
                        'change_user_id': self.env.uid,
                        'description': vals.get('description')
                    }
                    technic_usage_history_obj.create(history_values)
            vals['usage_value'] = usage_value
            res = super(TechnicUsage, self).write(vals)
        return res


class TechnicUsageHistory(models.Model):
    _name = "technic.usage.history"
    _description = "Technic usage history"
    _order = 'change_date DESC'

    technic_id = fields.Many2one('technic', 'Technic', ondelete='cascade')
    change_measurement = fields.Many2one('usage.uom', 'Change measurement')
    uom = fields.Many2one('product.uom', related='change_measurement.usage_uom_id', string='UOM', readonly=True)
    change_value = fields.Float('Change value')
    change_date = fields.Datetime('Change date',)
    change_user_id = fields.Many2one('res.users', 'Change user',)
    description = fields.Text('Description')

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        if 'change_value' in fields:
            fields.remove('change_value')
        return super(TechnicUsageHistory, self).read_group(domain, fields, groupby, offset, limit, orderby, lazy)


class TechnicOwnershipHistory(models.Model):
    _name = "technic.ownership.history"
    _description = "Technic ownership history"
    _order = 'change_date DESC'
    _rec_name = 'ownership_name'

    technic_id = fields.Many2one('technic', 'State', ondelete='cascade')
    ownership_name = fields.Selection([('ownership_type', 'Ownership Type'),
                                        ('partner', 'Partner'),
                                        ('ownership_company', 'Ownership Company'),
                                        ('ownership_department', 'Ownership Department'),
                                        ('respondent', 'Respondent'),
                                        ('current_company', 'Current Company'),
                                        ('current_department', 'Current Department'),
                                        ('current_respondent', 'Current Respondent'),
                                        ('location', 'Location'),
                                        ('usage_state', 'Usage State')], 'Change Information')
    ownership_type = fields.Selection([('own', 'Own'),
                                       ('leasing', 'Leasing'),
                                       ('partner', 'Partner'),
                                       ('rental', 'Rental')], 'Ownership type')
    usage_state = fields.Selection([('in_usage', 'In usage'),
                                   ('in_reserve', 'In reserve'),
                                   ('in_rent', 'In rent'),
                                   ('in_pawn', 'In pawn')], 'Usage state')
    change_ownership = fields.Char('Change Value')
    change_date = fields.Datetime('history date')
    change_user_id = fields.Many2one('res.users', 'Change user')
    description = fields.Text('Description')


class TechnicStateHistory(models.Model):
    _name = "technic.state.history"
    _description = "Technic state history"
    _order = 'change_date DESC'
    _rec_name = 'state_name'

    technic_id = fields.Many2one('technic', 'Technic passport', ondelete='cascade')
    state_name = fields.Selection([('draft', 'Draft'),
                              ('ready', 'Ready'),
                              ('in_repair', 'In repair'),
                              ('stop', 'Stop')], 'Status', Translate=True)
    change_date = fields.Datetime('history date')
    change_user_id = fields.Many2one('res.users', 'Change user')
    description = fields.Text('Description')


class TechnicColor(models.Model):
    _name = "technic.color"
    _description = "Technic color"

    name = fields.Char('Color', required=True)


class TechnicExtraInfo(models.Model):
    _name = "technic.extra.info"
    _description = "Extra Info"

    first_motohour = fields.Float('First motohour')
    first_km = fields.Float('First km')
    sum_work_motohour = fields.Float('Total work motohour')
    sum_work_km = fields.Float('Total work km')
    technic_id = fields.Many2one('technic', 'Technic')


