# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import api, fields, models, tools, _

class Brand(models.Model):
    _name = 'brand'
    _description = 'Brand Technic'
    _order = 'name ASC'

    manufacturer_id = fields.Many2one('res.partner', 'Manufacturer')
    name = fields.Char('Brand Name', size=64, required=True)
    image = fields.Binary('Logo')

    image_medium = fields.Binary(compute='_get_image', inverse='_set_image',
                                 string="Medium-sized image",
                                 help="Medium-sized logo of the brand. It is automatically "
                                      "resized as a 128x128px image, with aspect ratio preserved. "
                                      "Use this field in form views or some kanban views.")
    image_small = fields.Binary(compute='_get_image', inverse='_set_image', string="Small-sized image",
                                help="Small-sized photo of the brand. It is automatically "
                                     "resized as a 64x64px image, with aspect ratio preserved. "
                                     "Use this field anywhere a small image is required.")

    _sql_constraints = [
        ('brand_name_unique', 'unique(name)', 'Brand name must be unique!'),
    ]


    @api.multi
    def _get_image(self):
        for obj in self:
            result = tools.image_get_resized_images(obj.image, avoid_resize_medium=True)
            obj.image_medium = result.get('image_medium', False)
            obj.image_small = result.get('image_small', False)
        return result

    @api.multi
    def _set_image(self):
        for obj in self:
            obj.image_medium = tools.image_resize_image_big(obj.image_medium)
            obj.image_small = tools.image_resize_image_big(obj.image_small)
        return True

class TechnicType(models.Model):
    _name = 'technic.type'
    _description = 'Technic type'

    def _get_child_ids(self):
        result = {}
        for record in self:
            if record.type_ids:
                result[record.id] = [x.id for x in record.type_ids]
            else:
                result[record.id] = []
        return result

    name = fields.Char('Type name', required=True)
    is_aggregate = fields.Boolean('Is Aggregate')
    parent_id = fields.Many2one('technic.type', 'Parent')
    type_ids = fields.One2many('technic.type', 'parent_id', 'Types')
    child_id = fields.Many2many(compute=_get_child_ids, relation="technic.type", string="Child breakdown types")
    image = fields.Binary('Technic Condition')
    is_auto = fields.Boolean('Is DumpTruck')
    is_mechanism = fields.Boolean('Is Mechanism')
    image_medium = fields.Binary(compute='_get_image', inverse='_set_image',
                                 string="Medium-sized image",
                                 help="Medium-sized logo of the brand. It is automatically "
                                      "resized as a 128x128px image, with aspect ratio preserved. "
                                      "Use this field in form views or some kanban views.")

    @api.multi
    def _get_image(self):
        for obj in self:
            result = tools.image_get_resized_images(obj.image, avoid_resize_medium=True)
            obj.image_medium = result.get('image_medium', False)
            obj.image_small = result.get('image_small', False)
        return result

    @api.multi
    def _set_image(self):
        for obj in self:
            obj.image_medium = tools.image_resize_image_big(obj.image_medium)
            obj.image_small = tools.image_resize_image_big(obj.image_small)
        return True

class TechnicModel(models.Model):
    _name = 'technic.model'
    _description = 'Technic model'
    _rec_name = 'model_name'

    model_name = fields.Char('Model name', required=True)
    brand_id = fields.Many2one('brand', 'Brand', required=True)

class TechnicSubmodel(models.Model):
    _name = 'technic.submodel'
    _description = 'Technic sub-model'
    _rec_name = 'submodel_name'

    brand_id = fields.Many2one('brand', 'Brand name')
    technic_model_id = fields.Many2one('technic.model', 'Technic model', required=True, translate=True)
    submodel_name = fields.Char('Sub-model', required=True, translate=True)

class LiquidSystem(models.Model):
    _name = 'liquid.system'
    _description = 'Liquid System'

    name = fields.Char('System name', required=True, ondelete='cascade')
    description = fields.Text('Description')

class ProductionUom(models.Model):
    _name = 'production.uom'
    _description = 'Production unit of measure'

    def production_name_get(self):
        for object in self:
            reads = object.read()
            result = []
            for record in reads:
                production__uom_name = u'{0} /{1}'.format(record['divident_id'][1], record['divider_id'][1])
                result.append((record['id'], production__uom_name))
            object.name = production__uom_name

    name = fields.Char(compute=production_name_get, string='Production UOM')
    divident_id = fields.Many2one('product.uom', 'Divident unit', required=True)
    divider_id = fields.Many2one('product.uom', 'Divider unit', required=True)
    description = fields.Text('Description')


class ProductUom(models.Model):
    _inherit = 'product.uom'

    is_day = fields.Boolean('Is Day')
    is_motohour = fields.Boolean('Is Motohour')
    is_kilometer = fields.Boolean('Is Kilometer')


class UsageUom(models.Model):
    _name = 'usage.uom'
    _description = 'Usage unit of measure'

    def _is_time_category(self, uom):
        model_data = self.env['ir.model.data'].search([('model', '=', 'product.uom.categ'), ('name', '=', 'uom_categ_wtime')])
        time_categ_id = model_data.res_id
        for object in self:
            if uom.category_id.id == time_categ_id:
                return True
            else:
                return False

    name = fields.Char('Usage name', required=True)
    usage_uom_id = fields.Many2one('product.uom', 'UOM', required=True)
    auto_time = fields.Boolean('Auto update', help='If check this on time category measurement, it will be updated automatically belong to time.')
    is_time = fields.Boolean('Is time category')

    # дугуй, аккумлятор зэргийн ашиглалтыг техникийн ашиглалтын хэмжигдэхүүнээс шалтгаалж өөрчлөгдөж байх үүднээс дараах 2 талбарыг нэмэв
    is_motohour = fields.Boolean('Is Motohour')
    is_kilometer = fields.Boolean('Is Kilometer')

    @api.onchange('is_motohour')
    def onchange_motohour(self):
        if self.is_motohour:
            return {'value': {
                'is_kilometer': False
            }}
        else:
            return {'value': {
                'is_kilometer': True
            }}
        return True

    @api.onchange('is_kilometer')
    def onchange_km(self):
        if self.is_kilometer:
            return {'value': {
                'is_motohour': False
            }}
        else:
            return {'value': {
                'is_motohour': True
            }}
        return True

    @api.onchange("usage_uom_id")
    def onchange_uom(self):
        for object in self:
            uom = self.env['product.uom'].browse(object.usage_uom_id.id)
            if object._is_time_category(uom):
                return {
                    'value': {
                        'is_time': True
                    }
                }
            else:
                return {
                    'value': {
                        'is_time': False,
                        'auto_time': False,
                        'is_motohour': False,
                        'is_kilometer': False
                    }
                }
