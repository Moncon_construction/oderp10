# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from odoo import api, fields, models, tools, _
from datetime import datetime
from datetime import timedelta

class TechnicDocumentType(models.Model):
    _name = 'technic.document.type'
    _description = 'Technic document type'
    _rec_name = 'document_type_name'

    document_type_name = fields.Char('Document type name', required=True)
    expiry_date = fields.Integer('Expiry reminder', required=True, help=u"Тухайн төрлийн гэрчилгээ болон бичиг баримтын хүчинтэй хугацаа дуусахаас хэд хоногийн өмнө анхааруулга өгөхийг тоогоор (хоногийн тоо) бичих")

class TechnicDocument(models.Model):
    _name = 'technic.document'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = 'Technic document type'
    _rec_name = 'document_name'

    def _total_cost(self):
        all_cost = 0
        for obj in self:
            for extension in obj.extension_ids:
                all_cost += extension.cost
            obj.total_cost = all_cost

    state = fields.Selection([('draft', 'Draft'),
                               ('using', 'Using'),
                               ('aborted', 'Aborted')], 'State', default='draft')
    technic_id = fields.Many2one('technic', 'Technic', required=True)
    document_type_id = fields.Many2one('technic.document.type', 'Document type', required=True)
    document_name = fields.Char('Document number', required=True)
    respondent_id = fields.Many2one('hr.employee', 'Document respondent', required=True, track_visibility='onchange')
    extension_ids = fields.One2many('technic.document.extension', 'technic_document_id', 'Document extension')
    total_cost = fields.Float(compute=_total_cost, string='Total cost')
    register_date_order = fields.Datetime(compute='_date_order_register', string='Register date', store=True)
    expiry_date_order = fields.Datetime(compute='_date_order_expiry', string='Expiry date', store=True)

    @api.depends('extension_ids')
    def _date_order_register(self):
        document = self.env['technic.document.extension'].search([('technic_document_id', '=', self.id)], order="create_date desc", limit=1)
        self.register_date_order = document.register_date

    @api.depends('extension_ids')
    def _date_order_expiry(self):
        document = self.env['technic.document.extension'].search([('technic_document_id', '=', self.id)], order="create_date desc", limit=1)
        self.expiry_date_order = document.expiry_date

    @api.onchange("technic_id")
    def onchange_technic_id(self):
        for technic_obj in self.technic_id:
            return {
                'value': {
                    'respondent_id': technic_obj.current_respondent_id.id,
                }
            }

class TechnicDocumentExtension(models.Model):
    _name = 'technic.document.extension'
    _description = 'Technic document extension'

    technic_document_id = fields.Many2one('technic.document', 'Technic document')
    register_date = fields.Datetime('Register date', required=True, default=datetime.now())
    expiry_date = fields.Datetime('Expiry date', required=True)
    alert_date = fields.Datetime('Alert date')
    cost = fields.Float('Cost')
    state = fields.Selection([('using', 'Using'),
                               ('exceed', 'Exceed'),
                               ('finished', 'Finished')], 'State', default="using")
    description = fields.Char('Description')

    @api.model
    def create(self, vals):
        technic_document_obj = self.env['technic.document'].search([('id', '=', vals.get('technic_document_id'))])
        if vals.get('expiry_date'):
            technic_document_expiry_date = datetime.strptime(vals['expiry_date'], '%Y-%m-%d %H:%M:%S')
            document_type_expiry_date = timedelta(days=-(technic_document_obj.document_type_id.expiry_date))
            alert_date = document_type_expiry_date + technic_document_expiry_date
            vals.update({'alert_date': alert_date})
        return super(TechnicDocumentExtension, self).create(vals)

    @api.multi
    def write(self, vals):
        technic_document_obj = self.env['technic.document'].search([('id', '=', vals.get('technic_document_id'))])
        if vals.get('expiry_date'):
            technic_document_expiry_date = datetime.strptime(vals['expiry_date'], '%Y-%m-%d %H:%M:%S')
            document_type_expiry_date = timedelta(days=-(technic_document_obj.document_type_id.expiry_date))
            alert_date = document_type_expiry_date + technic_document_expiry_date
            vals.update({'alert_date': alert_date})
        return super(TechnicDocumentExtension, self).write(vals)

    @api.multi
    def document_check_date(self):
        for doc in self.env['technic.document'].search([('state', '=', 'using')]):
            document_type_obj = doc.document_type_id
            doc_obj = self.env['technic.document.extension'].search([('technic_document_id', '=', doc.id)],
                                                                    order='expiry_date desc', limit=1)
            diff = datetime.strptime(doc_obj.expiry_date, '%Y-%m-%d %H:%M:%S') - datetime.now()
            print diff.days
            if document_type_obj.expiry_date <= diff.days:
                template_obj = self.env.ref('l10n_mn_technic.email_template_to_respondent')
                data = {
                    'name': doc.technic_id.name,
                    'diff_day': diff.days,
                    'expiry_number_name': document_type_obj.document_type_name,
                    'expiry_date': doc_obj.expiry_date,
                    'current_respondent': doc.technic_id.current_respondent_id,
                    'base_url': self.env['ir.config_parameter'].get_param('web.base.url'),
                    'id': doc.id,
                    'sender': doc.env.user.name,
                }
                you = doc.technic_id.current_respondent_id.user_id.partner_id.id
                doc.env.context = data
                template_obj.send_mail(you)
                email = u'\n Дараах хэрэглэгч рүү имэйл илгээгдэв : ' + ('<b>' + ('</b>'.join(doc.technic_id.current_respondent_id.name)) + '</b>')
                doc.message_post(email)

            elif diff.days < 0:
                template_obj = self.env.ref('l10n_mn_technic.email_template_to_respondent_outoftime')
                data = {
                    'name': doc.technic_id.name,
                    'diff_day': diff.days * (-1),
                    'expiry_number_name': document_type_obj.document_type_name,
                    'expiry_date': doc_obj.expiry_date,
                    'current_respondent': doc.technic_id.current_respondent_id,
                    'base_url': self.env['ir.config_parameter'].get_param('web.base.url'),
                    'id': doc.id,
                    'sender': doc.env.user.name}
                you = doc.technic_id.current_respondent_id.user_id.partner_id.id
                doc.env.context = data
                template_obj.send_mail(you)
                email = u'\n Дараах хэрэглэгч рүү имэйл илгээгдэв : ' + ('<b>' + ('</b>'.join(doc.technic_id.current_respondent_id.name)) + '</b>')
                doc.message_post(email)

    @api.model
    def _cron_document_check_date(self):
        return self.document_check_date()

class InheritTechnicDocument(models.Model):
    _inherit = 'technic'

    def _document_count(self):
        for obj in self:
            obj_ids = self.env['technic.document'].search([('technic_id', '=', obj.id), ('state', '=', 'using')])
            obj.document_count = len(obj_ids)

    document_count = fields.Integer(compute=_document_count, string='Document count')
