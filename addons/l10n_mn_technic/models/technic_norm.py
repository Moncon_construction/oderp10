# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import api, fields, models, _

class TechnicNorm(models.Model):
    _name = 'technic.norm'
    _description = 'Technic norm'
    
    technic_norm_sequence_id = fields.Many2one('ir.sequence', readonly=True, store=True, string="Technic norm Sequence")
    name = fields.Char(compute='norm_name_get', string='Norm name', store=True)
    brand_id = fields.Many2one('brand', 'Technic brand', required=True)
    technic_type_id = fields.Many2one('technic.type', 'Technic type', required=True)
    technic_model_id = fields.Many2one('technic.model', 'Technic model', required=True)
    technic_submodel_id = fields.Many2one('technic.submodel', 'Technic sub-model')
    driver_license_category_technic_norm = fields.Many2many('driver.license.category', 'employee_driver_license_technic_rel', 'emp_id', 'license_id', "License Category")
    tire = fields.Boolean('Tire')
    basket = fields.Boolean('Basket')
    scoop = fields.Boolean('Scoop')
    chair = fields.Boolean('Chair')
    door = fields.Boolean('Door')
    drill = fields.Boolean('Drill')
    liquid_system = fields.Boolean('Liquid system')
    tire_number = fields.Integer('Tire number')
    basket_capacity = fields.Float('Basket capacity /m3/')
    basket_tofreight_capacity = fields.Float('Basket to freight capacity /tonnage/')
    basket_length = fields.Float('Basket length')
    basket_width = fields.Float('Basket width')
    scoop_capacity = fields.Float('Scoop capacity /tonnage/')
    chair_number = fields.Integer('Chair number')
    door_number = fields.Integer('Door number')
    drill_diameter = fields.Integer('Drill diameter /mm/')
    engine_type = fields.Selection([('gas', 'Gas'),
                                    ('petrol', 'Petrol'),
                                    ('diesel', 'Diesel'),
                                    ('hybrid', 'Hybrid'),
                                    ('electric', 'Electric'),
                                    ('other', 'Other')], 'Engine type')
    engine_capacity = fields.Float('Engine capacity')
    engine_power = fields.Float('Engine power /HP/')
    engine_spin = fields.Float('Engine spin')
    actual_tare = fields.Float('Actual tare')
    length = fields.Float('Lenght')
    width = fields.Float('Width')
    height = fields.Float('Height')
    fuel_capacity = fields.Float('Fuel capacity')
    gearbox_type = fields.Selection([('mechanic', 'Mechanic'),
                                      ('automatic', 'Automatic'),
                                      ('two-tier', 'two-tier')], 'Gearbox type')
    liquid_system_ids = fields.One2many('liquid.system.line', 'technic_norm_id', 'Liquid system')
    fuel_oum = fields.Selection([('mpg', 'MPG'),
                                  ('kmlitr', 'Km/Liter'),
                                  ('litrhour', 'Liter/hour'),
                                  ('litr100km', 'Liter/100km'), ], 'Fuel Unit of Measure')
    fuel_low = fields.Float('Fuel low')
    fuel_medium = fields.Float('Fuel medium')
    fuel_high = fields.Float('Fuel high')
    power_avg = fields.Float('Power average')
    production_uom_ids = fields.One2many('production.uom.line', 'technic_norm_id')
    usage_uom_ids = fields.One2many('usage.uom.line', 'technic_norm_id', ondelete='cascade', string='Usage uom')
    bunker = fields.Boolean('Bunker')
    bunker_capacity = fields.Float('Received bunker capacity')

    _sql_constraints = [
        ('technic_norm_unique', 'UNIQUE(name)', 'Technic norm must be unique!'),
    ]

    @api.model
    def create(self, vals):
        categ_code = self.env["ir.sequence"].next_by_code("technic.norm")
        code_sz = 3
        seq_obj = self.env['ir.sequence']
        seq_vals = {
            u'padding': code_sz,
            u'code': categ_code,
            u'name': 'test',
            u'implementation': u'standard',
            u'company_id': 1,
            u'use_date_range': False,
            u'number_increment': 1,
            u'prefix': False,
            u'date_range_ids': [],
            u'number_next_actual': 0,
            u'active': True,
            u'suffix': False
        }
        seq_id = seq_obj.sudo().create(seq_vals)
        vals.update({'technic_norm_sequence_id': seq_id.id})
        return super(TechnicNorm, self).create(vals)
    
    @api.depends('brand_id', 'technic_type_id', 'technic_model_id', 'technic_submodel_id')
    def norm_name_get(self):
        for obj in self:
            brand = obj.brand_id.name if obj.brand_id.name else ''
            type = '/' + obj.technic_type_id.name if obj.technic_type_id.name else ''
            model = '/' + obj.technic_model_id.model_name if obj.technic_model_id.model_name else ''
            submodel = '/' + obj.technic_submodel_id.submodel_name if obj.technic_submodel_id.submodel_name else ''
            obj.name = brand + type + model + submodel

class UsageUomLine(models.Model):
    _name = 'usage.uom.line'
    _description = 'Usage unit of measure'

    technic_norm_id = fields.Many2one('technic.norm', 'technic norm id')
    usage_uom_id = fields.Many2one('usage.uom', 'Usage unit of measure', required=True, ondelete='cascade')
    product_uom_id = fields.Many2one('product.uom', 'Product unit of measure', readonly=True)

    @api.onchange("usage_uom_id")
    def onchange_usage_uom_id(self):
        for obj in self:
            if obj.usage_uom_id:
                usage_name_obj = self.usage_uom_id
                return {'value': {'product_uom_id': usage_name_obj.usage_uom_id.id}}
            else:
                return {}

    @api.model
    def create(self, vals):
        if 'usage_uom_id' in vals:
            usage_uom_obj = self.env['usage.uom'].browse(vals['usage_uom_id'])
            vals.update({'product_uom_id': usage_uom_obj.usage_uom_id.id})
        return super(UsageUomLine, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'usage_name' in vals:
            usage_uom_obj = self.env['usage.uom'].browse(vals['usage_name'])
            vals.update({'product_uom_id': usage_uom_obj.usage_uom_id.id})
        return super(UsageUomLine, self).write(vals)

    _sql_constraints = [
        ('usage_uom_line', 'UNIQUE(technic_norm_id,usage_uom_id)', 'You can define only one usage unit of measure!'),
    ]

class ProductionUomLine(models.Model):
    _name = "production.uom.line"
    _description = "Production unit of measure"

    technic_norm_id = fields.Many2one('technic.norm', 'technic norm id')
    production_norm = fields.Float('Production norm')
    product_uom_id = fields.Many2one('product.uom', 'Production UOM')


class LiquidSystemLine(models.Model):
    _name = "liquid.system.line"
    _description = "Liquid system"
    _rec_name = "liquid_system_id"
    _order = "liquid_system_capacity DESC"

    technic_norm_id = fields.Many2one('technic.norm', 'technic norm id')
    liquid_system_id = fields.Many2one('liquid.system', 'Liquid system name')
    liquid_system_capacity = fields.Float('Capacity')
