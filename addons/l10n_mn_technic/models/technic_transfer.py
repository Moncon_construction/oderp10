# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from odoo import api, fields, models, _
from datetime import datetime
from odoo.tools.translate import translate

class TechnicTransfer(models.Model):
    _name = 'technic.transfer'
    _description = 'Technic transfer'


    transfer_type = fields.Selection([('state', 'State'),
                                       ('ownership_type', 'Ownership Type'),
                                       ('partner', 'Partner'),
                                       ('ownership_company', 'Ownership Company'),
                                       ('ownership_department', 'Ownership Department'),
                                       ('respondent', 'Respondent'),
                                       ('current_company', 'Current Company'),
                                       ('current_department', 'Current Department'),
                                       ('current_respondent', 'Current Respondent'),
                                       ('location', 'Location'),
                                       ('usage_state', 'Usage state')], 'Transfer type')
    state = fields.Selection([('draft', 'Draft'),
                               ('ready', 'Ready'),
                               ('in_repair', 'In repair'),
                               ('stop', 'Stop')], 'State')
    ownership_type = fields.Selection([('own', 'Own'),
                                       ('leasing', 'Leasing'),
                                       ('partner', 'Partner'),
                                       ('rental', 'Rental')], 'ownership type')
    partner_id = fields.Many2one('res.partner', 'Partner')
    ownership_company_id = fields.Many2one('res.company', 'ownership company')
    ownership_department_id = fields.Many2one('hr.department', 'ownership department')
    respondent_id = fields.Many2one('hr.employee', 'Respondent')
    current_company_id = fields.Many2one('res.company', 'Current company')
    current_department_id = fields.Many2one('hr.department', 'Current department')
    current_respondent_id = fields.Many2one('hr.employee', 'Current respondent')
    location_id = fields.Many2one('res.country', 'Location')
    usage_state = fields.Selection([('in_usage', 'In usage'),
                                   ('in_reserve', 'In reserve'),
                                   ('in_rent', 'In rent'),
                                   ('in_pawn', 'In pawn')], 'Usage state')
    description = fields.Text('Description', required=True)

    @api.multi
    def action_transfer(self):
        self.transfer_technic()
        return True

    @api.multi
    def transfer_technic(self):
        for technic_id in self.env.context.get('active_ids'):
            vals = {}
            if self.transfer_type == 'ownership_type':
                # vals['ownership_type'] = transfer.ownership_type
                vals['ownership_type'] = self.ownership_type
                # self.create_ownership_history(cr, uid, ids, technic_id=technic_id, context=context)
                self.create_ownership_history(technic_id)
            if self.transfer_type == 'partner':
                vals['partner_id'] = self.partner_id.id
                self.create_ownership_history(technic_id)
            if self.transfer_type == 'ownership_company':
                vals['ownership_company_id'] = self.ownership_company_id.id
                self.create_ownership_history(technic_id)
            if self.transfer_type == 'ownership_department':
                vals['ownership_department_id'] = self.ownership_department_id.id
                self.create_ownership_history(technic_id)
            if self.transfer_type == 'respondent':
                vals['respondent_id'] = self.respondent_id.id
                self.create_ownership_history(technic_id)
            if self.transfer_type == 'current_company':
                vals['current_company_id'] = self.current_company_id.id
                self.create_ownership_history(technic_id)
            if self.transfer_type == 'current_department':
                vals['current_department_id'] = self.current_department_id.id
                self.create_ownership_history(technic_id)
            if self.transfer_type == 'current_respondent':
                vals['current_respondent_id'] = self.current_respondent_id.id
                self.create_ownership_history(technic_id)
            if self.transfer_type == 'location':
                vals['location_id'] = self.location_id.id
                self.create_ownership_history(technic_id)
            if self.transfer_type == 'state':
                vals['state'] = self.state
                self.create_state_history(technic_id)
            if self.transfer_type == 'usage_state':
                vals['usage_state'] = self.usage_state
                self.create_ownership_history(technic_id)
            self.env['technic'].browse(technic_id).write(vals)
        return True

    @api.multi
    def create_state_history(self, technic_id):
        vals = {
            'technic_id': technic_id,
            'state_name': self.state,
            'change_date': datetime.now(),
            'change_user_id': self.env.user.id,
            'description': self.description
        }
        self.env['technic.state.history'].create(vals)
        return True

    @api.multi
    def create_ownership_history(self, technic_id):
        change_value = ''
        if self.transfer_type == 'partner':
            change_value = self.partner_id.name
        if self.transfer_type == 'ownership_type':
            change_value = self.source_translate(dict(self.env['technic.transfer'].fields_get(allfields=['ownership_type'])['ownership_type']['selection']).get(self.ownership_type))
        if self.transfer_type == 'usage_state':
            change_value = self.source_translate(dict(self.env['technic.transfer'].fields_get(allfields=['usage_state'])['usage_state']['selection']).get(self.ownership_type))
        if self.transfer_type == 'ownership_company':
            change_value = self.ownership_company_id.name
        if self.transfer_type == 'ownership_department':
            change_value = self.ownership_department_id.name
        if self.transfer_type == 'respondent':
            change_value = self.respondent_id.name
        if self.transfer_type == 'current_company':
            change_value = self.current_company_id.name
        if self.transfer_type == 'current_department':
            change_value = self.current_department_id.name
        if self.transfer_type == 'current_respondent':
            change_value = self.current_respondent_id.name
        if self.transfer_type == 'location':
            change_value = self.location_id.name

        vals = {
            'technic_id': technic_id,
            'ownership_name': self.transfer_type,
            'change_ownership': change_value,
            'change_date': datetime.now(),
            'change_user_id': self.env.user.id,
            'description': self.description
        }
        self.env['technic.ownership.history'].create(vals)
        return True

    @api.multi
    def source_translate(self, source):
        '''
        Translation method for selection field
        '''
        # currently only in selection fields
        result = translate(self.env.cr, 'selection', self.env.context['lang'], source) or source
        return result
