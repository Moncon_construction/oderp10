# -*- coding: utf-8 -*-
from io import BytesIO
import base64
from datetime import datetime
from datetime import timedelta
import dateutil.parser
import pytz

import xlsxwriter

from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport
from odoo.addons.l10n_mn_report.tools.report_excel_fit_sheet_wrapper import ReportExcelFitSheetWrapper  # @UnresolvedImport

standby_stages = [('standings', 'Standings'),
            ('repair', 'Repair'),
            ('all', 'All stage')]

class ReportTechnicDowntimeWizard(models.TransientModel):
    _name = 'report.technic.downtime.wizard'

    def _default_start_date(self):
        now = datetime.now()
        month = timedelta(days=30)
        return now - month

    def _get_groups(self):
        return [('technic_id', _('Technic')),
                ('technic_type_id', _('Technic Type'))]

    standby_stage = fields.Selection(standby_stages, 'Report Type', default="standings", required=True)
    group_by = fields.Selection(lambda self: self._get_groups(), 'Group By')
    start_date = fields.Date('Start date', default=_default_start_date, required=True)
    end_date = fields.Date('End date', default=lambda *a: datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT), required=True)
    technic = fields.Many2one('technic')
    technic_type = fields.Many2one('technic.type')
    show_in_scrap = fields.Boolean('Show In Scrap', default=False)

    def validate_fields(self):
        end_date = datetime.strptime("{0}".format(self.end_date), DEFAULT_SERVER_DATE_FORMAT)
        if end_date > datetime.now():
            raise UserError(_('End date is invalid.'))
        start_date = datetime.strptime("{0}".format(self.start_date), DEFAULT_SERVER_DATE_FORMAT)
        if start_date > end_date:
            raise UserError(_('Start date must be less than end date.'))

    def load_data(self):
        datas = self.env['technic.state.history'].search([('change_date', '>=', self.start_date), ('change_date', '<=', self.end_date)])

        if self.standby_stage == 'standings':
            datas = datas.filtered(lambda r: r.state_name == 'stop')
        elif self.standby_stage == 'repair':
            datas = datas.filtered(lambda r: r.state_name == 'in_repair')
        else:
            datas = datas.filtered(lambda r: r.state_name == 'in_repair' or r.state_name == 'stop')

        if self.technic:
            datas = datas.filtered(lambda r: r.technic_id == self.technic)

        if self.technic_type:
            datas = datas.filtered(lambda r: r.technic_id.technic_type_id == self.technic_type)

        if not self.show_in_scrap:
            datas = datas.filtered(lambda r: r.technic_id.in_scrap == False)

        return datas.sorted(lambda r: r.change_date)

    def compute_column_number(self):
        colx_number = 6

        if self.technic or self.group_by == 'technic_id':
            colx_number -= 1

        return colx_number

    def create_filters(self, book, sheet, rowx, output_obj):
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        sheet.merge_range(rowx, 0, rowx, self.compute_column_number() / 2, '%s: %s' % (_('Report Type'), output_obj._(dict(standby_stages)[self.standby_stage])), format_filter)
        rowx += 1

        if self.group_by:
            sheet.merge_range(rowx, 0, rowx, self.compute_column_number() / 2, '%s: %s' % (_('Group By'), output_obj._(dict(self._get_groups())[self.group_by])), format_filter)
            rowx += 1

        if self.technic:
            sheet.merge_range(rowx, 0, rowx, self.compute_column_number() / 2, '%s: %s' % (_('Technic'), self.technic.name), format_filter)
            rowx += 1

        if self.technic_type:
            sheet.merge_range(rowx, 0, rowx, self.compute_column_number() / 2, '%s: %s' % (_('Technic type'), self.technic_type.name), format_filter)
            rowx += 1

        if self.show_in_scrap:
            sheet.merge_range(rowx, 0, rowx, self.compute_column_number() / 2, '%s: %s' % (_('Show In Scrap'), self.show_in_scrap), format_filter)
            rowx += 1

        return sheet, rowx

    def create_addional_titles(self, book, sheet, rowx, colx):
        return book, sheet, rowx, colx

        if not self.technic and self.group_by != 'technic':
            sheet.merge_range(rowx, colx, rowx + 1, colx, _('Technic'), format_title)
            colx += 1

        return book, sheet, rowx, colx

    def group_data(self):
        datas = self.load_data()
        if self.group_by == 'technic_id':
            datas = datas.sorted(key=lambda r: r.technic_id.name)
        elif self.group_by == 'technic_type_id':
            datas = datas.sorted(key=lambda r: r.technic_id.technic_type_id.name)
        if self.group_by == 'technic':
            datas = datas.sorted(key=lambda r: r.technic_id.name)
        elif self.group_by == 'technic_type':
            datas = datas.sorted(key=lambda r: r.technic_id.technic_type_id.name)
        return datas

    def write_groups(self, book, sheet, rowx, colx, line, current_group, num):
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        if self.group_by in ['technic_id', 'technic_type_id']:
            current_group_value = _('Undefined')
            if self.group_by == 'technic_id':
                if current_group != line[self.group_by]:
                    current_group = line.technic_id
                    current_group_value = line.technic_id.name
                    num = 1
                    sheet.merge_range(rowx, colx, rowx, self.compute_column_number() - 1, current_group_value, format_group)
                    rowx += 1
            elif self.group_by == 'technic_type_id':
                if current_group != line.technic_id.technic_type_id:
                    current_group = line.technic_id.technic_type_id
                    current_group_value = line.technic_id.technic_type_id.name
                    num = 1
                    sheet.merge_range(rowx, colx, rowx, self.compute_column_number() - 1, current_group_value, format_group)
                    rowx += 1

        if self.group_by == 'technic':
            if current_group != line.technic:
                current_group = line.technic_id
                num = 1
                current_group_value = line.technic_id.name or _('Undefined')
                sheet.merge_range(rowx, colx, rowx, self.compute_column_number() - 1, current_group_value, format_group)
                rowx += 1

        if self.group_by == 'technic_type':
            if current_group != line.technic.technic_type_id:
                current_group = line.technic.technic_type_id
                num = 1
                current_group_value = line.technic.technic_type_id.name or _('Undefined')
                sheet.merge_range(rowx, colx, rowx, self.compute_column_number() - 1, current_group_value, format_group)
                rowx += 1

        return sheet, rowx, colx, current_group, num

    def create_addional_lines(self, book, sheet, rowx, colx, line):
        return book, sheet, rowx, colx

        if not self.technic and self.group_by != 'technic':
            sheet.write_string(rowx, colx, line.technic_id.name if line.technic else '', format_content_text)
            colx += 1
        return book, sheet, rowx, colx

    @api.multi
    def export_report(self):

        # validate fields
        self.validate_fields()

        # load data
        datas = self.load_data()

        if len(datas) == 0:
            raise UserError(_('No result is found.'))

        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_date = book.add_format(ReportExcelCellStyles.format_date)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_group = book.add_format(ReportExcelCellStyles.format_group)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_date = book.add_format(ReportExcelCellStyles.format_content_date)

        # create name
        report_name = _('Downtime')

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix='donwtime', form_title=report_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_portrait()
        sheet.set_paper(6)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.hide_gridlines(2)
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        rowx = 0

        # compute column number
        colx_number = self.compute_column_number()

        # create name
        sheet.merge_range(rowx, 0, rowx, colx_number - 1, report_name.upper(), format_name)
        sheet.set_row(rowx, 30, format_name)
        rowx += 1

        # create filters
        sheet, rowx = self.create_filters(book, sheet, rowx, report_excel_output_obj)

        # create date time
        if self.env.user.partner_id.tz:
            tz = pytz.timezone(self.env.user.partner_id.tz)
        else:
            tz = pytz.utc
        now_utc = datetime.now(pytz.timezone('UTC'))
        now_user_zone = now_utc.astimezone(tz)
        report_datetime = '%s: %s' % (_('Created On'), now_user_zone.strftime('%Y-%m-%d %H:%M:%S'))
        sheet.merge_range(rowx - 1, colx_number / 2 + 2, rowx - 1, colx_number - 2, report_datetime, format_date)

        # create titles
        colx = 0

        sheet.merge_range(rowx, colx, rowx + 1, colx, u'№', format_title)
        colx += 1

        if self.group_by != 'technic_id' and not self.technic:
            sheet.merge_range(rowx, colx, rowx + 1, colx, _('Technic'), format_title)
            colx += 1

        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Interpretation'), format_title)
        colx += 1

        # additional title
        book, sheet, rowx, colx = self.create_addional_titles(book, sheet, rowx, colx)
        sheet.merge_range(rowx, colx, rowx, colx + 1, _('Use'), format_title)
        sheet.write(rowx + 1, colx, _('Measurement'), format_title)
        sheet.write(rowx + 1, colx + 1, _('Value'), format_title)
        colx += 2

        sheet.merge_range(rowx, colx, rowx + 1, colx, _('Date stopped'), format_title)
        colx += 1

        rowx += 2

        sheet.repeat_rows(0, rowx - 1)
        sheet.freeze_panes(rowx, 0)

        # group data
        datas = self.group_data()

        # CREATE LINES
        num = 1
        current_group = ''

        for line in datas:
            colx = 0

            # write group
            sheet, rowx, colx, current_group, num = self.write_groups(book, sheet, rowx, colx, line, current_group, num)

            # write line
            usage_length = len(line.technic_id.technic_usage_ids)

            if usage_length <= 1:
                sheet.write_number(rowx, colx, num, format_content_number)
                colx += 1

                if self.group_by != 'technic_id' and not self.technic:
                    sheet.write(rowx, colx, line.technic_id.name, format_content_text)
                    colx += 1

                sheet.write(rowx, colx, line.description, format_content_text)
                colx += 1
            else:
                sheet.merge_range(rowx, colx, rowx + usage_length - 1, colx, num, format_content_number)
                colx += 1

                if self.group_by != 'technic_id' and not self.technic:
                    sheet.merge_range(rowx, colx, rowx + usage_length - 1, colx, line.technic_id.name, format_content_text)
                    colx += 1

                sheet.merge_range(rowx, colx, rowx + usage_length - 1, colx, line.description, format_content_text)
                colx += 1

            usage_rowx = rowx
            for usage in line.technic_id.technic_usage_ids:
                sheet.write(usage_rowx, colx, usage.usage_uom_id.name, format_content_text)
                sheet.write(usage_rowx, colx + 1, usage.usage_value, format_content_number)
                usage_rowx += 1
            colx += 2

            if usage_length <= 1:
                sheet.write(rowx, colx, line.change_date, format_content_text)
                colx += 1
            else:
                sheet.merge_range(rowx, colx, rowx + usage_length - 1, colx, line.change_date, format_content_text)
                colx += 1

            num += 1

            if usage_length <= 1:
                rowx += 1
            else:
                rowx = rowx + usage_length

        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())

        # call export function
        return report_excel_output_obj.export_report()