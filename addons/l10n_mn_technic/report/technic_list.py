# -*- coding: utf-8 -*-
from StringIO import StringIO
import base64
import time

import xlwt
from odoo import models, fields, api, _

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport
from odoo.addons.l10n_mn_report.tools.report_excel_fit_sheet_wrapper import ReportExcelFitSheetWrapper  # @UnresolvedImport

report_types = [('summary', 'Summary'),
                ('detail', 'Detail')]
groups = [('technic_type_id', 'Technic Type'),
          ('ownership_type', 'Ownership'),
          ('state', 'State')]

class ReportTechnicListWizard(models.TransientModel):
    _name = 'report.technic.list.wizard'

    report_type = fields.Selection(report_types, 'Report Type', default="summary", required=True)
    group_by = fields.Selection(groups, 'Group By')
    show_in_scrap = fields.Boolean('Show In Scrap', default=False)

    def get_state(self, ownership_type):
        if ownership_type == 'draft':
            return u'Ноорог'
        if ownership_type == 'ready':
            return u'Бэлэн'
        if ownership_type == 'in_road':
            return u'Замд яваа'
        if ownership_type == 'in_store':
            return u'Хадгалалтанд'
        if ownership_type == 'to_check':
            return u'Оношлох'
        if ownership_type == 'in_repair':
            return u'Засварт'
        if ownership_type == 'in_rent':
            return u'Түрээсэнд'
        if ownership_type == 'not_available':
            return u'Ашиглагдах боломжгүй'
        if ownership_type == 'to_sell':
            return u'Худалдсан'
        if ownership_type == 'scrap':
            return u'Актлагдсан'

    def get_ownership(self, state):
        if state == 'own':
            return u'Өөрийн'
        if state == 'leasing':
            return u'Лизинг'
        if state == 'partner':
            return u'Харилцагч'
        if state == 'rental':
            return u'Түрээсийн'

    @api.multi
    def export_report(self):
        # create workbook
        book = xlwt.Workbook(encoding='utf8')

        # create sheet
        report_name = _('Technic List')
        sheet = ReportExcelFitSheetWrapper(book.add_sheet(report_name))
        rowx = 0

        # compute column number
        if self.report_type == "summary":
            colx_number = 8
        else:
            colx_number = 18
        if self.group_by:
            colx_number = colx_number - 1
        if self.show_in_scrap:
            colx_number = colx_number + 1

        # create header
        report_number = _('Report №%s') % 1
        sheet.write_merge(rowx, rowx, 1, colx_number, report_number, ReportExcelCellStyles.number_xf, size='number')
        rowx += 1

        # create name
        sheet.row(rowx).height_mismatch = True
        sheet.row(rowx).height = 24 * 20
        sheet.write_merge(rowx, rowx, 1, colx_number, report_name.upper(), ReportExcelCellStyles.name_xf, size='name')
        rowx += 1

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix='technic_list', form_title=report_name).create({})

        # create filters
        sheet.write_merge(rowx, rowx, 1, colx_number, '%s: %s' % (_('Report Type'), report_excel_output_obj._(dict(report_types)[self.report_type])), ReportExcelCellStyles.filter_xf, size='filter')
        rowx += 1
        if self.group_by:
            sheet.write_merge(rowx, rowx, 1, colx_number, '%s: %s' % (_('Group By'), report_excel_output_obj._(dict(groups)[self.group_by])), ReportExcelCellStyles.filter_xf, size='filter')
            rowx += 1

        # create date time
        report_datetime = '%s: %s' % (_('Created On'), time.strftime('%Y.%m.%d %H:%M:%S'))
        sheet.write_merge(rowx, rowx, 1, colx_number, report_datetime, ReportExcelCellStyles.datetime_xf, size='created_on')
        rowx += 1

        # load data
        if self.show_in_scrap:
            datas = self.env['technic'].search([])
        else:
            datas = self.env['technic'].search([('in_scrap', '=', False)])

        # create titles
        sheet.row(rowx).height_mismatch = True
        sheet.row(rowx).height = 18 * 20
        colx = 1
        sheet.write(rowx, colx, '№', ReportExcelCellStyles.title_xf, size='title', no_merge=True)
        colx += 1
        sheet.write(rowx, colx, _('Name'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
        colx += 1
        if self.group_by != "technic_type_id":
            sheet.write(rowx, colx, _('Type'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
        sheet.write(rowx, colx, _('Serial/VIN number'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
        colx += 1
        if self.report_type == "detail":
            sheet.write(rowx, colx, _('Manufacture Year'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
            sheet.write(rowx, colx, _('Engine Number'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
        sheet.write(rowx, colx, _('State number'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
        colx += 1
        if self.group_by != "ownership_type":
            sheet.write(rowx, colx, _('Ownership'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
        if self.report_type == "detail":
            sheet.write(rowx, colx, _('Partner'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
            sheet.write(rowx, colx, _('Owner Company'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
            sheet.write(rowx, colx, _('Owner Department'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
            sheet.write(rowx, colx, _('Responsible for'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
            sheet.write(rowx, colx, _('Current Company'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
            sheet.write(rowx, colx, _('Current Department'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
            sheet.write(rowx, colx, _('Current Responsible for'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
            sheet.write(rowx, colx, _('Location'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
        sheet.write(rowx, colx, _('Usage'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
        colx += 1
        if self.group_by != "state":
            sheet.write(rowx, colx, _('State'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
        if self.show_in_scrap:
            sheet.write(rowx, colx, _('In Scrap'), ReportExcelCellStyles.title_xf, size='title', no_merge=True)
            colx += 1
        rowx += 1

        # create lines
        if self.group_by:
            datas = datas.search([], order=self.group_by)
        num = 1
        current_group = ''
        for line in datas:
            colx = 1
            if self.group_by:
                if current_group != line[self.group_by]:
                    current_group = line[self.group_by]
                    num = 1
                    if self.group_by == 'technic_type_id':
                        if line[self.group_by].name_get():
                            current_group_value = line[self.group_by].name_get()[0][1]
                        else:
                            current_group_value = line[self.group_by]
                    if self.group_by == 'ownership_type':
                        current_group_value = self.get_ownership(line.ownership_type)
                    if self.group_by == 'state':
                        current_group_value = self.get_state(line.state)
                    sheet.write_merge(rowx, rowx, colx, colx_number, current_group_value, ReportExcelCellStyles.group_xf, size='group', group=True)
                    rowx += 1

            sheet.write(rowx, colx, num, ReportExcelCellStyles.content_number_xf, size='content', no_merge=True)
            colx += 1
            sheet.write(rowx, colx, line.name, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
            colx += 1
            if self.group_by != "technic_type_id":
                sheet.write(rowx, colx, line.technic_norm_id.technic_type_id.name, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
            sheet.write(rowx, colx, line.serial_vin_number, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
            colx += 1
            if self.report_type == "detail":
                sheet.write(rowx, colx, line.create_date, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
                sheet.write(rowx, colx, line.engine_number, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
            sheet.write(rowx, colx, line.state_number, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
            colx += 1
            if self.group_by != "ownership_type":
                sheet.write(rowx, colx, self.get_ownership(line.ownership_type), ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
            if self.report_type == "detail":
                sheet.write(rowx, colx, line.partner_id.name, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
                sheet.write(rowx, colx, line.ownership_company_id.name, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
                sheet.write(rowx, colx, line.ownership_department_id.name, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
                sheet.write(rowx, colx, line.respondent_id.name, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
                sheet.write(rowx, colx, line.current_company_id.name, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
                sheet.write(rowx, colx, line.current_department_id.name, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
                sheet.write(rowx, colx, line.current_respondent_id.name, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
                sheet.write(rowx, colx, line.location_id.name, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
            usages = ''
            count_usage = 0
            for usage in line.technic_usage_ids:
                if count_usage == 0:
                    usages = '%s: %s' % (usage.usage_uom_id.name, usage.usage_value)
                else:
                    usages = '%s\n%s: %s' % (usages, usage.usage_uom_id.name, usage.usage_value)
                count_usage += 1
            sheet.write(rowx, colx, usages, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
            colx += 1
            if self.group_by != "state":
                sheet.write(rowx, colx, self.get_state(line.state), ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
            if self.show_in_scrap:
                if line.in_scrap:
                    in_scrap_value = _('Yes')
                else:
                    in_scrap_value = _('No')
                sheet.write(rowx, colx, in_scrap_value, ReportExcelCellStyles.content_text_xf, size='content', no_merge=True)
                colx += 1
            num += 1
            rowx += 1

        # prepare file data
        io_buffer = StringIO()
        book.save(io_buffer)
        io_buffer.seek(0)
        filedata = base64.encodestring(io_buffer.getvalue())
        io_buffer.close()

        # set file data
        report_excel_output_obj.filedata = filedata

        # call export function
        return report_excel_output_obj.export_report()
