# -*- coding: utf-8 -*-
from StringIO import StringIO
import base64
import time
import xlwt
from odoo import models, fields, api, _

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport

class ReportTechnicPassportWizard(models.TransientModel):

    _name = 'report.technic.passport.wizard'

    technic = fields.Many2one('technic', string='Technic', required=True)

    @api.multi
    def export_report(self):
        self.ensure_one()

        # create workbook
        book = xlwt.Workbook(encoding='utf8')

        # create sheet
        report_name = _('Technical Passport')
        sheet = book.add_sheet(report_name)
        sheet.show_grid = False
        rowx = 0

        # compute column number
        colx_number = 8
        sheet.col(1).width = 3500
        sheet.col(2).width = 2600
        sheet.col(3).width = 3400
        sheet.col(4).width = 3000
        sheet.col(5).width = 3300
        sheet.col(6).width = 2700
        sheet.col(7).width = 2500
        sheet.col(8).width = 3500
        sheet.row(4).height = 256 * 3

        # create header
        report_number = _('Report №%s') % 1
        sheet.write_merge(rowx, rowx, 1, colx_number, report_number, ReportExcelCellStyles.number_xf)
        rowx += 1

        # create date time
        report_datetime = '%s: %s' % (_('Created On'), time.strftime('%Y.%m.%d %H:%M:%S'))
        sheet.write_merge(rowx, rowx, 1, colx_number, report_datetime, ReportExcelCellStyles.datetime_xf)
        rowx += 1

        # create name
        sheet.row(rowx).height_mismatch = True
        sheet.row(rowx).height = 24 * 20
        sheet.write_merge(rowx, rowx, 0, colx_number, report_name.upper(), ReportExcelCellStyles.name_xf)
        rowx += 2

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix='technic_passport', form_title=report_name).create({})

        # create main information
        sheet.write_merge(rowx, rowx, 1, 2, '%s:' % _('Technic'), ReportExcelCellStyles.filter_xf)
        sheet.write_merge(rowx, rowx, 3, 4, self.technic.name, ReportExcelCellStyles.filter_xf)
        sheet.write_merge(rowx, rowx, 5, 6, '%s:' % _('Usage state'), ReportExcelCellStyles.filter_xf)
        sheet.write_merge(rowx, rowx, 7, 8, self.technic.usage_state, ReportExcelCellStyles.filter_xf)
        rowx += 1
        sheet.write_merge(rowx, rowx, 1, 2, '%s:' % _('Model'), ReportExcelCellStyles.filter_xf)
        sheet.write_merge(rowx, rowx, 3, 4, self.technic.technic_model_id.name_get()[0][1], ReportExcelCellStyles.filter_xf)
        sheet.write_merge(rowx, rowx, 5, 6, '%s:' % _('Current respondent'), ReportExcelCellStyles.filter_xf)
        sheet.write_merge(rowx, rowx, 7, 8, report_excel_output_obj._(self.technic.current_respondent_id.name), ReportExcelCellStyles.filter_xf)
        rowx += 1
        sheet.write_merge(rowx, rowx, 1, 2, '%s:' % _('Serial/VIN number'), ReportExcelCellStyles.filter_xf)
        sheet.write(rowx, 3, self.technic.serial_vin_number, ReportExcelCellStyles.filter_xf)
        sheet.write_merge(rowx, rowx, 5, 6, '%s:' % _('Location'), ReportExcelCellStyles.filter_xf)
        sheet.write_merge(rowx, rowx, 7, 8, report_excel_output_obj._(self.technic.location_id.name), ReportExcelCellStyles.filter_xf)
        rowx += 1
        sheet.write_merge(rowx, rowx, 1, 2, '%s:' % _('State Number'), ReportExcelCellStyles.filter_xf)
        sheet.write(rowx, 3, self.technic.state_number, ReportExcelCellStyles.filter_xf)
        sheet.write_merge(rowx, rowx, 5, 6, '%s:' % _('Ownership company'), ReportExcelCellStyles.filter_xf)
        sheet.write_merge(rowx, rowx, 7, 8, report_excel_output_obj._(self.technic.ownership_company_id.name), ReportExcelCellStyles.filter_xf)
        rowx += 1
        sheet.write_merge(rowx, rowx, 1, 2, '%s:' % _('Manufacture Year'), ReportExcelCellStyles.filter_xf)
        sheet.write(rowx, 3, self.technic.manufacture_date, ReportExcelCellStyles.filter_xf)
        sheet.write_merge(rowx, rowx, 5, 6, '%s:' % _('Ownership Type'), ReportExcelCellStyles.filter_xf)
        sheet.write_merge(rowx, rowx, 7, 8, report_excel_output_obj._(self.technic.ownership_type), ReportExcelCellStyles.filter_xf)
        rowx += 2
        sheet.write_merge(rowx, rowx, 1, 2, _('Engine number'), ReportExcelCellStyles.filter_xf)
        sheet.write(rowx, 3, self.technic.engine_number, ReportExcelCellStyles.filter_xf)
        rowx += 1
        sheet.write_merge(rowx, rowx, 1, 2, _('Engine type'), ReportExcelCellStyles.filter_xf)
        sheet.write(rowx, 3, self.technic.technic_norm_id.engine_type, ReportExcelCellStyles.filter_xf)
        rowx += 1
        sheet.write_merge(rowx, rowx, 1, 2, _('Engine capacity'), ReportExcelCellStyles.filter_xf)
        sheet.write(rowx, 3, self.technic.technic_norm_id.engine_capacity, ReportExcelCellStyles.filter_xf)
        rowx += 2

        # create usage
        sheet.write_merge(rowx, rowx, 0, colx_number, _('Usage'), ReportExcelCellStyles.title_xf)
        rowx += 1
        colx = 0
        num = 1
        sheet.write(rowx, colx, '№', ReportExcelCellStyles.group_xf)
        sheet.write_merge(rowx, rowx, 1, 3, _('Usage name'), ReportExcelCellStyles.group_xf)
        sheet.write_merge(rowx, rowx, 4, 6, _('Usage unit of measure'), ReportExcelCellStyles.group_xf)
        sheet.write_merge(rowx, rowx, 7, 8, _('Usage value'), ReportExcelCellStyles.group_xf)
        rowx += 1

        usage_data = self.technic.technic_usage_ids
        if usage_data:
            for line in usage_data:
                sheet.col(0).width = 256 * (num + 2)
                sheet.write(rowx, colx, num, ReportExcelCellStyles.content_number_xf)
                sheet.write_merge(rowx, rowx, 1, 3, line.usage_uom_id.name, ReportExcelCellStyles.content_text_xf)
                sheet.write_merge(rowx, rowx, 4, 6, line.product_uom_id.name, ReportExcelCellStyles.content_text_xf)
                sheet.write_merge(rowx, rowx, 7, 8, line.usage_value, ReportExcelCellStyles.content_text_xf)
                num += 1
                rowx += 1
        rowx += 1

        # create document
        sheet.write_merge(rowx, rowx, 0, colx_number, _('Document'), ReportExcelCellStyles.title_xf)
        rowx += 1
        colx = 0
        num = 1
        sheet.write(rowx, colx, '№', ReportExcelCellStyles.group_xf)
        sheet.write_merge(rowx, rowx, 1, 2, _('Document type'), ReportExcelCellStyles.group_xf)
        sheet.write_merge(rowx, rowx, 3, 4, _('Document number'), ReportExcelCellStyles.group_xf)
        sheet.write_merge(rowx, rowx, 5, 6, _('Current respondent'), ReportExcelCellStyles.group_xf)
        sheet.write_merge(rowx, rowx, 7, 8, _('Create date'), ReportExcelCellStyles.group_xf)
        rowx += 1

        datas = self.technic.technic_document_ids
        if datas:
            for doc in datas:
                sheet.write(rowx, colx, num, ReportExcelCellStyles.content_number_xf)
                sheet.write_merge(rowx, rowx, 1, 2, doc.respondent_id.name_get()[0][1], ReportExcelCellStyles.content_text_xf)
                sheet.write_merge(rowx, rowx, 3, 4, doc.document_name, ReportExcelCellStyles.content_text_xf)
                sheet.write_merge(rowx, rowx, 5, 6, doc.respondent_id.name_get()[0][1], ReportExcelCellStyles.content_text_xf)
                sheet.write_merge(rowx, rowx, 7, 8, doc.create_date, ReportExcelCellStyles.content_text_xf)
                num += 1
                rowx += 1
        rowx += 1

        sheet, rowx = self.env['report.technic.passport.wizard'].write_components(self, sheet, rowx)
        # prepare file data
        io_buffer = StringIO()
        book.save(io_buffer)
        io_buffer.seek(0)
        filedata = base64.encodestring(io_buffer.getvalue())
        io_buffer.close()

        # set file data
        report_excel_output_obj.filedata = filedata

        # call export function
        return report_excel_output_obj.export_report()

    def write_components(self, obj, sheet, rowx):
        return sheet, rowx
