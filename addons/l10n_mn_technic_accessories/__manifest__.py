# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    "name": "Техникийн дагалдах тоноглол, багаж хэрэгсэл",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",

    'summary': """
        Техникийн дагалдах тоноглол, багаж хэрэгслийн бүртгэл""",

    "description": """
        Уг модуль нь техникийн дагалдах тоноглол, багаж хэрэгслийг бүртгэх боломжийг олгоно.
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": [
        'l10n_mn_technic',
    ],
    "data": [
        'views/technic_accessories_view.xml',
        'views/product_accessories_view.xml',
        'views/technic_view.xml',
    ],
    "demo_xml": [
    ],
    "active": False,
    "installable": True,

}
