# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import api, fields, models

class TechnicAccessories(models.Model):
    _name = 'technic.accessories'
    _description = 'Technic accessoreis'
    _rec_name = 'product_product_id'

    state = fields.Selection([('in_store', 'In store'),
                                   ('using', 'Using')], 'State', default='in_store')
    product_product_id = fields.Many2one('product.product', 'Product', domain="[('is_accessories', '=', True)]", required=True, help='Product accessories')
    technic_id = fields.Many2one('technic', 'Technic')
    accessories_model = fields.Char('Accessories model')
    accessories_feature = fields.Many2many('accessories.feature', 'technic_accessories_feature_rel', 'tech_access_id', 'access_feature_id', 'Accessories feature')


    @api.model
    def create(self, vals):
        vals.update({'state': 'using'})

        return super(TechnicAccessories, self).create(vals)
    @api.multi
    def write(self, vals):
        vals.update({'state': 'using'})

        return super(TechnicAccessories, self).create(vals)

class accessoriesFeature(models.Model):
    _name = 'accessories.feature'
    _description = 'Accessories feature'

    name = fields.Char('Feature name')
