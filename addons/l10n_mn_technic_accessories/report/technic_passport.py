# -*- coding: utf-8 -*-
from odoo import models, _

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport

class ReportTechnicPassportWizard(models.TransientModel):
    _inherit = 'report.technic.passport.wizard'

    def write_components(self, obj, sheet, rowx):
        sheet, rowx = super(ReportTechnicPassportWizard, self).write_components(obj, sheet, rowx)
        accessories = self.env['technic.accessories'].search([('technic_id', '=', obj.technic.id)])

        #create accessories
        datas_accessories = accessories

        if datas_accessories:
            accessories_datas = obj.env['technic.accessories'].search([])
            if accessories_datas:
                colx_number = 8
                sheet.write_merge(rowx, rowx, 0, colx_number, _('Accessories'), ReportExcelCellStyles.title_xf)
                rowx += 1
                colx = 0
                num = 1
                sheet.write(rowx, colx, '№', ReportExcelCellStyles.group_xf)
                sheet.write_merge(rowx, rowx, 1, 2, _('Name'), ReportExcelCellStyles.group_xf)
                sheet.write_merge(rowx, rowx, 3, 4, _('Mark'), ReportExcelCellStyles.group_xf)
                sheet.write_merge(rowx, rowx, 5, 8, _('Feature'), ReportExcelCellStyles.group_xf)
                rowx += 1

        datas_accessories = accessories
        if datas_accessories:
            for data_accessories in accessories_datas:
                sheet.write(rowx, colx, num, ReportExcelCellStyles.content_number_xf)
                sheet.write_merge(rowx, rowx, 1, 2, data_accessories.product_product_id.name, ReportExcelCellStyles.content_number_xf)
                sheet.write_merge(rowx, rowx, 3, 4, data_accessories.accessories_model, ReportExcelCellStyles.content_number_xf)
                sheet.write_merge(rowx, rowx, 5, 8, data_accessories.accessories_feature.name, ReportExcelCellStyles.content_number_xf)
                num += 1
                rowx += 1
        rowx += 1
        return sheet, rowx
