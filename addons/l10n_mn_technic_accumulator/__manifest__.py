# -*- coding: utf-8 -*-
{
    'name': "Aккумлятор",

    'summary': """
        Техникийн бүртгэлд аккумляторын мэдээлэл хөтлөх""",

    'description': """
        Техникийг бүртгэлд аккумлятор болон аккумляторын нормыг бүртгэснээр аккумляторын ашиглалтыг сайжруулах боломжийг энэ модуль олгоно.
    """,

    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Technic',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['l10n_mn_technic_parts'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',

        'views/technic_accumulator_views.xml',
        'views/technic_norm_views.xml',
        'views/technic_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo.xml',
    ],

    'contributors': ['Bayarkhuu Bataa <bayarkhuu@asterisk-tech.mn>'],
}
