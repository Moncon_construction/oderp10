# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class ProductTemplate(models.Model):
    _inherit = 'product.product'


    technic_parts_type = fields.Selection(selection_add=[('technic_accumulator', _('Technic accumulator'))])

