# -*- coding: utf-8 -*-
from odoo import models, fields, api

class Technic(models.Model):
    _inherit = 'technic'

    def _accumulator_history_count(self):
        for obj in self:
            obj.accumulator_history_count = len(self.env['technic.accumulator.history'].search([('change_technic', '=', obj.id)]))

    accumulators = fields.One2many('technic.accumulator', 'technic', "Accumulators")
    accumulator_history_count = fields.Integer(compute=_accumulator_history_count)

    @api.multi
    def write(self, values):
        # Cut link from removed accumulators
        accumulator_dic = values.get("accumulators", None)
        cut_accumulator_ids = []
        if accumulator_dic:
            # change remove to cut link
            for accumulator_triplet in accumulator_dic:
                if accumulator_triplet[0] == 2:
                    accumulator_triplet[0] = 3
                    cut_accumulator_ids.append(accumulator_triplet[1])
            # update values
            values["accumulators"] = accumulator_dic

        # Call super write
        ret = super(Technic, self).write(values)

        # Update cut accumulators
        cut_accumulators = self.env['technic.accumulator'].search([('id', 'in', cut_accumulator_ids)])
        for cut_accumulator in cut_accumulators:
            # change state
            cut_accumulator.state = 'in_reserve'
            # create accumulator history
            cut_accumulator.create_accumulator_history(None, 'in_reserve', 'Accumulator is removed from technic registration.')

        return ret


class TechnicUsage(models.Model):
    _inherit = 'technic.usage'

    @api.multi
    def write(self, vals):
        res = super(TechnicUsage, self).write(vals)

        # update accumulator usage
        measurement_id = vals.get('usage_uom_id')
        value = vals.get('usage_value')
        technic_id = vals.get('technic_id')

        if measurement_id and value and technic_id:
            technic = self.env['technic'].browse(technic_id)
            measurement = self.env['usage.uom'].browse(measurement_id)
            for accumulator in technic.accumulators:
                has_install_usage = False
                install_technic_usage_value = 0
                install_history = accumulator.history_lines.filtered(lambda r: r.change_technic == technic).sorted(key=lambda r: r.create_date, reverse=True)

                if install_history:
                    install_technic_usage = install_history[0].change_technic_usages.filtered(lambda r: r.measurement == measurement)

                if install_technic_usage:
                    install_technic_usage_value = install_technic_usage[0].value
                    has_install_usage = True

                if has_install_usage:
                    current_usage = accumulator.usage_lines.filtered(lambda r: r.usage_measurement == measurement)

                    if current_usage:
                        previous_history = None
                        histories_technic_change = accumulator.history_lines.filtered(lambda r: r.change_technic).sorted(key=lambda r: r.create_date, reverse=True)
                        if histories_technic_change:
                            history_last_technic_change = histories_technic_change[0]
                            all_previous_history = accumulator.history_lines.filtered(lambda r: r.id < history_last_technic_change.id).sorted(key=lambda r: r.create_date, reverse=True)
                            previous_history = all_previous_history.filtered(lambda r: r.change_measurement == measurement)

                        additional_value = value - install_technic_usage_value

                        if additional_value < 0.0:
                            additional_value = 0.0
                        if previous_history:
                            previous_history_value = previous_history[0].change_value
                            current_usage.current_usage = previous_history_value + additional_value
                        else:
                            current_usage.current_usage = additional_value
        return res
