# -*- coding: utf-8 -*-
import datetime

from odoo import models, fields, api
from odoo import exceptions
from odoo.tools.translate import _
import time
from datetime import date, datetime
from dateutil import relativedelta


class TechnicAccumulatorNorm(models.Model):
    _name = 'technic.accumulator.norm'
    _description = 'Accumulator norm'
    _rec_name = 'usage_measurement'

    technic_norm = fields.Many2one('technic.type', 'Technic norm')
    product = fields.Many2one('product.product', 'Accumulator', required=True, domain="[('technic_parts_type','=','technic_accumulator')]")
    usage_measurement = fields.Many2one('usage.uom', 'Usage measurement', required=True)
    uom = fields.Many2one('product.uom', 'UOM', related='usage_measurement.usage_uom_id', readonly=True)
    norm = fields.Float('Norm value', required=True)

    _sql_constraints = [
        ('tn_acc_um_unique',
         'UNIQUE(technic_norm, product, usage_measurement)',
         "Already exists norm for this accumulator and measurement!"),

        ('norm_check',
         'CHECK(norm > 0)',
         "Norm value must be greater than 0."),
    ]

    @api.multi
    @api.constrains('product', 'usage_measurement')
    def _check_unique_norm(self):
        for rec in self:
            if not rec.technic_norm:
                if self.env['technic.accumulator.norm'].search_count([('product', '=', rec.product.id), ('usage_measurement', '=', rec.usage_measurement.id)]) > 1:
                    raise exceptions.ValidationError(_('Already exists accumulator norm for this measurement!'))

    # @api.onchange('technic_norm')
    # def _onchange_technic_norm(self):
    #     usage_measurements = self.env['usage.uom'].search([])
    #     if self.technic_norm:
    #         norm_usage_measurements_ids = []
    #         for line in self.technic_norm.usage_uom_ids:
    #             norm_usage_measurements_ids.append(line.usage_uom_id.id)
    #         usage_measurements = usage_measurements.browse(norm_usage_measurements_ids)
    #     self.usage_measurement = None
    #     return {'domain': {'usage_measurement': [('id', 'in', usage_measurements.ids)]}}

    def _read_group(self):
        if 'norm' in fields:
            fields.remove('norm')
        return super(TechnicAccumulatorNorm, self).read_group()

    @api.model
    def create(self, values):
        accumulator_norm = super(TechnicAccumulatorNorm, self).create(values)
        # check flag
        self.set_accumulator_flag(accumulator_norm.product)
        return accumulator_norm

    @api.multi
    def write(self, values):
        ret = super(TechnicAccumulatorNorm, self).write(values)
        # check flag
        for record in self:
            self.set_accumulator_flag(record.product)
        return ret

    def set_accumulator_flag(self, product):
        product.technic_parts_type = 'technic_accumulator'


class TechnicAccumulator(models.Model):
    _name = 'technic.accumulator'
    _description = 'Accumulator'

    def _default_project(self):
        projects = self.env['project.project'].search([('use_technic', '=', True), ('user_id', 'in', [self.env.user.id])])
        if len(projects) == 1:
            return projects[0]
        else:
            return None

    def _history_count(self):
        self.usage_history_count = len(self.env['technic.accumulator.history'].search([('accumulator_id', '=', self.id), ('change_measurement', '!=', False)]))
        self.state_history_count = len(self.env['technic.accumulator.history'].search([('accumulator_id', '=', self.id), ('change_state', '!=', False)]))
        self.technic_history_count = len(self.env['technic.accumulator.history'].search([('accumulator_id', '=', self.id), ('change_technic', '!=', False)]))

    @api.multi
    @api.depends('usage_percent')
    def _compute_norm_cost(self):
        for rec in self:
            rec.norm_cost = rec.expense_cost * rec.usage_percent / 100

    @api.multi
    @api.depends('date')
    def _compute_running_duration(self):
        for rec in self:
            if rec.date:
                date = datetime.strptime(rec.date, '%Y-%m-%d %H:%M:%S')
                today = datetime.strptime(time.strftime('%Y-%m-%d'), '%Y-%m-%d')
                date_diff = relativedelta.relativedelta(today, date)
                duration_str = ""
                if date_diff.years:
                    duration_str = str(date_diff.years) + u' жил'
                if date_diff.months:
                    duration_str += "\t" + str(date_diff.months) + u' сар'
                if date_diff.days:
                    duration_str += "\t" + str(date_diff.days) + u' өдөр'
                rec.spent_duration = date_diff.years * 12 + date_diff.months + date_diff.days / 30.0
            else:
                rec.spent_duration = 0

    @api.multi
    @api.depends('kilometer')
    def _compute_running_km(self):
        for rec in self:
            if rec.technic:
                technic_usage = self.env['technic.usage'].search([('technic_id', '=', rec.technic.id)])
                if technic_usage:
                    if technic_usage.usage_uom_id.is_kilometer:
                        rec.running_km = technic_usage.usage_value - rec.kilometer

                else:
                    rec.running_km = 0

    @api.multi
    @api.depends('motohour')
    def _compute_running_motohour(self):
        for rec in self:
            if rec.technic:
                technic_usage = self.env['technic.usage'].search([('technic_id', '=', rec.technic.id)])
                if technic_usage:
                    if technic_usage.usage_uom_id.is_motohour:
                        rec.running_motohour = technic_usage.usage_value - rec.motohour
                    else:
                        rec.running_motohour = 0

    technic = fields.Many2one('technic', 'Technic')
    product = fields.Many2one('product.product', 'Accumulator', required=True, domain="[('technic_parts_type','=','technic_accumulator')]")
    serial = fields.Char('Serial number')
    name = fields.Char(compute='_compute_name')
    state = fields.Selection([('in_use', 'In use'),
                              ('in_scrap', 'In scrap')], 'State', default='in_use')
    date = fields.Datetime('Registration date')
    usage_lines = fields.One2many('technic.accumulator.usage', 'accumulator_id', 'Usage lines')
    history_lines = fields.One2many('technic.accumulator.history', 'accumulator_id', 'Usage history lines')
    usage_history_count = fields.Integer(compute=_history_count)
    state_history_count = fields.Integer(compute=_history_count)
    technic_history_count = fields.Integer(compute=_history_count)
    norm = fields.Many2one('technic.accumulator.norm', 'Norm', compute='_compute_norm')
    usage_percent = fields.Float(string='Usage Percent', compute='_compute_usage_percent')
    project = fields.Many2one('project.project', 'Project', domain="[('use_technic','=',True)]",
                              default=_default_project)

    type = fields.Selection([('left', 'Left'), ('right', 'Right')], 'Note type', help='ACCUMULATOR PILE LOCATION')
    voltmeter = fields.Integer('Voltmeter')
    capacity = fields.Integer('Capacity', help='MEASURE BY AH')
    length = fields.Integer('Length', help='MEASURING BY MM')
    weight = fields.Integer('Weight', help='MEASURING BY MM')
    height = fields.Integer('Height', help='MEASURING BY MM')
    description = fields.Text('Description')
    
    qty = fields.Float(string='Quantity', required=True)
    expense_cost = fields.Float('Expense Cost', required=True)
    with_technic = fields.Boolean('Comes with technic')

    motohour = fields.Float('Motohour')
    kilometer = fields.Float('Kilometer')

    norm_cost = fields.Float('Norm Cost', compute='_compute_norm_cost')
    spent_duration = fields.Float(string='Current Duration /month/', compute='_compute_running_duration')
    running_km = fields.Float(string='Running Kilometer', compute='_compute_running_km')
    running_motohour = fields.Float(string='Running Motohour', compute='_compute_running_motohour')

    @api.onchange('technic')
    def onchange_technic(self):
        technic_usage = self.env['technic.usage'].search([('technic_id', '=', self.technic.id)])
        if technic_usage:
            for obj in technic_usage:
                if obj.usage_uom_id.is_kilometer:
                    self.kilometer = obj.usage_value
                else:
                    self.motohour = obj.usage_value

    @api.multi
    @api.depends('product', 'technic')
    def _compute_norm(self):
        for rec in self:
            if rec.technic:
                norms = self.env['technic.accumulator.norm'].search(
                    [('product', '=', rec.product.id), ('technic_norm', '=', rec.technic.technic_type_id.id)])
                if norms:
                    for norm in norms:
                        rec.norm = norm.id
                        break
                else:
                    product_norm = self.env['technic.accumulator.norm'].search(
                        [('product', '=', rec.product.id), ('technic_norm', '=', None)])
                    if product_norm:
                        rec.norm = product_norm.id
                        break
            else:
                norms = self.env['technic.accumulator.norm'].search(
                    [('product', '=', rec.product.id), ('technic_norm', '=', None)])
                for norm in norms:
                    rec.norm = norm.id
                    break

    @api.multi
    @api.depends('product', 'technic', 'norm')
    def _compute_usage_percent(self):
        for rec in self:
            if rec.norm and rec.norm.norm:
                if rec.norm.usage_measurement.is_motohour:
                    rec.usage_percent = rec.running_motohour / rec.norm.norm * 100
                elif rec.norm.usage_measurement.is_kilometer:
                    rec.usage_percent = rec.running_km / rec.norm.norm * 100
                elif rec.norm.usage_measurement.is_time:
                    rec.usage_percent = rec.spent_duration / rec.norm.norm * 100
            else:
                rec.usage_percent = 0

    @api.multi
    @api.depends('product', 'serial')
    def _compute_name(self):
        for rec in self:
            rec.name = u'{0} / {1}'.format(rec.product.name, rec.serial)

    _sql_constraints = [
        ('serial_unique',
         'UNIQUE(serial)',
         "Serial number must be unique!"),
    ]

    @api.multi
    def unlink(self):
        # check used accumulator
        for record in self:
            for usage_line in record.usage_lines:
                if usage_line.current_usage > 0:
                    raise exceptions.ValidationError("Can't delete used accumulator. You can scrap the accumulator.")
        # call super
        ret = super(TechnicAccumulator, self).unlink()
        return ret

    @api.model
    def create(self, vals):
        # call super
        accumulator = super(TechnicAccumulator, self).create(vals)
        description = _('Accumulator registered.')
        # create usages
        accumulator.create_non_exist_usages()
        # create accumulator state history
        self.env['technic.accumulator.history'].create({
            'accumulator_id': accumulator.id,
            'change_state': vals.get('state'),
            'description': description
        })
        # create accumulator technic history
        if vals.get('technic'):
            history = self.env['technic.accumulator.history'].create({
                'accumulator_id': accumulator.id,
                'change_technic': accumulator.technic.id,
                'description': description
            })
            for technic_usage in accumulator.technic.technic_usage_ids:
                self.env['technic.accumulator.history.technic.usage'].create({
                    'history': history.id,
                    'measurement': technic_usage.usage_uom_id.id,
                    'value': technic_usage.usage_value
                })
        # check flag
        self.set_accumulator_flag(accumulator.product)
        # return
        return accumulator

    @api.multi
    def write(self, vals):
        description = _('Accumulator information changed.')
        for accumulator in self:
            # create accumulator state history
            if accumulator.state != vals.get('state'):
                self.env['technic.accumulator.history'].create({
                    'accumulator_id': accumulator.id,
                    'change_state': vals.get('state'),
                    'description': description
                })
            # create accumulator technic history
            if 'technic' in vals:
                if vals['technic']:
                    new_technic = self.env['technic'].browse(vals['technic'])
                    if new_technic != accumulator.technic:
                        history = self.env['technic.accumulator.history'].create({
                            'accumulator_id': accumulator.id,
                            'change_technic': new_technic.id,
                            'description': description
                        })
                        for technic_usage in new_technic.technic_usage_ids:
                            self.env['technic.accumulator.history.technic.usage'].create({
                                'history': history.id,
                                'measurement': technic_usage.usage_uom_id.id,
                                'value': technic_usage.usage_value
                            })
                else:
                    self.env['technic.accumulator.history'].create({
                        'accumulator_id': accumulator.id,
                        'change_technic': None,
                        'description': description
                    })
        # call super
        result = super(TechnicAccumulator, self).write(vals)
        # create usages
        self.create_non_exist_usages()
        # check flag
        for record in self:
            self.set_accumulator_flag(record.product)
        # return
        return result

    @api.multi
    def create_non_exist_usages(self):
        '''
        Must call this method before every usage update
        '''
        for accumulator in self:
            if accumulator.technic:
                TechnicAccumulatorUsage = self.env['technic.accumulator.usage']
                technic_norm_measurements = self.env['usage.uom.line'].search([('technic_norm_id', '=', accumulator.technic.technic_norm_id.id)])
                for measurement in technic_norm_measurements:
                    if TechnicAccumulatorUsage.search_count([('accumulator_id', '=', accumulator.id), ('usage_measurement', '=', measurement.usage_uom_id.id)]) == 0:
                        usage_values = {
                            'accumulator_id': accumulator.id,
                            'usage_measurement': measurement.usage_uom_id.id
                        }
                        TechnicAccumulatorUsage.create(usage_values)

    def set_accumulator_flag(self, product):
        product.technic_parts_type = 'technic_accumulator'

class TechnicAccumulatorUsage(models.Model):
    _name = 'technic.accumulator.usage'
    _description = 'Usage line'

    accumulator_id = fields.Many2one('technic.accumulator', 'Accumulator', ondelete='cascade')
    usage_measurement = fields.Many2one('usage.uom', 'Usage measurement', required=True)
    uom = fields.Many2one('product.uom', 'UOM', related='usage_measurement.usage_uom_id', readonly=True)
    current_usage = fields.Float('Current usage', required=True, default=0)

    _sql_constraints = [
        ('acc_um_unique',
         'UNIQUE(accumulator_id, usage_measurement)',
         "So many occurrences of certain usage measurement is not available for an accumulator!"),

        ('usage_check',
         'CHECK(current_usage >= 0)',
         "Current usage must be greater than or equal to 0."),
    ]

    @api.multi
    def write(self, vals):
        # create usage history
        for usage in self:
            history_values = {
                'accumulator_id': usage.accumulator_id.id,
                'change_measurement': usage.usage_measurement.id,
                'change_value': vals.get('current_usage'),
                'change_measurement_technic': usage.accumulator_id.technic.id
            }
            self.env['technic.accumulator.history'].create(history_values)
        return super(TechnicAccumulatorUsage, self).write(vals)

class TechnicAccumulatorHistory(models.Model):
    _name = 'technic.accumulator.history'
    _description = 'Technic accumulator history'
    _order = 'create_date DESC'

    accumulator_id = fields.Many2one('technic.accumulator', 'Accumulator', ondelete='cascade')
    change_measurement = fields.Many2one('usage.uom', 'Change measurement')
    change_unit = fields.Many2one('product.uom', 'Change measurement unit', related='change_measurement.usage_uom_id', store=True)
    change_value = fields.Float('Change measurement value')
    change_measurement_technic = fields.Many2one('technic', 'Change measurement technic')
    change_technic = fields.Many2one('technic', 'Change technic')
    change_technic_usages = fields.One2many('technic.accumulator.history.technic.usage', 'history', 'Change technic usage')
    change_state = fields.Selection([
        ('in_use', 'In use'),
        ('in_scrap', 'In scrap')], 'Change state')
    description = fields.Text('Description')

    @api.model
    def create(self, vals):
        accumulator = self.env['technic.accumulator'].browse(vals.get('accumulator_id'))
        # check whether measurement value is changed or value is 0
        if 'change_measurement' in vals:
            if vals.get('change_value'):
                current_usage = accumulator.usage_lines.filtered(lambda r: r.usage_measurement.id == vals['change_measurement'])
                if current_usage.current_usage == vals['change_value']:
                    return
            else:
                return
        return super(TechnicAccumulatorHistory, self).create(vals)

class TechnicAccumulatorHistoryTechnicUsage(models.Model):
    _name = 'technic.accumulator.history.technic.usage'
    _description = 'Change technic usage'

    history = fields.Many2one('technic.accumulator.history', 'Accumulator history', ondelete='cascade')
    measurement = fields.Many2one('usage.uom', 'Change technic measurement')
    unit = fields.Many2one('product.uom', 'Change technic measure unit', related='measurement.usage_uom_id', store=True)
    value = fields.Float('Change technic measurement value')
