# -*- coding: utf-8 -*-
from odoo import models, fields


class TechnicNorm(models.Model):
    _inherit = 'technic.norm'

    accumulator_norms = fields.One2many('technic.accumulator.norm', 'technic_norm', "Accumulator norms")
