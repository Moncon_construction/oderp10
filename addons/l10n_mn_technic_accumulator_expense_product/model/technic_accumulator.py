# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from datetime import datetime
from odoo import models, fields, api, _
from datetime import datetime, time
from odoo.exceptions import UserError
import logging

_logger = logging.getLogger('STOCK')


class TechnicAccumulator(models.Model):
    _inherit = 'technic.accumulator'

    def _accumulator_expense_count(self):
        for this in self:
            this.accumulator_expense_count = len(self.env['product.expense'].search([('accumulator_expense', '=', this.id)]))

    accumulator_expense_count = fields.Integer(compute=_accumulator_expense_count, string='Expense count')

class TechnicAccumulatorHistory(models.Model):
    _inherit = 'technic.accumulator.history'

    change_state = fields.Selection([('draft', 'Draft'),
                                     ('in_use', 'In use'),
                                     ('in_scrap', 'In scrap')], 'Change state')


class ExpenseProduct(models.Model):
    _inherit = 'product.expense'

    accumulator_expense = fields.Many2one('technic.accumulator', 'Expense product')


class StockImmediateTransfer(models.TransientModel):
    _inherit = 'stock.immediate.transfer'

    @api.one
    def process(self):
        self.ensure_one()
        this = super(StockImmediateTransfer, self).process()
        stock_picking = self.pick_id

        if stock_picking.expense.is_technic_expense:
            if stock_picking.expense.technic_id:
                technic_id = stock_picking.expense.technic_id.id
                employee_id = stock_picking.expense.employee.id
                for line in stock_picking.move_lines:
                    if not line.product_id.technic_parts_type:
                        raise UserError(_("This product is not technic part: %s") % line.product_id.default_code)
                    elif line.product_id.technic_parts_type == 'technic_accumulator':
                        value = {'product': line.product_id.id,
                                 'serial': line.product_id.finance_code,
                                 'technic': technic_id,
                                 'date': self.force_date or time.strftime('%Y-%m-%d'),
                                 'name': stock_picking.expense.name,
                                 'description': stock_picking.expense.name,
                                 'move_id': line.id,
                                 'qty': line.product_qty,
                                 'state': 'in_use',
                                 'expense_cost': line.price_unit,
                                 'motohour': stock_picking.expense.technic_id.last_motohour,
                                 'kilometer': stock_picking.expense.technic_id.last_km,
                                 }

                        self.env['technic.accumulator'].create(value)
        return this
