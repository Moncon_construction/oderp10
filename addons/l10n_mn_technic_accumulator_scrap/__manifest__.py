# -*- coding: utf-8 -*-
{
    'name': "Аккумляторын акт",

    'summary': """
        Аккумляторын акт""",

    'description': """
        Аккумляторыг актлах хүсэлтийг бүртгэх, уг хүсэлтийг тодорхой урсгалын дагуу батлах үйлдлүүдийг энэ модулиар гүйцэтгэх боломжтой болно.
    """,

    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Technic',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'l10n_mn_technic_accumulator', 'l10n_mn_workflow_config', 'l10n_mn_technic_parts_scrap'],

    # always loaded
    'data': [
        'views/sequence.xml',
        'views/technic_accumulator_scrap_wizard.xml',
        'views/technic_accumulator_scrap_views.xml',
        'views/technic_accumulator_scrap_workflow.xml',
        'security/ir.model.access.csv',
        'security/technic_accumulator_scrap_security.xml',
        'views/accumulator_scrap_reason_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo.xml',
    ],

    'contributors': ['Bayarkhuu Bataa <bayarkhuu@asterisk-tech.mn>'],
}
