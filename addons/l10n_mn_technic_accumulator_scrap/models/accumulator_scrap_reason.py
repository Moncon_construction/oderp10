# -*- coding: utf-8 -*-
################################
#
#
#
################################
from odoo import models, fields, api

class AccumulatorScrapReason(models.Model):
    _name = 'accumulator.scrap.reason'

    name = fields.Char('name')
    note = fields.Text('Description')