# -*- coding: utf-8 -*-
import datetime

from odoo import models, fields, api
from odoo import exceptions
from odoo.tools.translate import _

class payableScrapRespondentsParts(models.Model):
    _inherit = 'payable.scrap.respondents.parts'

    accumulator_scrap_id = fields.Many2one('technic.accumulator.scrap', 'Accumulator Scrap')
    accumulator_cost = fields.Float(compute='_compute_cost_accumulator')

    @api.multi
    @api.depends('accumulator_scrap_id')
    def _compute_cost_accumulator(self):
        for rec in self:
            total = 0
            for acc in rec.accumulator_scrap_id.accumulators:
                total += acc.expense_cost - acc.norm_cost
            rec.accumulator_cost = total / 100 * rec.percent

class TechnicAccumulatorScrap(models.Model):
    _name = 'technic.accumulator.scrap'
    _description = 'Accumulator scrap'
    _order = 'date desc'
    _inherit = ['mail.thread']
    _rec_name = 'origin'

    origin = fields.Char('Accumulator scrap Reference', readonly=True, copy=False)
    date = fields.Datetime('Request date', default=lambda *a: datetime.datetime.now())
    technic = fields.Many2one('technic', 'Technic')
    accumulators = fields.Many2many('technic.accumulator', 'accumulator_accumulator_scrap_rel', 'scrap_id', 'accumulator_id', string='Accumulator',
                               domain="[('in_scrap', '!=', True), ('technic', '=', technic)]")
    created_user = fields.Many2one('res.users', 'Created user', default=lambda self: self.env.user)
    state = fields.Selection([('draft', 'Draft'),
                              ('waiting_approval', 'Waiting Approval'),
                              ('approved', 'Approved'),
                              ('cancelled', 'Cancelled'),
                              ('returned', 'Returned'),
                              ('done', 'Done')], 'State', default='draft')
    description = fields.Text('Description', required=True)
    workflow_id = fields.Many2one('workflow.config', 'Workflow')
    check_sequence = fields.Integer('Workflow Step', default=0)
    history_lines = fields.One2many('technic.accumulator.scrap.workflow.history', 'scrap_id', 'Workflow History Line')
    is_validator = fields.Boolean(compute='_compute_is_validator')
    is_creator = fields.Boolean(compute='_compute_is_creator')
    is_payable = fields.Boolean('Is payable')
    respondents = fields.One2many('payable.scrap.respondents.parts', 'accumulator_scrap_id', 'Employee')
    count_acc_account_invoice = fields.Integer(compute="_count_acc_account_invoice", string='Invoice')

    @api.multi
    def _count_acc_account_invoice(self):
        for this in self:
            this.count_acc_account_invoice = len(self.env['account.invoice'].search([('acc_scrap_id', '=', this.id)]))


    @api.multi
    @api.depends('check_sequence')
    def _compute_is_validator(self):
        for rec in self:
            history_obj = self.env['technic.accumulator.scrap.workflow.history']
            # get current validators
            validators = history_obj.search([('scrap_id', '=', rec.id), ('line_sequence', '=', rec.check_sequence)], limit=1, order='sent_date DESC').user_ids
            if self.env.user in validators:
                rec.is_validator = True
            else:
                rec.is_validator = False

    @api.multi
    def _compute_is_creator(self):
        for rec in self:
            if rec.created_user == self.env.user:
                rec.is_creator = True
            else:
                rec.is_creator = False

    @api.onchange('technic')
    def onchange_technic(self):
        self.accumulators = None

    @api.model
    def create(self, vals):
        if vals.get('origin', '-') == '-':
            vals['origin'] = self.env['ir.sequence'].next_by_code('technic.accumulator.scrap')
        creation = super(TechnicAccumulatorScrap, self).create(vals)
        employee = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        workflow_id = self.env['workflow.config'].get_workflow('employee', 'technic.accumulator.scrap', employee.id, None)
        if workflow_id:
            creation.workflow_id = workflow_id
            for accumulator in creation.accumulators:
                accumulator.in_scrap = True
        else:
            raise exceptions.Warning(_('There is no workflow defined!'))
        return creation

    @api.multi
    def action_set_to_draft(self):
        self.ensure_one()
        self.check_sequence = 0
        self.state = 'draft'

    @api.multi
    def action_send(self):
        self.ensure_one()
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].send('technic.accumulator.scrap.workflow.history', 'scrap_id', self, self.created_user.id)
            if success:
                self.check_sequence = current_sequence
                self.state = 'waiting_approval'

    @api.multi
    def action_return(self):
        self.ensure_one()
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].action_return('technic.accumulator.scrap.workflow.history', 'scrap_id', self, self.env.user.id)
            if success:
                self.check_sequence = current_sequence

    @api.multi
    def action_refuse(self):
        self.ensure_one()
        if self.workflow_id:
            success = self.env['workflow.config'].reject('technic.accumulator.scrap.workflow.history', 'scrap_id', self, self.env.user.id)
            if success:
                self.state = 'refused'

    @api.multi
    def action_approve(self):
        self.ensure_one()
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve('technic.accumulator.scrap.workflow.history', 'scrap_id', self, self.env.user.id)
            if success:
                if sub_success:
                    self.state = 'approved'
                else:
                    self.check_sequence = current_sequence

    @api.multi
    def action_finish(self):
        self.ensure_one()
        self.state = 'done'

class TechnicAccumulatorScrapWorkflowHistory(models.Model):
    _name = 'technic.accumulator.scrap.workflow.history'
    _description = 'Technic Accumulator Scrap Workflow History'
    _order = 'scrap_id, sent_date'

    STATE_SELECTION = [
        ('waiting', 'Waiting'),
        ('confirmed', 'Confirmed'),
        ('approved', 'Approved'),
        ('return', 'Return'),
        ('rejected', 'Rejected'),
    ]

    scrap_id = fields.Many2one('technic.accumulator.scrap', 'Scrap Request', readonly=True, ondelete='cascade')
    name = fields.Char('Verification Step', readonly=True)
    user_ids = fields.Many2many('res.users', 'res_users_technic_accumulator_scrap_workflow_history_ref', 'history_id', 'user_id', 'Validators')
    sent_date = fields.Datetime('Sent date', required=True, readonly=True)
    user_id = fields.Many2one('res.users', 'Validator', readonly=True)
    action_date = fields.Datetime('Action date', readonly=True)
    action = fields.Selection(STATE_SELECTION, 'Action', readonly=True)
    line_sequence = fields.Integer('Workflow Step')

class TechnicAccumulator(models.Model):
    _inherit = 'technic.accumulator'

    in_scrap = fields.Boolean('In scrap')
    reason = fields.Many2one('accumulator.scrap.reason', 'Reason')
