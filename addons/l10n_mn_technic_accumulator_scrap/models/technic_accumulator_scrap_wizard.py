# -*- coding: utf-8 -*-

from odoo import models, fields, api

class TechnicAccumulatorScrapWizard(models.TransientModel):
    _name = 'technic.accumulator.scrap.wizard'

    def _default_technic(self):
        return self.env['technic.accumulator.scrap'].browse(self._context.get('active_id')).technic

    def _default_accumulators(self):
        accumulators = self.env['technic.accumulator.scrap'].browse(self._context.get('active_id')).accumulators
        id_list = []
        for accumulator in accumulators:
            if accumulator.state not in 'in_scrap':
                id_list.append(accumulator.id)
        return self.env['technic.accumulator'].browse(id_list)

    @api.onchange('technic')
    def set_domain(self):
        res = {}
        accumulators = self.env['technic.accumulator.scrap'].browse(self._context.get('active_id')).accumulators
        id_list = []
        for accumulator in accumulators:
            if accumulator.state not in 'in_scrap':
                id_list.append(accumulator.id)
        res = {'domain': {'accumulators': [('id', 'in', id_list)]}}
        return res

    technic = fields.Many2one('technic', 'Technic', default=_default_technic)
    accumulators = fields.Many2many('technic.accumulator', 'accumulator_wizard_accumulator_scrap_rel', 'wizard_scrap_id',
                                    'accumulator_id', string='Accumulator', default=_default_accumulators)
    description = fields.Text('Description', required=True)

    @api.multi
    def done_request(self):
        state_done = False
        for acc in self.accumulators:
            acc.state = 'in_scrap'
        default_accumulators = self._default_accumulators()
        if not default_accumulators:
            state_done = True
        if state_done:
            self.env['technic.accumulator.scrap'].browse(self._context.get('active_id')).state = 'done'
        return True
