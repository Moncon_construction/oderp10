# -*- coding: utf-8 -*-


{
    "name": "Техник хөрөнгө, шилжилгээний данс",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",

    'summary': """
        Техникийн бүртгэлийг үндсэн хөрөнгөтэй холбох, шинжилгээний данстай холбох""",

    "description": """
        Уг модуль нь техникийн бүртгэлийг үндсэн хөрөнгө болон шинжилгээний данстай холбох боломжийг олгоно.
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": [
        'l10n_mn_account_asset', 'l10n_mn_technic',
    ],
    "data": [
        'views/technic_view.xml',
    ],
    "demo_xml": [
    ],
    'auto_install': True,
    "active": False,
    "installable": True,


}
