
# -*- coding: utf-8 -*-


from odoo import fields, models, api, _

class Technic(models.Model):
    _inherit = 'technic'

    account_asset_id = fields.Many2one('account.asset.asset', 'Account asset', groups="account.group_account_manager")
    account_analytic_id = fields.Many2one('account.analytic.account', 'Analytic account')

    @api.onchange('account_asset_id')
    def onchange_account_asset_id(self):
       for account_asset_asset_obj in self:
            account_asset_asset_obj.account_analytic_id = account_asset_asset_obj.account_asset_id.account_analytic_id


class Asset(models.Model):
    _inherit = 'account.asset.asset'

    technic_id = fields.Many2one('technic', 'Technic')