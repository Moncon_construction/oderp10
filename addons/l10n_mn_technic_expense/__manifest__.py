# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-Now Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

{
    "name": "Техникийн бараа материалын шаардах",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
        Шаардах дээр техникийн сэлбэг сонгож, мотоцагийг нь бүртгэх боломжтой болгоно. 
        Шаардах дээр шивсэн мотоцагаар Техникийн мотоцагийг шинэчилнэ.
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": ['l10n_mn_product_expense', 'l10n_mn_technic', 'l10n_mn_technic_asset_analytic'],
    "init": [],
    "data": [
        'views/hr_expense_product_view.xml',
        'report/product_expense_templates.xml'
    ],
    "demo_xml": [
    ],
    "active": False,
    "installable": True,
    'application': True,
}
