# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################
from odoo import fields, api, models, _


class HrExpenseProduct(models.Model):
    _inherit = 'product.expense'
    
    is_technic_expense = fields.Boolean('Is Technic Expense')
    technic_id = fields.Many2one('technic', 'Technic')

    
    # @api.onchange('is_technic_expense')
    # def onchange_is_technic_expense(self):
    #     if not self.is_expense_technic:
    #         self.technic_id = None

    @api.onchange('technic_id', 'account', 'analytic_account')
    def onchange_expense_account(self):
        if self.technic_id:
            self.analytic_account = self.technic_id.account_analytic_id.id if self.technic_id.account_analytic_id else None
        '''
        value = {'line_ids': []}
        if expense_account_id or analytic_account_id or self.technic_id:
            for expense in self:
                for line in expense.expense_line:
                    self.expense_line =
                    (
                        (1,line.id,{
                            'technic_id':           self.technic_id if self.technic_id else None,
                            'motohour':           technic.last_motohour,
                            'kilometer':           technic.last_km,
                            'expense_account_id':   expense_account_id,
                            'analytic_account_id':  analytic_account_id if not self.technic_id else (technic.account_analytic_id.id if technic.account_analytic_id else None),
                            'analytic_required':    analytic_required,
                            }
                        )
                    )
        '''
            
class HrExpenseProductLine(models.Model):
    _inherit = 'product.expense.line'

    @api.model
    def _get_technic(self):
        print '_get_technic ...'
        if self._context.get('technic_id'):
            print 'self._context.get(technic_id): ', self._context.get('technic_id')
            return self._context.get('technic_id')
        return False
     
    @api.onchange('technic_id')
    def onchange_technic(self):
        for technic in self:
            self.motohour = technic.technic_id.last_motohour
            self.kilometer = technic.technic_id.last_km

    technic_id = fields.Many2one('technic', 'Technic', default=_get_technic)
    motohour = fields.Float('Motohour')
    kilometer = fields.Float('Kilometer')


