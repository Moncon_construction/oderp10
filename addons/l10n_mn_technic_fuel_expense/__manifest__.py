# -*- coding: utf-8 -*-
{
    "name": "Техникийн түлшний шаардах",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": ['l10n_mn_product_expense', 'l10n_mn_technic_asset_analytic'],
    "init": [],
    "data": [
        'data/sequence.xml',
        'data/technic_expense_fuel_data.xml',
        'security/tech_fuel_exp_security.xml',
        'security/ir.model.access.csv',
        'views/mail_templates.xml',
        'views/technic_fuel_expense_view.xml',
        'views/technic_fuel_expense_workflow.xml',
        'views/stock_config_settings_view.xml',
    ],
    "demo_xml": [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}