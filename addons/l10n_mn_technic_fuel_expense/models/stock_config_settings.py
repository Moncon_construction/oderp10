# -*- coding: utf-8 -*-
from datetime import time, datetime
from odoo import models, fields, api, exceptions
import time
from odoo.tools.translate import _, _logger
from odoo import SUPERUSER_ID
from odoo.exceptions import except_orm
from odoo import http
from odoo.http import request


class FuelExpenseConfig(models.TransientModel):
    _inherit = 'stock.config.settings'

    module_l10n_mn_technic_fuel_expense_motohour = fields.Boolean(related="company_id.module_l10n_mn_technic_fuel_expense_motohour", string="Upgrade motohour to fuel expense")


class ResCompany(models.Model):
    _inherit = 'res.company'

    module_l10n_mn_technic_fuel_expense_motohour = fields.Boolean(string="Upgrade motohour to fuel expense")
