# -*- coding: utf-8 -*-
from datetime import time

from odoo import models, fields, api, exceptions
import time

from odoo.exceptions import except_orm
from odoo.tools.translate import _
from odoo import SUPERUSER_ID, api


class TechnicExpenseFuelLine(models.Model):
    _name = 'technic.expense.fuel.line'

    fuel_expense_id = fields.Many2one('technic.expense.fuel', 'Expense Fuel')
    technic_id = fields.Many2one('technic', 'Technic', required=True, copy=True)
    odometer = fields.Float('Odometer', store=True, copy=True)
    moto_hour = fields.Float('Moto hour', store=True, copy=True)
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account', copy=True, required=True,
                                          store=True)
    expense_account_id = fields.Many2one('account.account', string='Expense Account', copy=True)
    unit_quantity = fields.Float('Quantities', required=True, default=1.0, copy=True)
    product_state = fields.Selection((
        ('draft', 'Draft'),
        ('waiting_approved', 'Waiting  Approved'),
        ('expense_accepted', 'Expense Approved'),
        ('paid', 'Paid'),), 'Product state', default='draft')

    @api.onchange('technic_id')
    def onchange_technic_id(self):
        res = {}
        if self.technic_id:
            technic = self.env['technic'].search([('id', '=', self.technic_id.id)])
            if technic:
                self.analytic_account_id = technic.account_analytic_id.id
            technic_usage_obj = self.env['technic.usage'].search([('technic_id', '=', self.technic_id.id)])
            if technic_usage_obj:
                technic_usage = technic_usage_obj[0]
            else:
                raise exceptions.Warning(_('This technic does not have technic usage. Please specify technic usage!'))
            context = self._context
            if context.has_key('expense_account_id'):
                self.expense_account_id = context.get('expense_account_id')

    @api.multi
    def _get_stock_move_vals(self, picking_id, location_id, location_dest_id):
        vals = {}
        vals.update({'name': self.technic_id.name,
                     'origin': self.fuel_expense_id.name,
                     'picking_type_id': self.fuel_expense_id.stock_picking_type_id.id,
                     'picking_id': picking_id,
                     'product_id': self.fuel_expense_id.product_id.id,
                     'price_unit': self.fuel_expense_id.standard_price,
                     'product_uom': self.fuel_expense_id.product_id.product_tmpl_id.uom_id.id,
                     'location_id': location_id,
                     'location_dest_id': location_dest_id,
                     'product_uom_qty': self.unit_quantity,
                     'analytic_account_id': self.analytic_account_id and self.analytic_account_id.id or False,
                     'company_id': self.fuel_expense_id.company_id.id
                     })
        return vals