# -*- coding: utf-8 -*-
from datetime import time, datetime

from odoo import models, fields, api, exceptions
import time
from odoo.tools.translate import _, _logger
from odoo import SUPERUSER_ID
from odoo.exceptions import except_orm, UserError
from odoo import http
from odoo.http import request


class FuelExpense(models.Model):
    _name = 'technic.expense.fuel'
    _rec_name = 'name'
    _description = "Fuel Expense"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _order = "date DESC"

    @api.multi
    def name_get(self):
        res = []
        reads = self.read(['name'])
        for record in reads:
            res.append((record['id'], (record['name'] if record['name'] else '')))
        return res

    @api.multi
    def _employee_get(self):
        ids = self.env['hr.employee'].search([('user_id', '=', self.env.uid)])
        if ids:
            return ids[0]
        return False

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            product = self.env['product.product'].search([('id', '=', self.product_id.id)])
            if product:
                self.product_uom = product.uom_id.id
                self.cost_price = product.standard_price
            return {'values': {'product_uom': product.uom_id.id, 'cost_price': product.standard_price}}

    @api.onchange('consume_warehouse_id')
    def onchange_consume_warehouse_id(self):
        if self.consume_warehouse_id:
            for obj in self:
                stock_picking_type = self.env['stock.picking.type'].search(
                    [('warehouse_id', '=', obj.consume_warehouse_id.id), ('code', '=', 'outgoing')], limit=1)
                if stock_picking_type:
                    obj.stock_picking_type_id = stock_picking_type.id
                if obj.consume_warehouse_id._fields.get('stock_account_output_id'):
                    obj.expense_account_id = obj.consume_warehouse_id.stock_account_output_id

    @api.onchange('employee_id')
    def onchange_employee_id(self):
        self.company_id = False
        self.department_id = False
        if self.employee_id:
            employee = self.env['hr.employee'].search([('id', '=', self.employee_id.id)])
            if employee:
                self.department_id = employee.department_id.id
                self.company_id = employee.company_id.id

    @api.onchange('expense_account_id')
    def onchange_expense_account_id(self):
        if self.expense_account_id:
            for line in self.line_ids:
                line.expense_account_id = self.expense_account_id.id

    def _show_approve_button(self):
        res = {}
        history_obj = self.env['technic.expense.fuel.workflow.history']
        for expense in self:
            history = history_obj.search(
                [('fuel_expense_id', '=', expense.id), ('line_sequence', '=', expense.check_sequence)], limit=1,
                order='sent_date DESC')
            if history:
                res[expense.id] = (
                        expense.state in ('confirm') and self.env.uid in map(lambda x: x.id, history.user_ids))
                if res[expense.id]:
                    self.show_approve_button = True
            else:
                res[expense.id] = False
        return res

    @api.depends('product_id')
    def get_available_qty(self):
        for obj in self.filtered(lambda x: x.product_id):
            warehouse = obj.consume_warehouse_id
            if not warehouse:
                raise UserError(_('Warning!\nYou must select warehouse before add expense line!'))
            locations = self.env['stock.location'].search([('usage', '=', 'internal'), ('location_id', 'child_of', [warehouse.view_location_id.id])])
            loc_ids = [loc.id for loc in locations] if locations else []
            if obj.product_id:
                self.available_quantity = obj.product_id.get_qty_availability(loc_ids, obj.date)
            else:
                self.available_quantity = 0

    @api.depends('line_ids')
    def _calc_total_cost_price(self):
        total_price = 0
        for expense in self:
            for line in expense.line_ids:
                total_price += self.cost_price * line.unit_quantity
        self.total_cost_price = total_price

    @api.depends('line_ids')
    def _calc_product_remain(self):
        total_qty = 0
        remain = 0.0
        if self.product_id:
            for expense in self:
                for line in expense.line_ids:
                    if line:
                        total_qty += line.unit_quantity
                        remain = self.available_quantity - total_qty
                    else:
                        remain = 0
            if remain < 0:
                raise except_orm(('Warning!'), _('There is unsufficient remain!'))
            else:
                self.product_remain = remain

    @api.multi
    def _get_warehouse(self):
        default_warehouse = self.env.user.allowed_warehouses.ids
        warehouse_ids = self.env['stock.warehouse'].search([('id', 'in', default_warehouse), ('company_id', 'child_of', [self.env.user.company_id.id])], limit=1)
        return warehouse_ids

    def _domain_warehouses(self):
        return [
            ('id', 'in', self.env.user.allowed_warehouses.ids),
            '|', ('company_id', '=', False), ('company_id', 'child_of', [self.env.user.company_id.id])
        ]

    name = fields.Char('Name', copy=True)
    employee_id = fields.Many2one('hr.employee', 'Employee', default=_employee_get, copy=True)
    date = fields.Date('Date', default=fields.Date.today(), copy=True)
    end_date = fields.Date('End Date', copy=True)
    state = fields.Selection([
        ('draft', 'New'),
        ('cancelled', 'Refused'),
        ('confirm', 'Waiting Approval'),
        ('accepted', 'Approved'),
        ('done', 'Waiting Payment'),
        ('paid', 'Paid'),
    ],
        'Status', track_visibility='onchange', copy=False, default='draft')

    user_id = fields.Many2one('res.users', 'User', required=True, default=lambda self: self.env.user, copy=True)
    department_id = fields.Many2one('hr.department', 'Department', copy=True)
    line_ids = fields.One2many('technic.expense.fuel.line', 'fuel_expense_id', 'Technic Fuel Lines', copy=True)
    history_ids = fields.One2many('technic.expense.fuel.workflow.history', 'fuel_expense_id', 'Validations',
                                    copy=False)
    check_users = fields.Many2many('res.users', 'expense_fuel_check_user_rel', 'fuel_expense_id', 'user_id', 'Checkers',
                                   copy=False)
    check_sequence = fields.Integer('Workflow Step', copy=False, default=0)
    workflow_id = fields.Many2one('workflow.config', 'Workflow')
    date_valid = fields.Date('Validation Date', select=True, copy=False)
    user_valid = fields.Many2one('res.users', 'Validation By', copy=False,
                                 states={'draft': [('readonly', False)], 'confirm': [('readonly', False)]})
    expense_account_id = fields.Many2one('account.account', 'Expense Account', copy=True)
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account', copy=False)
    picking_type_id = fields.Many2one('stock.picking.type', 'Deliver To',
                                      help="This will determine picking type of incoming shipment", copy=True)
    consume_warehouse_id = fields.Many2one('stock.warehouse', 'Consumable Warehouse', copy=True, default=_get_warehouse, domain=_domain_warehouses)
    company_id = fields.Many2one('res.company', 'Company', required=True, copy=True)
    product_id = fields.Many2one('product.product', 'Product', copy=True, required=True)
    product_uom = fields.Many2one('product.uom', 'Product Uom', store=True, copy=True)
    available_quantity = fields.Float(compute=get_available_qty, string='Available quantity', store=True, copy=True)
    cost_price = fields.Float('Cost Price', store=True, copy=True)
    show_approve_button = fields.Boolean(compute=_show_approve_button, string='Show Approve Button?', store=False,
                                         copy=True)
    total_cost_price = fields.Float(compute=_calc_total_cost_price, string='Total Cost Price', store=True, copy=True)
    product_remain = fields.Float(compute=_calc_product_remain, string='Product Remain', store=True, copy=True)
    total_quantity = fields.Float(string='Total quantity', copy=True)
    note = fields.Text('Notes', readonly=True, copy=True, states={'draft': [('readonly', False)]})
    stock_picking_type_id = fields.Many2one('stock.picking.type', string="Location", domain="[('code', '=', 'outgoing'),('warehouse_id','=',warehouse)]", required=True, track_visibility='onchange')
    fuel_count_out_stock_picking = fields.Integer(compute='_fuel_count_out_stock_picking')
    standard_price = fields.Float(readonly=True, compute='_compute_standard_price')

    def _fuel_count_out_stock_picking(self):
        stock_picking = self.env['stock.picking'].search([('fuel_expense_id', '=', self.id)])
        self.fuel_count_out_stock_picking = len(stock_picking)

    @api.multi
    def action_view_fuel_expense(self):
        action = self.env.ref('stock.action_picking_tree')
        result = action.read()[0]

        result.pop('id', None)
        result['context'] = {}
        pick_ids = self.env['stock.picking'].search([('fuel_expense_id', '=', self.id)]).ids
        if len(pick_ids) > 1 or len(pick_ids) == 0:
            result['domain'] = "[('id','in',[" + ','.join(map(str, pick_ids)) + "])]"
        elif len(pick_ids) == 1:
            res = self.env.ref('stock.view_picking_form', False)
            result['views'] = [(res and res.id or False, 'form')]
            result['res_id'] = pick_ids and pick_ids[0] or False
        return result

    @api.model
    def create(self, vals):
        """ Override to avoid automatic logging of creation """
        vals['name'] = self.env['ir.sequence'].get('technic.expense.fuel') or ''
        workflow_config_id = ''

        if 'product_id' in vals:
            qty = self.available_quantity
            product = self.env['product.product'].search([('id', '=', self.product_id.id)])
            vals.update({'product_uom': product.uom_id.id, 'cost_price': product.standard_price, 'available_quantity': qty})
        if 'employee_id' in vals:
            employee_id = vals['employee_id']
        if 'department_id' in vals:
            department_id = vals['department_id']
        if 'consume_warehouse_id' in vals:
            warehouse_id = vals['consume_warehouse_id']
        employee = self.env['hr.employee'].search([('id', '=', vals['employee_id'])])
        workflow_config = self.env['workflow.config'].get_workflow('employee', 'technic.expense.fuel', employee.id, None)
        warehouse = self.env['stock.warehouse'].search([('id', '=', warehouse_id)])
        workflow_id = warehouse.workflow.id

        if workflow_id:
            vals['workflow_id'] = workflow_id
        elif workflow_config:
            workflow_config_id = workflow_config
            vals['workflow_id'] = workflow_config_id
        else:
            raise except_orm(('Warning!'), ('There is no workflow id defined!'))
        return super(FuelExpense, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'product_id' in vals:
            qty = self.available_quantity
            product = self.env['product.product'].browse(vals['product_id'])
            vals.update({'product_uom': product.uom_id.id, 'cost_price': product.standard_price, 'available_quantity': qty})
        else:
            qty = self.available_quantity
            product = self.env['product.product'].search([('id', '=', self.product_id.id)])
            vals.update({'product_uom': product.uom_id.id, 'cost_price': product.standard_price, 'available_quantity': qty})

        return super(FuelExpense, self).write(vals)

    @api.multi
    def set_draft(self):
        for this in self:
            this.write({'state': 'draft', 'check_sequence': 0})
            for line in this.line_ids:
                line.write({'product_state': 'draft'})
        return True

    @api.multi
    def expense_fuel_confirm(self):
        if self.workflow_id:
            workflow_obj = self.env['workflow.config']
            success, current_sequence = self.env['workflow.config'].send('technic.expense.fuel.workflow.history',
                                                                         'fuel_expense_id', self, self.env.user.id)
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(self.workflow_id.id, self,
                                                                                             self.env.user.id,
                                                                                             self.check_sequence + 1,
                                                                                             'next')
            if is_next:
                self.check_users = [(6, 0, next_user_ids)]
                self.change_line_state('waiting_approved')
            if success:
                self.check_sequence = current_sequence
                self.ensure_one()
                self.state = 'confirm'
                self.date_confirm = time.strftime('%Y-%m-%d')
                self.validating_user_id = self.env.user.id

    @api.multi
    def validate(self):
        self.ensure_one()
        follower_ids = []
        workflow_obj = self.env['workflow.config']
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'technic.expense.fuel.workflow.history', 'fuel_expense_id', self, self.env.user.id)
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(self.workflow_id.id, self,
                                                                                             self.env.user.id,
                                                                                             self.check_sequence + 1,
                                                                                             'next')
            next_users = self.env['res.users'].browse(next_user_ids)
            for user_id in next_users:
                self.message_follower_ids = [(4, user_id.partner_id.id)]
            if is_next:
                self.check_users = [(6, 0, next_user_ids)]
            if success:
                if sub_success:
                    self.state = 'accepted'
                    self.change_line_state('expense_accepted')
                    self.check_sequence = current_sequence
                    self.validating_user_id = self.env.user.id
                    self.show_approve_button = True
            if self.company_id.module_l10n_mn_technic_fuel_expense_motohour:
                for obj in self.line_ids:
                    for technic in obj.technic_id:
                        for usage_value in technic.technic_usage_ids:
                            if usage_value.usage_uom_id.is_kilometer:
                                usage_value.write({'usage_value': obj.odometer})
                            else:
                                usage_value.write({'usage_value': obj.moto_hour})
        return True

    def change_line_state(self, new_state):
        for this in self:
            for line in this.line_ids:
                line.write({'product_state': new_state})
        return True

    @api.multi
    def expense_product_accept(self):
        return self.write({'state': 'accepted', 'date_valid': time.strftime('%Y-%m-%d'), 'user_valid': self.env.uid})

    @api.multi
    def expense_fuel_cancelled(self):
        if self.workflow_id:
            success = self.env['workflow.config'].reject('technic.expense.fuel.workflow.history', 'fuel_expense_id',
                                                         self, self.env.user.id)
            if success:
                self.ensure_one()
                self.state = 'cancelled'


    @api.multi
    def create_out_picking(self):
        StockPicking = self.env['stock.picking']
        StockMove = self.env['stock.move']
        des_location = self.env['stock.location'].search([('scrap_location', '=', True)])
        values = {}
        for record in self:
            if self.stock_picking_type_id:
                if str(record.expense_account_id.user_type_id.type) in ('receivable', 'payable'):
                    values = {'fuel_expense_id': record.id,
                              'origin': record.name,
                              'company_id': record.company_id.id,
                              'picking_type_id': self.stock_picking_type_id.id,
                              'location_dest_id': des_location[0].id,
                              'location_id': self.stock_picking_type_id.default_location_src_id.id,
                              'partner_id': record.employee_id.address_home_id.id or False}
                else:
                    values = {'fuel_expense_id': record.id,
                              'origin': record.name,
                              'company_id': record.company_id.id,
                              'picking_type_id': self.stock_picking_type_id.id,
                              'location_dest_id': des_location[0].id,
                              'location_id': self.stock_picking_type_id.default_location_src_id.id}
            stock_picking_object = StockPicking.create(values)
            self.state = 'done'
            stock_picking_object.message_post_with_view(
                'l10n_mn_technic_fuel_expense.message_link_for_picking_fuel_expense',
                values={
                    'self': stock_picking_object,
                    'origin': record
                },
                subtype_id=self.env.ref('mail.mt_note').id
            )
            if self.stock_picking_type_id:
                if record.line_ids:
                    for line in record.line_ids:
                        move_lines_values = line._get_stock_move_vals(stock_picking_object.id, self.stock_picking_type_id.default_location_src_id.id,
                                                                      des_location[0].id)
                        StockMove.sudo().create(move_lines_values)

    @api.one
    @api.depends('product_id')
    def _compute_standard_price(self):
        module = self.sudo().env['ir.module.module'].search(
            [('name', '=', 'l10n_mn_sale_stock_picking_wave'), ('state', 'in', ('installed', 'to upgrade'))])
        if module:
            # Агуулах бүрээр өртөг тооцоолдог үед тухайн барааны агуулахаарх өртөг шаардах хуудасын
            # мөрүүдэд өртгөө авна
            if self.product_id and self.company_id and self.consume_warehouse_id:
                self._cr.execute("""
                                    SELECT standard_price FROM product_warehouse_standard_price
                                    WHERE company_id = %s AND warehouse_id = %s AND product_id = %s""" % (
                    self.company_id.id, self.consume_warehouse_id.id, self.product_id.id))
                results = self._cr.dictfetchall()
                if results and len(results) > 0:
                    if results[0]['standard_price']:
                        self.standard_price = results[0]['standard_price']
        else:
            self.standard_price = self.product_id.standard_price

class HrExpenseFuelWorkflowHistory(models.Model):
    _name = 'technic.expense.fuel.workflow.history'
    _description = 'Fuel Expense Workflow History'

    STATE_SELECTION = [('confirmed', 'Confirmed'),
                       ('approved', 'Approved'),
                       ('rejected', 'Rejected'),
                       ('drafted', 'Drafted'), ]

    fuel_expense_id = fields.Many2one('technic.expense.fuel', 'Expense', readonly=True, ondelete='cascade')
    name = fields.Char('Verification Step', required=True, readonly=True)
    user_ids = fields.Many2many('res.users', 'res_users_expense_fuel_workflow_history_ref', 'history_id', 'user_id',
                                string='Validators')
    sent_date = fields.Datetime('Sent date', required=True, readonly=True)
    user_id = fields.Many2one('res.users', 'Validator', readonly=True)
    action_date = fields.Datetime('Action date', readonly=True)
    action = fields.Selection(STATE_SELECTION, 'Action', readonly=True)
    line_sequence = fields.Integer('Workflow Step')


class TechnicFuelExpenseStockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    def write(self, values):
        result = super(TechnicFuelExpenseStockMove, self).write(values)
        if 'state' in values and values['state'] == 'done':
            for move in self:
                if move.picking_id.picking_type_id.code == 'outgoing':
                    if move.procurement_id:
                        if move.procurement_id.fuel_expense_line_id:
                            for moves in self:
                                moves.procurement_id.fuel_expense_line_id.write({'product_state': 'product_received'})
                                moves.procurement_id.write({'status': 'product_outgoing'})

        return result


class TechnicFuelExpenseStockPicking(models.Model):
    _inherit = 'stock.picking'

    fuel_expense_id = fields.Many2one('technic.fuel.expense', string='Technic Fuel Expense')


class FuelExpenseStockImmediateTransfer(models.TransientModel):
    _inherit = 'stock.immediate.transfer'

    @api.multi
    def process(self):
        self.ensure_one()
        res = super(FuelExpenseStockImmediateTransfer, self).process()
        fuel_expense = self.env['technic.expense.fuel'].search([('id', '=', self.pick_id.fuel_expense_id.id)])
        if fuel_expense:
            fuel_expense.state = 'paid'
        return res
