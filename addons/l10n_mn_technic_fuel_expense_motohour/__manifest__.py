# -*- coding: utf-8 -*-
{
    "name": "Техникийн түлшний шаардахын мото, километер цаг шинэчилэх",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": ['l10n_mn_technic_fuel_expense'],
    "init": [],
    "data": [
        'views/stock_config_settings_view.xml',
    ],
    "demo_xml": [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}