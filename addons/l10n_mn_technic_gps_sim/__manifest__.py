# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    "name": "GPS Техник",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",

    'summary': """
        Техникийн бүртгэлийг GPS - тэй холбох""",

    "description": """
        Уг модуль нь техникийн бүртгэлийг GPS холбох боломжийг олгоно.
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": [
        'l10n_mn_technic',
    ],
    "data": [
          # security
        'security/ir.model.access.csv',
        'views/technic_gps.xml',
        'views/inherit_technic_view.xml',
        'views/technic_gps_cron_view.xml',
        'views/hr_employee_view.xml',
        'wizard/check_gps_technic_download_view.xml',
    ],
    "demo_xml": [
    ],
    "active": False,
    "installable": True,

    'contributors': ['Chinzorig Odgerel <chinzorig.o@asterisk-tech.mn>'],
}
