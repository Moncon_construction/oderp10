# -*- coding: utf-8 -*-


from odoo import fields, models, api, exceptions


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    is_technic_employee = fields.Boolean('Is Technic')