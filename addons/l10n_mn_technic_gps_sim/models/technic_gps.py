# -*- coding: utf-8 -*-


from odoo import fields, models, api, exceptions, _  # @UnresolvedImport
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import time, datetime
import xml.etree.ElementTree as ET
import urllib2  # @UnresolvedImport
import time
import datetime
from datetime import datetime


class InheritTechnicGPS(models.Model):
    _inherit = 'technic'

    id_gps = fields.Integer('GPS id')
    odometer_count = fields.Integer('Odometer Count', compute='_odometer_count')
    technic_latitude = fields.Float('Geo Latitude', digits=(16, 5))
    technic_longitude = fields.Float('Geo Longitude', digits=(16, 5))
    last_km = fields.Float('Last kilometer', digits=(16, 1))
    last_motohour = fields.Float('Moto hour', digits=(16, 1))
    is_aggregate = fields.Boolean('Is Aggregate')


    @api.multi
    def _odometer_count(self):
        for obj in self:
            technic_id = obj.id
            obj_ids = self.env['technic.odometer'].search([('technic_id', '=', technic_id)])
            self.odometer_count = len(obj_ids)

    def get_technic_info(self):
        technics = self.env['technic'].search([('state', '=', 'ready')])
        # Техникийн gps тохиргооноос хамаар ч техникчийн мэдээлэл авдаг байсан болисон
        # gps_id = self.env['technic.gps'].search([])[0]

        technic_dic = {}
        for technic in technics:
            if technic.is_aggregate:
                technic_dic[technic.id] = {'name': technic.name,
                                           'register_number': technic.register_number,
                                           'type': 'aggregate',
                                           'bunker_capacity': technic.technic_norm_id.bunker_capacity,
                                           'length': technic.technic_norm_id.length,
                                           'width': technic.technic_norm_id.width,
                                           'height': technic.technic_norm_id.height,

                                           }
            elif not technic.is_aggregate:
                technic_dic[technic.id] = {'name': technic.name,
                                           'state_number': technic.state_number,
                                           'register_number': technic.register_number,
                                           'gps_number': technic.id_gps,
                                           'type': 'technic',
                                           }

        employee_drivers = self.env['hr.employee'].sudo().search([('is_technic_employee', '=', True)])
        for employee_driver in employee_drivers:
            technic_dic[employee_driver.id] = {'name': employee_driver.name,
                                               'lastname': employee_driver.last_name,
                                               'register_number': employee_driver.ssnid
                                               }        

        return technic_dic

    def action_check_gps(self):
        odo_meter_obj = self.env['technic.odometer']
        query = "SELECT id FROM technic_gps"
        self.env.cr.execute(query)
        gps_config_id = self.env.cr.dictfetchall()
        if gps_config_id:
            gps_config_obj = self.env['technic.gps'].browse(gps_config_id[0]['id'])
            if not gps_config_obj:
                raise exceptions.expert_orm(_(u'Анхаар!'), _(u'Техникийн GPS ийн тохиргооны бүртгэл хийгдэгүй байна!'))
            tree = ET.ElementTree(file=urllib2.urlopen('%s' % gps_config_obj.gps_url))
            root = tree.getroot()
            for vehicle in root.findall('vehicle'):
                gps_id = int(vehicle.find('id').text)
                technic_obj_query = 'SELECT id FROM technic WHERE id_gps = %s' % gps_id
                self.env.cr.execute(technic_obj_query)
                technic_obj_id = self.env.cr.dictfetchall()
                for obj_id in technic_obj_id:
                    odometer_meters = float(vehicle.find('odometer_meters').text)
                    odometer_delta = float(vehicle.find('odometer_delta').text)
                    lat = float(vehicle.find('lat').text)
                    lng = float(vehicle.find('lng').text)
                    odometer = (odometer_meters - odometer_delta) / 1000
                    timestamp = str(vehicle.find('timestamp').text)
                    dometer_id = self.env['technic.odometer'].search([('technic_id', '=', obj_id['id'])], order="id desc", limit=1)
                    engine = int(vehicle.find('engine').text)

                    moto_hour = 0
                    if dometer_id:
                        dometer_obj = self.env['technic.odometer'].browse(dometer_id.id)
                        moto_hour = dometer_obj.moto_hour
                        if dometer_obj.engine == 0:
                            start_date = timestamp
                        else:
                            if engine == 1:
                                start_date = dometer_obj.start_date
                            else:
                                timestamp_date = datetime.strptime(timestamp, DEFAULT_SERVER_DATETIME_FORMAT)
                                start_datetime = datetime.strptime(dometer_obj.start_date,
                                                                   DEFAULT_SERVER_DATETIME_FORMAT)
                                timedelta = timestamp_date - start_datetime
                                moto_hour += (timedelta.days * 24) + (float(timedelta.seconds) / 3600)
                                start_date = timestamp,

                    else:
                        start_date = timestamp
                    vals = {
                        'technic_id': obj_id['id'],
                        'date': time.strftime('%Y-%m-%d %H:%M:%S'),
                        'lat': lat,
                        'lng': lng,
                        'speed': float(vehicle.find('speed').text),
                        'engine': engine,
                        'timestamp': timestamp,
                        'start_date': start_date,
                        'fuel': float(vehicle.find('fuel').text),
                        'rfid': float(vehicle.find('RFID').text),
                        'odometer': odometer,
                        'moto_hour': moto_hour,
                    }
                    technic = self.env['technic'].browse(obj_id['id'])
                    technic.write({'technic_latitude': lat,
                                   'technic_longitude': lng,
                                   'last_km': odometer,
                                   'last_motohour': moto_hour, })
                    odo_meter = odo_meter_obj.create(vals)

    def download_technic_gps(self):
        par_obj = self.env['ir.config_parameter']
        technic_obj = self.env['technic']
        technic_id = technic_obj.search(self, limit=1)
        technic = technic_obj.browse(technic_id)
        technic.action_check_gps()
        return True


class TechnicOdometer(models.Model):
    _name = 'technic.odometer'

    technic_id = fields.Many2one('technic', string='Technic')
    date = fields.Date('Date')
    lat = fields.Float('latitude')
    lng = fields.Float('longitude')
    speed = fields.Float('Speed')
    engine = fields.Integer('Engine')
    start_date = fields.Datetime('Start Date')
    timestamp = fields.Datetime('GPS time')
    odometer = fields.Float('Odometer')
    moto_hour = fields.Float('Moto hour')
    fuel = fields.Float('Fuel')
    rfid = fields.Float('RFID')


class TechnicGPS(models.Model):
    _name = 'technic.gps'
    _description = 'Technic GPS'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    name = fields.Char('Name')
    gps_url = fields.Char('URL', required=True)
    job_id = fields.Many2one('hr.job', string='Job')

    def _action_cron_gps(self):
        query = "SELECT id FROM technic_gps"
        self.env.cr.execute(query)
        gps_config_id = self.env.cr.dictfetchall()
        gps_config_obj = self.env['technic.gps'].browse(gps_config_id[0]['id'])
        tree = ET.ElementTree(file=urllib2.urlopen('%s' % gps_config_obj.gps_url))
        root = tree.getroot()
        for vehicle in root.findall('vehicle'):
            pass

