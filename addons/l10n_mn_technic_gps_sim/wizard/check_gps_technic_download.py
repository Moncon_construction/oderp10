# -*- coding: utf-8 -*-

from odoo import fields, models


class CheckGpsTechnicDownload(models.TransientModel):
    _name = 'check.gps.technic.downoad'
    _description = 'Check GPS Technic Information Download'

    result = fields.Text(string='Result', readonly=True)

    def check(self):
        technic_obj = self.env['technic']
        technic_info = technic_obj.get_technic_info()
        self.write({'result': technic_info})
        return {'res_model': 'check.gps.technic.downoad',
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
                'type': 'ir.actions.act_window',
                'res_id': self.ids[0],
                }