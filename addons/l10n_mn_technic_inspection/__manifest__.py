# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    "name": "Техникийн үзлэг",
    'summary': """
        Техникийн үзлэг""",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
        Уг модуль нь техник дээр үзлэг хийх боломжийг олгоно.
    """,
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": ['l10n_mn_technic'],
    "init": [],
    "data": [
            'security/technic_inspection_security.xml',
            'security/ir.model.access.csv',
            'views/technic_inspection_view.xml',
            'views/technic_norm_views.xml',
            'views/technic_view.xml',
            'views/menu_view.xml',
            'views/sequence.xml',
            'report/report_inspection_sheet.xml',
            'report/report.xml',
            'report/inspection_list_wizard.xml',
            'report/print_inspected_sheet.xml',
    ],
    "demo_xml": [
    ],
    "active": False,
    "installable": True,

    'contributors': ['Baatar Bat-Orshikh <batorshikh.b@asterisk-tech.mn>'],
}
