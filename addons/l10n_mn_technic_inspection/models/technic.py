# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import models, fields, api


class Technic(models.Model):
    _inherit = 'technic'
    @api.multi
    def _inspection_count(self):
        for tech in self: inpections = self.env['technic.inspection'].search([('inspection_technic_id', '=', tech.id)])
        tech.inspection_count = len(inpections)
    inspection_count = fields.Integer(compute='_inspection_count', string='Inspection count')
