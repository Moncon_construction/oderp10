# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from datetime import datetime
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import UserError


class TechnicUsage(models.Model):
    _inherit = 'technic.usage'

    inspection_id = fields.Many2one('technic.inspection', 'Inspection')

class TechnicInspectionType(models.Model):
    _name = 'technic.inspection.type'
    _description = 'Technic inspection type'

    name = fields.Char(string='Inspection type name', required=True)
    description = fields.Text(string='Description')


class TechnicInspectionCategory(models.Model):
    _name = 'technic.inspection.category'
    _description = 'Technic inspection category'
    _order = 'sequence'

    name = fields.Char(string='Inspection category name', required=True)
    sequence = fields.Integer(string='Sequence', index=True)


class TechnicInspectionItem(models.Model):
    _name = 'technic.inspection.item'
    _description = 'Technic inspection item'

    inspection_category_id = fields.Many2one('technic.inspection.category', string='Inspection category name', ondelete='cascade', required=True)
    name = fields.Char(string='Inspection item name', required=True)
    sequence = fields.Integer(string='Sequence', index=True)


class TechnicInspectionPack(models.Model):
    _name = 'technic.inspection.pack'
    _description = 'Technic inspection pack'

    name = fields.Char('Inspection pack name', required=True)
    inspection_type_id = fields.Many2one('technic.inspection.type', string='Inspection type', required=True)
    inspection_items = fields.Many2many('technic.inspection.item', 'technic_inspection_item_rel', 'technic_inspection_pack_id', 'item_id', string='Items', required=True)


class TechnicInspectionNorm(models.Model):
    _name = 'technic.inspection.norm'
    _description = 'Technic inspection norm'

    norm_id = fields.Many2one('technic.norm', string='Norm')
    inspection_type_id = fields.Many2one('technic.inspection.type', string='Technic inspection type')
    inspection_pack_id = fields.Many2one('technic.inspection.pack', string='Technic inspection pack')

    @api.multi
    def onchange_pack_filter(self):
        res = {'domain': {'inspection_pack_id': []}}
        if self.inspection_type_id:
            res = {
                'domain': {'inspection_pack_id': [('inspection_type_id', '=', self.inspection_type_id)]},
                'value': {'inspection_pack_id': False},
            }
        return res

    _sql_constraints = [
        ('technic_ins_norm_unique', 'UNIQUE(norm_id, inspection_type_id)', 'Technic inspection type must be unique for a technic norm!'),
    ]


class TechnicInspection(models.Model):
    _name = 'technic.inspection'
    _description = 'Technic inspection'
    _order = 'create_date DESC'

    @api.one
    def _get_user(self):
        return self._uid

    origin = fields.Char(string='Inspection reference', index=True, readonly=True, copy=False, default='/')
    state = fields.Selection([('draft', 'New'),
                              ('done', 'Done')], string='State', default='draft')
    inspection_date = fields.Datetime(string='Inspection date', readonly=False, states={'done': [('readonly', True)]}, default=datetime.now())
    inspection_technic_id = fields.Many2one('technic', string='Technic name', required=True, readonly=False, states={'done': [('readonly', True)]})
    inspection_type_id = fields.Many2one('technic.inspection.type', string='Inspection type', required=True, states={'done': [('readonly', True)]})
    inspection_worker_id = fields.Many2many('hr.employee', 'inspec_users_rel', 'inspec_id', 'users_id', string='Workers', readonly=False, states={'done': [('readonly', True)]})
    inspection_respondent_id = fields.Many2one('hr.employee', string='Inspection respondent', readonly=False, states={'done': [('readonly', True)]})
    inspection_registrar_id = fields.Many2one('res.users', string='Registrar', readonly=False, states={'done': [('readonly', True)]}, default=_get_user)
    inspection_usage_ids = fields.One2many('technic.inspection.usage', 'inspection_id', readonly=True, states={'draft': [('readonly', False)]})
    technic_inspection_check_list_ids = fields.One2many('technic.inspection.check.list', 'inspection_id', readonly=False, states={'done': [('readonly', True)]})
    ins_transfer_type = fields.Selection([('state', 'State'),
                                           ('ownership_type', 'ownership type'),
                                           ('partner', 'Partner'),
                                           ('ownership_company', 'ownership company'),
                                           ('ownership_department', 'ownership department'),
                                           ('respondent', 'Respondent'),
                                           ('current_company', 'Current company'),
                                           ('current_department', 'Current department'),
                                           ('current_respondent', 'Current respondent'),
                                           ('location', 'Location'),
                                           ('usage_state', 'Usage state')], string='Transfer type', readonly=False, states={'done': [('readonly', True)]})

    ins_state = fields.Selection([('draft', 'Draft'),
                                   ('ready', 'Ready'),
                                   ('in_repair', 'In repair'),
                                   ('stop', 'Stop')], string='State', readonly=False, states={'done': [('readonly', True)]})
    ins_ownership_type = fields.Selection([('own', 'Own'),
                                       ('leasing', 'Leasing'),
                                       ('partner', 'Partner'),
                                       ('rental', 'Rental')], string='ownership type', readonly=False, states={'done': [('readonly', True)]})
    ins_partner_id = fields.Many2one('res.partner', string='Partner', readonly=False, states={'done': [('readonly', True)]})
    ins_ownership_company_id = fields.Many2one('res.company', string='ownership company', readonly=False, states={'done': [('readonly', True)]})
    ins_ownership_department_id = fields.Many2one('hr.department', string='ownership department', readonly=False, states={'done': [('readonly', True)]})
    ins_respondent_id = fields.Many2one('hr.employee', string='Respondent', readonly=False, states={'done': [('readonly', True)]})
    ins_current_company_id = fields.Many2one('res.company', string='Current company', readonly=False, states={'done': [('readonly', True)]})
    ins_current_department_id = fields.Many2one('hr.department', string='Current department', readonly=False, states={'done': [('readonly', True)]})
    ins_current_respondent_id = fields.Many2one('hr.employee', string='Current respondent', readonly=False, states={'done': [('readonly', True)]})
    ins_location_id = fields.Many2one('res.country', string='Location', readonly=False, states={'done': [('readonly', True)]})
    ins_usage_state = fields.Selection([('in_usage', 'In usage'),
                                        ('in_store', 'In store'),
                                        ('in_rent', 'In rent'),
                                        ('in_pawn', 'In pawn')], string='Usage state', readonly=False, states={'done': [('readonly', True)]})

    ins_description = fields.Text(string='Description', readonly=False, states={'done': [('readonly', True)]})
    project = fields.Many2one('project.project', string='Project')

    @api.model
    def create(self, vals):
        if vals.get('origin', '/') == '/':
            vals['origin'] = self.env['ir.sequence'].get('technic.inspection') or '/'

        # set project
        technic = self.env['technic'].browse([(vals.get('inspection_technic_id'))])
        if technic.project:
            vals['project'] = technic.project.id

        return super(TechnicInspection, self).create(vals)

    @api.multi
    def unlink(self):
        reg = self.read(['state'])
        for s in reg:
            if s['state'] not in ['draft']:
                raise UserError(_('You can delete only draft inspection!'))
            super(TechnicInspection, self).unlink()

    @api.multi
    def import_inspection_items(self):
        for obj in self:
            unlink_items = self.env['technic.inspection.check.list'].search([('inspection_id', '=', obj.id)])
            unlink_items.unlink()

            if obj.inspection_type_id:
                inspection_norm = self.env['technic.inspection.norm'].search([('norm_id', '=', obj.inspection_technic_id.technic_norm_id.id), ('inspection_type_id', '=', obj.inspection_type_id.id)])
                if inspection_norm:
                    inspection_pack = inspection_norm.inspection_pack_id
                    for item in inspection_pack.inspection_items:
                        data = {
                            'inspection_id': obj.id,
                            'technic_inspection_category_id': item.inspection_category_id.id,
                            'technic_inspection_item_id': item.id,
                            'inspection_isitnormal':True
                        }
                        self.env['technic.inspection.check.list'].create(data)
                else:
                    raise UserError(_('There is no inspection pack on technic norm!'))

            else:
                raise UserError(_('Please select inspection type!'))

    @api.multi
    def import_usage(self):
        for obj in self:
            unlink_lines = self.env['technic.inspection.usage'].search([('inspection_id', '=', obj.id)])
            unlink_lines.unlink()
            if obj.inspection_usage_ids:
                return {}

            norm_usage_measurements = self.env['usage.uom.line'].search([('technic_norm_id', '=', obj.inspection_technic_id.technic_norm_id.id)])
            if norm_usage_measurements:
                for measurement in norm_usage_measurements:
                    usage_value = 0.0
                    technic_usages = self.env['technic.usage'].search([('technic_id', '=', obj.inspection_technic_id.id), ('usage_uom_id', '=', measurement.usage_uom_id.id)])
                    if technic_usages:
                        usage_value = sum(moto.usage_value for moto in obj.inspection_technic_id.technic_usage_ids if
                            moto.usage_uom_id.id == measurement.usage_uom_id.id)
                    data = {
                        'inspection_id': obj.id,
                        'usage_uom_id': measurement.usage_uom_id.id,
                        'product_uom_id': measurement.product_uom_id.id,
                        'usage_value': usage_value,
                        'inspection_value': usage_value
                    }
                    self.env['technic.inspection.usage'].create(data)
            else:
                raise UserError(_('There is no usage measurement on technic norm!'))

    @api.multi
    def action_to_confirm(self):
        for obj in self:
            for usage in obj.inspection_usage_ids:
                print usage.diff_value
                if usage.diff_value > 0:
                    data = {
                        'technic_id': obj.inspection_technic_id.id,
                        'usage_uom_id': usage.usage_uom_id.id,
                        'product_uom_id': usage.usage_uom_id.usage_uom_id.id,
                        'usage_value': usage.diff_value,
                        'description': obj.ins_description,
                        'inspection_id': obj.id,
                    }
                    self.env['technic.usage'].create(data)
            # update transfer information
            vals = {}
            if obj.ins_transfer_type == 'ownership_type':
                vals['ownership_type'] = obj.ins_ownership_type
            if obj.ins_transfer_type == 'partner':
                vals['partner_id'] = obj.ins_partner_id.id
            if obj.ins_transfer_type == 'ownership_company':
                vals['ownership_company_id'] = obj.ins_ownership_company_id.id
            if obj.ins_transfer_type == 'ownership_department':
                vals['ownership_department_id'] = obj.ins_ownership_department_id.id
            if obj.ins_transfer_type == 'respondent':
                vals['respondent_id'] = obj.ins_respondent_id.id
            if obj.ins_transfer_type == 'current_company':
                vals['current_company_id'] = obj.ins_current_company_id.id
            if obj.ins_transfer_type == 'current_department':
                vals['current_department_id'] = obj.ins_current_department_id.id
            if obj.ins_transfer_type == 'current_respondent':
                vals['current_respondent_id'] = obj.ins_current_respondent_id.id
            if obj.ins_transfer_type == 'location':
                vals['location_id'] = obj.ins_location_id.id
            if obj.ins_transfer_type == 'state':
                vals['state'] = obj.ins_state
            if obj.ins_transfer_type == 'usage_state':
                vals['usage_state'] = obj.ins_usage_state

            if vals:
                vals['description'] = obj.ins_description
                vals['transfer_type'] = obj.ins_transfer_type
                transfer = self.env['technic.transfer'].create(vals)
                transfer.with_context({'active_ids': [obj.inspection_technic_id.id]}).transfer_technic()

            return super(TechnicInspection, obj).write({'state': 'done'})

    @api.multi
    def action_to_cancel(self):
        for obj in self:
            self.env['technic.usage'].search([('inspection_id', '=', obj.id)]).unlink()
            super(TechnicInspection, obj).write({'state': 'draft'})
            return True

    @api.multi
    def print_inspected_sheet(self):
        form = self.read()[0]
        data = {
            'ids': self.ids,
            'model': 'technic.inspection',
            'form': form
        }
        ret = self.env["report"].get_action([], 'l10n_mn_technic_inspection.print_inspected_sheet', data=data)
        return ret


class TechnicInspectionUsage(models.Model):
    _name = 'technic.inspection.usage'
    _description = 'Technic inspection usage'

    @api.depends('inspection_value', 'usage_value')
    def _diff_value(self):
        for obj in self:
            obj.diff_value = obj.inspection_value - obj.usage_value

    inspection_id = fields.Many2one('technic.inspection', string='Technic inspection', ondelete='cascade')
    usage_uom_id = fields.Many2one('usage.uom', string='Usage measurement')#, readonly=True)
    product_uom_id = fields.Many2one('product.uom', string='Unit of measure')#, readonly=True)
    usage_value = fields.Float(string='Usage value')
    inspection_value = fields.Float(string='Inspection value')
    diff_value = fields.Float(string='Difference value', compute=_diff_value, store=True)
    auto_time = fields.Boolean(related='usage_uom_id.auto_time', type="boolean")


class TechnicInspectionCheckList(models.Model):
    _name = 'technic.inspection.check.list'
    _description = 'Technic inspection check list'

    inspection_id = fields.Many2one('technic.inspection', string='Inspection', ondelete='cascade')
    technic_inspection_category_id = fields.Many2one('technic.inspection.category', string='Technic inspection category', ondelete='cascade')
    technic_inspection_item_id = fields.Many2one('technic.inspection.item', string='Technic inspection item', ondelete='cascade')
    inspection_isitnormal = fields.Boolean(string='Inspection normal')
    inspection_check = fields.Boolean(string='Inspection boolean')
    description = fields.Text(string='Inspection description')

    @api.onchange('technic_inspection_category_id')
    def onchange_category(self):
        items = self.env['technic.inspection.item'].search([('inspection_category_id','=',self.technic_inspection_category_id.id)])
        self.technic_inspection_item_id = ''
        return {'domain': {'technic_inspection_item_id': [('id', 'in', items.ids)]}}
