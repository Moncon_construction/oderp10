# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import models, fields

class TechnicNorm(models.Model):
    _inherit = 'technic.norm'
    technic_inspection_pack_ids = fields.One2many('technic.inspection.norm', 'norm_id', string='Technic inspection pack')
