# -*- coding: utf-8 -*-
from StringIO import StringIO
import base64
import time

import xlwt
from odoo import models, fields, api
from odoo.tools.translate import _

from odoo.addons.l10n_mn_technic_inspection.report.report_excel_fit_sheet_wrapper_inspection import ReportExcelFitSheetWrapperInspection  # @UnresolvedImport

report_types = [('summary', 'Summary'),
                ('detail', 'Detail')]
groups = [('draft', 'Draft'),
          ('done', 'Done')]

class ReportInspectionListWizard(models.Model):
    _name = 'report.inspection.list.wizard'

    date_from = fields.Datetime(string='Date from')
    date_to = fields.Datetime(string='Date to')
    group_by = fields.Selection(groups, string='Group by')
    filter = fields.Many2one('technic', string='Technic')

    name_xf = xlwt.easyxf('font: name Times New Roman, height 320, bold on; align: horz center, vert center, wrap on;')
    number_xf = xlwt.easyxf('font: name Times New Roman, height 200, bold on; align: horz right, vert center, wrap on;')
    datetime_xf = xlwt.easyxf(
        'font: name Times New Roman, height 200, bold on; align: horz right, vert center, wrap on;')
    filter_xf = xlwt.easyxf('font: name Times New Roman, height 200, bold on; align: horz left, vert center, wrap on;')
    title_xf = xlwt.easyxf(
        'font: name Times New Roman, height 240, bold on; align: horz center, vert center, wrap on; pattern: pattern solid, fore_color sky_blue; borders: top thin, left thin, bottom thin, right thin;')
    group_xf = xlwt.easyxf(
        'font: name Times New Roman, height 200, bold on; align: horz left, vert center, wrap on; pattern: pattern solid, fore_color pale_blue; borders: top thin, left thin, bottom thin, right thin;')
    content_number_xf = xlwt.easyxf(
        'font: name Times New Roman, height 180; align: horz center, vert center, wrap on; borders: top thin, left thin, bottom thin, right thin;')
    content_text_xf = xlwt.easyxf(
        'font: name Times New Roman, height 180; align: horz left, vert center, wrap on; borders: top thin, left thin, bottom thin, right thin;')
    content_float_xf = xlwt.easyxf(
        'font: name Times New Roman, height 180; align: horz right, vert center, wrap on; borders: top thin, left thin, bottom thin, right thin;',
        num_format_str='#,##0.00')

    @api.multi
    def export_report(self):
        context = self._context or {}
        # create workbook
        book = xlwt.Workbook(encoding='utf8')

        # create sheet
        report_name = _('Inspection List')
        sheet = ReportExcelFitSheetWrapperInspection(book.add_sheet(report_name))
        rowx = 0
        colx = 0
        colx_number = 13

        # browse model
        data_list = self.env['technic.inspection']

        # create filter xls
        data_filters = []
        if self.date_from:
            data_filters.append(('inspection_date', '>', self.date_from))
            sheet.write_merge(rowx, rowx, 1, colx_number, '%s: %s' % (_('Date from'), self.date_from), self.filter_xf, size='filter')
            rowx += 1
        if self.date_to:
            data_filters.append(('inspection_date', '<', self.date_to))
            sheet.write_merge(rowx, rowx, 1, colx_number, '%s: %s' % (_('Date to'), self.date_to), self.filter_xf, size='filter')
            rowx += 1
        if self.group_by:
            data_filters.append(('state', '=', self.group_by))
            sheet.write_merge(rowx, rowx, 1, colx_number, '%s: %s' % (_('Group By'), self.group_by), self.filter_xf, size='filter')
            rowx += 1
        if self.filter:
            data_filters.append(('inspection_technic_id', '=', self.filter.id))
            sheet.write_merge(rowx, rowx, 1, colx_number, '%s: %s' % (_('Filter'), self.filter.name), self.filter_xf, size='filter')
            rowx += 1

        data = data_list.search(data_filters)

        # create header
        report_number = _('Report №%s') % 1
        sheet.write_merge(rowx, rowx, 1, colx_number, report_number, size='number')
        rowx += 1

        # create name
        sheet.row(rowx).height_mismatch = True
        sheet.row(rowx).height = 24 * 20
        sheet.write_merge(rowx, rowx, 0, colx_number, report_name.upper(), self.title_xf, size='title')
        rowx += 1

        sheet.write_merge(rowx, rowx + 1, colx + 0, colx + 0, _('№'), self.title_xf, size='title')
        sheet.write_merge(rowx, rowx + 1, colx + 1, colx + 1, _('Origin'), self.title_xf, size='title')
        sheet.write_merge(rowx, rowx + 1, colx + 2, colx + 2, _('Date'), self.title_xf, size='title')
        sheet.write_merge(rowx, rowx + 1, colx + 3, colx + 3, _('Technic'), self.title_xf, size='title')
        sheet.write_merge(rowx, rowx + 1, colx + 4, colx + 4, _('Type'), self.title_xf, size='title')
        sheet.write_merge(rowx, rowx + 1, colx + 5, colx + 5, _('State'), self.title_xf, size='title')
        sheet.write_merge(rowx, rowx, colx + 6, colx + 7, _('Result'), self.title_xf, size='title')
        sheet.write_merge(rowx + 1, rowx + 1, colx + 6, colx + 6, _('Transfer type'), self.title_xf, size='title')
        sheet.write_merge(rowx + 1, rowx + 1, colx + 7, colx + 7, _('Description'), self.title_xf, size='title')
        sheet.write_merge(rowx, rowx, colx + 8, colx + 10, _('Inspection item'), self.title_xf, size='title')
        sheet.write_merge(rowx + 1, rowx + 1, colx + 8, colx + 8, _('Category'), self.title_xf, size='title')
        sheet.write_merge(rowx + 1, rowx + 1, colx + 9, colx + 9, _('Item'), self.title_xf, size='title')
        sheet.write_merge(rowx + 1, rowx + 1, colx + 10, colx + 10, _('Description'), self.title_xf, size='title')
        sheet.write_merge(rowx, rowx, colx + 11, colx + 13, _('Usage'), self.title_xf, size='title')
        sheet.write_merge(rowx + 1, rowx + 1, colx + 11, colx + 11, _('Usage uom'), self.title_xf, size='title')
        sheet.write_merge(rowx + 1, rowx + 1, colx + 12, colx + 12, _('Product uom'), self.title_xf, size='title')
        sheet.write_merge(rowx + 1, rowx + 1, colx + 13, colx + 13, _('Usage value'), self.title_xf, size='title')
        rowx += 1

        # cell data
        num = 0
        if data:
            for line in data:
                num = num + 1
                rowx += 1

                if len(line.inspection_usage_ids):
                    r = len(line.inspection_usage_ids) - 1
                else:
                    r = 0

                cat_name = ''
                item_name = ''
                description = ''
                # test = ''
                if line.technic_inspection_check_list_ids:
                    for l in line.technic_inspection_check_list_ids:
                        cat_name += '%s\n' % (l.technic_inspection_category_id.name)
                        item_name += '%s\n' % (l.technic_inspection_item_id.name)
                        description += '%s\n' % (l.description)
                        # test += '%s-%s-%s\n' % (l.technic_inspection_category_id.name,l.technic_inspection_item_id.name,l.description)

                sheet.write_merge(rowx, rowx + r, colx + 0, colx + 0, num, self.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + r, colx + 1, colx + 1, line.origin, self.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + r, colx + 2, colx + 2, line.inspection_date, self.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + r, colx + 3, colx + 3, line.inspection_technic_id.name, self.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + r, colx + 4, colx + 4, line.inspection_type_id.name, self.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + r, colx + 5, colx + 5, line.state, self.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + r, colx + 6, colx + 6, line.ins_transfer_type, self.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + r, colx + 7, colx + 7, line.ins_description, self.content_text_xf, size='content')
                # sheet.write_merge(rowx, rowx+r, colx + 8, colx + 10, test, self.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + r, colx + 8, colx + 8, cat_name, self.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + r, colx + 9, colx + 9, item_name, self.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + r, colx + 10, colx + 10, description, self.content_text_xf, size='content')

                row = rowx
                i = 0
                if line.inspection_usage_ids:
                    rowx += len(line.inspection_usage_ids) - 1
                    for usage in line.inspection_usage_ids:
                        sheet.write(row + i, colx + 11, usage.usage_uom_id.name, self.content_text_xf, size='content', no_merge=True)
                        sheet.write(row + i, colx + 12, usage.product_uom_id.name, self.content_text_xf, size='content', no_merge=True)
                        sheet.write(row + i, colx + 13, usage.usage_value, self.content_text_xf, size='content', no_merge=True)
                        i += 1
                else:
                    sheet.write(row + i, colx + 11, '', self.content_text_xf, size='content', no_merge=True)
                    sheet.write(row + i, colx + 12, '', self.content_text_xf, size='content', no_merge=True)
                    sheet.write(row + i, colx + 13, '', self.content_text_xf, size='content', no_merge=True)

        # prepare file data
        io_buffer = StringIO()
        book.save(io_buffer)
        io_buffer.seek(0)
        filedata = base64.encodestring(io_buffer.getvalue())
        io_buffer.close()

        report_excel_output_obj = self.env['excel.output.inspection.list'].with_context({'filename_prefix': 'inspection_list', 'form_title': report_name})
        val = {
            'filename': 'report',
            'filedata': filedata,
        }
        obj = report_excel_output_obj.create(val)

        return obj.export_report()

class ExcelOutputInspectionList(models.Model):
    _name = 'excel.output.inspection.list'

    filename = fields.Char(string='File name')
    filedata = fields.Binary(string='File data')

    @api.multi
    def export_report(self):
        context = self._context or {}
        filename = context.get('filename_prefix', 'report_excel_output')
        filename = "%s_%s.xls" % (filename, time.strftime('%Y%m%d_%H%M'),)
        form_title = filename

        self.write({'filename': filename})

        mod_obj = self.env['ir.model.data']
        form_res = mod_obj.get_object_reference('l10n_mn_technic_inspection', 'report_excel_output_inspection_list_view_form')
        form_id = form_res and form_res[1] or False
        return {
            'name': form_title,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'excel.output.inspection.list',
            'res_id': self.id,
            'views': [(form_id, 'form')],
            'context': context,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
