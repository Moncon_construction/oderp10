# -*- coding: utf-8 -*-
##############################################################################
#
##############################################################################

from odoo import models, api
from odoo.tools.translate import _
from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, comma_me, convert_curr


class PrintInspectionSheet(models.AbstractModel):
    _name = 'report.l10n_mn_technic_inspection.print_inspected_sheet'

    @api.multi
    def render_html(self, docargs, data=None):
        report_obj = self.env['report']
        technic_inspection_obj = self.env['technic.inspection']
        report_obj._get_report_from_name('l10n_mn_technic_inspection.print_inspected_sheet')
        context = self._context or {}
        technic_inspection = technic_inspection_obj.browse(context.get('active_ids'))

        docargs = {
            'doc_ids': data['ids'],
            'docs': technic_inspection,
            # 'usage': '',
        }
        return report_obj.render('l10n_mn_technic_inspection.print_inspected_sheet', docargs)
