# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import UserError

class ReportInspectionSheet(models.AbstractModel):
    _name = 'report.l10n_mn_technic_inspection.report_inspection_sheet'

    @api.multi
    def render_html(self, docargs, data=None):
        report_obj = self.env['report']
        technic_obj = self.env['technic']

        report_obj._get_report_from_name('l10n_mn_technic_inspection.report_inspection_sheet')

        technic = technic_obj.browse(data['active_id'])

        usage = self.env['usage.uom.line'].search([('technic_norm_id', '=', technic.technic_norm_id.id)])

        if data['form'][u'inspection_type']:
            inspection_norm = self.env['technic.inspection.norm'].search([('inspection_type_id', '=', data['form'][u'inspection_type'][0]), ('norm_id', '=', technic.technic_norm_id.id)])
            docargs = {
                'doc_ids': data['ids'],
                'docs': technic,
                'usage': usage,
                'inspection_pack': inspection_norm.inspection_pack_id,
            }
            return report_obj.render('l10n_mn_technic_inspection.report_inspection_sheet', docargs)

class ReportPickingInspectionType(models.Model):
    _name = 'report.picking.inspection.type'
    _description = 'Report picking inspection type'

    @api.multi
    def report_inspection(self):
        context = self._context or {}
        form = self.read()[0]
        data = {
            'ids': self.ids,
            'model': 'report.picking.inspection.type',
            'form': form,
            'active_id': context['active_id']
        }
        return self.env["report"].get_action([], 'l10n_mn_technic_inspection.report_inspection_sheet', data=data)

    inspection_type = fields.Many2one('technic.inspection.type', string='Inspection type', required=True)

    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        context = self._context or {}
        tech_obj = self.env['technic'].browse(context['active_id'])
        if view_type == 'form':
            if tech_obj.technic_norm_id.technic_inspection_pack_ids:
                res = super(ReportPickingInspectionType, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
                if 'inspection_type' in res['fields']:
                    res['fields']['inspection_type']['domain'] = [('id', 'in', map(lambda x: x.inspection_type_id.id, tech_obj.technic_norm_id.technic_inspection_pack_ids))]
            else:
                res = None
                raise UserError(_('Та техникийн норм тохиргоон дээр үзлэгийн багцаа сонгоно уу!'))
            return res
