# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    "name": "Техник захиалга",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
        Уг модуль нь техник захиалах боломжийг олгоно.
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": ['l10n_mn_technic_inspection', 'l10n_mn_product_expense'],
    "init": [],
    "data": [
        'security/technic_security.xml',
        'security/ir.model.access.csv',
        'email_templates/sent_to_dispatcher.xml',
        'email_templates/sent_to_stockkeeper.xml',
        'email_templates/sent_to_customer.xml',
        'views/technic_order_view.xml',
        'views/menu_view.xml',
        'views/sequence.xml',
        'views/technic_order_merge_view.xml',
        'report/report.xml',
        'report/print_order_sheet.xml',
    ],
    "demo_xml": [
    ],
    "active": False,
    "installable": True,
    'css': ['static/src/css/description.css', 'static/src/css/technic_order.css', ],

    'contributors': ['Sugarsukh Sukhbaatar <sugarsukh@asterisk-tech.mn>'],
}
