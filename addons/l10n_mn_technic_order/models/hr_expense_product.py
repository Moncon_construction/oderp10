# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from odoo import models, fields, api

class ProductExpense(models.Model):
    _inherit = 'product.expense'


    technic_order_id = fields.Many2one('technic.order', 'Technic order')
    technic_id = fields.Many2one('technic', 'Technic')
    fuel_sum = fields.Float('Fuel sum')
