# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from odoo import models, api  # @UnresolvedImport
import logging
_logger = logging.getLogger('STOCK')

# class stock_transfer_details(models.TransientModel):
#     _inherit = 'stock.transfer_details'
#
#     @api.one
#     def do_detailed_transfer(self):
#         this = super(stock_transfer_details, self).do_detailed_transfer()
#
#         stock_picking = self.env['stock.picking'].browse(self._context['active_id'])
#         if stock_picking.consume_order_id.expense_id.technic_order_id:
#             stock_picking.consume_order_id.expense_id.technic_order_id.state = 'going'
#         return this
