# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from odoo import fields, api, models, _  # @UnresolvedImport
from datetime import datetime
from openerp.http import request  # @UnresolvedImport
from odoo.exceptions import UserError

class TechnicOrder(models.Model):
    _name = "technic.order"
    _inherit = ['mail.thread', 'ir.needaction_mixin', ]
    _description = "Technic order"
    _rec_name = 'origin'

    @api.multi
    def _order_count(self):
        for obj in self:
            obj_ids = self.env['technic.order'].search([('order_id', '=', obj.id)])
            self.order_count = len(obj_ids)

    @api.multi
    def _expense_count(self):
        for obj in self:
            obj_ids = self.env['product.expense'].search([('technic_order_id', '=', obj.id)])
            self.expense_count = len(obj_ids)

    @api.multi
    def _total_distance(self):
        all_distance = 0

        for obj in self:
            for extension in obj.technic_route_ids:
                all_distance += extension.distance
            self.total_distance = all_distance


    @api.multi
    def _total_duration(self):
        all_duration = 0

        for obj in self:
            for extension in obj.technic_route_ids:
                all_duration += extension.duration_hour
            self.total_duration = all_duration

    @api.multi
    def _total_actual_tare(self):
        total = 0

        for obj in self:
            for tracker in obj.tracker_ids:
                total += tracker.actual_tare
            self.total_actual_tare = total

    def _get_mobile_phone(self):
        employee_obj = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        return employee_obj.mobile_phone

    state = fields.Selection([('draft', 'Draft'),
                                ('sent_dispatcher', 'Sent dispatcher'),
                                ('to_merge', 'Merge'),
                                ('pending', 'Pending'),
                                ('going', 'Going'),
                                ('done', 'Done')], 'State', default='draft')
    origin = fields.Char('Inspection reference', select=True, readonly=True, copy=False)
    priority = fields.Selection([('0', 'None'),
                                      ('1', 'Low'),
                                      ('2', 'Normal'),
                                      ('3', 'High')], 'Priority')
    order_id = fields.Many2one('technic.order', 'Merged order', states={'going': [('readonly', True)]})
    start_date = fields.Datetime('Start date', required=True, readonly=False, states={'going': [('readonly', True)]}, default=datetime.now())
    end_date = fields.Datetime('End date', required=True, readonly=False, states={'going': [('readonly', True)]})
    customer_company_id = fields.Many2one('res.partner', 'Customer partner', required=True, readonly=False, states={'going': [('readonly', True)]}, default=lambda self: self.env.user.company_id)
    customer_department_id = fields.Many2one('hr.department', 'Customer department', required=True, readonly=False, states={'going': [('readonly', True)]})
    res_users_id = fields.Many2one('res.users', 'User', readonly=True, states={'going': [('readonly', True)]}, default=lambda self: self.env.user.id)
    phone_number = fields.Integer('Phone number', required=True, readonly=False, states={'going': [('readonly', True)]}, default=_get_mobile_phone)
    technic_type_id = fields.Many2one('technic.type', 'Technic type', required=False, readonly=False, states={'going': [('readonly', True)]})
    description = fields.Char('Description', readonly=False, states={'going': [('readonly', True)]})
    out_date =  fields.Datetime('Out date')
    tracker =  fields.Boolean('Tracker')
    order_count =  fields.Integer('Order count',compute='_order_count')
    expense_count =  fields.Integer( 'Expense count', compute='_expense_count')
    shipment_amount =  fields.Float('Shipment amount /tn/')
    total_distance =  fields.Float(compute='_total_distance', string='Total distance')
    total_duration =  fields.Float(compute='_total_duration', string='Total duration')
    close_description =  fields.Char('Close description', states={'going': [('required', True)]})
    open_fuel =  fields.Float('Start fuel tank amount')
    close_fuel =  fields.Float('Last fuel', states={'going':[('required', True)]})
    technic_route_ids =  fields.One2many('technic.route', 'technic_order_id', 'Technic route')
    tracker_ids =  fields.One2many('order.tracker', 'technic_order_id', 'Trackers')
    technic_ids =  fields.One2many('technic.distribution', 'technic_order_id', 'Technic distribution')
    technic_order_start_usage_ids =  fields.One2many('technic.order.start.usage', 'technic_order_id', readonly=True, states={'draft': [('readonly', False)]})
    total_actual_tare =  fields.Float(compute='_total_actual_tare', string='Total actual tare')
    fuel_tracker =  fields.Float('Fuel tracker', readonly=True)
    fuel_shipment =  fields.Float('Fuel shipment', readonly=True)
    fuel_norm =  fields.Float('Fuel norm', readonly=True)
    fuel_total =  fields.Float('Fuel total', readonly=True)
    total_km =  fields.Float('Total km', readonly=True)
    technic_fuel_norm =  fields.Float('Technic fuel norm')
    is_winter =  fields.Boolean('Is winter')
    company_id = fields.Many2one('res.company', 'Company')

    @api.model
    def create(self, vals):
        if vals.get('origin', '/') == '/':
            vals['origin'] = self.env['ir.sequence'].get('technic.order') or '/'
        order = super(TechnicOrder, self).create(vals)

        return order

    def action_send_dispatcher(self):
        self.send_notification_dispatcher('sent_to_dispatcher')
        super(TechnicOrder, self).write({'state': 'sent_dispatcher'})

        return True

    def action_dispatcher_cancel(self):
        super(TechnicOrder, self).write({'state': 'draft'})
        return True

    def action_create_expense(self):
        technic_ids = []
        for tech_obj in self.technic_ids:
            technic_ids.append(tech_obj.id)
        if len(technic_ids) == 1:
            if tech_obj.technic_id.current_respondent_id:
                list1 = []
                list2 = []
                employee_obj = tech_obj.technic_id.current_respondent_id
                for a in employee_obj.driver_license_category_technic:
                    list1.append(a.id)
                technic_norm_obj = tech_obj.technic_id.technic_norm_id
                for b in technic_norm_obj.driver_license_category_technic_norm:
                    list2.append(b.id)
                s = 0
                for ll in list2:
                    if ll in list1:
                        s += 1
                if s != len(list2):
                    raise UserError((u'Сонгосон жолооч, операторын жолооны үнэмлэхний ангилал зөрж байна!'))

            else:
                raise UserError((u'Та жолооч, оператороо сонгоно уу!'))

            tracker_name = []
            tracker_actual_tare = 0
            if self.tracker_ids:
                for self.trackers_id in self.tracker_ids:
                    tracker_name.append(self.trackers_id.technic_id.technic_name)
                    tracker_actual_tare += self.trackers_id.technic_id.technic_norm_id.actual_tare
            amount_distance = 0
            for technic_route_id in self.technic_route_ids:
                amount_distance += technic_route_id.distance
            fuel_norm_technic = amount_distance * self.technic_ids.technic_id.technic_norm_id.fuel_medium / 100
            fuel_plus = amount_distance * ((self.shipment_amount / 1000) + (tracker_actual_tare / 1000)) * 1.4 / 100

            company_ids = self.env['res.company'].search([('partner_id', '=', self.customer_company_id.id)])
            company_id = 1
            if company_ids:
                for company in company_ids:
                    company_id = company.id
            data = {
                    'name': self.origin,
                    'technic_order_id': self.id,
                    'employee': self.technic_ids.current_respondent_id.id,
                    'date': datetime.now(),
                    'department': self.customer_department_id.id,
                    'description': self.origin + u', Шатахууны норм=' + str(fuel_norm_technic) + u' Ачааны тонны нэмэгдэл=' + str(fuel_plus),
                    'company_id': company_id,
                    'fuel_sum': fuel_norm_technic + fuel_plus,
                    'technic_id': self.technic_ids.technic_id.id
                    }

            self.env['product.expense'].create(data)

            super(TechnicOrder, self).write({'state': 'pending'})
            return True
        else:
            raise UserError((u'Та техникээ сонгоно уу!'))

    def send_notification_dispatcher(self, signal):
        groups = {'sent_to_dispatcher': 'group_technic_order_dispatcher'}

        states = {'sent_to_dispatcher': u'Диспетчерт мэйл илгээгдсэн'}

        group_user_ids = None
        if self:

            if signal in ['sent_to_dispatcher']:
                der_notif_groups = self.env['ir.model.data'].get_object_reference('l10n_mn_technic_order', groups[signal])
                group_user_ids = self.env['res.users'].search([('groups_id', 'in', [der_notif_groups[1]])])

            data = {
                    'origin': self.origin,
                    'base_url': self.env['ir.config_parameter'].get_param('web.base.url'),
                    'action_id': self.env['ir.model.data'].get_object_reference('l10n_mn_technic_order', 'action_technic_order')[1],
                    'id': self.id,
                    'db_name': request.session.db,  # @UndefinedVariable
                    'state': states[signal],
                    'sender': self.env.user.name,
                    'start_date': self.start_date,
                    'end_date': self.end_date,
                    'technic_type': self.technic_type_id.name,
                    }
            # if group_user_ids:
            #     users = self.env['res.users'].browse(group_user_ids)
            #     print users, 'dddddddd\n\n'
            #     for user in users:
            #         self.env['mail.template'].send_mail(user.id)
            #         email = u'\n Дараах хэрэглэгч рүү имэйл илгээгдэв : ' + ('</b>'.join(user.login))
            #         self.env['technic.order'].message_post(body=email)
            # else:
            #     raise UserError(_('Warning!'), _('Not found receiver. Please contact administrator. '))
        return True

    def send_notification_customer(self):

        technic_order_obj = self[0]
        user = technic_order_obj.res_users_id

        data = {
            'origin': technic_order_obj.origin,
            'base_url': self.env['ir.config_parameter'].get_param('web.base.url'),
            'action_id': self.env['ir.model.data'].get_object_reference('l10n_mn_technic_order', 'action_technic_order')[1],
            'id': technic_order_obj.id,
            'db_name': request.session.db,  # @UndefinedVariable
            'sender': self.env.user.name,
            'start_date': technic_order_obj.start_date,
            'end_date': technic_order_obj.end_date,
            'technic_id': technic_order_obj.technic_ids.technic_id.technic_name,
            'driver': technic_order_obj.technic_ids.current_respondent_id.name,
            'fuel_norm': technic_order_obj.distance * 2 * technic_order_obj.technic_ids.technic_id.technic_norm_id.fuel_medium / 100
            }

        template_id = self.env['ir.model.data'].get_object_reference('l10n_mn_technic_order', 'technic_order_send_customer_template')[1]
        self.env['email.template'].send_mail(template_id, user.id, force_send=False, context=data)
        email = u'\n Дараах хэрэглэгч рүү имэйл илгээгдэв : ' + ('</b>'.join(user.login))
        self.env['technic.order'].message_post(body=email)
        return True

    def action_close(self):
        self.env['technic'].write(self.technic_ids.technic_id.id, {'state': 'ready'})
        super(TechnicOrder, self).write({'state': 'done'})
        return True

    def print_order_sheet(self):
        form = self.read()[0]
        data = {
            'ids': self.ids,
            'model': 'technic.order',
            'form': form
        }

        action_id = self.env["report"].get_action([], 'l10n_mn_technic_inspection.print_technic_order_sheet', data=data)

        return action_id

    @api.multi
    def import_technic(self):
        ins_line_ids = self.env['technic.distribution'].search([('technic_order_id', '=', self.id)])
        ins_line_ids.unlink()
        if self.technic_ids:
            return {}

        if self.technic_type_id:
            tech_ids = self.env['technic'].search([('technic_type_id', '=', self.technic_type_id.id), ('state', '=', 'ready')])
            tech_objs = self.env['technic'].browse(tech_ids)
        else:
            tech_ids = self.env['technic'].search([('state', '=', 'ready')])
            tech_objs = self.env['technic'].browse(tech_ids)

        if tech_objs:
            for tech_obj in tech_objs:
                res = ''
                for technic_usage_id in tech_obj.technic_usage_ids:
                    if res:
                        res = u'{0}\n{1}: {2}'.format(res, technic_usage_id.product_uom_id.name, technic_usage_id.usage_value)
                    else:
                        res = u'{0}: {1}'.format(technic_usage_id.product_uom_id.name, technic_usage_id.usage_value)
                data = {
                    'technic_order_id': self.id,
                    'technic_id': tech_obj.id,
                    'state_number': tech_obj.state_number,
                    'serial_vin_number': tech_obj.serial_vin_number,
                    'current_respondent_id': tech_obj.current_respondent_id.id,
                    'technic_usage': res
                }
                self.env['technic.distribution'].create(data)
        else:
            raise UserError((u'Бэлэн төлөвтэй техник байхгүй байна.'))
        return True

    @api.multi
    def unlink(self):
        for s in self:
            if s.state not in 'draft':
                raise UserError(_(u'You can delete only draft technic order!'))
            super(TechnicOrder, self).unlink()

    def import_usage_open(self):
        technic_list = []
        for technic in self.technic_ids:
            technic_list.append(technic.technic_id.id)
            if len(technic_list) == 1:
                if technic.technic_id:
                    ins_line_ids = self.env['technic.order.start.usage'].search([('technic_order_id', '=', self.id)])
                    ins_line_ids.unlink()
                    if self.technic_order_start_usage_ids:
                        return {}
                    norm_usage_measurements = self.env['usage.uom.line'].search([('technic_norm_id', '=', technic.technic_id.technic_norm_id.id)])
                    if norm_usage_measurements:
                        for measurement in norm_usage_measurements:
                            usage_value = 0.0
                            technic_usages = self.env['technic.usage'].search([('technic_id', '=', technic.technic_id.id), ('usage_uom_id', '=', measurement.usage_uom_id.id)])
                            if technic_usages:
                                usage_value = technic_usages.usage_value
                            data = {
                                'technic_order_id': self.id,
                                'usage_uom_id': measurement.usage_uom_id.id,
                                'product_uom_id': measurement.product_uom_id.id,
                                'usage_value': usage_value,
                            }
                            self.env['technic.order.start.usage'].create(data)
                    else:
                        raise UserError(_(u'There is no usage measurement on technic norm!'))
                else:
                    raise UserError(_(u'You should first pick technic!'))
            else:
                raise UserError(_(u'You should first pick technic!'))

    def calculate_fuel(self):
        fuel_med_norm = self.technic_ids.technic_id.technic_norm_id.fuel_medium
        if fuel_med_norm > 0:
            winter = 0
            if self.is_winter:
                winter = 5

            totalkm = self.total_distance
            fuel_shipment = ((self.shipment_amount * totalkm * 1.4) / 100)
            fuel_tracker = (totalkm * self.total_actual_tare * 1.4) / 100
            fuel_norm = totalkm * ((fuel_med_norm + ((fuel_med_norm * winter) / 100))) / 100

            vals = {
                'technic_fuel_norm': fuel_med_norm,
                'fuel_tracker': fuel_tracker,
                'fuel_shipment': fuel_shipment,
                'fuel_norm': fuel_norm,
                'fuel_total': fuel_tracker + fuel_shipment + fuel_norm,
                'total_km': totalkm,
                }
            self.write(vals)
        else:
            raise UserError(_(u'Check fuel medium!'))


class TechnicDistribution(models.Model):
    _name = 'technic.distribution'
    _description = 'Technic distribution'

    technic_order_id = fields.Many2one('technic.order', 'Technic order')
    technic_id = fields.Many2one('technic', 'Technic name')
    current_respondent_id = fields.Many2one('hr.employee', 'Driver', required=False, readonly=False)
    technic_usage = fields.Char('Technic usage')

    def select(self):
        if not self.technic_id.ownership_company_id:
            raise UserError((u'Сонгосон техникийн эзэмшигч компани хоосон байна.'))
        self.technic_order_id.company_id = self.technic_id.ownership_company_id.id
        delete_id = self.env['technic.distribution'].search([('technic_order_id', '=', self.technic_order_id.id), ('id','!=', self.id)])
        delete_id.unlink()
        return {'type': 'ir.actions.client', 'tag': 'reload', }

class OrderTracker(models.Model):
    _name = 'order.tracker'
    _description = 'Order tracker'

    technic_order_id = fields.Many2one('technic.order', 'Technic order')
    technic_type_id = fields.Many2one('technic.type', 'Technic type')
    technic_id = fields.Many2one('technic', 'Technic name')
    serial_vin_number = fields.Char('Searial vin number', readonly=True)
    state_number = fields.Char('State number', readonly=True)
    actual_tare = fields.Float('Actual tare', readonly=True)


    @api.onchange('technic_id')
    def onchange_technic_id(self):
        return {
            'value': {
                'serial_vin_number': self.technic_id.serial_vin_number,
                'state_number': self.technic_id.state_number,
                'actual_tare': self.technic_id.technic_norm_id.actual_tare,
                }
            }

    @api.model
    def create(self, vals):
        if 'technic_id' in vals:
            technic_obj = self.env['technic'].browse(vals.get('technic_id'))
            vals.update({'serial_vin_number': technic_obj.serial_vin_number,
                         'state_number': technic_obj.state_number,
                         'actual_tare': technic_obj.technic_norm_id.actual_tare})
        return super(OrderTracker, self).create(vals)

    @api.multi
    def write(self, vals):

        if 'technic_id' in vals:
            technic_obj = self.env['technic'].browse(vals.get('technic_id'))
            vals.update({'serial_vin_number': technic_obj.serial_vin_number,
                         'state_number': technic_obj.state_number,
                         'actual_tare': technic_obj.technic_norm_id.actual_tare})
        return super(OrderTracker, self).write(vals)

class PartnerDistance(models.Model):
    _name = 'partner.distance'
    _description = " Partner distance"

    @api.depends('partner_from_id', 'partner_to_id')
    def distance_name_get(self):
        # reads = self.read(['partner_from_id', 'partner_to_id'])
        if self.partner_from_id and self.partner_to_id:
            distance_name = u'{0} - {1}'.format(self.partner_from_id.name, self.partner_to_id.name)
            self.name = distance_name


    name = fields.Char(compute='distance_name_get',  string='Distance name', store=True)
    partner_from_id = fields.Many2one('technic.order.partner', 'Out location')
    partner_to_id = fields.Many2one('technic.order.partner', 'In location')
    line_ids = fields.One2many('partner.distance.line', 'partner_distance_id', 'Lines')


    _sql_constraints = [
        ('partner_distance_unique', 'UNIQUE(name)', 'Partner distance must be unique!'),
    ]

class PartnerDistanceLine(models.Model):
    _name = "partner.distance.line"
    _description = "Partner distance line"


    partner_distance_id = fields.Many2one('partner.distance', 'Partner distance')
    name = fields.Char('Line name', required=True)
    model_id = fields.Many2one('technic.type', 'Technic type', required=True)
    distance = fields.Float('Distance', required=True)
    duration_hour = fields.Float('Duration hour', required=True)


class TechnicRoute(models.Model):
    _name = "technic.route"


    date = fields.Datetime('Date')
    technic_order_id = fields.Many2one('technic.order', string='Technic order')
    start = fields.Many2one('technic.order.partner', string='Start')
    partner_distance_id = fields.Many2one('partner.distance', 'Partner distance')
    partner_distance_line = fields.Many2one('partner.distance.line', 'Partner distance line')
    distance = fields.Float('Distance')
    duration_hour = fields.Float('Duration hour')


    @api.onchange('start')
    def onchange_start(self):
        if not self.start:
            return {}

        return {
            'value': {
                'partner_distance_id': False,
                'partner_distance_line': False,
                'distance': False,
                'duration_hour': False
                }
            }

    @api.onchange('partner_distance_line')
    def onchange_lines(self):
        return {
            'value': {
                'distance': self.partner_distance_line.distance,
                'duration_hour': self.partner_distance_line.duration_hour,
                }
            }

class TechnicOrderPartner(models.Model):
    _name = 'technic.order.partner'

    name = fields.Char('Location name')

class TechnicOrderStart_usage(models.Model):
    _name = 'technic.order.start.usage'
    _description = 'Technic order start usage'

    technic_order_id = fields.Many2one('technic.order', 'Technic order', ondelete='cascade')
    usage_uom_id = fields.Many2one('usage.uom', string='Usage measurement', readonly=True)
    product_uom_id = fields.Many2one('product.uom', 'Unit of measure', readonly=True)
    usage_value = fields.Float('Usage value')
    auto_time = fields.Boolean(related='usage_uom_id.auto_time', type="Boolean")

