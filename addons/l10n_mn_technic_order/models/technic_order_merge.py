# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################
from odoo import fields, models  # @UnresolvedImport
from odoo.exceptions import UserError

class TechnicOrderMerge(models.TransientModel):
    _name = 'technic.order.merge'
    _description = 'Technic order merge'

    technic_ids = fields.One2many('technic.distribution.merge', 'merge_id', 'Technic distribution', readonly=False)

    def default_get(self):
        """ To get default values for the object.
        @param self: The object pointer.
        @param cr: A database cursor
        @param uid: ID of the user currently logged in
        @param fields: List of fields for which we want default values
        @param context: A standard dictionary
        @return: A dictionary which of fields with values.
        """

        res = super(TechnicOrderMerge, self).default_get(fields)
        technic_order_objs = self.env['technic.order'].browse(active_ids)
        for technic_order_obj in technic_order_objs:
            obj = technic_order_obj

            if obj.technic_ids:
                return {}

            data = []

            tech_ids = self.env['technic'].search([('technic_type_id', '=', obj.technic_type_id.id), ('state', '=', 'ready')])
            tech_objs = self.env['technic'].browse(tech_ids)

            if tech_objs:
                for tech_obj in tech_objs:
                    res_value = ''
                    for technic_usage_id in tech_obj.technic_usage_ids:
                        if res_value:
                            res_value = u'{0}\n{1}: {2}'.format(res_value, technic_usage_id.product_uom_id.name, technic_usage_id.usage_value)
                        else:
                            res_value = u'{0}: {1}'.format(technic_usage_id.product_uom_id.name, technic_usage_id.usage_value)

                    data.append((0, 0, {
                        'merge_id': obj.id,
                        'technic_id': tech_obj.id,
                        'state_number': tech_obj.state_number,
                        'serial_vin_number': tech_obj.serial_vin_number,
                        'current_respondent_id': tech_obj.current_respondent_id.id,
                        'engine_type': tech_obj.technic_norm_id.engine_type,
                        'technic_usage': res_value
                    }))

                    res['technic_ids'] = data
            else:
                raise UserError((u'Warning!'), (u'Бэлэн төлөвтэй техник байхгүй байна.'))

        return res

    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        context = dict(self._context or {})
        active_ids = context.get('active_ids')
        technic_order_objs = self.env['technic.order'].browse(active_ids)
        res = super(TechnicOrderMerge, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if view_type == 'form':
            check = []
            for technic_order_obj in technic_order_objs:
                if technic_order_obj.state != 'sent_dispatcher':
                    check.append(0)
                else:
                    check.append(1)
            if 0 in check:
                raise UserError((u'Та зөвхөн диспетчерт илгээгдсэн төлөвтэй захиалгыг нэгтгэх боломжтой!'))
            return res

    def action_merge(self):
        checked_technic = []
        for technic_obj_add in self.technic_ids:
            if technic_obj_add.check == True:
                checked_technic.append(technic_obj_add.technic_id.id)
        if len(checked_technic) == 0:
                raise UserError((u'Та техникээ сонгоно уу.'))
        if len(checked_technic) == 1:
            technic_order_obj = self.env['technic.order'].browse(active_ids[0])
        else:
            raise ((u'Та зөвхөн нэг л техник сонгох хэрэгтэй.'))

        order_data = {
                'state': 'sent_dispatcher',
                'start_date': technic_order_obj.start_date,
                'end_date': technic_order_obj.end_date,
                'customer_company_id': technic_order_obj.customer_company_id.id,
                'customer_department_id': technic_order_obj.customer_department_id.id,
                'partner_distance_id': technic_order_obj.partner_distance_id.id,
                'partner_distance_lines': technic_order_obj.partner_distance_lines.id,
                'technic_type_id': technic_order_obj.technic_type_id.id,
                }

        new_technic_order = self.env['technic.order'].create(order_data)
        technic_obj = self.env['technic'].browse(checked_technic[0])
        res_value = ''
        for technic_usage_id in technic_obj.technic_usage_ids:
            if res_value:
                res_value = u'{0}\n{1}: {2}'.format(res_value, technic_usage_id.product_uom_id.name, technic_usage_id.usage_value)
            else:
                res_value = u'{0}: {1}'.format(technic_usage_id.product_uom_id.name, technic_usage_id.usage_value)

        technic_data = {
            'technic_order_id': new_technic_order,
            'technic_id': technic_obj.id,
            'current_respondent_id': technic_obj.current_respondent_id,
            'engine_type': technic_obj.technic_norm_id.engine_type,
            'technic_usage': res_value
        }

        self.env['technic.distribution'].create(technic_data)

        technic_order_objs = self.env['technic.order'].browse(active_ids)
        for technic_order_obj in technic_order_objs:
            vals = {
                'state': 'to_merge',
                'order_id': new_technic_order,
                }
            self.env['technic.order'].write(technic_order_obj.id, vals)

        return True

class TechnicDistributionMerge(models.TransientModel):
    _name = 'technic.distribution.merge'
    _description = 'Technic distribution merge'

    merge_id = fields.Many2one('technic.order.merge', 'Technic order')
    technic_id = fields.Many2one('technic', 'Technic name', readonly=True)
    check = fields.Boolean('Select')
    state_number = fields.Char('State number', readonly=True)
    vin_serial_number = fields.Char('Serial number', readonly=True)
    current_respondent_id = fields.Many2one('hr.employee', 'Driver', required=False, readonly=True)
    engine_type = fields.Selection([('gas', 'Gas'),
                                    ('petrol', 'Petrol'),
                                    ('diesel', 'Diesel'),
                                    ('hybrid', 'Hybrid'),
                                    ('electric', 'Electric'),
                                    ('other', 'Other')], 'Engine type', readonly=True)
    technic_usage = fields.Char('Technic usage', readonly=True)
