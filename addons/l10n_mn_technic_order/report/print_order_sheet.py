# -*- coding: utf-8 -*-
##############################################################################
#
##############################################################################

from odoo import models, api

class PrintTechnicOrderSheet(models.TransientModel):
    _name = 'report.l10n_mn_technic_inspection.print_technic_order_sheet'

    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        technic_order = self.env['technic.order']
        report = report_obj._get_report_from_name('l10n_mn_technic_inspection.print_technic_order_sheet')
        technic_order_obj = technic_order.browse(docids)

        docargs = {
            'doc_ids': data['ids'],
            'docs': None,
            'technic_order': technic_order_obj,
        }
        return report_obj.render('l10n_mn_technic_inspection.print_technic_order_sheet', docargs)
