# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    "name": "Техник захиалга гэрээний модультай холбогдох",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
        Уг модуль нь техник захиалгыг гэрээний модультай холбох боломжийг олгоно.
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": ['l10n_mn_technic_order'],#, 'l10n_mn_contract_management'],
    "init": [],
    "data": [
        'views/technic_order_view.xml',
    ],

    "active": False,
    "installable": True,
    'contributors': ['Sugarsukh Sukhbaatar <sugarsukh@asterisk-tech.mn>'],
}
