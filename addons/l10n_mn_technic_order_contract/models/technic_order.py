# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-Now Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################
from odoo import fields, models, api

class TechnicOrder(models.Model):
    _inherit = 'technic.order'
    
    contract_id = fields.Many2one('contract.management', 'Contract')

class TechnicDistribution(models.Model):
    _inherit = 'technic.distribution'

    product_id = fields.Many2one('product.product', 'Product')
