# -*- coding: utf-8 -*-
##############################################################################
#
# Asterisk Technologies LLC, Enterprise Management Solution
# Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved #
# Email : info@asterisk-tech.mn
# Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import models, fields, api
# from odoo.tools.translate import _
# import logging
# _logger = logging.getLogger(__name__)

class ContractInvoiceLineWizard(models.Model):
	_name = "contract.invoice.line.wizard"

	wizard_id = fields.Many2one('create.contract.wizard', 'Wizard')
	product_id = fields.Many2one('product.product', 'Product', required=True)
	name = fields.Text('Description', required=True)
	quantity = fields.Float('Quantity', required=True)
	uom_id = fields.Many2one('product.uom', 'Unit of Measure', required=True)
	price_unit = fields.Float('Unit Price', required=True)
	start_date = fields.Date('Start Date')
	end_date = fields.Date('End Date')

class ContractTermLineWizard(models.Model):
	_name = 'contract.term.line.wizard'

	wizard_id = fields.Many2one('create.contract.wizard', 'Wizard'),
	source_id = fields.Many2one('contract.execution.source', string='Source'),
	product_id = fields.Many2one('product.product', string='Product'),
	price_unit = fields.Float('Unit Price'),
	limit = fields.Float('Limit'),
	is_contract_limit = fields.Boolean('Is Contract Limit'),
	start_date = fields.Date('Start Date'),
	end_date = fields.Date('End Date'),


class CreateContractWizard(models.Model):
	_name = 'create.contract.wizard'
	_description = 'Create Contract Wizard'

	contract_category_id = fields.Many2one('contract.category', required=True, string='Contract Category'),
	type = fields.Selection([('variable', 'Variable'), ('constant', 'Constant')], required=True, string='Type'),
	term_lines = fields.One2many('contract.term.line.wizard', 'wizard_id', 'Term Lines'),
	sale_lines = fields.One2many('contract.invoice.line.wizard', 'wizard_id', 'Sale Lines')

	def default_get(self):
		""" To get default values for the object.
		@param self: The object pointer.
		@param cr: A database cursor
		@param uid: ID of the user currently logged in
		@param fields: List of fields for which we want default values
		@param context: A standard dictionary
		@return: A dictionary which of fields with values.
		"""
		res = super(CreateContractWizard, self).default_get(self)
		if'active_id' in self:
			order = self.env['sale.order'].browse(['active_id'])
			wizard_lines = []
			res['type'] = 'constant'
			for line in order.order_line:
				wizard_lines.append((0, 0, {
					'product_id': line.product_id.id,
					'limit': line.product_uom_qty,
					'price_unit': line.price_unit,
				}))
			if 'term_lines' in fields:
				res['term_lines'] = wizard_lines
			sale_lines = []
			for line in order.order_line:
				sale_lines.append((0, 0, {
					'product_id': line.product_id.id,
					'name': line.name,
					'quantity': line.product_uom_qty,
					'uom_id': line.product_uom.id,
					'price_unit': line.price_unit,
					}))
			if 'sale_lines' in fields:
				res['sale_lines'] = sale_lines
		return res

	def create_contract(self, vals):
		contract_obj = self.env['contract.management']
		active_ids = self['active_ids']
		form = self.browse()
		for order in self.env['sale.order'].browse(self.ids):
			contract_id = contract_obj.create({
					'name': order.name,
					'partner_id': order.partner_id.id,
					'order_id': order.id,
					'recurring_sale_orders': True,
					'auto_type': form.type,
					'category_id': form.contract_category_id.id,
					'contract_currency_id': order.pricelist_id.currency_id.id,
					'total_amount': order.amount_total,
					'type': 'contract',
					'state': 'draft',
				})
			self.write({'project_id': contract_id})
			if form.type == 'constant':
				sale_line_obj = self.env['contract.invoice.line']
				for line in form.sale_lines:
					sale_line_obj.create({
							'product_id': line.product_id.id,
							'analytic_account_id': contract_id,
							'name': line.name,
							'quantity': line.quantity,
							'uom_id': line.uom_id.id,
							'price_unit': line.price_unit,
							'start_date': line.start_date,
							'end_date': line.end_date
						})
			else:
				term_line_obj = self.env['contract.term.line']
				for line in form.term_lines:
					term_line_obj.create({
							'contract_id': contract_id,
							'source_id': line.source_id.id,
							'product_id': line.product_id.id,
							'price_unit': line.price_unit,
							'limit': line.limit,
							'is_contract_limit': line.is_contract_limit,
							'start_date': line.start_date,
							'end_date': line.end_date,
							'last_date': line.start_date
					})
		return {
				'domain': "[('id','='," + str(contract_id) + ")]",
				'name': _('Contract'),
				'view_type': 'form',
				'view_mode': 'tree,form',
				'res_model': 'contract.management',
				'view_id': False,
				'type': 'ir.actions.act_window'
				}
