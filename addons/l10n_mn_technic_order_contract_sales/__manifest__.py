# -*- coding: utf-8 -*-
{
    'name': "Sales of Technic Order",
    'description': """
        Техник захиалгаас борлуулалт үүсгэх бол энэ модулийг суулгана
    """,
    'author': "Asterisk Technologies LLC",
    'website': "http://asterisk-tech.mn",
    'category': 'Technic Order, Mongolian Modules',
    'version': '1.0',
    'depends': ['l10n_mn_technic_order_contract', 'l10n_mn_sale'],
    'data': [
        'views/technic_order_view.xml',
        'wizard/to_create_sales_order_view.xml',
    ],
    'contributors': ['Asterisk Technologies LLC <info@asterisk-tech.mn>'],
}
