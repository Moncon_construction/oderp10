# -*- coding: utf-8 -*-
import math

from odoo import fields, models, api, _  # @UnresolvedImport
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport


class Saleorderline(models.Model):
    _inherit = 'sale.order'

    is_technic_sale = fields.Boolean('Is Technic Sale')

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    technic_id = fields.Many2one('technic', string='Technic')


class TechnicOrder(models.Model):
    _inherit = 'technic.order'

    @api.multi
    def _count_sale_order(self):
        res = {}
        for order in self:
            res[order.id] = 1 if order.sale_order_id else 0
        return res

    sale_order_id = fields.Many2one('sale.order', 'Sale Order')
    sale_order_count = fields.Float(compute='_count_sale_order', type='integer', string='Sale Order Count')

    def view_sale_order(self):
        mod_obj = self.env['ir.model.data']
        dummy, action_id = tuple(mod_obj.get_object_reference('sale', 'action_orders'))
        action = self.env['ir.actions.act_window'].read()
        order_id = []
        for order in self:
            order_id = order.sale_order_id.id
        return action

    @api.multi
    def action_create_sale_order(self):
        mod_obj = self.env['ir.model.data']
        
        if not self.customer_company_id:
            raise UserError(_('Please select partner of this technic order.'))
        if not self.contract_id:
            raise UserError(_('Please select contract of this technic order.'))
        for technic in self.technic_ids:
            if not technic.product_id:
                raise UserError(_('Please select product of  (%s) technic order\'s (%s) technic.') % (self.origin, self.technic_id.name))
        if self.sale_order_id:
            raise ValidationError(_('One of selected work orders (%s) already has sales order.') % self.origin)

        sale_order_id = self.env['sale.order'].create({
                                                        'partner_id': self.customer_company_id.id,
                                                        'project_id': self.contract_id.id,
                                                        'date_order': self.start_date,
                                                        'is_technic_sale': True,
                                                        'origin': self.origin,
                                                        'company_id':self.company_id.id,
                                                     })
        so_line_vals = {}
        for technic_line in self.technic_ids:
            if technic_line.product_id.id not in so_line_vals:

                so_line_vals[technic_line.product_id.id] = {'order_id': sale_order_id.id,
                                                          'product_id': technic_line.product_id.id,
                                                          'name': technic_line.product_id.name,
                                                          'product_uom_qty': self.total_km,
                                                          'product_uos_qty': self.total_km,
                                                          'technic_id': technic_line.technic_id.id,
                                                          'price_display': technic_line.product_id.lst_price,
                                                          'product_uom': technic_line.product_id.uom_id.id,
                                                          'price_unit': technic_line.product_id.lst_price,
                                                          'tax_id': [(6, 0, [x.id for x in technic_line.product_id.taxes_id])]
                                                          }
            else:
                so_line_vals[technic_line.product_id]['product_uom_qty'] += self.total_km
                so_line_vals[technic_line.product_id]['product_uos_qty'] += self.total_km

        for val in so_line_vals.values():
            self.env['sale.order.line'].create(val)

        self.env['technic.order'].write({'sale_order_id': sale_order_id.id})

        res = mod_obj.get_object_reference('sale', 'view_order_form')
        res_id = res and res[1] or False,

        return {
            'name': _('Sale Order'),
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res_id],
            'res_model': 'sale.order',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': sale_order_id.id,
        }

    def unlink(self):
        for to in self:
            if to.sale_order_id:
                raise Warning(_('Inappropriate Operation'), _('Delete related sale order first.'))
        return super(TechnicOrder, self).unlink()