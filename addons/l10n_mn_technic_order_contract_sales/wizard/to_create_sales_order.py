# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import fields, models, api, _
from datetime import datetime, date
from odoo.exceptions import UserError, ValidationError  # @UnresolvedImport

class ToCreateSalesOrder(models.Model):
    _name = "to.create.sales.order"
    _description = "Create Sales of Technic Order"

    @api.model
    def create_sales_order(self, vals):
        active_ids = self._context['active_ids'] or []

        technic_order = self.env['technic.order']
        mod_obj = self.env['ir.model.data']
        
        partner_ids = []
        contract_ids = []
        months = []
        
        rec = None
        records = technic_order.browse(active_ids)
        origin = ''
        for record in records:
            if not record.customer_company_id:
                raise UserError(_('Please select partner of this technic order.'))
            if not record.contract_id:
                raise UserError(_('Please select contract of this (%s) technic order.') % record.origin)
            for technic in record.technic_ids:
                if not technic.product_id:
                    raise UserError(_('Please select product of  (%s) technic order\'s (%s) technic.') % (record.origin, technic.technic_id.name))
            if record.sale_order_id:
                raise ValidationError(_('One of selected technic orders (%s) already has sales order.') % record.origin)
            if len(record.technic_ids) > 1:
                raise ValidationError(_(u'Техник хуваарилалтын 1 ээс олон мөр байна техник сонгоно уу') % record.origin)
            if record.customer_company_id.id not in partner_ids:
                partner_ids.append(record.customer_company_id.id)
            if record.contract_id.id not in contract_ids:
                contract_ids.append(record.contract_id.id)
            if record.start_date[:7] not in months:
                months.append(record.start_date[:7])
            origin += record.origin + ', '
            rec = record
                
        if len(partner_ids) > 1:
            raise ValidationError(_('You can only create sales of one partner\'s work order.'))
        if len(contract_ids) > 1:
            raise ValidationError(_('You can only create sales of one contract\'s work order.'))
        if len(months) > 1:
            raise ValidationError(_('You can only create sales of one month\'s work order.'))
        
        day = rec.start_date[:7]+'-28'
        rec_dict = {}
        for record in records:
            if record.company_id.id not in rec_dict:
                rec_dict[record.company_id.id] = {
                    'records': [record]
                }
            else:
                rec_dict[record.company_id.id]['records'].append(record)
        for dict in rec_dict:
            sale_order_id = self.env['sale.order'].create({'partner_id': rec.customer_company_id.id,
                                                        'project_id': rec.contract_id.analytic_account_id.id if rec.contract_id.analytic_account_id else False,
                                                        'date_order': datetime.strptime(day, "%Y-%m-%d"),
                                                        'origin': origin,
                                                        'is_technic_sale': True,
                                                        'company_id': dict
                                                         })
            so_line_vals = {}
            for record in rec_dict[dict]['records']:
                for technic_line in record.technic_ids:
                    if technic_line.product_id.id not in so_line_vals.keys():
                        so_line_vals[technic_line.product_id.id] = {'order_id': sale_order_id.id,
                                                                  'product_id': technic_line.product_id.id,
                                                                  'name': technic_line.product_id.name,
                                                                  'product_uom_qty': record.total_km,
                                                                  'product_uos_qty': record.total_km,
                                                                  'technic_id': technic_line.technic_id.id,
                                                                  'price_display': technic_line.product_id.lst_price,
                                                                  'product_uom': technic_line.product_id.uom_id.id,
                                                                  'price_unit': technic_line.product_id.lst_price,
                                                                  'tax_id': [(6, 0, [x.id for x in technic_line.product_id.taxes_id])]
                                                                  }
                    else:
                        so_line_vals[technic_line.product_id.id]['product_uom_qty'] += record.total_km
                        so_line_vals[technic_line.product_id.id]['product_uos_qty'] += record.total_km

            for val in so_line_vals.values():
                self.env['sale.order.line'].create(val)

            for record in rec_dict[dict]['records']:
                record.sale_order_id = sale_order_id.id
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
