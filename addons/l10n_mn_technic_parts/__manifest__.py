# -*- coding: utf-8 -*-

{
    'name': "Сэлбэг",

    'summary': """
        Техникийн бүртгэлд сэлбэгийн мэдээлэл хөтлөх""",

    'description': """
        Техникийг бүртгэлд сэлбэг болон сэлбэгийн нормыг бүртгэснээр сэлбэгийн ашиглалтыг сайжруулах боломжийг энэ модуль олгоно.
    """,

    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Technic',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['l10n_mn_technic', 'stock'],

    # always loaded
    'data': [
        'security/technic_parts_security.xml',
        'security/ir.model.access.csv',
        'views/technic_parts_views.xml',
        'views/technic_norm_views.xml',
        'views/technic_views.xml',
        'views/product_views.xml',
        'report/part_list_view.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo.xml',
    ],

    'contributors': ['Bayarkhuu Bataa <bayarkhuu@asterisk-tech.mn>'],
}
