# -*- coding: utf-8 -*-
from odoo import models, fields, _

class ProductTemplate(models.Model):
    _inherit = 'product.product'

    technic_parts_type = fields.Selection([('technic_part', 'Technic parts')],  string="Part type")

