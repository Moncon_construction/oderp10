# -*- coding: utf-8 -*-
from odoo import models, fields, api


class Technic(models.Model):
    _inherit = 'technic'

    parts = fields.One2many('technic.parts', 'technic', string="Spare parts")
    parts_count = fields.Integer(string='Spare parts count', compute='_compute_parts_count')
    parts_history_count = fields.Integer(string='Spare parts history count', compute='_compute_parts_history_count')

    @api.multi
    @api.depends('parts')
    def _compute_parts_count(self):
        for rec in self:
            rec.parts_count = len(rec.parts)

    @api.multi
    def _compute_parts_history_count(self):
        for rec in self:
            histories = self.env['technic.parts.history'].search([('change_technic', '=', rec.id)])
            rec.parts_history_count = len(histories)

    @api.multi
    def write(self, values):
        # Cut link from removed parts
        parts_dic = values.get("parts", None)
        cut_parts_ids = []
        if parts_dic:
            # change remove to cut link
            for part_triplet in parts_dic:
                if part_triplet[0] == 2:
                    part_triplet[0] = 3
                    cut_parts_ids.append(part_triplet[1])
            # update values
            values["parts"] = parts_dic

        # Call super write
        ret = super(Technic, self).write(values)

        # Update cut parts
        cut_parts = self.env['technic.parts'].search([('id', 'in', cut_parts_ids)])
        for cut_part in cut_parts:
            # change state
            cut_part.state = 'in_reserve'

        # update spare parts project
        project_id = values.get('project')
        if project_id:
            project = self.env['project.project'].search([('id', '=', project_id)])[0]
            for technic in self:
                for part in technic.parts:
                    part.project = project

        return ret


class TechnicUsage(models.Model):
    _inherit = 'technic.usage'

    @api.multi
    def write(self, vals):
        res = super(TechnicUsage, self).write(vals)

        # update spare part usage
        measurement_id = vals.get('usage_uom_id')
        value = vals.get('usage_value')
        technic_id = vals.get('technic_id')
        if measurement_id and value and technic_id:
            technic = self.env['technic'].browse(technic_id)
            measurement = self.env['usage.uom'].browse(measurement_id)
            for part in technic.parts:
                has_install_usage = False
                install_technic_usage_value = 0
                install_history = part.histories.filtered(lambda r: r.change_technic == technic).sorted(key=lambda r: r.create_date, reverse=True)
                if install_history:
                    install_technic_usage = install_history[0].change_technic_usages.filtered(lambda r: r.measurement == measurement)
                    if install_technic_usage:
                        install_technic_usage_value = install_technic_usage[0].value
                        has_install_usage = True
                if has_install_usage:
                    current_usage = part.usages.filtered(lambda r: r.measurement == measurement)
                    if current_usage:
                        previous_history = None
                        histories_technic_change = part.histories.filtered(lambda r: r.change_technic).sorted(key=lambda r: r.create_date, reverse=True)
                        if histories_technic_change:
                            history_last_technic_change = histories_technic_change[0]
                            all_previous_history = part.histories.filtered(lambda r: r.id < history_last_technic_change.id).sorted(key=lambda r: r.create_date, reverse=True)
                            previous_history = all_previous_history.filtered(lambda r: r.change_measurement == measurement)
                        additional_value = value - install_technic_usage_value
                        if additional_value < 0.0:
                            additional_value = 0.0
                        if previous_history:
                            previous_history_value = previous_history[0].change_value
                            current_usage.value = previous_history_value + additional_value
                        else:
                            current_usage.value = additional_value
        return res
