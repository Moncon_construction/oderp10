# -*- coding: utf-8 -*-
from odoo import models, fields

class TechnicNorm(models.Model):
    _inherit = 'technic.norm'

    part_norms = fields.One2many('technic.parts.norm', 'technic_norm', string="Spare part norms")

class TechnicType(models.Model):
    _inherit = 'technic.type'

    parts_norm_warn_users = fields.Many2many('res.users', 'technic_type_warn_users_rel', 'type_id', 'user_id', 'Parts Norm Expiration Warning Users')
    parts_norm_warn_groups = fields.Many2many('res.groups', 'technic_type_warn_groups_rel', 'type_id', 'group_id', 'Parts Norm Expiration Warning Groups')
