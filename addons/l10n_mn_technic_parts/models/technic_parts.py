# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo import exceptions
from odoo.tools.translate import _
from datetime import datetime, date, time, timedelta
import dateutil.relativedelta as relativedelta


class TechnicPartsNorm(models.Model):
    _name = 'technic.parts.norm'
    _description = 'Spare part norm'

    technic_norm = fields.Many2one('technic.type', 'Technic norm')
    product = fields.Many2one('product.product', 'Product', required=True, domain="[('technic_parts_type','=','technic_part')]")
    usage_measurement = fields.Many2one('usage.uom', 'Usage measurement', required=True)
    unit = fields.Many2one('product.uom', 'Measure unit', related='usage_measurement.usage_uom_id', readonly=True)
    norm = fields.Float('Norm value', required=True)
    notification_percent = fields.Integer('Notification Percent', required=True, default=95,
                                          help='If spare part usage is greater than this percent, system will send notification email to spare part manager.')

    @api.multi
    def name_get(self):
        name = ''
        res = []
        for line in self:
            if line.technic_norm:
                name += line.technic_norm.name + ', '
            name += '[' + str(line.product.default_code) + '] ' + line.product.name
            res.append((line.id, line.usage_measurement.name))
        return res

    _sql_constraints = [
        ('tn_p_um_unique',
         'UNIQUE(technic_norm, product, usage_measurement)',
         "Only one norm value of a spare part is available for technic norm and usage measurement!"),

        ('norm_check',
         'CHECK(norm > 0)',
         "Norm value must be greater than 0."),

        ('percent_check',
         'CHECK(notification_percent > 0 and notification_percent <= 100)',
         "Notification percent must be 1-100."),
    ]

    @api.multi
    @api.constrains('product', 'usage_measurement')
    def _check_unique_norm(self):
        for rec in self:
            if not rec.technic_norm:
                if self.env['technic.parts.norm'].search_count(
                        [('product', '=', rec.product.id), ('usage_measurement', '=', rec.usage_measurement.id)]) > 2:
                    raise exceptions.ValidationError(_('Already exists spare part norm for this measurement!'))

    # @api.onchange('technic_norm')
    # def _onchange_technic_norm(self):
    #     usage_measurements = self.env['usage.uom'].search([])
    #     if self.technic_norm:
    #         norm_usage_measurements_ids = []
    #         for line in self.technic_norm.usage_uom_ids:
    #             norm_usage_measurements_ids.append(line.usage_uom_id.id)
    #         usage_measurements = usage_measurements.browse(norm_usage_measurements_ids)
    #     self.usage_measurement = None
    #     return {'domain': {'usage_measurement': [('id', 'in', usage_measurements.ids)]}}

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        if 'norm' in fields:
            fields.remove('norm')
        if 'notification_percent' in fields:
            fields.remove('notification_percent')
        return super(TechnicPartsNorm, self).read_group(domain, fields, groupby, offset=offset, limit=limit, orderby=orderby, lazy=lazy)

    @api.model
    def create(self, values):
        part_norm = super(TechnicPartsNorm, self).create(values)
        # check flag
        self.set_part_flag(part_norm.product)
        return part_norm

    @api.multi
    def write(self, values):
        ret = super(TechnicPartsNorm, self).write(values)
        # check flag
        for record in self:
            self.set_part_flag(record.product)
        return ret

    def set_part_flag(self, product):
        product.technic_parts_type = 'technic_part'


class ResUsers(models.Model):
    _inherit = 'res.users'

    def send_parts_norm_expiration_email(self):
        technic_obj = self.env['technic']
        technic_type_obj = self.env['technic.type']
        technic_parts_obj = self.env['technic.parts']

        type_ids = technic_type_obj.search(['|', ('parts_norm_warn_groups', '!=', False),
                                                     ('parts_norm_warn_users', '!=', False)])

        notif = {}
        for type in technic_type_obj.browse(type_ids):
            if type.parts_norm_warn_users:
                for user in type.parts_norm_warn_users:
                    if user.id not in notif.keys():
                        notif[user.id] = {}
                    technic_parts_ids = technic_parts_obj.search([('technic.technic_type_id', '=', type.id),
                                                                           ('state', '=', 'in_use')])
                    for part in technic_parts_obj.browse(technic_parts_ids):
                        if part.technic not in notif[user.id].keys():
                            notif[user.id][part.technic] = {part.id: {}}
                        elif part.id not in notif[user.id][part.technic].keys():
                            notif[user.id][part.technic][part.id] = {}

                        notif[user.id][part.technic][part.id] = {'date': part.date, }
                        # 'part': part.product.name,
                        # 'norm': part.usage_percent }

            if type.parts_norm_warn_groups:
                user_ids = self.env['res.users'].search([
                    ('groups_id', 'in', [group.id for group in type.parts_norm_warn_groups])])
                for user_id in user_ids:
                    if user_id not in notif.keys():
                        notif[user_id] = {}
                    technic_parts_ids = technic_parts_obj.search([('technic.technic_type_id', '=', type.id),
                                                                           ('state', '=', 'in_use')])
                    for part in technic_parts_obj.browse(technic_parts_ids):
                        if part.technic not in notif[user_id].keys():
                            notif[user_id][part.technic] = {part.id: {}}
                        elif part.id not in notif[user_id][part.technic].keys():
                            notif[user_id][part.technic][part.id] = {}

                        notif[user_id][part.technic][part.id] = {'date': part.date, }
                        # 'part': part.product.name,
                        # 'norm': part.usage_percent }

        for user_id, mail_body in notif.iteritems():
            mail = ''
            for technic, parts in mail_body.iteritems():
                # for p_id, values in parts.iteritems():
                mail += """
                    <tr>
                        <td>%s</td>
                    </tr>
                """ % (technic.technic_name)
            print 'mail: ', mail
            self.send_notification(user_id, mail)

        return True

    def send_notification(self, mail_body, user_id):
        model_obj = self.env.get('ir.model.data')
        template_id = self.env['ir.model.data'].get_object_reference('l10n_mn_technic_parts',
                                                                          'parts_norm_expire_notif_cron_email_template')[
            1]
        print 'user.id: ', user_id
        self.env['email.template'].send_mail(template_id, user_id, force_send=False,
                                                  context={'mail_body': mail_body})
        return True


class TechnicParts(models.Model):
    _name = 'technic.parts'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    _description = 'Spare part'

    def _default_project(self):
        projects = self.env['project.project'].search(
            [('use_technic', '=', True)])
        if len(projects) == 1:
            return projects[0]
        else:
            return None

    @api.multi
    @api.depends('product', 'technic')
    def _compute_norm_id(self):
        for rec in self:
            if rec.technic:
                norms = self.env['technic.parts.norm'].search(
                    [('product', '=', rec.product.id), ('technic_norm', '=', rec.technic.technic_type_id.id)])
                if norms:
                    for norm in norms:
                        rec.norm_id = norm.id
                        break
                else:
                    product_norm = self.env['technic.parts.norm'].search(
                        [('product', '=', rec.product.id), ('technic_norm', '=', None)])
                    if product_norm:
                        rec.norm_id = product_norm.id
                        break
            else:
                norms = self.env['technic.parts.norm'].search(
                    [('product', '=', rec.product.id), ('technic_norm', '=', None)])
                for norm in norms:
                    rec.norm_id = norm.id
                    break

    @api.multi
    @api.depends('date')
    def _compute_running_duration(self):
        for rec in self:
            if rec.date:
                date = datetime.strptime(rec.date, '%Y-%m-%d')
                today = datetime.today()
                date_diff = relativedelta.relativedelta(today, date)
                duration_str = ""
                if date_diff.years:
                    duration_str = str(date_diff.years) + u' жил'
                if date_diff.months:
                    duration_str += "\t" + str(date_diff.months) + u' сар'
                if date_diff.days:
                    duration_str += "\t" + str(date_diff.days) + u' өдөр'
                rec.spent_duration = date_diff.years * 12 + date_diff.months + date_diff.days / 30.0
            else:
                rec.spent_duration = 0

    @api.multi
    @api.depends('kilometer')
    def _compute_running_km(self):
        for rec in self:
            if rec.technic:
                rec.running_km = rec.technic.last_km - rec.kilometer
            else:
                rec.running_km = 0

    @api.multi
    @api.depends('motohour')
    def _compute_running_motohour(self):
        for rec in self:
            if rec.technic:
                rec.running_motohour = rec.technic.last_motohour - rec.motohour
            else:
                rec.running_motohour = 0

    with_technic = fields.Boolean('Comes with technic')
    technic = fields.Many2one('technic', 'Technic', domain="[('state','!=','draft')]")
    employee_id = fields.Many2one('hr.employee', 'Owner Employee', domain="[('state','!=','draft')]")
    product = fields.Many2one('product.product', 'Product', required=True, domain="[('technic_parts_type','=','technic_part')]")
    serial = fields.Char('Serial number')
    name = fields.Char(compute='_compute_name')
    date = fields.Date('Date')
    state = fields.Selection([('in_use', 'In use'),
                              ('in_scrap', 'In scrap')], 'State', default='in_use')
    usages = fields.One2many('technic.parts.usage', 'part', 'Current usage')
    histories = fields.One2many('technic.parts.history', 'part', 'Spare part history')
    project = fields.Many2one('project.project', 'Project', domain="[('use_technic','=',True)]",
                              default=_default_project)
    install_date = fields.Datetime('Installed Date', compute='_compute_install_date')
    install_technic_usage = fields.Char('Install Technic Usage', compute='_compute_install_technic_usage')
    norm = fields.Char(string='Norm', compute='_compute_norm')
    norm_id = fields.Many2one('technic.parts.norm', 'Norm', compute='_compute_norm_id')
    qty = fields.Float(string='Quantity', required=True)
    usage_percent = fields.Float(string='Usage Percent', compute='_compute_usage_percent')
    usage_expiring = fields.Boolean('Usage expiring', compute='_compute_usage_expiring')
    description = fields.Text('Description')
    expense_cost = fields.Float('Expense Cost', required=True)
    norm_cost = fields.Float('Norm Cost', compute='_compute_norm_cost')
    motohour = fields.Float('Motohour')
    kilometer = fields.Float('Kilometer')

    spent_duration = fields.Float(string='Current Duration /month/', compute='_compute_running_duration')
    running_km = fields.Float(string='Running Kilometer', compute='_compute_running_km')
    running_motohour = fields.Float(string='Running Motohour', compute='_compute_running_motohour')

    @api.multi
    @api.depends('product', 'serial')
    def _compute_name(self):
        for rec in self:
            rec.name = u'{0} / {1}'.format(rec.product.name, rec.serial)

    @api.multi
    @api.depends('technic', 'histories')
    def _compute_install_date(self):
        for rec in self:
            if not rec.technic:
                rec.install_date = None
            else:
                rec.install_date = \
                rec.histories.filtered(lambda r: r.change_technic).sorted(key=lambda r: r.create_date, reverse=True)[
                    0].create_date

    @api.multi
    @api.depends('technic', 'histories')
    def _compute_install_technic_usage(self):
        for rec in self:
            if not rec.technic:
                rec.install_technic_usage = ''
            else:
                histories_change_technic = rec.histories.filtered(lambda r: r.change_technic).sorted(
                    key=lambda r: r.create_date, reverse=True)
                if histories_change_technic:
                    change_technic_usages = histories_change_technic[0].change_technic_usages
                    res = ''
                    for usage in change_technic_usages:
                        if res:
                            res = u'{0}\n{1}: {2}'.format(res, usage.measurement.name, usage.value)
                        else:
                            res = u'{0}: {1}'.format(usage.measurement.name, usage.value)
                    rec.install_technic_usage = res
                else:
                    rec.install_technic_usage = ''

    @api.multi
    @api.depends('product', 'technic', 'norm_id')
    def _compute_usage_percent(self):
        for rec in self:
            print rec.norm_id.norm, 'rec.norm_id.norm\n\n'
            if rec.norm_id and rec.norm_id.norm:
                if rec.norm_id.usage_measurement.is_motohour:
                    rec.usage_percent = rec.running_motohour / rec.norm_id.norm * 100
                elif rec.norm_id.usage_measurement.is_kilometer:
                    rec.usage_percent = rec.running_km / rec.norm_id.norm * 100
                elif rec.norm_id.usage_measurement.is_time:
                    rec.usage_percent = rec.spent_duration / rec.norm_id.norm * 100
            else:
                rec.usage_percent = 0

    @api.multi
    @api.depends('usage_percent')
    def _compute_norm_cost(self):
        for rec in self:
            rec.norm_cost = rec.expense_cost * rec.usage_percent / 100

    @api.multi
    @api.depends('product', 'usage_percent')
    def _compute_usage_expiring(self):
        for rec in self:
            if rec.usage_percent >= 100:
                rec.usage_expiring = False
            else:
                norms = self.env['technic.parts.norm'].search([('product', '=', rec.product.id)])
                res = False
                remove_norms = None
                for norm in norms:
                    if len(norms.filtered(lambda r: r.usage_measurement == norm.usage_measurement)) > 1:
                        if remove_norms:
                            remove_norms = remove_norms + norms.filtered(lambda
                                                                             r: r.usage_measurement == norm.usage_measurement and r.technic_norm != rec.technic.technic_norm_id)
                        else:
                            remove_norms = norms.filtered(lambda
                                                              r: r.usage_measurement == norm.usage_measurement and r.technic_norm != rec.technic.technic_norm_id)
                if remove_norms:
                    norms = norms - remove_norms
                for norm in norms:
                    usage = rec.usages.filtered(lambda r: r.measurement == norm.usage_measurement)
                    if usage:
                        if (usage.value / norm.norm) * 100.0 > norm.notification_percent:
                            res = True
                rec.usage_expiring = res

    '''
    _sql_constraints = [
        ('serial_unique',
         'UNIQUE(serial)',
         "Serial number must be unique!"),
    ]
    '''

    @api.onchange('technic')
    def _onchange_technic(self):
        if self.with_technic:
            usage_history_ids = self.env['technic.usage.history'].search([('technic_id', '=', self.technic.id)],
                                                                         order='change_date ASC', limit=1)
            for usage_history_id in usage_history_ids:
                if usage_history_id.change_measurement.is_kilometer:
                    self.kilometer = usage_history_id.change_value
                else:
                    self.kilometer = 0
                if usage_history_id.change_measurement.is_motohour:
                    self.motohour = usage_history_id.change_value
                else:
                    self.motohour = 0
        if self.technic:
            self.project = self.technic.project
            return {
                'domain': {
                    'project': [('id', 'in', [self.technic.project.id])]
                }
            }
        else:
            projects = self.env['project.project'].search(
                [('use_technic', '=', True)])
            return {
                'domain': {
                    'project': [('id', 'in', projects.ids)]
                }
            }

    @api.multi
    def create_non_exist_usages(self):
        '''
        Must call this method before every usage update
        '''
        for part in self:
            if part.technic:
                TechnicPartsUsage = self.env['technic.parts.usage']
                technic_norm_measurements = self.env['usage.uom.line'].search(
                    [('technic_norm_id', '=', part.technic.technic_norm_id.id)])
                for measurement in technic_norm_measurements:
                    if TechnicPartsUsage.search_count(
                            [('part', '=', part.id), ('measurement', '=', measurement.usage_uom_id.id)]) == 0:
                        usage_values = {
                            'part': part.id,
                            'measurement': measurement.usage_uom_id.id
                        }
                        TechnicPartsUsage.create(usage_values)

    def set_part_flag(self, product):
        product.technic_parts_type = 'technic_part'


class ProductKits(models.Model):
    _name = 'technic.product.kits'

    kit_product_id = fields.Many2one('product.product', 'Kit Product', required=True)
    qty = fields.Float('Quantity', required=True)
    technic_part_id = fields.Many2one('technic.parts', 'Technic Part', required=True)


class TechnicParts(models.Model):
    _inherit = 'technic.parts'

    @api.multi
    @api.depends('product.has_product_kits')
    def _compute_has_product_kits(self):
        for part in self:
            print part.product.product_tmpl_id.id, 'aaaa\n\n'
            print part.product.product_tmpl_id.has_product_kits, 'ssss\n\n'
            print part.product.has_product_kits, 'aaaa\n\n'
            part.has_product_kits = part.product.has_product_kits

    has_product_kits = fields.Boolean('Has Product Kits', compute='_compute_has_product_kits', store=True)
    product_kit_ids = fields.One2many('technic.product.kits', 'technic_part_id', 'Product Kits')

    @api.model
    def create(self, vals):
        # get project
        if vals.get('technic'):
            vals['project'] = self.env['technic'].search([('id', '=', vals.get('technic'))])[0].project.id

        # call super
        part = super(TechnicParts, self).create(vals)

        description = _('Spare part registered.')

        # create usages
        part.create_non_exist_usages()

        # create part state history
        self.env['technic.parts.history'].create({
            'part': part.id,
            'change_state': vals.get('state'),
            'description': description
        })

        # create part technic history
        if vals.get('technic'):
            history = self.env['technic.parts.history'].create({
                'part': part.id,
                'change_technic': part.technic.id,
                'description': description
            })
            for technic_usage in part.technic.technic_usage_ids:
                self.env['technic.parts.history.technic.usage'].create({
                    'history': history.id,
                    'measurement': technic_usage.usage_uom_id.id,
                    'value': technic_usage.usage_value
                })

        # check flag
        self.set_part_flag(part.product)

        # return
        return part

    @api.multi
    def write(self, vals):

        # get project
        if vals.get('technic'):
            vals['project'] = self.env['technic'].search([('id', '=', vals.get('technic'))])[0].project.id

        description = _('Spare part information changed.')
        for part in self:
            # create part state history
            if part.state != vals.get('state'):
                self.env['technic.parts.history'].create({
                    'part': part.id,
                    'change_state': vals.get('state'),
                    'description': description
                })

            # create part technic history
            if 'technic' in vals:
                if vals['technic']:
                    new_technic = self.env['technic'].browse(vals['technic'])
                    if new_technic != part.technic:
                        history = self.env['technic.parts.history'].create({
                            'part': part.id,
                            'change_technic': new_technic.id,
                            'description': description
                        })
                        for technic_usage in new_technic.technic_usage_ids:
                            self.env['technic.parts.history.technic.usage'].create({
                                'history': history.id,
                                'measurement': technic_usage.usage_uom_id.id,
                                'value': technic_usage.usage_value
                            })
                else:
                    self.env['technic.parts.history'].create({
                        'part': part.id,
                        'change_technic': None,
                        'description': description
                    })

        # call super
        result = super(TechnicParts, self).write(vals)

        # create usages
        self.create_non_exist_usages()

        # check flag
        for record in self:
            self.set_part_flag(record.product)

        # return
        return result

    @api.multi
    def unlink(self):
        # check used part
        for record in self:
            for usage in record.usages:
                if usage.value > 0:
                    raise exceptions.ValidationError("Can't delete used spare part. You can scrap the spare part.")
        # call super
        ret = super(TechnicParts, self).unlink()
        return ret


class TechnicPartsUsage(models.Model):
    _name = 'technic.parts.usage'
    _description = 'Spare part usage'

    part = fields.Many2one('technic.parts', 'Spare part', ondelete='cascade')
    measurement = fields.Many2one('usage.uom', 'Usage measurement', required=True)
    unit = fields.Many2one('product.uom', 'Usage measure unit', related='measurement.usage_uom_id', store=True)
    value = fields.Float('Usage value', required=True, default=0)

    _sql_constraints = [
        ('p_um_unique',
         'UNIQUE(part, measurement)',
         "So many occurrences of certain usage measurement is not available for a spare part!"),

        ('usage_check',
         'CHECK(value >= 0)',
         "Current usage must be greater than or equal to 0."),
    ]

    @api.multi
    def write(self, vals):
        # create usage history
        for usage in self:
            history_values = {
                'part': usage.part.id,
                'change_measurement': usage.measurement.id,
                'change_value': vals.get('value'),
                'change_measurement_technic': usage.part.technic.id
            }
            self.env['technic.parts.history'].create(history_values)

        return super(TechnicPartsUsage, self).write(vals)


class TechnicPartsHistory(models.Model):
    _name = 'technic.parts.history'
    _description = 'Spare part history'
    _order = 'create_date DESC'

    part = fields.Many2one('technic.parts', 'Spare part', ondelete='cascade')
    change_measurement = fields.Many2one('usage.uom', 'Change measurement')
    change_unit = fields.Many2one('product.uom', 'Change measurement unit', related='change_measurement.usage_uom_id',
                                  store=True)
    change_value = fields.Float('Change measurement value')
    change_measurement_technic = fields.Many2one('technic', 'Change measurement technic')
    change_technic = fields.Many2one('technic', 'Change technic')
    change_technic_usages = fields.One2many('technic.parts.history.technic.usage', 'history', 'Change technic usage')
    change_state = fields.Selection([
        ('in_use', 'In use'),
        ('in_scrap', 'In scrap')], 'Change state')
    description = fields.Text('Description')
    latest = fields.Boolean(string='Is latest', compute='_compute_latest', store=True)

    @api.multi
    @api.depends('part', 'change_technic')
    def _compute_latest(self):
        for rec in self:
            if rec.change_technic and rec.change_technic == rec.part.technic:
                rec.latest = True
            else:
                rec.latest = False

    @api.model
    def create(self, vals):
        part = self.env['technic.parts'].browse(vals.get('part'))

        # check whether measurement value is changed or value is 0
        if 'change_measurement' in vals:
            if vals.get('change_value'):
                current_usage = part.usages.filtered(lambda r: r.measurement.id == vals['change_measurement'])
                if current_usage.value == vals['change_value']:
                    return
            else:
                return

        return super(TechnicPartsHistory, self).create(vals)


class TechnicPartsHistoryTechnicUsage(models.Model):
    _name = 'technic.parts.history.technic.usage'
    _description = 'Change technic usage'

    history = fields.Many2one('technic.parts.history', 'Spare part history', ondelete='cascade')
    measurement = fields.Many2one('usage.uom', 'Change technic measurement')
    unit = fields.Many2one('product.uom', 'Change technic measure unit', related='measurement.usage_uom_id', store=True)
    value = fields.Float('Change technic measurement value')
