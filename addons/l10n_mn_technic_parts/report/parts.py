# -*- coding: utf-8 -*-

from StringIO import StringIO
import base64
import time

import xlwt

from odoo import models, fields, api, _
from datetime import datetime

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport
from odoo.addons.l10n_mn_report.tools.report_excel_fit_sheet_wrapper import ReportExcelFitSheetWrapper  # @UnresolvedImport

class ReportPartListWizard(models.TransientModel):
    _name = 'report.part.list.wizard'

    STATES = [('in_use', 'Use'),
              ('in_scrap', 'Scrap')]

    technic_id = fields.Many2one('technic', 'Filter')
    group_by = fields.Selection(STATES, 'Group by')

    @api.multi
    def export_report(self):
        # create workbook
        book = xlwt.Workbook(encoding='utf8')

        # create sheet
        report_name = _('Part List')
        sheet = ReportExcelFitSheetWrapper(book.add_sheet(report_name))
        rowx = 0
        colx = 0
        colx_number = 8

        filename = "%s_%s.xls" % (_('parts list'), datetime.now().strftime('%Y%m%d_%H%M%S'))

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix='part_list', form_title=report_name).create({})

        # create header
        report_number = _('Report №%s') % 1
        sheet.write_merge(rowx, rowx, colx, colx_number, report_number, ReportExcelCellStyles.number_xf, size='number')
        rowx += 1

        sheet.write_merge(rowx, rowx, colx, colx_number, report_name.upper(), ReportExcelCellStyles.name_xf, size='name')
        rowx += 1

        # data
        filter = []
        if self.technic_id:
            filter.append(('technic', '=', self.technic_id.id))
            sheet.write_merge(rowx, rowx, colx, colx_number, '%s: %s' % (_('Technic'), self.technic_id.name), ReportExcelCellStyles.filter_xf, size='filter')
            rowx += 1
        if self.group_by:
            if self.group_by == 'in_use':
                sheet.write_merge(rowx, rowx,colx, colx_number, '%s: %s' % (_('Group by'), _('Use')),
                                  ReportExcelCellStyles.filter_xf, size='filter')
            rowx += 1
        if self.group_by:
            if self.group_by == 'in_scrap':
                sheet.write_merge(rowx, rowx, colx, colx_number, '%s: %s' % (_('Group by'), _('Scrap')),
                                  ReportExcelCellStyles.filter_xf, size='filter')
            rowx += 1
        data = self.env['technic.parts'].search(filter)

        # create title
        sheet.write_merge(rowx, rowx+1, colx, colx, _('№'), ReportExcelCellStyles.title_xf, size='name')
        sheet.write_merge(rowx, rowx+1, colx+1, colx+1, _('Part'), ReportExcelCellStyles.title_xf, size='name')
        sheet.write_merge(rowx, rowx+1, colx+2, colx+2, _('Serial'), ReportExcelCellStyles.title_xf, size='name')
        sheet.write_merge(rowx, rowx+1, colx+3, colx+3, _('Technic'), ReportExcelCellStyles.title_xf, size='name')
        sheet.write_merge(rowx, rowx, colx+4, colx+6, _('Usage'), ReportExcelCellStyles.title_xf, size='name')
        sheet.write_merge(rowx+1, rowx+1, colx+4, colx+4, _('Measurement'), ReportExcelCellStyles.title_xf, size='name')
        sheet.write_merge(rowx+1, rowx+1, colx+5, colx+5, _('Uom'), ReportExcelCellStyles.title_xf, size='name')
        sheet.write_merge(rowx+1, rowx+1, colx+6, colx+6, _('Current usage'), ReportExcelCellStyles.title_xf, size='name')
        sheet.write_merge(rowx, rowx+1, colx+7, colx+7, _('Usage history'), ReportExcelCellStyles.title_xf, size='name')
        sheet.write_merge(rowx, rowx+1, colx+8, colx+8, _('Parts history'), ReportExcelCellStyles.title_xf, size='name')
        rowx += 2

        # create cell
        print 'asdasd',data
        if data:
            num = 0
            for data_list in data:
                usage_history_str = ''
                part_history_str = ''
                usage_history_lines = self.env['technic.parts.history'].search([('change_measurement', '!=', False), ('part', '=', data_list.id)])
                for usage_history in usage_history_lines:
                    usage_history_str += '%s: %s: %s: %s: ,\n' % (usage_history.create_date, usage_history.change_measurement.name, usage_history.change_value, usage_history.description)
                part_history_lines = self.env['technic.parts.history'].search([('change_technic', '!=', False), ('part', '=', data_list.id)])
                for part_history in part_history_lines:
                    part_history_str += '%s: %s: %s: %s: ,\n' % (part_history.create_date, part_history.change_technic.name, part_history.change_state, part_history.description)
                num += 1

                row = rowx
                if data_list.usages:
                    for usage in data_list.usages:
                        sheet.write(row, colx+4, usage.measurement.name, ReportExcelCellStyles.content_text_xf, size='content')
                        sheet.write(row, colx+5, usage.unit.name, ReportExcelCellStyles.content_text_xf, size='content')
                        sheet.write(row, colx+6, usage.value, ReportExcelCellStyles.content_text_xf, size='content')
                        row += 1
                else:
                    sheet.write(row, colx+4, '', ReportExcelCellStyles.content_text_xf,size='content')
                    sheet.write(row, colx+5, '', ReportExcelCellStyles.content_text_xf,size='content')
                    sheet.write(row, colx+6, '', ReportExcelCellStyles.content_text_xf,size='content')
                    row += 1
                sheet.write_merge(rowx, rowx + len(data_list.usages) - 1, colx + 0, colx + 0, num, ReportExcelCellStyles.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + len(data_list.usages) - 1, colx + 1, colx + 1, data_list.product.name, ReportExcelCellStyles.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + len(data_list.usages) - 1, colx + 2, colx + 2, data_list.serial, ReportExcelCellStyles.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + len(data_list.usages) - 1, colx + 3, colx + 3, data_list.technic.name, ReportExcelCellStyles.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + len(data_list.usages) - 1, colx + 7, colx + 7, usage_history_str, ReportExcelCellStyles.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + len(data_list.usages) - 1, colx + 8, colx + 8, part_history_str, ReportExcelCellStyles.content_text_xf, size='content')
                rowx += len(data_list.usages)

        # prepare file data
        io_buffer = StringIO()
        book.save(io_buffer)
        filedate = base64.encodestring(io_buffer.getvalue())
        io_buffer.close()

        val = {'file_data': filedate, 'file_name': filename}
        export_id = self.env['excel.output.part.discharge'].create(val)

        return {
            'view_mode': 'form',
            'res_id': export_id.id,
            'res_model': 'excel.output.part.discharge',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'context': self._context,
            'target': 'new',
        }
