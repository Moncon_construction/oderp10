# -*- coding: utf-8 -*-
from openerp import models
from openerp.tools.translate import _

from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport

class ReportTechnicPassportWizard(models.TransientModel):
    _inherit = 'report.technic.passport.wizard'

    def write_components(self, obj, sheet, rowx):
        sheet, rowx = super(ReportTechnicPassportWizard, self).write_components(obj, sheet, rowx)

        #create parts
        part_datas = obj.technic.parts
        if part_datas:
            colx_number = 8
            sheet.write_merge(rowx, rowx, 0, colx_number, _('Parts Information'), ReportExcelCellStyles.title_xf)
            rowx += 1
            colx = 0
            num = 1
            sheet.write(rowx, colx, '№', ReportExcelCellStyles.group_xf)
            sheet.write_merge(rowx, rowx, 1, 2, _('Parts'), ReportExcelCellStyles.group_xf)
            sheet.write(rowx, 3, _('Uom'), ReportExcelCellStyles.group_xf)
            sheet.write(rowx, 4, _('Norm value'), ReportExcelCellStyles.group_xf)
            sheet.write(rowx, 5, _('Usage measurement'), ReportExcelCellStyles.group_xf)
            sheet.write(rowx, 6, _('Current usage'), ReportExcelCellStyles.group_xf)
            sheet.write(rowx, 7, _('Residual'), ReportExcelCellStyles.group_xf)
            sheet.write(rowx, 8, _('Installed Date'), ReportExcelCellStyles.group_xf)
            rowx += 1

        usage_data = obj.technic.technic_usage_ids
        for lines in usage_data:
            usage_value_line = lines.usage_value

        acc_data = obj.env['technic.parts'].search([('technic', '=', obj.technic.id)])

        for data_acc in acc_data:
            norm_register_data = obj.env['technic.parts.norm'].search([('product', '=', data_acc.product.id)])
            n = 'None'
            if norm_register_data.norm:
                n = norm_register_data
            t = len(data_acc.usages) - 1
            sheet.write_merge(rowx, rowx + t, 0, 0, num, ReportExcelCellStyles.content_number_xf)
            sheet.write_merge(rowx, rowx + t, 1, 2, data_acc.product.name, ReportExcelCellStyles.content_text_xf)
            sheet.write_merge(rowx, rowx + t, 8, 8, data_acc.date, ReportExcelCellStyles.content_number_xf)
            num += 1
            for data_acc_usage in data_acc.usages:
                sheet.write(rowx, 3, data_acc_usage.measurement.name, ReportExcelCellStyles.content_text_xf)
                if type(n) == str:
                    sheet.write(rowx, 4, 0, ReportExcelCellStyles.content_number_xf)
                elif data_acc_usage.measurement.id == n.usage_measurement.id:
                    sheet.write(rowx, 4, n.norm, ReportExcelCellStyles.content_number_xf)
                sheet.write(rowx, 5, data_acc_usage.measurement.name, ReportExcelCellStyles.content_text_xf)
                sheet.write(rowx, 6, data_acc_usage.value, ReportExcelCellStyles.content_text_xf)
                sheet.write(rowx, 7, usage_value_line - data_acc_usage.value, ReportExcelCellStyles.content_text_xf)
                rowx += 1
        rowx += 1

        return sheet, rowx
