# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-Now Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

{
    "name": "Агуулахаас сэлбэг шаардах",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
        Уг модуль нь бараа материалын шаардахаар сэлбэг шаардсан бол барааг олгоход сэлбэгийн бүртгэлийг автоматаар үүсгэх боломжийг олгоно.
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": ['l10n_mn_stock',
                'l10n_mn_technic_parts',
                'l10n_mn_product_expense',
                'l10n_mn_technic_expense'],
    "init": [],
    "data": [
        'data/technic_parts_cron.xml',
        'views/technic_parts_view.xml',
        'wizard/change_technic_wizard_view.xml',
        'wizard/change_information_wizard_view.xml'
    ],
    "demo_xml": [
    ],
    "active": False,
    "installable": True,
}
