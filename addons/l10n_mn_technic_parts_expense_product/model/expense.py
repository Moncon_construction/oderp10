# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from datetime import datetime, time
from odoo import models, fields, api, _
from odoo.exceptions import UserError

class ProductExpense(models.Model):
    _inherit = 'product.expense'
    
    def _parts_count(self):
        for this in self:
            this.parts_count = len(self.technic_ids.ids)

    parts_count = fields.Integer(compute=_parts_count, string='Parts count')
    technic_ids = fields.One2many('technic.parts', 'expense_id', string='Technic Parts')