# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from datetime import datetime, time
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import logging

_logger = logging.getLogger('STOCK')

class TechnicParts(models.Model):
    _inherit = 'technic.parts'
    
    @api.depends('kilometer', 'motohour', 'technic.last_km', 'technic.last_motohour',  'norm_id.norm')
    def _compute_usage_percent_motohour_km(self):
        for obj in self:
            if obj.norm_id.norm > 0:
                obj.usage_percent_motohour = (obj.technic.last_motohour - obj.motohour) / obj.norm_id.norm * 100
                obj.usage_percent_km = (obj.technic.last_km - obj.kilometer) / obj.norm_id.norm * 100
                
    @api.multi
    @api.depends('kilometer')
    def _compute_running_km(self):
        for rec in self:
            if rec.technic and rec.state == 'in_use':
                rec.running_km = rec.technic.last_km - rec.kilometer + rec.start_performance_km
            else:
                rec.running_km = rec.scrap_km

    @api.multi
    @api.depends('motohour')
    def _compute_running_motohour(self):
        for rec in self:
            if rec.technic and rec.state == 'in_use':
                rec.running_motohour = rec.technic.last_motohour - rec.motohour + rec.start_performance_motohour
            else:
                rec.running_motohour = rec.scrap_motohour
                
    expense_id = fields.Many2one('product.expense', 'Expense')
    move_id = fields.Many2one('stock.move', 'Stock Move')
    start_performance_motohour = fields.Float('Start Performance Motohour', track_visibility='onchange', copy=False)
    start_performance_km = fields.Float('Start Performance KM', track_visibility='onchange', copy=False)
    technic = fields.Many2one('technic', 'Technic', domain="[('state','!=','draft')]", track_visibility='onchange')
    usage_percent_motohour = fields.Float('Usage Percent (Motohour)', compute='_compute_usage_percent_motohour_km')
    usage_percent_km = fields.Float('Usage Percent (KM)', compute='_compute_usage_percent_motohour_km')
    scrap_motohour = fields.Float('Scrap Motohour')
    scrap_km = fields.Float('Scrap KM')
    running_km = fields.Float(string='Running Kilometer', compute='_compute_running_km')
    running_motohour = fields.Float(string='Running Motohour', compute='_compute_running_motohour')
    
class StockImmediateTransfer(models.TransientModel):
    _inherit = 'stock.immediate.transfer'

    @api.multi
    def process(self):
        self.ensure_one()
        this = super(StockImmediateTransfer, self).process()
        stock_picking = self.pick_id

        technic_id = None
        employee_id = None
        if stock_picking.expense.is_technic_expense:
            if stock_picking.expense.technic_id:
                technic_id = stock_picking.expense.technic_id.id
                employee_id = stock_picking.expense.employee.id
                for line in stock_picking.move_lines:
                    if line.product_id.technic_parts_type == 'technic_part':
                        qty = line.product_qty
                        while qty != 0:
                            value = {'product': line.product_id.id,
                                     'technic': technic_id,
                                     'employee_id': employee_id,
                                     'date': self.force_date or time.strftime('%Y-%m-%d'),
                                     'move_id': line.id,
                                     'qty': 1,
                                     'state': 'in_use',
                                     'expense_cost': line.price_unit,
                                     'motohour': stock_picking.expense.technic_id.last_motohour,
                                     'kilometer': stock_picking.expense.technic_id.last_km,
                                     'expense_id': stock_picking.expense.id
                                     }
                            qty -= 1
                            part_id = self.env['technic.parts'].create(value)

                            if  line.product_id.product_kits:
                                for kit in line.product_id.product_kits:
                                    self.env['technic.product.kits'].create({'kit_product_id': kit.kit_product_id.id,
                                                                             'qty': kit.qty,
                                                                             'technic_part_id': part_id.id
                                                                             })

        return this
