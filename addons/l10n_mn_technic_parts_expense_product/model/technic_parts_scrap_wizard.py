# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions

class TechnicPartsScrapWizard(models.TransientModel):
    _inherit = 'technic.parts.scrap.wizard'

    @api.multi
    def done_request(self):
        res = super(TechnicPartsScrapWizard, self).done_request()
        for part in self.parts:
            part.scrap_motohour = part.running_motohour
            part.scrap_km = part.running_km
        return res