# -*- encoding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
##############################################################################

from odoo import models, fields, api, _
from odoo import exceptions
from datetime import datetime

class ChangeTechnicInformationWizard(models.TransientModel):
    _name = 'change.technic.information.wizard'
    _description = "Change Technic Parts Information"
    
    initial_motohour = fields.Float('Initial Motohour')
    initial_kilometer = fields.Float('Initial Kilometer')
    
    @api.multi
    def change_technic_information(self):
        ''' Сэлбэгийн мэдээллийг солих'''
        context = self._context
        parts = self.env['technic.parts'].browse(context.get('active_ids'))
        for part in parts:
            if part.state == 'in_use':
                if self.initial_motohour != 0:
                    part.motohour = self.initial_motohour
                if self.initial_kilometer != 0:
                    part.kilometer = self.initial_kilometer
        return {'type': 'ir.actions.act_window_close'}