# -*- coding: utf-8 -*-
{
    'name': "Сэлбэгийг Эзэмшигчид Картлах",

    'summary': """
        Сэлбэгийг Эзэмшигчид Картлах""",

    'description': """
        Сэлбэгийг эзэмшигчид картлах, шилжүүлэх боломж олгоно
    """,

    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Technic',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['l10n_mn_technic_parts'],

    # always loaded
    'data': [
        'wizard/technic_parts_move_wizard_view.xml',
        'wizard/technic_parts_daily_usage_wizard_view.xml',
        'views/technic_parts_move_view.xml',
        'views/technic_parts_daily_usage_view.xml',
        'security/ir.model.access.csv',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo.xml',
    ],

    'contributors': ['Bayarkhuu Bataa <bayarkhuu@asterisk-tech.mn>'],
}
