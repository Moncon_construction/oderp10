# -*- encoding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
##############################################################################

from odoo import models, fields, api, _
from odoo import exceptions
from datetime import datetime

class TechnicPartsMove(models.Model):
    _name = 'technic.parts.daily.usage'
    _description = "Technic Parts Daily Usage"
    
    technic_part_id = fields.Many2one('technic.parts', 'Technic Part')
    date = fields.Date('Date')
    using_employee_id = fields.Many2one('hr.employee', 'New Owner')
    usage_state = fields.Selection([('in_use', 'In use'),
                                    ('in_stock', 'In stock')], 'Usage State', default="in_stock")
    description = fields.Text('Description')
    
class TechnicParts(models.Model):
    _inherit = 'technic.parts'
    
    usage_state = fields.Selection([('in_use', 'In use'),
                                    ('in_stock', 'In stock')], 'Usage State', default="in_stock")
    using_employee_id = fields.Many2one('hr.employee', 'Current Employee')
    
    daily_usage_ids = fields.One2many('technic.parts.daily.usage','technic_part_id','Technic Part Daily Usage')
    
    @api.multi
    def get_back(self):
        ''' Сэлбэг буцаан авах '''
        part = self.env['technic.parts'].browse(self._context.get('active_id'))
        part.using_employee_id = None
        self.env['technic.parts.daily.usage'].create({'technic_part_id': part.id,
                                                      })
