# -*- encoding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
##############################################################################

from odoo import models, fields, api, _
from odoo import exceptions
from datetime import datetime

class TechnicPartsMove(models.Model):
    _name = 'technic.parts.move'
    _description = "Technic Parts Move"
    
    technic_part_id = fields.Many2one('technic.parts', 'Technic Part')
    date = fields.Date('Date')
    old_owner_id = fields.Many2one('hr.employee', 'Old Owner', required=True)
    new_owner_id = fields.Many2one('hr.employee', 'New Owner', required=True)
    description = fields.Text('Description')
    
class TechnicParts(models.Model):
    _inherit = 'technic.parts'
    
    part_move_ids = fields.One2many('technic.parts.move','technic_part_id','Technic Part Moves')