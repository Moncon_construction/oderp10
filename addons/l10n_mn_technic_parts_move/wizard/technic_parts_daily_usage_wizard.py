# -*- encoding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
##############################################################################

from odoo import models, fields, api, _
from odoo import exceptions
from datetime import datetime

class TechnicPartsDailyUsageWizard(models.TransientModel):
    _name = 'technic.parts.daily.usage.wizard'
    _description = "Technic Parts Move"
    
    @api.multi
    def give(self):
        ''' Сэлбэг олгох '''
        context = self._context
        part = self.env['technic.parts'].browse(context.get('active_id'))
        
        move_obj = self.env['technic.parts.daily.usage']
        if self.give_employee_id:
            move_vals = {
                'date': self.date,
                'using_employee_id': self.give_employee_id.id,
                'usage_state': 'in_use',
                'description': self.description,
                'technic_part_id': part.id
            }
            move_id = move_obj.create(move_vals)
            part.using_employee_id = self.give_employee_id.id
            part.usage_state = 'in_use'

        return {'type': 'ir.actions.act_window_close'}
    
    date = fields.Date('Date', default=lambda *a:datetime.now().strftime('%Y-%m-%d'))
    give_employee_id = fields.Many2one('hr.employee', 'New Owner', required=True)
    description = fields.Text('Description')
    
class TechnicPartsDailyUsageWizard(models.TransientModel):
    _name = 'technic.parts.daily.usage.get.back.wizard'
    _description = "Technic Parts Move"
    
    @api.multi
    def get_back(self):
        ''' Сэлбэг буцаан авах '''
        context = self._context
        part = self.env['technic.parts'].browse(context.get('active_id'))
        
        move_obj = self.env['technic.parts.daily.usage']
        move_vals = {
            'date': self.date,
            'using_employee_id': None,
            'usage_state': 'in_stock',
            'description': self.description,
            'technic_part_id': part.id
        }
        move_id = move_obj.create(move_vals)
        part.using_employee_id = None
        part.usage_state = 'in_stock'

        return {'type': 'ir.actions.act_window_close'}
    
    date = fields.Date('Date', default=lambda *a:datetime.now().strftime('%Y-%m-%d'))
    description = fields.Text('Description')