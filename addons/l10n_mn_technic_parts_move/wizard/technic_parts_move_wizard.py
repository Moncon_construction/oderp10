# -*- encoding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
##############################################################################

from odoo import models, fields, api, _
from odoo import exceptions
from datetime import datetime

class TechnicPartsMoveWizard(models.TransientModel):
    _name = 'technic.parts.move.wizard'
    _description = "Technic Parts Move"
    
    def _get_old_owner(self):
        context = self._context
        parts = self.env['technic.parts'].browse(context.get('active_id'))
        if parts.employee_id:
            return parts.employee_id.id
        else:
            return False
    
    @api.multi
    def change_owner(self):
        ''' Сэлбэгийн эзэмшигч солих'''
        context = self._context
        part = self.env['technic.parts'].browse(context.get('active_id'))
        move_obj = self.env['technic.parts.move']
        if self.new_owner_id:
            move_vals = {
                'date': self.date,
                'old_owner_id': self.old_owner_id.id,
                'new_owner_id': self.new_owner_id.id,
                'description': self.description,
                'technic_part_id': part.id
            }
            move_id = move_obj.create(move_vals)
            part.employee_id = self.new_owner_id.id
                
        return {'type': 'ir.actions.act_window_close'}
    
    old_owner_id = fields.Many2one('hr.employee', 'Old Owner', default=_get_old_owner, required=True)
    new_owner_id = fields.Many2one('hr.employee', 'New Owner', required=True)
    description = fields.Text('Description')
    date = fields.Date('Date', default=lambda *a:datetime.now().strftime('%Y-%m-%d'))