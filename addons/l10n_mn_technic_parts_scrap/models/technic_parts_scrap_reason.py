# -*- coding: utf-8 -*-
################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
################################
from odoo import models, fields, api

class TechnicPartsScrapReason(models.Model):
    _name = 'technic.parts.scrap.reason'

    name = fields.Char('name', required = True)
    note = fields.Text('Description')