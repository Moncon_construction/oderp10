# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions

class TechnicPartsScrapWizard(models.TransientModel):
    _name = 'technic.parts.scrap.wizard'

    def _default_technic(self):
        return self.env['technic.parts.scrap'].browse(self._context.get('active_id')).technic

    def _default_part(self):
        parts_scrap = self.env['technic.parts.scrap'].browse(self._context.get('active_id')).parts
        part_list = []
        for part in parts_scrap:
            if part.state not in 'in_scrap':
                part_list.append(part.id)
        return self.env['technic.parts'].browse(part_list)

    @api.onchange('technic')
    def _set_domain(self):
        res = {}
        parts_scrap = self.env['technic.parts.scrap'].browse(self._context.get('active_id')).parts
        part_list = []
        for part in parts_scrap:
            if part.state not in 'in_scrap':
                part_list.append(part.id)
        res = {'domain': {'parts': [('id', 'in', part_list)]}}
        return res

    technic = fields.Many2one('technic', default=_default_technic, string='Technic')
    parts = fields.Many2many('technic.parts', 'part_wizard_part_scrap_rel', 'part_scrap_wizard_id', 'part_id', string='Parts', default=_default_part,)
    description = fields.Text('Description', required=True)

    @api.multi
    def done_request(self):
        self.ensure_one()
        state_done = False
        for part in self.parts:
            part.state = 'in_scrap'
        default_parts = self._default_part()
        if not default_parts:
            state_done = True
        if state_done:
            self.env['technic.parts.scrap'].browse(self._context.get('active_id')).state = 'done'
        return True
