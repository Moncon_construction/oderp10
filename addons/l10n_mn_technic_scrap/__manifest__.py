# -*- coding: utf-8 -*-
{
    'name': "Техникийн акт",

    'summary': """
        Техникийн акт""",

    'description': """
        Техникийг актлах хүсэлтийг бүртгэх, уг хүсэлтийг тодорхой урсгалын дагуу батлах үйлдлүүдийг энэ модулиар гүйцэтгэх боломжтой болно.
    """,

    'author': "Asterisk Technologies LLC",
    'website': "http://www.asterisk-tech.mn",

    # Categories can be used to filter modules in modules listing
    # for the full list
    'category': 'Technic',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'l10n_mn_technic', 'l10n_mn_workflow_config'],

    # always loaded
    'data': [
        'views/technic_scrap_views.xml',
        'views/technic_scrap_workflow.xml',
        'views/technic_scrap_wizard.xml',
        'views/technic_views.xml',
        'security/ir.model.access.csv',
        'security/technic_scrap_security.xml',
        'views/sequence.xml',
        'views/technic_scrap_reason_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo.xml',
    ],

    'contributors': ['Bayarkhuu Bataa <bayarkhuu@asterisk-tech.mn>'],
}
