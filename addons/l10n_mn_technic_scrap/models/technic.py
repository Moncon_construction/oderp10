# -*- coding: utf-8 -*-
from odoo import models, fields, api

class TechnicPassport(models.Model):
    _inherit = 'technic'

    ready_to_scrap = fields.Boolean(compute='_compute_ready_to_scrap')
    scrap_requests = fields.One2many('technic.scrap', 'technic', 'Scrap requests')

    @api.multi
    @api.depends('scrap_requests')
    def _compute_ready_to_scrap(self):
        for rec in self:
            ret = False
            for req in rec.scrap_requests:
                if req.state == "approved":
                    ret = True
            rec.ready_to_scrap = ret

    @api.multi
    def scrap(self):
        self.ensure_one()
        self.in_scrap = True
        scrap_requests = self.env['technic.scrap'].search([('technic', '=', self.id)])
        for req in scrap_requests:
            req.action_finish()
            self.state = 'stop'
