# -*- coding: utf-8 -*
import datetime
from odoo import models, fields, api
from odoo.exceptions import except_orm

class TechnicScrap(models.Model):
    _name = 'technic.scrap'
    _description = 'Technic scrap'
    _order = 'date desc'
    _inherit = ['mail.thread']

    origin = fields.Char('Scrap Reference', readonly=True, copy=False)
    date = fields.Datetime('Request date', default=lambda *a: datetime.datetime.now())
    technic = fields.Many2one('technic', 'Technic', domain=['!','|','|',('ready_to_scrap','=',True),('in_scrap','=',True),('state','=','draft')], required=True)
    created_user = fields.Many2one('res.users', 'Created user', default=lambda self: self.env.user)
    state = fields.Selection([('request', 'Request'),
                              ('waiting_approval', 'Waiting Approval'),
                              ('approved', 'Approved'),
                              ('refused', 'Refused'),
                              ('returned', 'Returned'),
                              ('done', 'Done')], 'State', default='request')
    description = fields.Text('Description', required=True)
    """ Fields which are used in workflow """
    workflow_id = fields.Many2one('workflow.config', 'Workflow')
    check_sequence = fields.Integer('Workflow Step', default=0)
    history_lines = fields.One2many('technic.scrap.workflow.history', 'scrap_id', 'Workflow History Line')
    is_validator = fields.Boolean(compute='_compute_is_validator')
    is_creator = fields.Boolean(compute='_compute_is_creator')

    scrap_reason = fields.Many2one('technic.scrap.reason', 'Scrap reason')

    @api.multi
    @api.depends('check_sequence')
    def _compute_is_validator(self):
        for rec in self:
            history_obj = self.env['technic.scrap.workflow.history']
            validators = history_obj.search([('scrap_id', '=', rec.id), ('line_sequence', '=', rec.check_sequence)],
                                             limit=1, order='sent_date DESC').user_ids
            if self.env.user in validators:
                rec.is_validator = True
            else:
                rec.is_validator = False

    @api.multi
    def _compute_is_creator(self):
        for rec in self:
            if rec.created_user == self.env.user:
                rec.is_creator = True
            else:
                rec.is_creator = False

    @api.model
    def create(self, vals):
        if vals.get('origin', '-') == '-':
            vals['origin'] = self.env['ir.sequence'].next_by_code('technic.scrap')
        employee = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        workflow_id = self.env['workflow.config'].get_workflow('employee', 'technic.scrap', employee.id, None)
        if workflow_id:
            vals['workflow_id'] = workflow_id
        else:
            raise except_orm(('Warning!'), ('There is no workflow id defined!'))
        return super(TechnicScrap, self).create(vals)

    @api.multi
    def action_set_to_request(self):
        self.ensure_one()
        self.check_sequence = 0
        self.state = 'request'

    @api.multi
    def action_send(self):
        self.ensure_one()
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].send('technic.scrap.workflow.history', 'scrap_id', self, self.created_user.id)
            if success:
                self.check_sequence = current_sequence
                self.state = 'waiting_approval'

    @api.multi
    def action_return(self):
        self.ensure_one()
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].action_return('technic.scrap.workflow.history', 'scrap_id', self, self.env.user.id)
            if success:
                self.check_sequence = current_sequence

    @api.multi
    def action_refuse(self):
        self.ensure_one()
        if self.workflow_id:
            success = self.env['workflow.config'].reject('technic.scrap.workflow.history', 'scrap_id', self, self.env.user.id)
            if success:
                self.state = 'refused'

    @api.multi
    def action_approve(self):
        self.ensure_one()
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve('technic.scrap.workflow.history', 'scrap_id', self, self.env.user.id)
            if success:
                if sub_success:
                    self.state = 'approved'
                else:
                    self.check_sequence = current_sequence

    @api.multi
    def action_finish(self):
        self.ensure_one()
        self.state = 'done'

class TechnicScrapWorkflowHistory(models.Model):
    _name = 'technic.scrap.workflow.history'
    _description = 'Technic Scrap Workflow History'
    _order = 'scrap_id, sent_date'

    STATE_SELECTION = [
        ('waiting', 'Waiting'),
        ('confirmed', 'Confirmed'),
        ('approved', 'Approved'),
        ('return', 'Return'),
        ('rejected', 'Rejected'),
    ]

    scrap_id = fields.Many2one('technic.scrap', 'Scrap Request', readonly=True, ondelete='cascade')
    name = fields.Char('Verification Step', readonly=True)
    user_ids = fields.Many2many('res.users', 'res_users_technic_scrap_workflow_history_ref', 'history_id', 'user_id', string='Validators')
    sent_date = fields.Datetime('Sent date', required=True, readonly=True)
    user_id = fields.Many2one('res.users', 'Validator', readonly=True)
    action_date = fields.Datetime('Action date', readonly=True)
    action = fields.Selection(STATE_SELECTION, 'Action', readonly=True)
    line_sequence = fields.Integer('Workflow Step')

    