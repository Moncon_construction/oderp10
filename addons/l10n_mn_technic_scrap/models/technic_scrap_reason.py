# -*- coding: utf-8 -*

from odoo import models, fields, api

class TechnicScrapReason(models.Model):
    _name = 'technic.scrap.reason'

    name = fields.Char('Name', required = True)
    note = fields.Text('Description')
