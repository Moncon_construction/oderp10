# -*- coding: utf-8 -*-

from odoo import models, fields, api

class TechnicScrapWizard(models.TransientModel):
    _name = 'technic.scrap.wizard'

    def _default_technic(self):
        return self.env['technic'].browse(self._context.get('active_id'))

    technic = fields.Many2one('technic', required=True, default=_default_technic)
    description = fields.Text(string='Description', required=True)

    @api.multi
    def create_request(self):
        self.ensure_one()
        self.env['technic.scrap'].create({
            'technic': self.technic.id,
            'description': self.description
        })
