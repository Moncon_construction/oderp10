# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    'name': 'Дугуй',
    'version': '1.0',
    'author': 'Asterisk Technologies LLC',
    'description': """ Mongolian Technic Tire """,
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'depends': ['l10n_mn_technic_parts', 'l10n_mn_report', 'l10n_mn_technic'],
    'data': [
        'view/register_view.xml',
        'view/setting_view.xml',
        'view/type_view.xml',
        'view/technic_tire_view.xml',
        'security/technic_tire_security.xml',
        'security/ir.model.access.csv',
        'template/tire_email_template.xml',
        'security/cron_technic_tire_auto_warning.xml',
        'report/tire_list_wizard.xml',
    ],
    "demo_xml": [],
    "active": False,
    "installable": True,
    'css': ['static/src/css/base.css','static/src/css/description.css',],
}