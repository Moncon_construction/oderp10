# -*- coding: utf-8 -*-
import time
from datetime import datetime
from odoo import models, fields, exceptions
from odoo.exceptions import ValidationError
from odoo import api
from odoo.tools.translate import _
from dateutil import relativedelta

class TireRegister(models.Model):
    _name = 'tire.register'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    @api.depends('serial', 'tire_setting_id', 'product_id', 'technic_id')
    def _set_name(self):
        brand = ''
        size = ''
        serial = ''
        product_code = ''
        technic = ''
        if self.tire_setting_id.brand_id.name:
            brand = self.tire_setting_id.brand_id.name + '/'
        if self.tire_setting_id:
            size = '('+ str(int(self.norm_width)) + '/' + str(int(self.norm_diameter) / 2) + '/' + 'R/' + str(int(self.norm_height)) +')'+ '/'
        if self.serial:
            serial = self.serial + '/'
        if self.product_id:
            product_code = self.product_id.default_code + '/'
        if self.technic_id:
            technic = self.technic_id.name
        self.name = u'{0} {1} {2} {3} {4}'.format(brand, size, serial, product_code, technic)

    @api.depends('tire_history')
    def _tire_history_count(self):
        self.tire_history_state_count = len(self.env['tire.history'].search([('history_id', '=', self.id), ('change_state', '!=', False)]))
        self.tire_history_technic_count = len(self.env['tire.history'].search([('history_id', '=', self.id), ('change_technic_id', '!=', False)]))
        self.tire_history_usage_count = len(self.env['tire.history'].search([('history_id', '=', self.id), ('change_measurement', '!=', False)]))

    def _default_project(self):
        project_ids = self.env['project.project'].search([('use_technic', '=', True), ('user_id', 'in', [self.env.uid])])
        if len(project_ids) == 1:
            return project_ids[0]
        else:
            return None


    @api.multi
    @api.depends('product_id', 'technic_id', 'tire_setting_id')
    def _compute_usage_percent(self):
        for rec in self:
            if rec.tire_setting_id and rec.tire_setting_id.norm:
                if rec.tire_setting_id.usage_measurement.is_motohour:
                    rec.usage_percent = rec.running_motohour / rec.tire_setting_id.norm * 100
                elif rec.tire_setting_id.usage_measurement.is_kilometer:
                    rec.usage_percent = rec.running_km / rec.tire_setting_id.norm * 100
                elif rec.tire_setting_id.usage_measurement.is_time:
                    rec.usage_percent = rec.spent_duration / rec.tire_setting_id.norm * 100
            else:
                rec.usage_percent = 0

    @api.multi
    @api.depends('product_id')
    def _compute_norm_id(self):
        for rec in self:
            norms = self.env['tire.setting'].search([('product_product_id', '=', rec.product_id.id),
                                                     ('technic_norm_id', '=', rec.technic_id.technic_type_id.id)])
            if norms:
                for norm in norms:
                    rec.tire_setting_id = norm.id
                    break
            else:
                product_norm = self.env['tire.setting'].search(
                    [('product_product_id', '=', rec.product_id.id), ('technic_norm_id', '=', None)])
                if product_norm:
                    for norm in product_norm:
                        rec.tire_setting_id = norm.id
                        break

    @api.multi
    @api.depends('usage_percent')
    def _compute_norm_cost(self):
        for rec in self:
            rec.norm_cost = rec.expense_cost * rec.usage_percent / 100

    @api.multi
    @api.depends('date_record')
    def _compute_running_duration(self):
        for rec in self:
            if rec.date_record:
                date = datetime.strptime(rec.date_record, '%Y-%m-%d %H:%M:%S')
                today = datetime.strptime(time.strftime('%Y-%m-%d'), '%Y-%m-%d')
                date_diff = relativedelta.relativedelta(today, date)
                duration_str = ""
                if date_diff.years:
                    duration_str = str(date_diff.years) + u' жил'
                if date_diff.months:
                    duration_str += "\t" + str(date_diff.months) + u' сар'
                if date_diff.days:
                    duration_str += "\t" + str(date_diff.days) + u' өдөр'
                rec.spent_duration = date_diff.years * 12 + date_diff.months + date_diff.days / 30.0
            else:
                rec.spent_duration = 0

    @api.multi
    @api.depends('kilometer')
    def _compute_running_km(self):
        for rec in self:
            if rec.technic_id:
                technic_usage = self.env['technic.usage'].search([('technic_id', '=',rec.technic_id.id)])
                if technic_usage:
                    for tech_usage in technic_usage:
                        if tech_usage.usage_uom_id.is_kilometer:
                            rec.running_km = tech_usage.usage_value - rec.kilometer
            else:
                rec.running_km = 0

    @api.multi
    @api.depends('motohour')
    def _compute_running_motohour(self):
        for rec in self:
            if rec.technic_id:
                technic_usage = self.env['technic.usage'].search([('technic_id', '=', rec.technic_id.id)])
                if technic_usage:
                    for tech_usage in technic_usage:
                        if tech_usage.usage_uom_id.is_motohour:
                            rec.running_motohour = tech_usage.usage_value - rec.motohour
                else:
                    rec.running_motohour = 0


    name = fields.Char(compute='_set_name', readonly=True, store=True, string='Tire name')
    technic_id = fields.Many2one('technic', 'Technic', domain="[('technic_norm_id.tire', '=', True)]")
    employee_id = fields.Many2one('hr.employee', 'Owner Employee', domain="[('state_id','!=','draft')]")
    tire_setting_id = fields.Many2one('tire.setting', string='Tire norm', ondelete='cascade', compute='_compute_norm_id')
    description = fields.Text('Description')
    
    serial = fields.Char('Serial')
    current_position = fields.Integer('Current position', readonly=False)
    tread_current_deep = fields.Float('Tread current deep', readonly=False, help="MM")
    tread_depreciation_percent = fields.Float(compute='_tread_depreciation_percent', string='Tread depreciation percent', store=True)
    state = fields.Selection([('using', 'Using'),
                              ('rejected', 'Rejected')], string='Status', readonly=True, default='using')
    date_record = fields.Datetime('Date of record', default=datetime.now())
    tire_usage = fields.One2many('tire.usage', 'usage_id', 'Usage', ondelete='cascade')
    # tire_usage_history = fields.One2many('tire.usage.history', 'usage_history_id', 'Usage', ondelete='cascade')
    tire_history = fields.One2many('tire.history', 'history_id', 'Usage', ondelete='cascade')
    tire_history_state_count = fields.Integer(compute=_tire_history_count, string='Tire state count')
    tire_history_technic_count = fields.Integer(compute=_tire_history_count, string='Tire technic count')
    tire_history_usage_count = fields.Integer(compute=_tire_history_count, string='Tire norm count')
    project = fields.Many2one('project.project', string='Project', readonly=True,
                              states={'using': [('readonly', False)]}, domain="[('use_technic','=',True)]", default=_default_project)
    usage_percent = fields.Float(compute=_compute_usage_percent, string='Usage Percent')
    
    norm_width = fields.Float('Norm width', help='MM', related='tire_setting_id.norm_width', store=True, readonly=True)
    norm_height = fields.Float('Norm height', help='%', related='tire_setting_id.norm_height', store=True, readonly=True)
    norm_diameter = fields.Float('Norm diameter', help='ИНЧ', related='tire_setting_id.norm_diameter', store=True, readonly=True)
    
    tire_type_id = fields.Many2one('tire.type', string='Tire type', related='tire_setting_id.tire_type_id', store=True, readonly=True)
    product_id = fields.Many2one('product.product', string='Product', required=True, domain="[('technic_parts_type','=','is_tire')]")
    technic_model_id = fields.Many2one('technic.model', string='Tire model', related='tire_setting_id.technic_model_id', store=True, readonly=True)
    brand_id = fields.Many2one('brand', string='Tire brand', related='tire_setting_id.brand_id', store=True, readonly=True)
    expense_cost = fields.Float('Expense Cost', required=True)
    qty = fields.Float(string='Quantity', required=True)
    with_technic = fields.Boolean('Comes with technic')
    motohour = fields.Float('Motohour', store=True)
    kilometer = fields.Float('Kilometer', store=True)

    norm_cost = fields.Float('Norm Cost', compute='_compute_norm_cost')
    spent_duration = fields.Float(string='Current Duration /month/', compute='_compute_running_duration')
    running_km = fields.Float(string='Running Kilometer', compute='_compute_running_km')
    running_motohour = fields.Float(string='Running Motohour', compute='_compute_running_motohour')

    _sql_constraints = [
        ('serial_uniq', 'unique(serial)', 'Serial must be unique!'),
    ]
    
    @api.one
    @api.constrains('current_position', 'tread_current_deep')
    def check_const(self):
        if self.tread_current_deep > self.tire_setting_id.norm_tread_deep:
            raise ValidationError("Field Current deep must be low")
        if self.current_position > self.technic_id.technic_norm_id.tire_number:
            raise ValidationError("Field Current position must be low")

    @api.depends('tread_current_deep')
    def _tread_depreciation_percent(self):
        for this in self:
            if this.tread_current_deep != 0 and this.tire_setting_id.norm_tread_deep != 0:
                this.tread_depreciation_percent = this.tread_current_deep * 100 / this.tire_setting_id.norm_tread_deep

    @api.onchange('technic_id')
    def onchange_technic(self):
        self.current_position = self.technic_id.technic_norm_id.tire_number
        technic_usage = self.env['technic.usage'].search([('technic_id','=',self.technic_id.id)])
        if technic_usage:
            for obj in technic_usage:
                if obj.usage_uom_id.is_kilometer:
                    self.kilometer = obj.usage_value
                else:
                    self.motohour = obj.usage_value
        # else:
        #     raise exceptions.Warning(_('This technic does not have technic usage. Please specify technic usage!'))

    @api.onchange('tire_setting_id')
    def onchange_tire_setting(self):
        self.tread_current_deep = self.tire_setting_id.norm_tread_deep

    @api.onchange('brand_id')
    def brand_filter_model(self):
        if self.brand_id.id:
            res = {'domain': {'technic_model_id': [('brand_id', '=', self.brand_id.id)]}}
        else:
            res = {'domain': {'technic_model_id': []}}
        return res

    @api.onchange('technic_model_id', 'tire_type_id', 'norm_width', 'norm_height', 'norm_diameter')
    def filter_norm(self):
        # self.tire_setting_id = False
        res = {}
        domain = []
        if self.technic_model_id:
            domain += [('technic_model_id', '=', self.technic_model_id.id)]
        if self.tire_type_id:
            domain += [('tire_type_id', '=', self.tire_type_id.id)]
        if self.norm_width:
            domain += [('norm_width', '<=', self.norm_width)]
        if self.norm_height:
            domain += [('norm_height', '<=', self.norm_height)]
        if self.norm_diameter:
            domain += [('norm_diameter', '<=', self.norm_diameter)]
        res.update({'tire_setting_id': domain})
        return {'domain': res}

    @api.model
    def create(self, values):
        creation = super(TireRegister, self).create(values)
        description = _('Registered.')
        # create usages
        creation.create_non_exist_usages()
        # state history
        self.tire_history.create({
            'history_id': creation.id,
            'change_state': creation.state,
            'description': description})
        # technic history
        if values.get('technic_id'):
            info = self.tire_history.create({
                'history_id': creation.id,
                'change_technic_id': creation.technic_id.id,
                'description': description})
            # usage history
            for uom in creation.technic_id.technic_usage_ids:
                self.env['change.information'].create({
                    'info_id': info.id,
                    'change_usage_uom': uom.usage_uom_id.id,
                    'change_value': uom.usage_value
                })
        return creation

    @api.multi
    def write(self, values):
        description = _('Updated.')
        for tire in self:
            # create tire state history
            if tire.state != values.get('state'):
                self.env['tire.history'].create({
                    'history_id': tire.id,
                    'change_state': values.get('state'),
                    'description': description,
                })
            # create technic history
            if 'technic_id' in values:
                if values['technic_id']:
                    if values['technic_id'] != tire.technic_id.id:
                        info = self.env['tire.history'].create({
                            'history_id': tire.id,
                            'change_technic_id': values['technic_id'],
                            'description': description
                        })
                        for technic_usage in self.env['technic'].browse(values['technic_id']).technic_usage_ids:
                            self.env['change.information'].create({
                                'info_id': info.id,
                                'change_usage_uom': technic_usage.usage_uom_id.id,
                                'change_value': technic_usage.usage_value
                            })
                else:
                    self.env['tire.history'].create({
                        'history_id': tire.id,
                        'change_technic': None,
                        'description': description
                    })
        update = super(TireRegister, self).write(values)
        # create usage
        self.create_non_exist_usages()
        return update

    @api.multi
    def unlink(self):
        reg = self.read(['state'])
        for s in reg:
            if s['state'] not in ['using']:
                raise ValidationError("Files only in using state can be deleted!")
        unlink_id = super(TireRegister, self).unlink()
        return unlink_id

    @api.model
    def auto_norm_warning_cron(self):
        query = """SELECT r.id AS tire,
                          r.serial AS serial,
                          r.tread_current_deep AS deep,
                          r.tread_depreciation_percent AS percent,
                          u.id AS usage_id,
                          u.current_usage AS usage,
                          u.usage_register_uom AS uom,
                          s.norm AS norm,                          
                          u.usage_register_measure as norm_uom,
                          s.id AS tire_norm,
                          s.norm_tread_deep AS norm_deep,
                          s.warning_limit AS warning
                   FROM tire_register AS r
                          FULL JOIN tire_usage AS u
                            ON u.usage_id = r.id
                          FULL JOIN tire_setting AS s
                            ON s.id = r.tire_setting_id                                                       
                   WHERE r.tread_depreciation_percent >= s.warning_limit
                          OR s.norm != 0
                          AND u.current_usage * 100 / s.norm >= s.warning_limit"""
        self.env.cr.execute(query)
        records = self.env.cr.dictfetchall()
        if records:
            self.send_notif(records)

    @api.multi
    def send_notif(self, records):
        notif_groups = self.env.ref('l10n_mn_technic_tire.group_technic_tire_manager').id
        group_user_ids = self.env['res.users'].search([('groups_id', 'in', [notif_groups])])
        for r in records:
            self.env.context = {
                'document_name': u'Дугуй ашиглалтын анхааруулга',
                'name': u'Дугуйн бүртгэл',
                'base_url': self.env['ir.config_parameter'].get_param('web.base.url'),
                'action_id': self.env.ref('l10n_mn_technic_tire.tire_register_action').id,
                'id': r['tire'],
                'db_name': self.env.cr.dbname,
                'serial': r['serial'],
                'deep': r['deep'],
                'norm_deep': r['norm_deep'],
                'percent': r['percent'],
                'uom': r['uom'],
                'usage': r['usage'],
                'warning': r['warning'],
                'norm_usage': r['norm_usage'],
                'sender': 'OdERP',
            }
            template_id = self.env.ref('l10n_mn_technic_tire.tire_email_template_notif')
            if group_user_ids:
                user_emails = []
                for user in group_user_ids:
                    user_emails.append(user.login)
                    # template_id.send_mail(user.id, force_send=True, context=context)
                    self.pool['email.template'].send_mail(self.env.cr, self.env.uid, template_id.id, user.id, force_send=True, context=self.env.context)
                email = u'\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
                self.env['tire.register'].browse(r['tire']).message_post(body=email)
            else:
                raise ValidationError("No sending email found ! \n Contact your administrator")
        return True

    @api.multi
    def create_non_exist_usages(self):
        for tire in self:
            if tire.technic_id:
                TireUsage = self.env['tire.usage']
                technic_norm_measurements = self.env['usage.uom.line'].search([('technic_norm_id', '=', tire.technic_id.technic_norm_id.id)])
                for measurement in technic_norm_measurements:
                    if TireUsage.search_count([('usage_id', '=', tire.id), ('usage_register_measure', '=', measurement.usage_uom_id.id)]) == 0:
                        usage_values = {
                            'usage_id': tire.id,
                            'usage_register_measure': measurement.usage_uom_id.id
                        }
                        TireUsage.create(usage_values)

# usage tab
class TireUsage(models.Model):
    _name = 'tire.usage'

    usage_id = fields.Many2one('tire.register', 'Usage', ondelete='cascade')
    usage_register_measure = fields.Many2one('usage.uom', 'Usage UOM')
    usage_register_uom = fields.Many2one('product.uom', 'UOM', relation='usage_register_uom.usage_uom_id', store=True)
    current_usage = fields.Integer('Current usage', default=0)

    @api.multi
    def write(self, vals):
        # create usage history
        for usage in self:
            history_values = {
                'history_id': usage.usage_id.id,
                'change_measurement': usage.usage_register_measure.id,
                'change_value': vals.get('current_usage'),
                'change_measurement_technic': usage.usage_id.technic_id.id
            }
            self.env['tire.history'].create(history_values)
        return super(TireUsage, self).write(vals)


class TireHistory(models.Model):
    _name = 'tire.history'
    _order = 'create_date DESC'

    history_id = fields.Many2one('tire.register', 'History', ondelete='cascade')

    change_measurement = fields.Many2one('usage.uom', 'Change measurement')
    change_unit = fields.Many2one('product.uom', 'Change measurement unit', related='change_measurement.usage_uom_id', store=True)
    change_value = fields.Float('Change measurement value')
    change_measurement_technic = fields.Many2one('technic', 'Change measurement technic')

    change_date = fields.Datetime('Changed Date')
    change_tire_norm_id = fields.Many2one('tire.setting', 'Changed tire norm')
    change_technic_id = fields.Many2one('technic', 'Changed technic')
    change_location = fields.Integer('Changed location')
    change_tread_deep = fields.Integer('Changed tread deep')
    change_state = fields.Selection([('using', 'Using'),
                              ('rejected', 'Rejected')], string='State', readonly=True)
    change_technic_usages = fields.One2many('change.information', 'info_id', 'Changed information', ondelete='cascade')
    edited_employee = fields.Many2one('hr.employee', 'Changed employee', default=lambda self: self.edited_employee)
    edited_user = fields.Many2one('res.users', 'Changed user', default=lambda self: self.edited_user)
    description = fields.Text('Description')

    # @api.depends (edited_user, edited_employee)
    def _set_user_employee(self):
        resource = self.env['resource.resource'].search([('user_id', '=', self.env.user.id)])
        employee = self.env['hr.employee'].search([('resource_id', '=', resource[0].id)])
        self.edited_user = self.env.uid.id
        self.edited_employee = employee.id


# Changed usage
class ChangeInformation(models.Model):
    _name = 'change.information'

    info_id = fields.Many2one('tire.history', 'Info', ondelete='cascade')
    change_usage_uom = fields.Many2one('usage.uom', 'Changed usage UOM')
    change_product_uom = fields.Many2one('product.uom', 'Changed measure unit', related='change_usage_uom.usage_uom_id', store=True)
    change_value = fields.Integer('Changed value', default=0)


class Technic(models.Model):
    _inherit ='technic'

    last_km = fields.Float('km', digits=(16, 1))
    last_motohour = fields.Float('motohour', digits=(16, 1))