# -*- coding: utf-8 -*-

from odoo import models, fields, api


class TireSettings(models.Model):
    _name = 'tire.setting'
    _description = 'Tire Setting'

    @api.multi
    def name_get(self):
        result = []
        for s in self:
            name = s.product_product_id.name+'/'+s.brand_id.name+'/'+s.technic_model_id.model_name
            result.append((s.id, name))
        return result

    product_product_id = fields.Many2one('product.product', string='Tire product', required=True, help='Product Tire',
                                         domain="[('technic_parts_type','=','is_tire')]")
    technic_norm_id = fields.Many2one('technic.type', 'Technic Norm')
    brand_id = fields.Many2one('brand', string='Tire brand', required=True)
    technic_model_id = fields.Many2one('technic.model', string='Tire model', required=True)
    tire_type_id = fields.Many2one('tire.type', string='Tire type', required=True)
    norm_width = fields.Float('Norm width', required=True, help='MM')
    norm_height = fields.Float('Norm height', required=True, help='12')
    norm_diameter = fields.Float('Norm diameter', required=True, help='ИНЧ')
    norm_tread_deep = fields.Float('Norm deep', required=True, help='MM')
    norm_pressure = fields.Integer('Norm pressure', required=True, help='PSI')
    warning_limit = fields.Integer('Warning limit', required=True, default=90)
    norm = fields.Float('Norm value', required=True)
    usage_measurement = fields.Many2one('usage.uom', string='Usage UOM', required=True)

    _constraints = [
        ('norm_width', 'Not allowed zero (0) !.', ['transit_warehouse', 'warehouse_id']),
    ]

    @api.onchange('brand_id')
    def onchange_filte_model(self):
        if self.brand_id:
            res = {
                'domain': {'technic_model_id': [('brand_id', '=', self.brand_id.id)]},
            }
        else:
            res = {
                'value': {'technic_model_id': False},
                'domain': {'technic_model_id': []}
            }
        return res

    @api.onchange('technic_model_id')
    def onchange_set_brand(self):
        self.brand_id = self.technic_model_id.brand_id.id







