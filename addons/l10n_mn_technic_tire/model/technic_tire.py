# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from odoo import models, fields, api


class Technic(models.Model):
    _inherit = 'technic'

    def _tire_count(self):
        res = {}
        for obj in self:
            obj.tire_count = len(self.env['tire.register'].search([('technic_id', '=', obj.id)]))

    tires = fields.One2many('tire.register', 'technic_id', string='Tire')
    tire_count = fields.Integer(compute=_tire_count, string='Tire count')


class TechnicUsage(models.Model):
    _inherit = 'technic.usage'

    @api.multi
    def write(self, vals):
        res = super(TechnicUsage, self).write(vals)
        # update tire usage
        measurement_id = vals.get('usage_uom_id')
        value = vals.get('usage_value')
        technic_id = vals.get('technic_id')

        if measurement_id and value and technic_id:
            technic = self.env['technic'].browse(technic_id)
            measurement = self.env['usage.uom'].browse(measurement_id)
            for tire in technic.tires:
                has_install_usage = False
                install_technic_usage_value = 0
                install_history = tire.tire_history.filtered(lambda r: r.change_technic_id == technic).sorted(key=lambda r: r.create_date, reverse=True)

                if install_history:
                    install_technic_usage = install_history[0].change_technic_usages.filtered(lambda r: r.change_usage_uom == measurement)

                if install_technic_usage:
                    install_technic_usage_value = install_technic_usage[0].change_value
                    has_install_usage = True

                if has_install_usage:
                    current_usage = tire.tire_usage.filtered(lambda r: r.usage_register_measure == measurement)

                    if current_usage:
                        previous_history = None
                        histories_technic_change = tire.tire_history.filtered(lambda r: r.change_technic_id).sorted(key=lambda r: r.create_date, reverse=True)
                        if histories_technic_change:
                            history_last_technic_change = histories_technic_change[0]
                            all_previous_history = tire.tire_history.filtered(lambda r: r.id < history_last_technic_change.id).sorted(key=lambda r: r.create_date, reverse=True)
                            previous_history = all_previous_history.filtered(lambda r: r.change_measurement == measurement)

                        additional_value = value - install_technic_usage_value

                        if additional_value < 0.0:
                            additional_value = 0.0
                        if previous_history:
                            previous_history_value = previous_history[0].change_value
                            current_usage.current_usage = previous_history_value + additional_value
                        else:
                            current_usage.current_usage = additional_value
        return res