# -*- coding: utf-8 -*-

from odoo import models, fields, api


class TireType(models.Model):
    _name = 'tire.type'
    _description = 'Tire Type'

    @api.multi
    def name_get(self):
        result = []
        for s in self:
            name = s.tire_type
            result.append((s.id, name))
        return result

    tire_type = fields.Char('Tire type', required=True)



