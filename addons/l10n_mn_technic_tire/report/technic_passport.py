# -*- coding: utf-8 -*-
from odoo import models
from odoo.tools.translate import _

from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport


class ReportTechnicPassportWizard(models.TransientModel):
    _inherit = 'report.technic.passport.wizard'

    def write_components(self, obj, sheet, rowx):
        sheet, rowx = super(ReportTechnicPassportWizard, self).write_components(obj, sheet, rowx)


        # create tire information
        tire_datas = obj.technic.tires
        if tire_datas:
            tire_datas = obj.env['tire.register'].search([])
            if tire_datas:
                colx_number = 8
                sheet.write_merge(rowx, rowx, 0, colx_number, _('Tire Information'), ReportExcelCellStyles.title_xf)
                rowx += 1
                colx = 0
                num = 1
                sheet.write(rowx, colx, '№', ReportExcelCellStyles.group_xf)
                sheet.write(rowx, 1, _('Tire name'), ReportExcelCellStyles.group_xf)
                sheet.write(rowx, 2, _('Size'), ReportExcelCellStyles.group_xf)
                sheet.write(rowx, 3, _('Serial'), ReportExcelCellStyles.group_xf)
                sheet.write(rowx, 4, _('Current_position'), ReportExcelCellStyles.group_xf)
                sheet.write(rowx, 5, _('Tread depreciation percent'), ReportExcelCellStyles.group_xf)
                sheet.write(rowx, 6, _('Current usage'), ReportExcelCellStyles.group_xf)
                sheet.write(rowx, 7, _('Residual'), ReportExcelCellStyles.group_xf)
                sheet.write(rowx, 8, _('Date of record'), ReportExcelCellStyles.group_xf)
                rowx += 1

        usage_data = obj.technic.technic_usage_ids
        for tire_usage in usage_data:
            usage_value_line = tire_usage.usage_value

        tire_datas = obj.technic.tires
        if tire_datas:
            tire_datas = obj.env['tire.register'].search([('technic_id', '=', obj.technic.id)])
            if tire_datas:

                for each_tire in tire_datas:
                    current_usage_tire = each_tire.tire_usage

                    for tire_current_usage in current_usage_tire:
                        usage_value_tire = tire_current_usage.current_usage

                for t_data in tire_datas:

                    sheet.write(rowx, colx, num, ReportExcelCellStyles.content_number_xf)
                    sheet.write(rowx, 1, t_data.name, ReportExcelCellStyles.content_text_xf)
                    sheet.write(rowx, 2, '%i / %i / %i' % (t_data.norm_width, t_data.norm_height, t_data.norm_diameter), ReportExcelCellStyles.content_text_xf)
                    sheet.write(rowx, 3, t_data.serial, ReportExcelCellStyles.content_text_xf)
                    sheet.write(rowx, 4, t_data.current_position, ReportExcelCellStyles.content_text_xf)
                    sheet.write(rowx, 5, t_data.tread_depreciation_percent, ReportExcelCellStyles.content_text_xf)

                    current_usage_tire = t_data.tire_usage
                    for tire_current_usage in current_usage_tire:
                        usage_value_tire = tire_current_usage.current_usage
                        #  Техникийн пасспорт дээр дугуйн хэсэг дээр засвар орох
                        # sheet.write(rowx, 6, tire_current_usage.current_usage, ReportExcelCellStyles.content_text_xf)
                        # sheet.write(rowx, 7, (usage_value_line - usage_value_tire), ReportExcelCellStyles.content_text_xf)

                    sheet.write(rowx, 8, t_data.date_record, ReportExcelCellStyles.content_text_xf)

                    num += 1
                    rowx += 1
            rowx += 1

        return sheet, rowx
