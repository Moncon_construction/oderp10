# -*- coding: utf-8 -*-

from StringIO import StringIO
import base64
import time

import xlwt

from odoo import models, fields, api, _
from datetime import datetime
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles  # @UnresolvedImport
from odoo.addons.l10n_mn_report.tools.report_excel_fit_sheet_wrapper import \
    ReportExcelFitSheetWrapper  # @UnresolvedImport


class ReportTireListWizard(models.TransientModel):
    _name = 'report.tire.list.wizard'

    STATES = [('using', 'Using'),
              ('rejected', 'Rejected')]

    technic_id = fields.Many2one('technic', 'Filter')
    group_by = fields.Selection(STATES, 'Group by')

    # date_from = fields.Date('Date from')
    # date_until = fields.Date('Date until')

    @api.multi
    def export_report(self):
        # create workbook
        book = xlwt.Workbook(encoding='utf8')
        filename = "%s_%s.xls" % (_('Tire List'), datetime.now().strftime('%Y%m%d_%H%M%S'))
        # create sheet
        report_name = _('Tire List')
        sheet = ReportExcelFitSheetWrapper(book.add_sheet(report_name))

        # create report object


        rowx = 0
        colx = 0

        # define title and header
        title_list = [('№'), _('Technic'), _('Brand'), _('Model'), _('Type'), _('Width'),
                      _('Height'), _('Diameter'), _('Norm'), _('Serial'), _('Current position'),
                      _('Tread current deep'), _('Tread depreciation'), _('Recorded date'),
                      _('Usage'), (None), (None), _('Usage history'), _('Tire history')]
        title_list_sub = [(None), (None), (None), (None), (None),
                          (None), (None), (None), (None), (None),
                          (None), (None), (None), (None),
                          _('Usage uom'), _('Uom'), _('Current usage'), (None), (None)]

        colx_number = len(title_list_sub)

        # create header
        report_number = _('Report №%s') % 1
        sheet.write_merge(rowx, rowx, 1, colx_number, report_number, ReportExcelCellStyles.number_xf, size='number')
        rowx += 1

        # create name
        sheet.row(rowx).height_mismatch = True
        sheet.row(rowx).height = 24 * 20
        sheet.write_merge(rowx, rowx, colx + 1, colx_number, report_name.upper(), ReportExcelCellStyles.name_xf,
                          size='name')
        rowx += 1
        # prepare data and filter
        tire_obj = self.env['tire.register']
        search_condition = []

        # filter
        if self.technic_id:
            sheet.write_merge(rowx, rowx, colx + 1, colx_number,
                              '%s: %s' % (_('Technic'), self.technic_id.name), ReportExcelCellStyles.filter_xf,
                              size='filter')
            rowx += 1
            search_condition.append(('technic_id.id', '=', self.technic_id.id))
        if self.group_by:
            if self.group_by == 'using':
                sheet.write_merge(rowx, rowx, colx + 1, colx_number, '%s: %s' % (_('Group by'), _('Using')),
                                  ReportExcelCellStyles.filter_xf, size='filter')
            rowx += 1
        if self.group_by:
            if self.group_by == 'rejected':
                sheet.write_merge(rowx, rowx, colx + 1, colx_number, '%s: %s' % (_('Group by'), _('Rejected')),
                                  ReportExcelCellStyles.filter_xf, size='filter')
            rowx += 1
            search_condition.append(('state', '=', self.group_by))

        data_list = tire_obj.search(search_condition)

        # create title
        a = 0
        sheet.write_merge(rowx, rowx, 14, 16, title_list[14], ReportExcelCellStyles.title_xf, size='name')
        for i in xrange(0, len(title_list)):
            sheet.write_merge(rowx + 1 if title_list_sub[i] != None else rowx, rowx + 1,
                              i, i,
                              title_list_sub[i] if title_list_sub[i] != None else title_list[i],
                              ReportExcelCellStyles.title_xf, size='name')
        rowx += 1

        num = 0
        if data_list:
            for line in data_list:
                num = num + 1
                rowx += 1

                tire_usage_history_str = ''
                tire_history_str = ''
                if "tire_usage_history" in line:
                    changed_info = ''
                    for history in line.tire_usage_history:
                        for change in history.changed_information:
                            changed_info += '\n%s-%s' % (change.changed_usage_measure.name, change.changed_uom)
                        tire_usage_history_str += '%s, %s, %s, \n' % (history.date, history.description, changed_info)

                if line.tire_history:
                    for t_history in line.tire_history:
                        tire_history_str += '%s, %s, %s, %s, %s \n' % (
                        t_history.change_date, t_history.change_technic_id.name,
                        t_history.change_location, t_history.change_state, t_history.description)

                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx, colx,
                                  colx, num, ReportExcelCellStyles.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx, colx + 1,
                                  colx + 1, line.technic_id.name, ReportExcelCellStyles.content_text_xf,
                                  size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx, colx + 2,
                                  colx + 2, line.brand_id.name, ReportExcelCellStyles.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx, colx + 3,
                                  colx + 3, line.technic_model_id.model_name, ReportExcelCellStyles.content_text_xf,
                                  size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx, colx + 4,
                                  colx + 4, line.tire_type_id.tire_type, ReportExcelCellStyles.content_text_xf,
                                  size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx, colx + 5,
                                  colx + 5, line.norm_width, ReportExcelCellStyles.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx, colx + 6,
                                  colx + 6, line.norm_height, ReportExcelCellStyles.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx, colx + 7,
                                  colx + 7, line.norm_diameter, ReportExcelCellStyles.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx, colx + 8,
                                  colx + 8, line.tire_setting_id.name_get(), ReportExcelCellStyles.content_text_xf,
                                  size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx, colx + 9,
                                  colx + 9, line.serial, ReportExcelCellStyles.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx,
                                  colx + 10, colx + 10, line.current_position, ReportExcelCellStyles.content_text_xf,
                                  size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx,
                                  colx + 11, colx + 11, line.tread_current_deep, ReportExcelCellStyles.content_text_xf,
                                  size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx,
                                  colx + 12, colx + 12, line.tread_depreciation_percent,
                                  ReportExcelCellStyles.content_text_xf, size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx,
                                  colx + 13, colx + 13, line.date_record, ReportExcelCellStyles.content_text_xf,
                                  size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx,
                                  colx + 17, colx + 17, tire_usage_history_str, ReportExcelCellStyles.content_text_xf,
                                  size='content')
                sheet.write_merge(rowx, rowx + len(line.tire_usage) - 1 if len(line.tire_usage) > 0 else rowx,
                                  colx + 18, colx + 18, tire_history_str, ReportExcelCellStyles.content_text_xf,
                                  size='content')

                row = rowx
                r = 0
                if line.tire_usage:
                    rowx += len(line.tire_usage) - 1
                    for usage in line.tire_usage:
                        sheet.write(row + r, 14, usage.usage_register_measure.name,
                                    ReportExcelCellStyles.content_text_xf, size='content')
                        sheet.write(row + r, 15, usage.usage_register_uom, ReportExcelCellStyles.content_text_xf,
                                    size='content')
                        sheet.write(row + r, 16, usage.current_usage, ReportExcelCellStyles.content_text_xf,
                                    size='content')
                        r += 1
                else:
                    sheet.write(row + r, 14, '', ReportExcelCellStyles.content_text_xf, size='content')
                    sheet.write(row + r, 15, '', ReportExcelCellStyles.content_text_xf, size='content')
                    sheet.write(row + r, 16, '', ReportExcelCellStyles.content_text_xf, size='content')

        # sheet.write(rowx, i, title_list[i], ReportExcelCellStyles.title_xf, size='title', no_merge=True)

        # prepare file data
        io_buffer = StringIO()
        book.save(io_buffer)
        filedate = base64.encodestring(io_buffer.getvalue())
        io_buffer.close()

        val = {'file_data': filedate, 'file_name': filename}
        export_id = self.env['excel.output.part.discharge'].create(val)

        return {
            'view_mode': 'form',
            'res_id': export_id.id,
            'res_model': 'excel.output.part.discharge',
            'view_type': 'form',
            'type': 'ir.actions.act_window',
            'context': self._context,
            'target': 'new',
        }
