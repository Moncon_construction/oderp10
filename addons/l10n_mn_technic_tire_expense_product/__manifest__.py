# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

{
    "name": "Дугуй агуулахаас шаардах",
    "version": "1.0",
    "author": "Asterisk Technologies LLC",
    "description": """
        Уг модуль нь техникт дугуй тавих үед агуулахад бараа материалын хүсэлт үүсгэх боломжийг олгоно.
""",
    'website': "http://www.asterisk-tech.mn",
    "category": "Mongolian Modules",
    "depends": ['l10n_mn_stock',
                'l10n_mn_technic_tire',
                'l10n_mn_product_expense',
                'l10n_mn_technic_expense'],
    "init": [],
    "data": [
        'data/technic_tire_cron.xml',
        'view/technic_tire_view.xml',
        'wizard/change_tire_information_wizard_view.xml',
        'wizard/change_tire_technic_wizard_view.xml'
    ],
    "demo_xml": [
    ],
    "active": False,
    "installable": True,
}
