# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from datetime import datetime, time
from odoo import models, fields, api, _
from odoo.exceptions import UserError

class ProductExpense(models.Model):
    _inherit = 'product.expense'
    
    def _tire_expense_count(self):
        for this in self:
            this.tire_expense_count = len(self.tire_ids.ids)
            
    tire_expense_count = fields.Integer(compute=_tire_expense_count, string='Tires count')
    tire_ids = fields.One2many('tire.register', 'expense_id', string='Tires')