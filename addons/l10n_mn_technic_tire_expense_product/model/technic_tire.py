# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution
#    Copyright (C) 2013-2015 Asterisk Technologies LLC (<http://www.erp.mn/, http://asterisk-tech.mn/&gt;). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 88005462, 976 + 94100149
#
##############################################################################

from datetime import datetime, time
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import logging
_logger = logging.getLogger('STOCK')

class TechnicTire(models.Model):
    _inherit = 'tire.register'
    
    @api.depends('running_motohour', 'running_km')
    def _compute_usage_percent_motohour_km(self):
        for obj in self:
            if obj.tire_setting_id.norm > 0:
                obj.usage_percent_motohour = obj.running_motohour / obj.tire_setting_id.norm * 100
                obj.usage_percent_km = obj.running_km / obj.tire_setting_id.norm * 100
                
    @api.multi
    @api.depends('kilometer')
    def _compute_running_km(self):
        for rec in self:
            if rec.technic_id and rec.state != 'rejected':
                rec.running_km = rec.technic_id.last_km - rec.kilometer + rec.start_performance_km
            else:
                rec.running_km = rec.scrap_km

    @api.multi
    @api.depends('motohour')
    def _compute_running_motohour(self):
        for rec in self:
            if rec.technic_id and rec.state != 'rejected':
                rec.running_motohour = rec.technic_id.last_motohour - rec.motohour + rec.start_performance_motohour
            else:
                rec.running_motohour = rec.scrap_motohour

    state = fields.Selection([('draft', 'Draft'),
                              ('using', 'Using'),
                              ('rejected', 'Rejected')], 'State')

    expense_id = fields.Many2one('product.expense', string='Expense')
    start_performance_motohour = fields.Float('Start Performance Motohour', track_visibility='onchange', copy=False)
    start_performance_km = fields.Float('Start Performance KM', track_visibility='onchange', copy=False)
    usage_percent_motohour = fields.Float('Usage Percent (Motohour)', compute='_compute_usage_percent_motohour_km')
    usage_percent_km = fields.Float('Usage Percent (KM)', compute='_compute_usage_percent_motohour_km')
    scrap_motohour = fields.Float('Scrap Motohour')
    scrap_km = fields.Float('Scrap KM')
    technic_id = fields.Many2one('technic', 'Technic', domain="[('technic_norm_id.tire', '=', True)]", track_visibility='onchange')
    
    @api.multi
    def write(self, values):
        update = super(TechnicTire, self.with_context({'is_expense': True})).write(values)
        return update

class TireHistory(models.Model):
    _inherit = 'tire.history'

    change_state = fields.Selection([('draft', 'Draft'),
                                     ('using', 'Using'),
                                     ('rejected', 'Rejected')], 'Change state')


class StockImmediateTransfer(models.TransientModel):
    _inherit = 'stock.immediate.transfer'

    @api.multi
    def process(self):
        self.ensure_one()
        this = super(StockImmediateTransfer, self).process()
        stock_picking = self.pick_id
        if stock_picking.expense.is_technic_expense:
            if stock_picking.expense.technic_id:
                technic_id = stock_picking.expense.technic_id.id
                employee_id = stock_picking.expense.employee.id
                for line in stock_picking.move_lines:
                    if line.product_id.technic_parts_type == 'is_tire':
                        qty = line.product_qty
                        while qty != 0:
                            value = {'name': stock_picking.expense.name,
                                     'product_id': line.product_id.id,
                                     'technic_id': technic_id,
                                     'employee_id': employee_id,
                                     'date_record': self.force_date or time.strftime('%Y-%m-%d'),
                                     'qty': 1,
                                     'state': 'using',
                                     'expense_cost': line.price_unit,
                                     'motohour': stock_picking.expense.technic_id.last_motohour,
                                     'kilometer': stock_picking.expense.technic_id.last_km,
                                     'expense_id': stock_picking.expense.id
                                     }
                            qty -= 1
                            self.env['tire.register'].create(value)
        return this