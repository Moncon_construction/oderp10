# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions


class TireScrapWizard(models.TransientModel):
    _inherit = 'tire.scrap.wizard'

    @api.multi
    def done_request(self):
        for tire in self.tire_ids:
            tire.scrap_motohour = tire.running_motohour
            tire.scrap_km = tire.running_km
        res = super(TireScrapWizard, self).done_request()
        return res