# -*- encoding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.     
#
##############################################################################

from odoo import models, fields, api, _
from odoo import exceptions
from datetime import datetime

class ChangeTireTechnicWizard(models.TransientModel):
    _name = 'change.tire.technic.wizard'
    _description = "Change Tire Technic"
    
    technic_id = fields.Many2one('technic', 'Technic')
    
    @api.multi
    def change_tire_technic(self):
        ''' Дугйн Техникийг солих'''
        context = self._context
        if self.technic_id:
            tires = self.env['tire.register'].browse(context.get('active_ids'))
            for tire in tires:
                if tire.state == 'using':
                    tire.start_performance_motohour = tire.running_motohour
                    tire.start_performance_km = tire.running_km
                    tire.technic_id = self.technic_id.id
                    tire.motohour = self.technic_id.last_motohour
                    tire.kilometer = self.technic_id.last_km
        return {'type': 'ir.actions.act_window_close'}