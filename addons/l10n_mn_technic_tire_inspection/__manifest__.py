# -*- coding: utf-8 -*-

{
    'name': 'Дугуйн үзлэг',
    'version': '1.0',
    'author': 'Asterisk Technologies LLC',
    'description': """ Mongolian Technic Tire Inspection """,
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'depends': ['l10n_mn_technic_tire'],
    'data': [
        'view/inspection_view.xml',
    ],
    "demo_xml": [],
    "active": False,
    "installable": True,
    'css': ['static/src/css/base.css','static/src/css/description.css',],
}