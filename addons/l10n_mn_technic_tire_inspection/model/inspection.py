# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)

class TireInspection(models.Model):
    _name = 'tire.inspection'
    _description = 'Tire Inspection'
    _order = 'recorded_date desc'

    @api.depends('tire_inspect_history_count')
    def _tire_inspect_history_count(self):
        self.tire_inspect_history_count = len(self.env['inspection.history'].search([('history_id', '=', self.id)]))

    date_inspected = fields.Datetime('Date', required=True, default=datetime.now())
    technic_id = fields.Many2one('technic', 'Technic', ondelete='cascade')
    employee_id = fields.Many2one('hr.employee', 'Employee', required=True)
    recorded_date = fields.Datetime('Date recorded', default=datetime.now())
    technic_tire_inspect = fields.One2many('technic.tire.inspect', 'inspect_id', 'Tire', ondelete='cascade')
    state = fields.Selection([('draft', 'Draft'), ('approved', 'Approved')], 'State', default='draft')
    tire_inspect_history_count = fields.Integer(compute=_tire_inspect_history_count, string='Inspect history')

    @api.one
    def import_tire_info(self):
        val = {}
        tires = self.env['tire.register'].search([('technic_id', '=', self.technic_id.id if self.technic_id else False)])
        self.technic_tire_inspect.unlink()
        if tires:
            for tire in tires:
                val = {
                    'inspect_id': self.id,
                    'ins_tech_id': tire.technic_id.id,
                    'location_inspect': tire.current_position,
                    'tire_id': tire.id,
                    'brand_inspect': tire.brand_id.id,
                    'model_inspect': tire.technic_model_id.id,
                    'serial_inspect': tire.serial,
                }
                self.technic_tire_inspect.create(val)
        else:
            raise ValidationError("Didn't find any tire!")

    @api.multi
    def write(self, val):
        if 'state' not in val and self.state == 'approved':
            raise ValidationError("Cannot edit approved file.\n Make it draft first")
        creation = super(TireInspection, self).write(val)
        return creation

    @api.multi
    def approve(self):
        for inspect in self.technic_tire_inspect:
            self.env['tire.register'].search([('serial', '=', inspect.serial_inspect)]).tread_current_deep = inspect.avarage_deep
            self.env['inspection.history'].create({
                'history_id': self.id,
                'inspect_date': datetime.now(),
                'tire_id': inspect.tire_id.id,
                'avarage_deep': inspect.avarage_deep,
                'state': 'approved'
            })
        self.state = 'approved'

    @api.multi
    def draft(self):
        for tire in self.technic_tire_inspect:
            update_obj = self.env['inspection.history'].search([('history_id', '!=', self.id),
                                                       ('tire_id', '=', tire.tire_id.id),
                                                       ('state', '=', 'approved')],
                                                      order="inspect_date desc", limit=1)
            if update_obj:
                tire.tire_id.tread_current_deep = update_obj.avarage_deep
            else:
                tire.tire_id.tread_current_deep = tire.tire_id.tire_setting_id.norm_tread_deep

class TechnicTireInspect(models.Model):
    _name = 'technic.tire.inspect'

    inspect_id = fields.Many2one('tire.inspection', 'Inspect', ondelete='cascade')
    ins_tech_id = fields.Many2one('technic', 'Technic')
    location_inspect = fields.Integer('Tire location on technic', readonly=True)
    brand_inspect = fields.Many2one('brand', 'Brand', readonly=True)
    tire_id = fields.Many2one('tire.register', 'Model', readonly=True)
    model_inspect = fields.Many2one('technic.model', 'Model', readonly=True)
    serial_inspect = fields.Char('Serial', readonly=True)
    inspect_dot = fields.One2many('inspect.dot', 'dot_id', 'Inspect dot')
    dot_number = fields.Integer(compute='_compute_dot', string='Dot number', store=True)
    avarage_deep = fields.Float(compute='_compute_dot', string='Avarage deep', store=True)
    tread_depreciation_percent = fields.Float(compute='_compute_dot', string='Inspected tread depreciation percent', store=True)
    pressure = fields.Float('Pressure')
    description = fields.Text('Description')

    @api.depends('inspect_dot')
    def _compute_dot(self):
        for rec in self:
            avarage = 0
            tread_percent = 0
            tire_id = self.env['tire.register'].search([('serial', '=', rec.serial_inspect)])
            for r in rec.inspect_dot:
                avarage += r.inspect_tread_deep
            if len(rec.inspect_dot) != 0:
                avarage = avarage / len(rec.inspect_dot)
            if tire_id.tire_setting_id.norm_tread_deep:
                tread_percent = (avarage * 100) / tire_id.tire_setting_id.norm_tread_deep
            rec.dot_number = len(rec.inspect_dot)
            rec.avarage_deep = avarage
            rec.tread_depreciation_percent = tread_percent

class Inspect_dot(models.Model):
    _name = 'inspect.dot'

    dot_id = fields.Many2one('technic.tire.inspect', 'Dot', ondelete='cascade')
    inspect_tread_deep = fields.Float('Inspected tread deep')

class InspectionHistory(models.Model):
    _name = 'inspection.history'
    _order = 'inspect_date desc'

    history_id = fields.Many2one('tire.inspection', 'Inspection history', ondelete="cascade")
    inspect_date = fields.Datetime('Inspect date')
    tire_id = fields.Many2one('tire.register', 'Tire')
    avarage_deep = fields.Float('Avarage deep')
    state = fields.Selection([('draft', 'Draft'), ('approved', 'Approved')], 'State', default='draft')
