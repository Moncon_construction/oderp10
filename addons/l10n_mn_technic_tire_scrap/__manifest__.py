# -*- coding: utf-8 -*-
##############################################################################
#
#
#
##############################################################################

{
    'name': 'Дугуйн акт',
    'version': '1.0',
    'author': 'Asterisk Technologies LLC',
    'description': """ Mongolian Technic Tire Scrap """,
    'website' : 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'depends': ['l10n_mn_technic_tire', 'l10n_mn_technic_scrap','l10n_mn_workflow_config'],
    'data': [
        'view/tire_scrap_reason_view.xml',
        'view/sequence.xml',
        'wizard/tire_scrap_wizard_view.xml',
        'view/tire_scrap_view.xml',
        'view/tire_scrap_workflow.xml',
        'security/ir.model.access.csv',
    ],
    "demo_xml": [],
    "active": False,
    "installable": True,
    'css': ['static/src/css/base.css', 'static/src/css/description.css'],
}
