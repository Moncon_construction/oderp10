# -*- coding: utf-8 -*-
################################
#
#
#
################################
from datetime import datetime
from odoo import models, fields, api
from odoo.exceptions import except_orm


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    tire_scrap_id = fields.Many2one('tire.scrap', 'Tire scrap')


class TireScrap(models.Model):
    _name = 'tire.scrap'
    _description = 'Tire scrap'
    _inherit = ['mail.thread']
    _rec_name = 'origin'

    @api.multi
    def _count_tire_account_invoice(self):
        for this in self:
            this.count_tire_account_invoice = len(this.respondents.ids)

    origin = fields.Char('Tire scrap Reference', readonly=True, copy=False)
    name = fields.Char(compute='_set_name', string='Name')
    technic_id = fields.Many2one('technic', string='Technic')

    tire_ids = fields.Many2many('tire.register', 'tire_tire_scrap_rel', 'scrap_id', 'tire_id', string='Tires',
                               domain="[('state', '=', 'using'), ('technic_id', '=', technic_id)]")

    state = fields.Selection([('draft', 'Draft'),
                              ('waiting_approval', 'Waiting approval'),
                              ('approved', 'Approved'),
                              ('refused', 'Refused'),
                              ('done', 'Done')], string='Status', default='draft')
    created_user = fields.Many2one('res.users', 'Created user', default=lambda self: self.env.user)
    date = fields.Datetime('Date', default=datetime.now())
    description = fields.Text('Description')
    workflow_id = fields.Many2one('workflow.config', 'Workflow')
    check_sequence = fields.Integer('Workflow Step', default=0)
    history_lines = fields.One2many('tire.scrap.workflow.history', 'scrap_id', 'Workflow History Line')
    is_validator = fields.Boolean(compute='_compute_is_validator')
    is_creator = fields.Boolean(compute='_compute_is_creator')
    is_payable = fields.Boolean('Is payable')
    count_tire_account_invoice = fields.Integer(compute='_count_tire_account_invoice', string='Invoice')

    @api.multi
    @api.depends('technic_id')
    def _set_name(self):
        res = {}
        res[self.id] = u'{0}/{1}'.format(self.technic_id.name, u'TS')
        return res

    @api.multi
    @api.depends('check_sequence')
    def _compute_is_validator(self):
        for rec in self:
            history_obj = self.env['tire.scrap.workflow.history']
            validators = history_obj.search([('scrap_id', '=', rec.id), ('line_sequence', '=', rec.check_sequence)], limit=1, order='sent_date DESC').user_ids
            if self.env.user in validators:
                rec.is_validator = True
            else:
                rec.is_validator = False

    @api.multi
    def _compute_is_creator(self):
        for rec in self:
            if rec.created_user == self.env.user:
                rec.is_creator = True
            else:
                rec.is_creator = False

    @api.onchange('technic_id')
    def onchange_technic(self):
        self.tire_ids = None

    @api.multi
    def action_set_to_draft(self):
        self.ensure_one()
        self.check_sequence = 0
        self.state = 'draft'

    @api.multi
    def action_send(self):
        self.ensure_one()
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].send('tire.scrap.workflow.history', 'scrap_id', self, self.env.uid)
            if success:
                self.check_sequence = current_sequence
                self.state = 'waiting_approval'

    @api.multi
    def action_return(self):
        self.ensure_one()
        if self.workflow_id:
            success, current_sequence = self.env['workflow.config'].action_return('tire.scrap.workflow.history', 'scrap_id', self, self.env.user.id)
            if success:
                self.check_sequence = current_sequence

    @api.multi
    def action_refuse(self):
        self.ensure_one()
        if self.workflow_id:
            success = self.env['workflow.config'].reject('tire.scrap.workflow.history', 'scrap_id', self, self.env.user.id)
            if success:
                self.state = 'refused'

    @api.multi
    def action_approve(self):
        self.ensure_one()
        if self.workflow_id:
            success, sub_success, current_sequence = self.env['workflow.config'].approve(
                'tire.scrap.workflow.history', 'scrap_id', self, self.env.user.id)
            if success:
                if sub_success:
                    self.state = 'approved'
                else:
                    self.check_sequence = current_sequence

    @api.multi
    def action_finish(self):
        self.ensure_one()
        self.state = 'done'


class payableScrapRespondentsParts(models.Model):
    _inherit = 'payable.scrap.respondents.parts'

    tire_scrap_id = fields.Many2one('tire.scrap', 'Tire Scrap')
    tire_cost = fields.Float(compute='_compute_cost_tire')

    @api.multi
    @api.depends('tire_scrap_id')
    def _compute_cost_tire(self):
        for rec in self:
            total = 0
            for tire in rec.tire_scrap_id.tire_ids:
                total += tire.expense_cost - tire.norm_cost
            rec.tire_cost = total / 100 * rec.percent

class TireScrap(models.Model):
    _inherit = 'tire.scrap'

    respondents = fields.One2many('payable.scrap.respondents.parts', 'tire_scrap_id', 'Employee')

    @api.model
    def create(self, vals):
        if vals.get('origin', '-') == '-':
            vals['origin'] = self.env['ir.sequence'].next_by_code('technic.parts.scrap')
        create_id = super(TireScrap, self).create(vals)
        workflow_id = False
        employee = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
        workflow_id = self.env['workflow.config'].get_workflow('employee', 'tire.scrap', employee[0].id, None)
        if workflow_id:
            create_id.workflow_id = workflow_id
        else:
            raise except_orm(('Warning!'), ('There is no workflow id defined!'))
        return create_id

class TireScrapWorkflowHistory(models.Model):
    _name = 'tire.scrap.workflow.history'
    _description = 'Tire Scrap Workflow History'
    _order = 'scrap_id, sent_date'

    STATE_SELECTION = [
        ('waiting', 'Waiting'),
        ('confirmed', 'Confirmed'),
        ('approved', 'Approved'),
        ('return', 'Return'),
        ('rejected', 'Rejected'),
    ]

    scrap_id = fields.Many2one('tire.scrap', 'Scrap Request', readonly=True, ondelete='cascade')
    name = fields.Char('Verification Step', readonly=True)
    user_ids = fields.Many2many('res.users', 'res_users_tire_scrap_workflow_history_ref', 'history_id', 'user_id', string='Validators')
    sent_date = fields.Datetime('Sent date', required=True, readonly=True)
    user_id = fields.Many2one('res.users', 'Validator', readonly=True)
    action_date = fields.Datetime('Action date', readonly=True)
    action = fields.Selection(STATE_SELECTION, 'Action', readonly=True)
    line_sequence = fields.Integer('Workflow Step')


class TireRegister(models.Model):
    _inherit = 'tire.register'

    in_scrap = fields.Boolean('In scrap')
    reason = fields.Many2one('tire.scrap.reason', 'Reason')
