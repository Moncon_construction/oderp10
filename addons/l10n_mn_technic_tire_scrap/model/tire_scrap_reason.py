# -*- coding: utf-8 -*-
################################
#
#
#
################################
from odoo import models, fields, api

class TireScrapReason(models.Model):
    _name = 'tire.scrap.reason'

    name = fields.Char('name')
    note = fields.Text('Description')