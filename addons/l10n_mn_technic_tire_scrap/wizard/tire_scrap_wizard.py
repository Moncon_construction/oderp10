# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions


class TireScrapWizard(models.TransientModel):
    _name = 'tire.scrap.wizard'

    def _default_technic(self):
        return self.env['tire.scrap'].browse(self._context.get('active_id')).technic_id

    def _default_tires(self):
        tire_rs = self.env['tire.scrap'].browse(self._context.get('active_id')).tire_ids
        tire_list = []
        for tire in tire_rs:
            if tire.state not in "rejected":
                tire_list.append(tire.id)
        return self.env['tire.register'].browse(tire_list)

    @api.onchange('technic_id')
    def set_domain_for_tires(self):
        res = {}
        tire_rs = self.env['tire.scrap'].browse(self._context.get('active_id')).tire_ids
        tire_list = []
        for tire in tire_rs:
            if tire.state not in "rejected":
                tire_list.append(tire.id)
        res = {'domain': {'tire_ids': [('id', 'in', tire_list)]}}
        return res

    technic_id = fields.Many2one('technic', default=_default_technic, string='Technic')
    tire_ids = fields.Many2many('tire.register', 'tire_scrap_wizard_tire_scrap_rel', 'tire_scrap_wizard_id', 'tire_register_id', string='Tire', default=_default_tires)
    description = fields.Text(string='Description', required=True, stored=False)

    @api.multi
    def done_request(self):
        state_done = False
        for tire in self.tire_ids:
            tire.state = 'rejected'
        def_tire = self._default_tires()
        if not def_tire:
            state_done = True
        if state_done:
            self.env['tire.scrap'].browse(self._context.get('active_id')).state = 'done'
        return True
