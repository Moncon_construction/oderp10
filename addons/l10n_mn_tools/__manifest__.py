# -*- coding: utf-8 -*-
{
    'name': "Mongolian Tools",
    'version': '1.0',
    'depends': ['l10n_mn_low_value_asset'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Asset Modules',
    'description': """
    """,
    'data': [
        'views/low_value_asset_tool_views.xml',
        'views/product_product_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
