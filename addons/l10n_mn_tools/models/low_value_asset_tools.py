# -*- coding: utf-8 -*-
import urllib

from odoo import models, fields, api, _
from random import choice
from string import digits
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError


class LowValueAsset(models.Model):
    _inherit = 'low.value.asset'

    _sql_constraints = [
        ('barcode_uniq', 'unique(barcode)', _("A barcode unique")),
    ]
    is_tools = fields.Boolean('Tools', defualt=True)
    include_kit_id = fields.Many2one('low.value.asset')
    included_kit_ids = fields.One2many('low.value.asset', 'include_kit_id', readonly=True)

    def write(self, vals):
        for obj in self:
            if 'include_kit_id' in vals:
                if obj.id == vals['include_kit_id']:
                    raise UserError(_("You can't choose yourself"))
        return super(LowValueAsset, self).write(vals)