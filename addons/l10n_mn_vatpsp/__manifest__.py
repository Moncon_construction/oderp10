# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

{
    'name': "Mongolian VATS (ebarimt)",
    'version': '1.0',
    'description': """
        ebarimt
    """,
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'depends': [
        'l10n_mn_contacts', 'l10n_mn_account'
    ],
    'data': [
        'data/vatpsp_data.xml',
        'security/ir.model.access.csv',
        'report/ebarimt_receipt_view.xml',
        'views/configuration_view.xml',
        'views/vat_category_code_view.xml',
        'views/check_vatpsp_view.xml',
        'views/res_partner_view.xml',
        'views/account_invoice_view.xml',
        'views/paper_format_view.xml',
        'views/print_ebarimt_receipt_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
