# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

import configuration
import vat_category_code
import vatpsp_service
import qr_code_history
import res_company
import res_partner
import check_vatpsp
import account_invoice
import ebarimt_receipt