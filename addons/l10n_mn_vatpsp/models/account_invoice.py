# -*- coding: utf-8 -*-

import logging
import base64
import time
import datetime
import dateutil.relativedelta

from odoo import exceptions
from odoo import api, fields, models, _
from odoo.tools import float_compare, float_round

_logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def _get_stock(self):
        for invoice in self:
            stock = self.env['stock.inventory'].sudo().search([('name', '=', invoice.origin)])
            if stock:
                invoice.is_stock = True
            else:
                invoice.is_stock = False

    vat_bill_id = fields.Char('VAT Bill ID', size=64, readonly=True, copy=False)
    vat_bill_type = fields.Selection([('1', 'Citizens Sales'),
                                      ('2', 'Organizations Purchase'),
                                      ('3', 'Organizations Sales')], 'VAT Bill Type', size=32, readonly=True, copy=False)
    vat_lottery = fields.Char('VAT Lottery', size=128, readonly=True, copy=False)
    vat_qr_data = fields.Char('VAT QR Code', readonly=True, copy=False)

    vat_register_no = fields.Char('VAT Register NO', size=64, readonly=True, copy=False)
    vat_returnbill_id = fields.Char('VAT Return Bill ID', size=64, readonly=True, copy=False)
    is_stock = fields.Boolean('Is stock', compute=_get_stock, default=True)  # НӨАТУС табыг гаргаж ирэхгүй гэвэл үүнийг True болгоно.
    not_send = fields.Boolean('Not send')  # Ebarimt руу илгээхгүй гэвэл үүнийг сонгоно.

    def vatpsp_commit(self):
        """
            Татварын систем рүү борлуулалтын мэдээллээ илгээх функц.
            Ebarimt руу илгээх өгөгдлийн параметрийн нэмэлт тайлбар:
                posNo: Тухайн байгууллагын дотоод кассын дугаар
                customerNo: Худалдан авагч байгууллагын ТТД эсвэл Иргэний регистерийн дугаар
                billType: Харилцагч хувь бол эцсийн хэрэглэгч буюу 1 гэсэн утга авна. Харин 3 бол компани хооронд гэж үзнэ.
                billIdSuffix: Баримтын ДДТД-ыг давхцуулахгүйн тулд олгох дотоод дугаарлалт. Тухайн өдөртөө дахин давтагдашгүй дугаар байна
                returnBillId: Засварлах баримтын дугаар буцаалт хийсэн тохиолдолд хөтлөгдөнө
                taxType: Тухайн бараа ажил үйлчилгээний ангилалын кодод харгалзах төрлийн код байна. 1,2,3 утгуудыг авдаг.
                stocks: Бараа ажил үйлчилгээний жагсаалт - Нэхэмжлэхийн мөрүүдийн утга
        """
        _logger.info('CALLED vatpsp_commit()')
        json_lines = []
        amount_tax = 0
        amount_subtotal = 0
        barcode = ""
        customer_no = ""
        report_month = ""
        PosAPI_billType = "1"
        is_vats_goods = True

        currency_obj = self.env['res.currency']
        vatpsp_obj = self.env['vatpsp.service']
        qr_code_history = self.env['qr.code.history']

        from_currency = self.currency_id
        to_currency = self.company_id.currency_id

        # Холболт идэвхтэй эсэхийг шалгаж байна.
        alive = vatpsp_obj.check_alive(self.company_id.id)
        if not alive:
            return True

        # TODO
        # global_vat = float(self.env['ir.config_parameter'].get_param('report.tax') or '0')

        # Харилцагчийн ТТД буюу регистрийг авч байна.
        if self.partner_id.parent_id:
            if self.partner_id.parent_id.is_company:
                PosAPI_billType = "3"
                if self.partner_id.parent_id.register == False:
                    raise exceptions.except_orm(_('Warning !'), _('Please insert register number of the partner information!!!'))
                else:
                    customer_no = self.partner_id.parent_id.register

        elif self.partner_id.is_company:
                PosAPI_billType = "3"
                if self.partner_id.register == False:
                    raise exceptions.except_orm(_('Warning !'), _('Please insert register number of the partner information!!!'))
                else:
                    customer_no = self.partner_id.register

        elif self.partner_id.register:
            customer_no = self.partner_id.register

        # Нэхэмжлэлийн бүх мөр бараа сонгогдсон байна. Учир нь бараа ажил үйлчилгээний кодыг авахын тулд юм.
        for move in self.invoice_line_ids:
            if not move.product_id:
                is_vats_goods = False

        if self.amount_total > 0:
            taxType = ''
            for move in self.invoice_line_ids:
                taxType = str(move.product_id.vat_category_code)
                if taxType not in ['1','2','3']:
                    raise exceptions.except_orm(_('Warning !'), _('Please insert product of the category code /ebarimt/ !!!'))

                if move.price_unit > 0.0:
                    price_unit = 0
                    subtotal = 0
                    untaxedtotal = 0
                    if move.discount != 0.0:
                        price_unit = (move.price_unit / 100) * (100 - move.discount)
                    else:
                        price_unit = move.price_unit

                    subtotal = price_unit * move.quantity
                    untaxedtotal = price_unit * move.quantity

                    if move.product_id.barcode:
                        ean = move.product_id.barcode
                    else:
                        ean = move.product_id.vat_category_code
                        if not ean:
                            raise exceptions.except_orm(_('Warning !'), _('There is no category code. Please set category code product or product category!\n'))

                    # Нэхэмжлэлийн дүнг компанийн валют руу хөрвүүлж байна.
                    price_unit = currency_obj._compute(from_currency, to_currency, price_unit)
                    untaxedtotal = currency_obj._compute(from_currency, to_currency, untaxedtotal)
                    subtotal = currency_obj._compute(from_currency, to_currency, subtotal)

                    json_lines.append({
                        'code': str(move.product_id.default_code or ''),
                        'name': vatpsp_obj.clean(move.product_id.name or '?'),
                        'measureUnit': move.uom_id.name,
                        'qty': "%.2f" % float_round(move.quantity, 2),
                        'unitPrice': "%.2f" % float_round(price_unit, 2),
                        'totalAmount': "%.2f" % float_round(move.price_subtotal, 2),
                        'cityTax': "0.00",
                        'vat': "%.2f" % float_round(subtotal - untaxedtotal, 2),
                        'barCode': str(ean) or ""
                    })

                    amount_subtotal += subtotal
                    amount_tax += (subtotal - untaxedtotal)

            # Хэрэв нэхэмжлэлийн бүх мөр бараа, ажил үйлчилгээтэй холбогдоогүй бол урамшууллын системд холбогдохгүй
            if is_vats_goods:
                json_order = {
                    'amount': "%.2f" % float_round(self.amount_total, 2)
                    , 'vat': "%.2f" % float_round(self.amount_tax, 2)
                    , 'cashAmount': "%.2f" % float_round(self.amount_total, 2)
                    , 'nonCashAmount': "0.00"
                    , 'cityTax': "0.00"
                    , 'reportMonth': report_month
                    , 'taxType': taxType
                    # , 'districtCode': invoice.warehouse_id.vatpsp_code
                    # , 'posNo': "{0:04d}".format(invoice.warehouse_id.id)
                    , 'districtCode': '26'
                    , 'posNo': '0001'
                    , 'customerNo': customer_no
                    , 'billType': PosAPI_billType
                    , 'billIdSuffix': u"{0:06d}".format(int(str(self.id)[-6:]))
                    , 'returnBillId': ""
                    , 'stocks': json_lines
                }

                # Ebarimt руу илгээх үйлдэл хийж байна.
                try:
                    res = self.env['vatpsp.service'].request_put(self.company_id.id, json_order)
                    _logger.info('SEND JSON ORDER :: %s' % json_order)
                except ValueError:
                    raise exceptions.except_orm(_('Warning !'), _('There is something problem happens in connecting VATPSP service!\n'))
                if not res['success']:
                    # Илгээх үйлдэл амжилтгүй болвол алдааны мэдээлэл, кодыг харуулна.
                    raise exceptions.except_orm(_('Warning !'), _('There is something problem happens in connecting VATPSP service!\n (%s)%s') % (res['errorCode'],res['message']))

                # Эцсийн хэрэглээнд гаргасан борлуулалтын баримт сугалааны дугааргүй хэвлэгдэвэл тухайн түүхийг хадгалж байна.
                if PosAPI_billType == "1":
                    if res['lottery'] == u'':
                        info = vatpsp_obj.request_get_information(self.company_id.id)

                        # Алдааны түүхийг хадгалж байна.
                        qr_code_history.create({
                            'information': info
                            , 'bill_id': res['billId']
                            , 'company_id': self.company_id.id
                        })

                # Нэхэмжлэлд холбогдох ebarimt-ын утгуудыг оноож байна.
                self.write({
                    'vat_bill_id': res['billId']
                    , 'vat_bill_type': res['billType']
                    , 'vat_register_no': customer_no
                    , 'vat_lottery': res['lottery']
                    , 'vat_qr_data': res['qrData']
                })

                # Төлбөрийн баримтанд сугалааны дугаар болон qr кодыг дамжуулж байна.
                datas = {
                    'vat_qr_code': res['qrData']
                    ,'vat_lottery': res['lottery']
                }

                # Төлбөрийн баримт үүсгэнэ.
                try:
                    pdf = self.get_pdf_file(datas)
                except Exception as e:
                    _logger.error('Could not create ebarimt invoice.', exc_info=True)
                    raise Warning(_('Error'), _('Could not config_parameter ebarimt invoice. \n%s') % e.message)

                # Ирсэн баримтыг нэхэмжлэлд хавсаргаж байна.
                result = base64.b64encode(pdf)
                attachment_data = {
                    'name': '%s.pdf' % self.number,
                    'datas_fname': '%s.pdf' % self.number,
                    'datas': result,
                    'res_model': 'account.invoice',
                    'res_id': self.id,
                }

                # Баримтыг хавсаргах үйлдэл хийж байна.
                self.env['ir.attachment'].create(attachment_data)

            return True

    def get_pdf_file(self, data):
        pdf = self.env['report'].get_pdf([self.id], 'l10n_mn_vatpsp.report_ebarimt_receipt', data=data)
        return pdf

    def vatpsp_refund(self):
        """
            Борлуулалтаас үүссэн нэхэмжлэлийн буцаалт хийхэд энэ функц дуудагдана.
        """
        _logger.info('CALLED vatpsp_refund()')
        if self.refund_invoice_id:
            if not self.refund_invoice_id.vat_bill_id:
                # НӨАТ-н урамшуулалгүй нэхэмжлэхийн буцаалтыг мөн илгээхгүй
                return True
            else:
                refund_time = datetime.datetime.now().time()
                refund_time = refund_time.strftime("%H:%M:%S")
                # raise exceptions.except_orm(_('Warning !'), _('There is something problem happens in connecting VATPSP service!\n'))
                try:
                    # Өмнө нь НӨАТ-д бүртгэгдсэн буцаах баримтыг илгээж байна.
                    json_order = {
                        "returnBillId": self.refund_invoice_id.vat_bill_id
                        , "date": str(self.refund_invoice_id.date_invoice) + str(" %s" % (refund_time))
                    }
                    res = self.env['vatpsp.service'].request_return_bill(self.company_id.id, json_order)
                    _logger.info('SEND REFUND JSON :: %s' % json_order)
                except ValueError:
                    raise exceptions.except_orm(_('Warning !'), _('There is something problem happens in connecting VATPSP service!\n'))
                if not res['success']:
                    # Илгээх үйлдэл амжилтгүй болвол алдааны мэдээлэл, кодыг харуулна.
                    raise exceptions.except_orm(_('Warning !'), _('There is something problem happens in connecting VATPSP service!\n (%s)%s') % (res['errorCode'], res['message']))

                self.write({
                    'vat_bill_id': self.refund_invoice_id.vat_bill_id,
                    'vat_bill_type': self.refund_invoice_id.vat_bill_type,
                    'vat_register_no': self.refund_invoice_id.vat_register_no
                })

        return True
