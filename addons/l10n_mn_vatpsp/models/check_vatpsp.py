# -*- coding: utf-8 -*-

import logging

from odoo import api, fields, models, _

_logger = logging.getLogger(__name__)

class CheckVatpsp(models.Model):
    _name = 'check.vatpsp'
    _description = 'Check VATPSP'
    
    result = fields.Text(string='Result', readonly=True)
    company_id = fields.Many2one('res.company', string='Company', required=True)
    
    @api.multi
    def check(self):
        '''
            Хэрэглэгчийн PosAPI-н дагуу холболт хийгдсэн эсэхийг шалгана.
        '''
        self.ensure_one()
        result = []

        # Холболтыг шалгаж байна.
        res = self.env['vatpsp.service'].request_check_api(self.company_id.id)
        result.append('PosAPI::checkApi :%s' % res)

        # Холболтыг шалгаад үр дүнг нь харуулна.
        res = self.env['vatpsp.service'].request_get_information(self.company_id.id)
        result.append('PosAPI::getInformation :%s' % res)
        
        self.write({'result':u'\n'.join(result)})
        return {
            'res_model':'check.vatpsp',
            'view_type':'form',
            'view_mode':'form',
            'target': 'new',
            'type': 'ir.actions.act_window',
            'res_id': self.id,
            'context': self.env.context
        }

    @api.multi
    def send(self):
    	'''
    	   Бүртгэгдсэн баримтуудыг илгээх эсвэл PosAPI анх удаа холболт хийж байгаа үед энэ функц дор хаяж 1 удаа дуудагдах ёстой.
    	'''
        self.ensure_one()
        result = []

        # Холболтыг шалгаж байна.
        res = self.env['vatpsp.service'].request_check_api(self.company_id.id)
        result.append('PosAPI::checkApi :%s' % res)

        # Холболтыг шалгаад үр дүнг нь харуулна.
        res = self.env['vatpsp.service'].request_get_information(self.company_id.id)
        result.append('PosAPI::getInformation :%s' % res)

        # Бүртгэгдсэн баримтуудыг илгээж байна. 
        res = self.env['vatpsp.service'].request_send_data(self.company_id.id)
        result.append('PosAPI::sendData :%s' % res)
        
        self.write({'result':u'\n'.join(result)})
        return {
            'res_model':'check.vatpsp',
            'view_type':'form',
            'view_mode':'form',
            'target': 'new',
            'type': 'ir.actions.act_window',
            'res_id': self.id,
            'context': self.env.context
        }
