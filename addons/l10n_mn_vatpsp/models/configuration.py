# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo import exceptions


class CategoryType(models.Model):
    _name = 'category.type'

    code = fields.Integer(string='Code')
    name = fields.Char('Name')
    taxes = fields.Many2many('account.tax', 'type_id', 'tax_id', 'taxes_type_rel', 'Taxes')


class ProductTemplate(models.Model):
    _inherit = 'product.template'
    _name = 'product.template'

    @api.model
    def _get_vat_category_code(self):
        category_code = ''
        for obj in self:
            if not obj.vat_category_code_id:
                categ = obj.categ_id
                if obj.categ_id.vat_category_code_id:
                    category_code = obj.categ_id.vat_category_code_id.type_id.code
                else:
                    while categ.parent_id:
                        if categ.parent_id.vat_category_code_id:
                            category_code = categ.parent_id.vat_category_code_id.type_id.code
                            break
                        categ = categ.parent_id
                obj.vat_category_code = category_code
            else:
                obj.vat_category_code = obj.vat_category_code_id.type_id.code

    vat_category_code = fields.Char(string='Vat Category Code', compute=_get_vat_category_code)
    vat_category_code_id = fields.Many2one('vat.category.code', string='Vat Category Code', track_visibility='onchange', help='This field is used Vatpsp. So you should configure this field on product or product category.')

    @api.multi
    def write(self, vals):
        res = super(ProductTemplate, self).write(vals)
        IrModuleModule = self.env['ir.module.module']
        if not self.vat_category_code:
            point_of_sale = IrModuleModule.sudo().search([('name', '=', 'point_of_sale')])
            if point_of_sale:
                if point_of_sale.state != 'to install':
                    raise exceptions.except_orm(_('Warning !'), _('There is no category code. Please set category code product or product category!\n Product name is %s') % self.display_name)
            else:
                raise exceptions.except_orm(_('Warning !'), _('There is no category code. Please set category code product or product category!\n Product name is %s') % self.display_name)

        return res

    @api.model
    def create(self, values):
        res = super(ProductTemplate, self).create(values)
        for obj in self:
            if not obj.vat_category_code:
                raise exceptions.except_orm(_('Warning !'), _('There is no category code. Please set category code product or product category!\n Product name is %s') % obj.display_name)
        return res


class ProductCategory(models.Model):
    _inherit = 'product.category'

    vat_category_code_id = fields.Many2one('vat.category.code', string='VATPSP Category Code', help='This field is used Vatpsp. So you should configure this field on product or product category.')


class CountryState(models.Model):
    _inherit = 'res.country.state'

    vatpsp_code = fields.Char('VATPSP Code')
