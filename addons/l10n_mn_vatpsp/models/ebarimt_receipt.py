# -*- coding: utf-8 -*-

from odoo import models

from odoo.addons.l10n_mn_report.models.report_helper import verbose_numeric, comma_me, convert_curr  # @UnresolvedImport


class EbarimtReceipt(models.AbstractModel):
    _name = 'report.l10n_mn_vatpsp.report_ebarimt_receipt'

    def get_from_invoice_address(self, invoice):
        '''
            Төлбөрийн баримтын борлуулагч гэсэн хэсгийн мэдээллийг авахад ашиглана.
        '''
        name = street = email = '...........'
        phone = fax = '...........'
        site = employee = '..........'
        sale_obj = self.env['sale.order']
        if invoice:
            address = False
            if invoice.type in ('in_invoice', 'out_refund'):
                address = invoice.partner_id
                site = invoice.partner_id.website or ''
                name = invoice.partner_id.name or '...........'
                register = invoice.partner_id.register or ''
            else:
                address = invoice.company_id.partner_id
                site = address.website or '...........'
                name = address.name or '...........'
                register = address.register or '...........'
            if address:
                phone = address.phone or '...........'
                email = address.email or '...........'
                fax = address.fax or '...........'
                street = address.street or ''
                if address.street2:
                    street += address.street2 or ''

            if invoice.origin:
                order = sale_obj.search([('name', '=', invoice.origin)], limit=1)
                employee = order.user_id.name

        res = {
            'name': name, 'phone': phone, 'fax': fax, 'email': email, 'register': register, 'website': site, 'address': street, 'employee': employee
        }

        return res

    def get_to_invoice_address(self, invoice):
        '''
            Худалдан авагчийн мэдээллийг авна.
        '''
        street = name = '...........'
        phone = fax = '...........'
        if invoice:
            address = False
            if invoice.type in ('in_invoice', 'out_refund'):
                address = invoice.company_id.partner_id
                name = invoice.company_id.partner_id.name or '...........'
                register = invoice.company_id.partner_id.register or ''
            else:
                address = invoice.partner_id
                name = invoice.partner_id.name or '...........'
                register = invoice.partner_id.register or ''

            if address:
                phone = address.phone or '...........'
                fax = address.fax or '...........'
                street = address.street or '...........'
                if address.street2:
                    street += address.street2 or ''

        res = {
            'name': name,
            'phone': phone,
            'fax': fax,
            'address': street,
            'register': register
        }

        return res

    def render_html(self, ids, data=None):
        report_obj = self.env['report']
        invoice_obj = self.env['account.invoice']

        report = report_obj._get_report_from_name('l10n_mn_vatpsp.report_ebarimt_receipt')
        inv = invoice_obj.browse(ids[0])
        verbose_total_dict = {}
        get_from_invoice_address = self.get_from_invoice_address(inv)
        get_to_invoice_address = self.get_to_invoice_address(inv)

        curr = u''
        div_curr = u''
        get_total = {
            'amount': 0.0, 'paid': 0.0, 'tax': 0.0, 'discount': 0.0, 'currency': inv.currency_id.name
        }

        total_amount = sum(line.price_subtotal for line in inv.invoice_line_ids)
        total_tax = sum(line.amount for line in inv.tax_line_ids)
        total_discount = sum((line.discount / 100.0) * (line.price_unit * line.quantity) for line in inv.invoice_line_ids if line.discount > 0)
        total_amount_total = total_amount + total_discount

        if inv.currency_id:
            curr = inv.currency_id.integer
            div_curr = inv.currency_id.divisible

        total_to_word = verbose_numeric(abs(total_amount + total_tax))
        verbose_total_dict[inv.id] = convert_curr(total_to_word, curr, div_curr)
        get_total['amount'] = comma_me(total_amount_total)
        get_total['tax'] = comma_me(total_tax)
        get_total['discount'] = comma_me(total_discount)
        get_total['paid'] = comma_me(total_amount + total_tax)
        qr_code = False
        lottery = False
        if data:
            qr_code = data['vat_qr_code']
            lottery = data['vat_lottery']

        docargs = {
            'doc_ids': ids,
            'doc_model': report.model,
            'docs': inv,
            'verbose_amount': verbose_total_dict,
            'from_address': get_from_invoice_address,
            'to_address': get_to_invoice_address,
            'sumtotal': get_total,
            'qr_code': qr_code,
            'lottery': lottery,
            'data_report_margin_top': 5,
            'data_report_header_spacing': 5,
        }

        return report_obj.render('l10n_mn_vatpsp.report_ebarimt_receipt', docargs)
