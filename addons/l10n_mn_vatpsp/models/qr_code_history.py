# -*- coding: utf-8 -*-

from odoo import fields, models


class QrCodeHistory(models.Model):
    _name = 'qr.code.history'
    _description = 'Not Qr Code Sales History'

    information = fields.Text(string='Get information')
    bill_id = fields.Char(string='Bill ID')
    company_id = fields.Many2one('res.company', string='Company')
