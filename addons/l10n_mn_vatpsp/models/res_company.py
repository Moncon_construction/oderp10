# -*- coding: utf-8 -*-

from odoo import fields, models


class ResCompany(models.Model):
    _inherit = 'res.company'

    code_history_ids = fields.One2many('qr.code.history', 'company_id', 'Code Error History')
