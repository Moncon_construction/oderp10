# -*- encoding: utf-8 -*-

import json
import logging
import requests

from odoo import api, fields, models, _

_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    _inherit = 'res.partner'

    is_found = fields.Boolean(string='Is found /Ebarimt/')
    is_citypayer = fields.Boolean(string='Is citypayer')

    @api.onchange('register')
    def onchange_register(self):
        # Ebarimt-с харилцагчийн регистрийн дугаарыг оруулаад мэдээллийг татна.
        url_prefix = 'http://info.ebarimt.mn/rest/merchant/info?regno='
        if self.register:
            # Регистрын дугаарыг сервистэй холбож байна.
            url = u'{0}{1}'.format(url_prefix, self.register)

            resp = requests.get(url=url)
            data = None
            try:
                data = json.loads(resp.text)
            except Exception as e:
                _logger.error('Connection failed.', exc_info=True)
                _logger.error(_('Error'), _('Could not connect to json device. \n%s') % e.message)
            self.is_found = data['found']
            self.is_citypayer = data['citypayer']
            self.is_taxpayer = data['vatpayer']

