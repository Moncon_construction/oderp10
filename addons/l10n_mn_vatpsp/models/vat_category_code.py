# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo import exceptions

class CategoryCode(models.Model):
    _name = 'vat.category.code'
    _description = 'Product Category Code Vat'

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if not args:
            args = args or []
        ids = self.browse()
        if name:
            ids = self.search([('name', operator, name)] + args, limit=limit)
        if not ids:
            ids = self.search([('code', operator, name)] + args, limit=limit)
        result = ids.name_get()
        return result

    @api.depends('name', 'code')
    def name_get(self):
        res = []
        for pcode in self:
            name = pcode.name + ': ' + pcode.code
            res.append((pcode.id, name))
        return res

    name = fields.Char(string='Name')
    code = fields.Char(string='Code')
    type_id = fields.Many2one('category.type', required=True, string='Category Type')