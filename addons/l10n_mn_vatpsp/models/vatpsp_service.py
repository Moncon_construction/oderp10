# -*- encoding: utf-8 -*-

import sys
import json
import logging

import traceback

from odoo import models
from odoo.tools import config
from odoo.tools.misc import detect_ip_addr as _detect_ip_addr
from odoo import exceptions, _

_logger = logging.getLogger(__name__)


class VatpspService(models.TransientModel):
    _name = 'vatpsp.service'
    _description = 'VATPSP Service'

    def clean(self, text):
        res = text.replace("*", 'x').replace('"', '').replace('\\', '')
        return res

    def send_data_all(self):
        '''
        Татварын систем рүү мэдээллийг илгээнэ.
        '''
        server_data = self.env['ir.config_parameter'].get_param('vatpsp_service_data')
        server_data = eval(server_data)
        company_ids = server_data['vatpsp_bridge'].keys()
        for company_id in company_ids:
            try:
                self.request_send_data(company_id)
            except Exception:
                error_message = traceback.format_exc()
                _logger.error(error_message)

        return True

    def check_alive(self, company_id):
        '''
        Татварын системд холбогдох тохиргоо
        '''
        server_data = self.env['ir.config_parameter'].get_param('vatpsp_service_data')

        if server_data:
            server_data = eval(server_data)

            # Системийн параметрт vatpsp_bridge гэсэн dictionary утгат холболт хийх Ip, Port, PosAPI-н замын тохируулсан байдаг.
            return company_id in server_data['vatpsp_bridge']
        else:
            return False

    def request_check_api(self, company_id):
        '''
        Тухайн компаны VATPSP_bridge сервис-г дуудна.
        Хэрэглэгчийн системийн тогтвортой ажиллагааг хангах шаардлагын улмаас PosAPI сангийн
        ажиллагааг шалгана. Хэрэглэгчийн системийг ажиллуулж буй үйлдлийн системийн хэрэглэгч нь заавал
        өөрийн HOME directory-той байх ёстой. Хэрэв уг шаардлагыг хангаагүй бол уг функц нь амжилтгүй гэсэн
        утгыг буцаана.
        '''
        return self.redirect2proxy('check_api', company_id)

    def request_get_information(self, company_id):
        '''
        Тухайн компаны VATPSP_bridge сервис-г дуудна.
        Хэрэглэгчийн систем нь нэгээс олон PosAPI ашиглаж буй үед харьцаж буй PosAPI-гийн мэдээллийг
        харах шаардлага тулгардаг. Уг функц нь уг асуудлыг шийдэж буй бөгөөд уг функцийн тусламжтайгаар
        хэрэглэгчийн систем нь тухайн ашиглаж буй PosAPI-гийн дотоод мэдээллүүдийг авна.
        '''
        return self.redirect2proxy('get_information', company_id)

    def request_put(self, company_id, data):
        '''
        Тухайн компаны VATPSP_bridge сервис-г дуудна.
        Хэрэглэгчийн системээс борлуулалтын мэдээллийг хүлээн авч буцаан баримтын ДДТД, сугалааны
        дугаар, баримт хэвлэсэн огноо, баримтын код, QrCode гэсэн утгуудыг нэмж боловсруулан буцаана.
        '''
        return self.redirect2proxy('put', company_id, data)

    def request_send_data(self, company_id):
        '''
        Тухайн компаны VATPSP_bridge сервис-г дуудна.
        Баримтын мэдээллийг хуулинд заасны дагуу борлуулалт гэж үзэн бүртгэсэнээс хойш 72 цагийн
        дотор илгээх ёстой. Уг функц нь борлуулалтын мэдээллийг татварын нэгдсэн системд илгээнэ.
        Мөн хэрэглэгчийн системд PosAPI-г анх удаа суурьлуулж буй эсвэл сугалааны багц дууссан нөхцөлд
        мөн уг функцийг ажиллуулж тохиргоо болон шинэ сугалааны багцыг татаж авна.
        '''
        return self.redirect2proxy('send_data', company_id)

    def request_return_bill(self, company_id, data):
        '''
        Тухайн компаны VATPSP_bridge сервис-г дуудна.
        Хэрэглэгчийн системээс бүртгэгдэн гарсан баримтийг хүчингүй болгоно. Уг функцээр буцаасан
        нөхцөлд тухайн баримтан дээрх сугалаа болон дахин давтагдашгүй төлбөрийн дугаар/ДДТД/ зэргийг
        хүчингүй гэж үзнэ.
        '''

        return self.redirect2proxy('return_bill', company_id, data)

    def redirect2proxy(self, method, company_id, *args, **kwargs):
        # Бүртгэгдсэн системийн параметрийг дуудаж байна. Системийн параметрт холболт хийх утгуудыг оруулсан байдаг.
        server_data = self.env['ir.config_parameter'].get_param('vatpsp_service_data')

        if not server_data:
            return exceptions.except_orm(_('Error !'), _('There is missing something configuration for connect to VATPSP service'))

        server_data = eval(server_data)
        if company_id not in server_data['vatpsp_bridge']:
            return False

        # Холбогдох зам
        bridge_path = server_data['vatpsp_bridge'][company_id]['path']
        # PosAPI-г хөрвүүлэхэд үүссэн файлын нэр
        bridge_name = server_data['vatpsp_bridge'][company_id]['module']
        # Серверийн IP
        host = server_data['vatpsp_bridge'][company_id]['host']
        # Серверийн порт
        port = server_data['vatpsp_bridge'][company_id]['port']

        _logger.info('HOST:: %s' % host)
        _logger.info('DETECT:: %s' % _detect_ip_addr())
        _logger.info('CONFIG:: %s' % config['xmlrpc_port'])
        _logger.info('PORT:: %s' % port)

        if host == str(_detect_ip_addr()) and str(config['xmlrpc_port']) == port:
            # Local server
            res = getattr(self, 'vatpsp_' + method)(bridge_path, bridge_name, *args, **kwargs)
        else:
            # Redirect to mirror server
            raise exceptions.except_orm(_('Error !'), _('Check the hosts and ports you have set up for the server system parameters. /vatpsp_service_data parameter/'))

        return res

    def vatpsp_check_api(self, bridge_path, bridge_name):
        '''
        Call PosAPI::checkApi
        '''
        sys.path.append(bridge_path)
        vatpsp = __import__(bridge_name)
        res = False
        try:
            res = json.loads(vatpsp.checkApi())
        except Exception:
            error_message = traceback.format_exc()
            _logger.error(error_message)
            res = {
                'success': False, 'errorCode': 'local', 'message': error_message
            }

        return res

    def vatpsp_get_information(self, bridge_path, bridge_name):
        '''
        Call PosAPI::getInformation
        '''
        sys.path.append(bridge_path)
        vatpsp = __import__(bridge_name)
        res = False
        try:
            res = json.loads(vatpsp.getInformation())
            if 'extraInfo' in res and res['extraInfo'].get('lastSentDate', False) is None:
                res['extraInfo']['lastSentDate'] = False
        except Exception:
            error_message = traceback.format_exc()
            _logger.error(error_message)
            res = {
                'success': False, 'errorCode': 'local', 'message': error_message
            }

        return res

    def vatpsp_send_data(self, bridge_path, bridge_name):
        '''
        Call PosAPI::sendData
        '''
        sys.path.append(bridge_path)
        vatpsp = __import__(bridge_name)
        res = False
        try:
            res = json.loads(vatpsp.sendData())
        except Exception:
            error_message = traceback.format_exc()
            _logger.error(error_message)
            res = {
                'success': False, 'errorCode': 'local', 'message': error_message
            }

        return res

    def vatpsp_put(self, bridge_path, bridge_name, data):
        '''
        Call PosAPI::put
        '''
        sys.path.append(bridge_path)
        vatpsp = __import__(bridge_name)
        res = False
        try:
            json_arg = json.dumps(data).decode('unicode-escape').encode('utf8')
            res = json.loads(vatpsp.putData(json_arg))
        except Exception:
            error_message = traceback.format_exc()
            _logger.error(error_message)
            res = {
                'success': False, 'errorCode': 'local', 'message': error_message
            }

        return res

    def vatpsp_return_bill(self, bridge_path, bridge_name, data):
        '''
        Call PosAPI::returnBill
        '''
        sys.path.append(bridge_path)
        vatpsp = __import__(bridge_name)
        res = False
        try:
            res = json.loads(vatpsp.returnBill(json.dumps(data).decode('unicode-escape').encode('utf8')))
        except Exception:
            error_message = traceback.format_exc()
            _logger.error(error_message)
            res = {
                'success': False, 'errorCode': 'local', 'message': error_message
            }

        return res
