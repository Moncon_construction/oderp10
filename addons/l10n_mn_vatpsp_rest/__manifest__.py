# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

{
    'name': "Mongolian VATS (ebarimt) RESTful connection",
    'version': '1.0',
    'description': """
EBARIMT
====================
Ubuntu 16-д зориулсан REST service холболтын модуль
l10n_mn_vatpsp module суусан байх шаардлагатай
    """,
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website' : 'http://asterisk-tech.mn',
    'depends': ['l10n_mn_vatpsp'],
    'data': [
        'data/vatpsp_data.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False
}
