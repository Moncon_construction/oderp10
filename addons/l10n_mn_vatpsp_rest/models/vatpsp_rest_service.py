# -*- encoding: utf-8 -*-

import json
import logging
import traceback

from odoo import api, models
from odoo.tools import config
from odoo import exceptions, _
import urllib2
import urllib

_logger = logging.getLogger(__name__)


class VatpspServiceRest(models.TransientModel):
    _inherit = 'vatpsp.service'

    _description = 'VATPSP REST Client'

    def send_data_all(self):
        '''
            Татварын систем рүү мэдээллийг илгээнэ.
        '''
        server_data = self.env['ir.config_parameter'].get_param('vatpsp_rest_service')

        if server_data:
            server_data = eval(server_data)

            for company_id in server_data['vatpsp_rest']:

                try:
                    self.request_send_data(company_id)
                except Exception:
                    error_message = traceback.format_exc()
                    _logger.error(error_message)

        return True

    @api.model
    def check_alive(self, company_id):
        '''
            Татварын системд холбогдох тохиргоо
        '''
        server_data = self.env['ir.config_parameter'].get_param('vatpsp_rest_service')

        if server_data:
            server_data = eval(server_data)

            # Системийн параметрт vatpsp_bridge гэсэн dictionary утгат холболт хийх Ip, Port, PosAPI-н замын тохируулсан байдаг.
            return company_id in server_data['vatpsp_rest']
        else:
            return False

    @api.model
    def redirect2proxy(self, method, company_id, *args, **kwargs):

        # Бүртгэгдсэн системийн параметрийг дуудаж байна. Системийн параметрт холболт хийх утгуудыг оруулсан байдаг.
        server_data = self.env['ir.config_parameter'].get_param('vatpsp_rest_service')

        if not server_data:
            return exceptions.except_orm(_('Error !'), _('There is missing something configuration for connect to VATPSP service'))

        server_data = eval(server_data)
        if company_id not in server_data['vatpsp_rest']:
            return False

        # Серверийн IP
        host = server_data['vatpsp_rest'][company_id]['host']
        # Серверийн порт
        port = server_data['vatpsp_rest'][company_id]['port']

        _logger.info('HOST:: %s' % host)
        _logger.info('CONFIG:: %s' % config['xmlrpc_port'])
        _logger.info('PORT:: %s' % port)

        # Local server
        res = getattr(self, 'vatpsp_rest_' + method)(host, port, *args, **kwargs)

        return res

    def vatpsp_rest_check_api(self, host, port):
        '''
            Call PosAPI::checkApi
        '''

        res = False
        try:
            res = urllib2.urlopen("http://%s:%s/checkAPI/" % (host, port)).read()
            res = json.loads(res)
        except Exception:
            error_message = traceback.format_exc()
            _logger.error(error_message)
            res = {
                'success': False, 'errorCode': 'local', 'message': error_message
            }

        return res

    def vatpsp_rest_get_information(self, host, port):
        '''
            Call PosAPI::getInformation
        '''

        res = False
        try:
            res = urllib2.urlopen("http://%s:%s/getInformation/" % (host, port)).read()
            res = json.loads(res)
            if 'extraInfo' in res and res['extraInfo'].get('lastSentDate', False) is None:
                res['extraInfo']['lastSentDate'] = False
        except Exception:
            error_message = traceback.format_exc()
            _logger.error(error_message)
            res = {
                'success': False, 'errorCode': 'local', 'message': error_message
            }

        return res

    def vatpsp_rest_send_data(self, host, port):
        '''
            Call PosAPI::sendData
        '''

        res = False
        try:
            res = urllib2.urlopen("http://%s:%s/sendData/" % (host, port)).read()
            res = json.loads(res)
        except Exception:
            error_message = traceback.format_exc()
            _logger.error(error_message)
            res = {
                'success': False, 'errorCode': 'local', 'message': error_message
            }

        return res

    def vatpsp_rest_put(self, host, port, data):
        '''
            Call PosAPI::put
        '''

        res = False
        try:

            json_arg = json.dumps(data).decode('unicode-escape').encode('utf8')
            json_arg = urllib.urlencode({"param": json_arg})
            request = urllib2.Request("http://%s:%s/put/" % (host, port))
            request.add_header("Content-Type", "application/x-www-form-urlencoded")
            request.get_method = lambda: 'POST'
            request.add_data(json_arg)

            res = urllib2.urlopen(request).read()
            res = json.loads(res)
        except Exception:
            error_message = traceback.format_exc()
            _logger.error(error_message)
            res = {
                'success': False, 'errorCode': 'local', 'message': error_message
            }

        return res

    def vatpsp_rest_return_bill(self, host, port, data):
        '''
            Call PosAPI::returnBill
        '''

        res = False
        try:
            json_arg = json.dumps(data).decode('unicode-escape').encode('utf8')
            json_arg = urllib.urlencode({"param": json_arg})
            request = urllib2.Request("http://%s:%s/returnBill/" % (host, port))
            request.add_header("Content-Type", "application/x-www-form-urlencoded")
            request.get_method = lambda: 'POST'
            request.add_data(json_arg)

            res = urllib2.urlopen(request).read()
            res = json.loads(res)
        except Exception:
            error_message = traceback.format_exc()
            _logger.error(error_message)
            res = {
                'success': False, 'errorCode': 'local', 'message': error_message
            }

        return res
