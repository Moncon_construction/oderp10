# -*- coding: utf-8 -*-

# Part of OdERP. See LICENSE file for full copyright and licensing details.

{
    'name': "Mongolian VATS (ebarimt) for sale",
    'version': '1.0',
    'description': """
        ebarimt
    """,
    'category': 'Mongolian Modules',
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'depends': [
        'l10n_mn_sale', 'l10n_mn_vatpsp'
    ],
    'data': [
        'views/sale_config_settings_view.xml',
        'views/configuration_view.xml',
        'views/vat_category_code_view.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': True
}
