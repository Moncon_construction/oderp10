# -*- coding: utf-8 -*-

import logging
import base64
import time
import datetime
import dateutil.relativedelta

from odoo import exceptions
from odoo import api, fields, models, _
from odoo.tools import float_compare, float_round

_logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def invoice_validate(self):
        """
            Нэхэмжлэх батлах үед НӨАТ-н урамшууллын функцыг дуудахын тулд дахин тодорхойлов.
        """
        _logger.info("CALLED invoice_validate() OVERRIDDEN FUNCTION")
        for invoice in self:
            if invoice.type == 'out_invoice' and not invoice.not_send:
                # Хэрвээ агуулахын тооллогын тохируулгаас үүссэн нэхэмжлэх байвал Ebramit илгээхгүй.
                stock = self.env['stock.inventory'].search([('name', '=', invoice.origin)])
                if stock:
                    return super(AccountInvoice, self).invoice_validate()
                # Хэрвээ борлуулалтаас үүссэн урьдчилгаа нэхэмжлэх байвал Ebarimt илгээхгүй.
                if invoice.is_prepayment:
                    if invoice.company_id.not_send_citizen:
                        if (invoice.partner_id.is_company or invoice.partner_id.parent_id.is_company) and \
                                (not invoice.partner_id.register or invoice.partner_id.parent_id.register):
                            invoice.vatpsp_commit()

                    elif not invoice.company_id.not_send_citizen:
                        invoice.vatpsp_commit()

            # НӨАТ-н буцаалт тооцохгүй эсэх
            if not invoice.company_id.not_refund_vatpsp:
                if invoice.type == 'out_refund':
                    invoice.vatpsp_refund()
        return super(AccountInvoice, self).invoice_validate()
