# -*- coding: utf-8 -*-

from odoo import fields, models


class ResCompany(models.Model):
    _inherit = 'res.company'

    not_send_citizen = fields.Boolean('Not send citizen', default=False)
    not_refund_vatpsp = fields.Boolean('Not refund vatpsp', default=False)
    sale_paperformat_id = fields.Many2one('report.paperformat', string='Paper format')
