# -*- coding: utf-8 -*-

from odoo import fields, models


class SaleConfiguration(models.TransientModel):
    _inherit = 'sale.config.settings'

    not_send_citizen = fields.Boolean('Not send citizen', related='company_id.not_send_citizen')
    not_refund_vatpsp = fields.Boolean('Not refund vatpsp', related='company_id.not_refund_vatpsp')
    sale_paperformat_id = fields.Many2one('report.paperformat', string='Paper format', related='company_id.sale_paperformat_id')
