# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian - Web',
    'category': 'Hidden',
    'version': '1.0',
    'depends': ['web', 'l10n_mn_base'],
    'author': "Asterisk Technologies LLC",
    'description': """
         Odoo-ийн үндэс болсон Web Client-ийн модулийг OdERP дээр удамшуулав.
    """,
    'website' : 'http://asterisk-tech.mn',
    'qweb': ['static/src/xml/activate_developer_mode.xml'],
    'data': [
             'views/widgets.xml',
            ],
    'installable': True,
    'auto_install': True
}
