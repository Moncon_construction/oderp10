
odoo.define('l10n_mn_web.ColumnProgressBar', function (require) {
"use strict";

var core = require('web.core');
var utils = require('web.utils');
var Widget = require('web.Widget');
var ListView = require('web.ListView');
var Model = require('web.DataModel');

var QWeb = core.qweb;
var _t = core._t;
var list_widget_registry = core.list_widget_registry;

function set_beauty(value) {
    var value = value;
    if(value.length == 2){
    	value = new Array(2).join('\xa0\xa0\xa0') + value;
    }
    else if(value.length == 1){
    	value = new Array(2).join('\xa0\xa0\xa0\xa0\xa0') + value;
    }
    return value;
}

/**
 * @param {Array} 
 */
var ColumnProgressBar = ListView.Column.extend({
    /**
     * Return a formatted progress bar display
     *
     * @private
     */
    _format: function (row_data, options) {
        return _.template(
            '<progress value="<%-value%>" max="100"><%-value%>%</progress> <%-value1%>%')({
                value: _.str.sprintf("%.0f", row_data[this.id].value || 0),
            	value1: set_beauty(_.str.sprintf("%.0f", row_data[this.id].value || 0))
            });
    }
});

list_widget_registry.add('field.progressbar_with_value', ColumnProgressBar);

});

