# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian Web Loan Request',
    'version': '1.0',
    'category': 'Base',
    'author': 'Asterisk Technologies LLC',
    'website': 'http://asterisk-tech.mn',
    'description': """
        Loan Request - Allows you to submit a loan request form to the web
    """,
    'depends': ['website_form', 'l10n_mn_loan_management'],
    'data': [
        'data/data.xml',
        'views/templates.xml',
    ],
    'license': 'GPL-3',
}