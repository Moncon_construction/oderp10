# -*- coding: utf-8 -*-
from odoo import http, _
from odoo.http import request

class WebLoanRequest(http.Controller):

    @http.route('/page/request', auth='public', type='http', website=True)
    def loan_request(self, **kw):
        """ Зээлийн хүсэлт форм руу шилжих функц """
        values, errors = {}, {}
        return request.render('l10n_mn_web_loan_request.loan_request', values)

    def send_request(self, qcontext):
        """ Зээлийн хүсэлт үүсгэх функц """
        values = {key: qcontext.get(key) for key in ('last_name', 'sur_name', 'com_name', 'register', 'phone', 'email', 'company', 'year', 'job', 'certificate_number',
                                                      'monthly_income', 'origin_income', 'type_id', 'loan_amount', 'collateral', 'loan_period', 'employment_status',
                                                      'purpose_id', 'operation_type', 'operation_year', 'emp_name', 'website', 'address', 'duration_of_employment',
                                                      'manager_name', 'manager_phone', 'is_paid_animal_tax', 'duration_of_farmer', 'is_registered_animal', 'is_insurance_animal',
                                                      'is_insurance', 'is_reported_tax', 'profession', 'is_than_score', 'school', 'course', 'pension_book_number', 'pension_date')}
        if values.get('register'):
            request.env['loan.order'].create_loan_order(values)

    @http.route('/request/form', type='http', auth='public', website=True)
    def checkout(self, *args, **kw):
        qcontext = request.params.copy()
        if 'error' not in qcontext and request.httprequest.method == 'POST':
            try:
                if qcontext.get('type_id'):
                    self.send_request(qcontext)
                    return request.render('l10n_mn_web_loan_request.request_thanks')
                else:
                    qcontext['message'] = _("Please fill it correctly")
            except Exception, e:
                qcontext['error'] = e.message or e.name
        values, errors = {}, {}
        purpose = request.env['loan.purpose']
        loan_type = request.env['product.product.loan']
        qcontext = {
            'checkout': values,
            'purpose': purpose,
            'loan_type': loan_type,
            'purposes': purpose.sudo().get_website_loan_purpose(),
            'loan_types': loan_type.sudo().get_website_product_loan(),
            'error': errors,
            'is_person': True,
        }

        return request.render('l10n_mn_web_loan_request.loan_request_form', qcontext)