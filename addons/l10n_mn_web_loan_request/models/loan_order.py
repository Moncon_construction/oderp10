# -*- coding: utf-8 -*-

from odoo import models, api
from datetime import datetime

class LoanPurpose(models.Model):
    _inherit = 'loan.purpose'

    def get_website_loan_purpose(self):
        purpose = self.sudo().search([])
        return purpose

class ProductProductLoan(models.Model):
    _inherit = 'product.product.loan'

    def get_website_product_loan(self):
        return self.sudo().search([])


class LoanOrder(models.Model):
    _inherit = 'loan.order'

    @api.multi
    def create_loan_order(self, values):
        """ Зээлийн хүсэлт үүсгэх функц """
        partner_id = self.env['res.partner'].sudo().search([('register', '=', values.get('register'))])
        if partner_id:
            partner_id.update_partner(values)
        else:
            partner_id = self.env['res.partner'].create_partner(values)
        loan_product = self.env['product.product.loan'].sudo().search([('id', '=', values.get('type_id'))])
        loan_purpose = self.env['loan.purpose'].sudo().search([('id', '=', values.get('purpose_id'))])
        loan_state = self.env['loan.category.change.type'].sudo().search([], limit=1)
        loan_id = self.sudo().create({
                    'name': partner_id.name + " " + values.get('loan_amount', 0),
                    'amount': values.get('loan_amount', 0),
                    'interest': loan_product.min_interest,
                    'loan_purpose_id': loan_purpose.id,
                    'loan_state': loan_state.id,
                    'loss_percent': 20,
                    'partner_id': partner_id.id,
                    'payment_day': 25,
                    'period': values.get('loan_period', 1),
                    'product_id': loan_product.id,
                    'schedule_type': 'type3',
                    'notes': values.get('purpose_id'),
                    'installment_date': datetime.now(),
                    'fixed_payment': 1,
                    'company_id': self.env.user.company_id.id,
                })
        return loan_id

