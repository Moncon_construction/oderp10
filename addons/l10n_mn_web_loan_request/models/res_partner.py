# -*- coding: utf-8 -*-

from odoo import models, api

class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def prepare_partner_values(self, vals):
        """ Ажил эрхлэлтийн байдлаас хамааран харилцагчийн өгөгдлийг бэлдэх функц """
        value = {
            'name': vals['com_name'] if vals['com_name'] else vals['last_name'] + ' ' + vals['sur_name'],
            'is_company': True if vals['com_name'] else False,
            'email': vals['email'],
            'phone': vals['phone'],
            'street': vals['address'] if vals['com_name'] else '',
            'employment_status': vals['employment_status'],
        }
        if vals['employment_status'] == 'corp_emp':
            value.update({
                'function': vals['job'],
                'duration_of_employment': vals['year'],
                'house_income': vals['monthly_income'],
            })
        elif vals['employment_status'] == 'farmer':
            value.update({
                'duration_of_farmer': vals['duration_of_farmer'],
                'is_registered_animal': vals['is_registered_animal'] if vals['is_registered_animal'] is not None else False,
                'is_insurance_animal': vals['is_insurance_animal'] if vals['is_insurance_animal'] is not None else False,
                'is_paid_animal_tax': vals['is_paid_animal_tax'] if vals['is_paid_animal_tax'] is not None else False,
            })
        elif vals['employment_status'] == 'student':
            value.update({
                'profession': vals['profession'],
                'is_than_score': vals['is_than_score'] if vals['is_than_score'] is not None else False,
                'school': vals['school'],
                'course': vals['course'],
            })
        elif vals['employment_status'] == 'government_emp':
            value.update({
                'manager_name': vals['manager_name'],
                'manager_phone': vals['manager_phone'],
                'function': vals['job'],
                'duration_of_employment': vals['year'],
                'house_income': vals['monthly_income'],
            })
        elif vals['employment_status'] == 'retired':
            value.update({
                'pension_book_number': vals['pension_book_number'],
                'pension_date': vals['pension_date'],
            })
        elif vals['employment_status'] == 'company':
            value.update({
                'operation_type': vals['operation_type'],
                'operation_year': vals['operation_year'],
                'website': vals['website'],
                'certificate_number': vals['certificate_number'],
            })
        elif vals['employment_status'] == 'self_emp':
            value.update({
                'is_insurance': vals['is_insurance'],
                'is_reported_tax': vals['is_reported_tax'],
            })
        return value

    @api.multi
    def create_partner(self, vals):
        """ Харилцагч шинээр үүсгэх функц """
        partner = self.env['res.partner'].sudo().create(self.prepare_partner_values(vals))
        partner.write({'register': vals['register']})
        return partner

    @api.multi
    def update_partner(self, vals):
        """ Харилцагчийн мэдээллийг шинэчилэх функц """
        partner = self.env['res.partner'].sudo().search([('register', '=', vals.get('register'))])
        partner.sudo().write(self.prepare_partner_values(vals))