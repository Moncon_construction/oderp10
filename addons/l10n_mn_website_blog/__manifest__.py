# -*- coding: utf-8 -*-
# Part of OdERP. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian Blogs',
    'category': 'Website',
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'version': '1.0',
    'depends': ['website_blog'],
    'data': [
        'views/website_blog_templates.xml',
    ],
    'installable': True,
}
