# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mongolian Workflow Config',
    'version': '1.0',
    'category': 'Base',
    'author': 'Asterisk Technologies LLC',
    'maintainer': 'nasanochir@asterisk-tech.mn',
    'website': 'http://asterisk-tech.mn',
    'description': """
        L10n_mn - Workflow Config
    """,
    'depends': ['hr'],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/workflow_config_view.xml'
    ],
    'license': 'GPL-3',
}
