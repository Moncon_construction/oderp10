# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import time
import logging
from odoo.exceptions import UserError
import sys
reload(sys)

_logger = logging.getLogger(__name__)


class WorkflowConfig(models.Model):
    """ Ажлын урсгалын тохиргоо """
    _name = 'workflow.config'
    _description = "All Workflows Configure"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _order = "id desc"

    active = fields.Boolean(string='Active', default=True)
    name = fields.Char(string='Name', required=True, size=128)
    template_id = fields.Many2one('mail.template', string='Mail Template')
    line_ids = fields.One2many('workflow.config.line', 'config_id', 'Config Line')
    department_id = fields.Many2one('hr.department', string='Department')
    model_id = fields.Many2one('ir.model', string='Model')
    company_id = fields.Many2one('res.company', string='Company')

    @api.multi
    def write(self, vals):
        # Тухайн хэлтэс дээр ажлын урсгал өмнө нь үүссэн байвал анхааруулга өгнө.
        if vals.get('department_id') is not False or vals.get('department_id') is not None:
            department_id = vals.get('department_id') if 'department_id' in vals else self.department_id.id
        if vals.get('model_id') is not False or vals.get('model_id') is not None:
            model_id = vals.get('model_id') if 'model_id' in vals else self.model_id.id
        if vals.get('company_id') is not False or vals.get('company_id') is not None:
            company_id = vals.get('company_id') if 'company_id' in vals else self.company_id.id

        # self._cr.execute(""" SELECT name
        #                    FROM workflow_config
        #                    WHERE department_id  = %s and company_id = %s and model_id = %s and id != %s
        #                    """, (department_id, company_id, model_id, self.id))
        # workflow_config = self._cr.fetchall()
        # if len(workflow_config) != 0:
        #     desc = _("The workflow config is registered in the system. \n Please check workflow config!")
        #     raise UserError(desc)
        return super(WorkflowConfig, self).write(vals)

    @api.model
    def create(self, vals):
        # Тухайн хэлтэс дээр ажлын урсгал өмнө нь үүссэн байвал анхааруулга өгнө.
        if vals.get('department_id') is not False or vals.get('department_id') is not None:
            department_id = vals.get('department_id')
        if vals.get('model_id') is not False or vals.get('model_id') is not None:
            model_id = vals.get('model_id')
        if vals.get('company_id') is not False or vals.get('company_id') is not None:
            company_id = vals.get('company_id')

        # self._cr.execute(""" SELECT name
        #                    FROM workflow_config
        #                    WHERE department_id  = %s and company_id = %s and model_id = %s
        #                    """, (department_id, company_id, model_id))
        # workflow_config = self._cr.fetchall()
        # if len(workflow_config) != 0:
        #     desc = _("The workflow config is registered in the system. \n Please check workflow config!")
        #     raise UserError(desc)
        return super(WorkflowConfig, self).create(vals)

    @api.onchange('department_id', 'model_id')
    def onchange_type(self):
        if self.department_id and self.model_id:
            self.update({'name': u'%s хэлтэсийн %s обектийн урсгал тохиргоо.' % (self.department_id.name, self.model_id.name)})

    """Ажлын урсгал эхлүүлэх буюу илгээх функц
        model: түүх бичих моделийн нэрийг string утгаар дамжуулна
        parent_id: түүх бичиж буй моделийн эцэгийн id - г дамжуулна
        obj: Ажлын урсгалыг ашиглаж буй обектийг дамжуулна. жишээ нь: self
        uid: нэмтэрсэн хэрэглэгчийн user id дамжуулна
        Буцаах утгууд
            success: Дараагийн алхамд амжилттай шилжүүлвэл True, үгүй бол False
            current_sequence: шилжүүлэх алхамын дараалалын дугаар. integer
    """
    @api.multi
    def send(self, model, parent_id, obj, uid, assigned_vals=None):
        """Хэрэв ажлын урсгалтай бөгөөд дараагийн алхамд амжиллттай шилжүүлээд түүх бичигдсэн бол True утга буцаана"""
        success = False
        current_sequence = False
        workflow_obj = self.env['workflow.config']
        user_obj = self.env['res.users']
        history_obj = self.env[model]
        if obj.workflow_id:
            """ ажлын урсгалын дараагийн алхамыг олох функцыг дуудаж байна """
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(obj.workflow_id.id, obj, uid, obj.check_sequence + 1, 'next')
            if is_next:
                obj.message_subscribe_users(user_ids=next_user_ids)
                """Хэн хэн гэдэг хэрэглэгчидийг дагагчаар нэмсэнийг харуулах мессеж илгээж байна"""
                next_user_names = u', '.join(map(lambda x: x.name, user_obj.browse(next_user_ids)))
                obj.message_post(body=u'Хүсэлт ажлын урсгалаар илгээгдлээ. Дараагийн шатанд хянагчид:%s' % next_user_names)
                """ Түүхийн мөр нэмэх """
                history_obj.create({
                    parent_id: obj.id,
                    'user_ids': [(6, 0, next_user_ids)],
                    'name': activity,
                    'sent_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                    'line_sequence': current_sequence,
                })
                if next_user_ids:
                    self.send_email(obj, next_user_ids, assigned_vals=assigned_vals)
                return True, current_sequence
        return success, current_sequence

    """Ажлын урсгал батлах эсвэл дараагийн шатны хянагчид илгээх функц
        model: түүх бичих моделийн нэрийг string утгаар дамжуулна
        parent_id: түүх бичиж буй моделийн эцэгийн id - г дамжуулна
        obj: Ажлын урсгалыг ашиглаж буй обектийг дамжуулна. жишээ нь: self
        uid: нэмтэрсэн хэрэглэгчийн user id дамжуулна
    """
    @api.multi
    def approve(self, model, parent_id, obj, uid):
        """Хэрэв ажлын урсгалтай бөгөөд дараагийн алхамд амжиллттай шилжүүлээд түүх бичигдсэн бол True утга буцаана"""
        success = False
        """Ажлын урсгал дуусч байгаа бол True утга буцаана"""
        sub_success = False
        current_sequence = False
        workflow_obj = self.env['workflow.config']
        user_obj = self.env['res.users']
        history_obj = self.env[model]
        if obj.workflow_id:
            """ ажлын урсгалын дараагийн алхамыг олох функцыг дуудаж байна """
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(obj.workflow_id.id, obj, obj.env.user.id, obj.check_sequence + 1, 'next')
            """Хэрэв дараагийн алхам олдвол иишээ орно"""
            if is_next:
                """Дараагийн алхамд илгээгдсэн түүхийн мөрийг олж байна"""
                history = history_obj.search([(parent_id, '=', obj.id)], order=' id DESC', limit=1)
                """олсон түүхийн мөр дээр зөвшөөрсөн түүхийг бичиж байна"""
                history.write({
                    'user_id': obj.env.user.id,
                    'action_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                    'action': 'approved',
                })
                """Дараачийн алхам батлах хэрэглэгчидийг дагагчаар нэмэх хэсэг"""
                obj.message_subscribe_users(user_ids=next_user_ids)
                """Хэн хэн гэдэг хэрэглэгчидийг дагагчаар нэмсэнийг харуулах мессеж илгээж байна"""
                next_user_names = u', '.join(map(lambda x: x.name, user_obj.browse(next_user_ids)))
                obj.message_post(body=u'Хүсэлт ажлын урсгалаар хянагдлаа. Дараагийн шатанд хянагчид:%s' % next_user_names)
                """ Түүхийн мөр нэмэх """
                history_obj.create({
                    parent_id: obj.id,
                    'user_ids': [(6, 0, next_user_ids)],
                    'name': activity,
                    'sent_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                    'line_sequence': current_sequence,
                })

                if next_user_ids:
                    self.send_email(obj, next_user_ids, assigned_vals=None)

                return True, False, current_sequence
            else:
                """Ажлын урсгалын төгсгөл бол ийшээ орно"""
                """Хамгийн сүүлчийн түүхийн мөрийг олоод дуусгана"""
                history = history_obj.search([(parent_id, '=', obj.id)], order=' id DESC', limit=1)
                history.write({
                    'user_id': obj.env.user.id,
                    'action_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                    'action': 'approved',
                })
                return True, True, current_sequence
        return success, sub_success, current_sequence
        """Буцаах утгууд
            success: Дараагийн алхамд амжилттай шилжүүлвэл True, үгүй бол False
            sub_success: Ажилын урсгалын төгсгөл бол True, үгүй бол False
            current_sequence: шилжүүлэх алхамын дараалалын дугаар. integer
        """

    """ Ажлын урсгалыг өмнөх алхамд буюу урагшаа нэг алхам буцаах функц
        model: түүх бичих моделийн нэрийг string утгаар дамжуулна
        parent_id: түүх бичиж буй моделийн эцэгийн id - г дамжуулна
        obj: Ажлын урсгалыг ашиглаж буй обектийг дамжуулна. жишээ нь: self
        uid: нэмтэрсэн хэрэглэгчийн user id дамжуулна"""
    @api.multi
    def action_return(self, model, parent_id, obj, uid):
        success = False
        current_sequence = False
        workflow_obj = self.env['workflow.config']
        user_obj = self.env['res.users']
        history_obj = self.env[model]
        if obj.workflow_id:
            is_next, next_user_ids, activity, current_sequence = workflow_obj.assign_to_next(obj.workflow_id.id, obj, uid, obj.check_sequence - 1, 'back')
            if is_next:
                history = history_obj.search([(parent_id, '=', obj.id)], order=' id DESC', limit=1)
                history.write({
                    'user_id': obj.env.user.id,
                    'action_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                    'action': 'rejected' if model == 'workflow.history' else 'return',
                })
                next_user_names = u', '.join(map(lambda x: x.name, user_obj.browse(next_user_ids)))
                obj.message_post(body=u'Хүсэлт ажлын урсгалаар буцаагдлаа. Дараагийн шатанд хянагчид:%s' % next_user_names)
                history_obj.create({
                    parent_id: obj.id,
                    'user_ids': [(6, 0, next_user_ids)],
                    'name': activity,
                    'sent_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                    'line_sequence': current_sequence,
                })

                if next_user_ids:
                    self.send_email(obj, next_user_ids, assigned_vals=None)

                return True, current_sequence
        return success, current_sequence
        """Буцаах утгууд
            success: Амжилтай буцаавал True, эсвэл False
            current_sequence: шилжүүлэх алхамын дараалалын дугаар. integer
        """

    """ Ажлын урсгалыг цуцлах функц
        model: түүх бичих моделийн нэрийг string утгаар дамжуулна
        parent_id: түүх бичиж буй моделийн эцэгийн id - г дамжуулна
        obj: Ажлын урсгалыг ашиглаж буй обектийг дамжуулна. жишээ нь: self
        uid: нэмтэрсэн хэрэглэгчийн user id дамжуулна"""
    @api.multi
    def reject(self, model, parent_id, obj, uid):
        success = False
        history_obj = self.env[model]
        if obj.workflow_id:
            history = history_obj.search([(parent_id, '=', obj.id)], order=' id DESC', limit=1)
            history.write({
                'user_id': obj.env.user.id,
                'action_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                'action': 'rejected',
            })
            return True
        return success
        """Буцаах утгууд
            success: Амжилтай цуцлавал True, эсвэл False
        """

    """ Ажлын урсгалыг олох функц
        type: employee, department гэсэн утгуудын аль нэгийг дамжуулна. Жишээ нь ажлын урсгалыг employee-оор олох бол employee утга дамжуулна
        model: Ажлын урсгалыг ашиглаж буй моделийн нэрийг  string-ээр дамжуулна
        employee_id: type нь employee гэж дамжуулсан бол энэ талбарыг заавал цэнэглэнэ. employee id-г дамжуулна. hr.employee
        department_id: type нь department гэж дамжуулсан бол энэ талбарыг заавал цэнэглэнэ. department id-г дамжуулна. hr.department"""
    @api.multi
    def get_workflow(self, wtype, model, employee_id=None, department_id=None):
        employee_obj = self.env['hr.employee']
        department_obj = self.env['hr.department']
        workflow_obj = self.env['workflow.config']
        model_id = self.env['ir.model'].search([('model', '=', model)])
        if not model_id:
            raise UserError(_('There is no model defined for (%s)!' % model))

        workflow_id = False
        if wtype == 'employee':
            employee = employee_obj.search([('id', '=', employee_id)])
            if employee:
                if employee.department_id:
                    """ажилтаны хэлтэсийг авч байна"""
                    department = employee.department_id
                    """Хэлтэсээр дээш гүйгээд хэлтэс тохируулсан тохиргоог олох хүртэл эсвэл хамгийн дээд талын эцэг хэлтэ олох хүртэл давтана"""
                    while 1 == 1:
                        workflow_ids = workflow_obj.search([('model_id', '=', model_id.id), ('department_id', '=', department.id)], limit=1)
                        if workflow_ids:
                            workflow_id = workflow_ids.id
                            break
                        else:
                            department_ids = department_obj.search([('id', '=', department.id)], limit=1)
                            if department_ids.parent_id:
                                department = department_ids.parent_id
                            else:
                                break
                else:
                    raise UserError(_('There is no department defined for employee.'))
            else:
                raise UserError(_('There is no employee defined for this user.'))
        elif wtype == 'department':
            department = department_obj.search([('id', '=', department_id)])
            """Хэлтэсээр дээш гүйгээд хэлтэс тохируулсан тохиргоог олох хүртэл эсвэл хамгийн дээд талын эцэг хэлтэ олох хүртэл давтана"""
            while 1 == 1:
                workflow_ids = workflow_obj.search([('model_id', '=', model_id.id), ('department_id', '=', department.id)], limit=1)
                if workflow_ids:
                    workflow_id = workflow_ids.id
                    break
                else:
                    department_ids = department_obj.search([('id', '=', department.id)])
                    if department_ids.parent_id:
                        department = department_ids.parent_id
                    else:
                        break
        return workflow_id
        """Буцаах утгууд
            workflow_id: Ажлын урсгалыг буцаана, олдохгүй бол False :
        """

    '''Дундын имэйл илгээдэг функц assign_to_next функц дотор ашигласан'''
    @api.multi
    def send_email(self, model_object, user_ids, assigned_vals=None):
        mail_obj = model_object
        object_name = model_object._name
        base_url = self.env['ir.config_parameter'].get_param('web.base.url')
        you_ids = []
        for line in user_ids:
            user = self.env['res.users'].search([('id', '=', line)])
            if user.partner_id.email and user.partner_id.notify_email == 'always':
                you_ids.append(user.partner_id.email)
            else:
                _logger.warning("Email was not sent to this user: %s ", user.partner_id.name)
        if you_ids:
            for you in you_ids:
                outgoing_email = self.env['ir.mail_server'].sudo().search([])
                if not outgoing_email:
                    raise UserError(_('There is no configuration for outgoing mail server. Please contact system administrator.'))
                else:
                    outgoing_email = self.env['ir.mail_server'].sudo().search([('id', '=', outgoing_email[0].id)])
                    obj = self.env['ir.model'].search([('model', '=', mail_obj._name)])
                    if obj:
                        obj = obj.name
                    else:
                        obj = object_name

                    url = "%s/mail/view?model=%s&amp;res_id=%s" % (unicode(base_url), object_name, mail_obj.id)
                    body = self.get_email_body(model_object, url, obj)
                        
                    if not assigned_vals:
                        vals = {
                            'state': 'outgoing',
                            'subject': _(u'Respond: %s') % obj or model_object._name,
                            'body_html': body,
                            'email_to': you,
                            'email_from': outgoing_email.smtp_user,
                        }
                    else:
                        vals = {
                            'state': 'outgoing',
                            'subject': assigned_vals['subject'] if 'subject' in assigned_vals else _(u'Respond: %s') % (object_name),
                            'body_html': assigned_vals['body_html'] if 'body_html' in assigned_vals else body,
                            'email_to': you,
                            'email_from': outgoing_email.smtp_user,
                        }
                    email_obj = self.env['mail.mail'].create(vals)
                    if email_obj:
                        email_obj.send()
                        mail_obj.message_post(body=_('Mail sent to: %s') % (you))
        return True

    def get_email_body(self, model_object, url, obj=None):
        return u"""
                <p>Сайн байна уу, </br></p><p>Таны батлах %s, %s.</br></p>
                <p><a href="%s" target="_blank" style="background-color: #9E588B; margin-top: 10px; padding: 10px; text-decoration: none; color: #fff; border-radius: 5px; font-size: 16px;">Холбоос</a></p>
                <p>Od ERP Автомат Имэйл </p>
            """ % (obj if obj else model_object.name, model_object._name, url)

    """ Ажлын урсгалыг олох функц
        workflow_id: Ажлын урсгалын тохиргооны id-г дамжуулна
        current: Одоогийн ажиллаж буй обектийг дамжуулна
        uid: Нэвтэрсэн хэрэглэгчийн user id дамжуулна. res.users
        check_sequence: шилжүүлэх алхамын дугаарыг дамжуулна.
        step_type: next, back 2 утгын аль нэгийг дамжуулна. хойш нь нэг алхам шилжүүлэх бол next, өмнөх алхамд шилжүүлэх бол back"""
    @api.multi
    def assign_to_next(self, workflow_id, current, uid, check_sequence, step_type):
        line_obj = self.env['workflow.config.line']
        employee_obj = self.env['hr.employee']
        group_obj = self.env['res.groups']
        next_step = line_obj.search([('config_id', '=', workflow_id), ('sequence', '=', check_sequence)], order='sequence', limit=1)
        """Ажилын урсгал нийт хэдэн алхамтай болохыг олж байна"""
        self.env.cr.execute(
            """SELECT id FROM workflow_config_line WHERE config_id = %s""" % workflow_id)
        row_count = self.env.cr.rowcount

        if next_step:
            while row_count >= check_sequence:
                """Ажлын урсгал дээр нөхцөлт илэрхийлэлийн утгыг шалгаж байна"""
                if not eval(str(next_step.expression), {'object': current}):
                    """Ажлын урсгалыг дараагийн алхамд шилжүүлэх үү, өмнөх алхамд шилжүүлэх үү гэдэгийг тодорхойлж байна"""
                    if step_type == 'next':
                        check_sequence += 1
                    elif step_type == 'back':
                        check_sequence -= 1
                    next_step = line_obj.search([('config_id', '=', workflow_id), ('sequence', '=', check_sequence)], order='sequence', limit=1)
                else:
                    break
                
        is_next_step_existing = False
        next_users = []
        next_activity = False
        current_sequence = False
        if next_step:
            # Үүсгэгч үед
            if next_step.type == 'creator':
                is_next_step_existing = True
                next_activity = next_step.name
                current_sequence = next_step.sequence
                next_users.append(current.create_uid.id)
            # Тогсон хэрэглэгч үед
            elif next_step.type == 'fixed':
                is_next_step_existing = True
                next_users.append(next_step.user_id.id)
                next_activity = next_step.name
                current_sequence = next_step.sequence
            elif next_step.type == 'department':
                # Алба хэлтэсийн шатлал үед
                if uid:
                    employee_id = employee_obj.search([('user_id', '=', uid)], limit=1)
                    if employee_id:
                        if employee_id.parent_id:
                            if employee_id.sudo().parent_id.user_id:
                                is_next_step_existing = True
                                next_users.append(employee_id.sudo().parent_id.user_id.id)
                                next_activity = next_step.name
                                current_sequence = next_step.sequence
                            else:
                                raise UserError(_('There is no user defined for this employee:  name=%s (id=%s)' % (employee_id.parent_id.name, employee_id.parent_id.id)))
                        else:
                            raise UserError(_('There is no manager defined for this user: name=%s (id=%s)' % (employee_id.name, employee_id.id)))
                    else:
                        raise UserError(_('There is no employee defined for this user.'))
                else:
                    raise UserError(_('There is no sender id defined!'))
            elif next_step.type == 'group':
                # Групп үед
                for group in group_obj.browse(next_step.group_id.id):
                    for user in group.users:
                        next_users.append(user.id)
                    is_next_step_existing = True
                    next_activity = next_step.name
                    current_sequence = next_step.sequence
            else:
                raise UserError(_('There is no approve step defined!'))
        else:
            current_sequence = check_sequence
        return is_next_step_existing, next_users, next_activity, current_sequence
        """Буцаах утгууд
            is_next_step_existing: Дараагийн алхамтай эсэх True or False,
            next_users: Дараагийн алхам илгээх хэрэглэгчидийн id-уудыг листээр буцаана. res.users,
            next_activity: Дараагийн алхамын утга, string,
            current_sequence Дараагийн алхамын дараалалыг буцаана. integer:
        """

    @api.multi
    def get_next_step(self, current_obj, uid, step_seq, step_type):
        """ 
            Ажлын урсгалын дараагийн алхамыг олж илгээх функц: self.env['workflow.config'].assign_to_next(...) функцтай үүргийн хувьд төстэй ч дараахи давуу талтай:
                Үүнд: * Хэрвээ (step_type == 'next') байгаад дараагийн алхам олдохгүй бол хамгийн сүүлийн алхмыг буцаана.
                      * Хэрвээ (step_type == 'back') байгаад өмнөх алхам олдохгүй бол хамгийн эхний алхмыг буцаана.
                      
            Функцийн Параметр:
                current_obj [OBJECT]:      Уг функцыг дуудсан идэвхтэй объектийг дамжуулна.
                uid [INTEGER]:             Нэвтэрсэн хэрэглэгчийн /res.users/ id-г дамжуулна. 
                step_seq [INTEGER]:        Шилжүүлэх алхамын дугаарыг дамжуулна.
                step_type [CHAR]:         'step_seq' алхмын ӨМНӨХ алхмаас буюу уг 'step_seq' алхам уруу хэрхэн шилжих талаархи утга байна. Үүнд
                                                Хойш нь нэг алхам шилжүүлэх   ----->   'next'
                                                Өмнөх алхамд шилжүүлэх        ----->   'back'.
        """
        
        step_obj = self.env['workflow.config.line']
        employee_obj = self.env['hr.employee']
        group_obj = self.env['res.groups']
        
        step = step_obj.search([('config_id', '=', self.id), ('sequence', '=', step_seq)], order='sequence', limit=1)
        
        # Ажлын урсгалын хамгийн эхний, сүүлийн болон нийт алхмыг олох
        self.env.cr.execute("""
            SELECT COUNT(*) AS total_step_count, MIN(sequence) as first_seq, MAX(sequence) as last_seq
            FROM workflow_config_line 
            WHERE config_id = %s
        """ % self.id)
        result = self.env.cr.dictfetchall()
        
        if not (result and len(result) > 0 and result[0]['total_step_count'] > 0):
            raise UserError(_("Please define workflow line for this: %s !!!") %self.name)
        
        total_step_count = result[0]['total_step_count']
        first_seq = result[0]['first_seq']
        last_seq = result[0]['last_seq']
        
        # Ажлын урсгалын нийт алхмаар гүйж дараагийн алхамын sequence-г олох хэсэг
        while total_step_count >= step_seq:
            # Ажлын урсгалын алхам дээрхи нөхцөлт илэрхийлэлийн утгыг шалгаж байна. 
            if step and not eval(step.expression, {'object': current_obj}):
                # Ажлын урсгалыг дараагийн алхамд шилжүүлэх үү, өмнөх алхамд шилжүүлэх үү гэдгийг тодорхойлж байна
                if step_type == 'next':
                    step_seq += 1 
                elif step_type == 'back':
                    step_seq -= 1
                step = step_obj.search([('config_id', '=', self.id), ('sequence', '=', step_seq)], order='sequence', limit=1)
            else:
                break
            
        if not step:
            step = step_obj.search([('config_id', '=', self.id)], order='sequence', limit=1)
            if not step:
                raise UserError(_("Please check workflow line: %s !!!") %self.name)
                
        # Алхам нь тухайн ажлын урсгалд байхгүй бол хамгийн сүүлийн болон эхний алхмуудаас харгалзахыг оноож ажлын урсгалын алхамыг шинэчлэх хэсэг
        if step_type == 'next':
            step_seq = last_seq if step_seq > last_seq else step_seq
        elif step_type == 'back':
            step_seq = first_seq if step_seq < first_seq else step_seq
        step = step_obj.search([('config_id', '=', self.id), ('sequence', '=', step_seq)], order='sequence', limit=1)
        
        next_user_ids = []
        if step.type == 'creator': # Үүсгэгч үед
            next_user_ids.append(current_obj.create_uid.sudo().id)
        elif step.type == 'fixed': # Тогсон хэрэглэгч үед
            next_user_ids.append(step.user_id.sudo().id)
        elif step.type == 'department': # Алба хэлтэсийн шатлал үед
            if uid:
                employee_id = employee_obj.search([('user_id', '=', uid), ('company_id', '=', self.company_id.id)], limit=1)
                if employee_id:
                    if employee_id.parent_id:
                        if employee_id.sudo().parent_id.user_id:
                            next_user_ids.append(employee_id.sudo().parent_id.user_id.sudo().id)
                        else:
                            raise UserError(_('There is no user defined for this employee:  name=%s (id=%s)' % (employee_id.parent_id.name, employee_id.parent_id.id)))
                    else:
                        raise UserError(_('There is no manager defined for this user: name=%s (id=%s)' % (employee_id.name, employee_id.id)))
                else:
                    raise UserError(_('There is no employee defined for this user.'))
            else:
                raise UserError(_('There is no sender id defined!'))
        elif step.type == 'group': # Групп үед
            for group in group_obj.browse(step.group_id.id):
                for user in group.users:
                    next_user_ids.append(user.sudo().id)
        else:
            raise UserError(_('There is no approve step defined!'))
        
        return step, next_user_ids
        """
            Буцаах утгууд:
                next_user_ids [LIST[INTEGER]]:     Дараагийн алхам илгээх хэрэглэгчидийн id-уудыг листээр буцаана. res.users,
                step [OBJECT]:                     Дараагийн алхмыг буцаана. 
        """
        
class WorkflowConfigLine(models.Model):
    """ Ажлын урсгалын тохиргооны мөр """

    _name = 'workflow.config.line'
    _description = 'All Workflows Configure Line'
    _order = 'sequence'

    STATE_SELECTION = [
        ('waiting', 'Waiting'),
        ('confirmed', 'Confirmed'),
        ('approved', 'Approved'),
        ('return', 'Return'),
        ('rejected', 'Rejected'),

    ]

    name = fields.Char(string='Name', size=128, required=True)
    config_id = fields.Many2one('workflow.config', string='Workflow Config', required=True, ondelete='cascade')
    sequence = fields.Integer(string='Sequence', required=True, default=1)
    type = fields.Selection([
        ('creator', 'Creator'),
        ('fixed', 'Fixed'),
        ('department', 'Department'),
        ('group', 'Group')], string='Type', required=True, default='fixed')
    user_id = fields.Many2one('res.users', string='User')
    group_id = fields.Many2one('res.groups', string='Group')
    expression = fields.Text(string='Expression', default='True')

    _sql_constraints = [
        ('sequence_nonzero', 'check(sequence > 0)',
         'Sequence must be greater than zero!')
    ]

    @api.onchange('type')
    def onchange_type(self):
        self.update({
            'user_id': False,
            'group_id': False
        })

    @api.multi
    def write(self, vals):
        if vals.get('type') == 'creator':
            vals.update({
                'user_id': False,
                'group_id': False,
            })
        if vals.get('type') == 'fixed':
            vals.update({
                'group_id': False,
            })
        if vals.get('type') == 'group':
            vals.update({
                'user_id': False,
            })
        if vals.get('type') == 'depart':
            vals.update({
                'user_id': False,
                'group_id': False,
            })

        return super(WorkflowConfigLine, self).write(vals)


class WorkflowHistory(models.Model):
    _name = 'workflow.history'
    _description = 'Workflow History'
    _order = 'sent_date'

    STATE_SELECTION = [('approved', 'Approved'),
                       ('rejected', 'Rejected'),
                       ('drafted', 'Drafted')]

    name = fields.Char(strig='Verification Step', required=True, readonly=True)
    user_ids = fields.Many2many('res.users', 'res_users_workflow_history_ref', 'history_id', 'user_id', string='Approves')
    user_id = fields.Many2one('res.users', string='Approved user', readonly=True)
    sent_date = fields.Datetime(string='Sent date', required=True, readonly=True)
    action_date = fields.Datetime(string='Approved date', readonly=True)
    action = fields.Selection(STATE_SELECTION, string='Action', readonly=True)
    line_sequence = fields.Integer(string='Workflow Step')
