# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': "Model Data Export",
    'version': '1.0',
    'depends': ['base'],
    'author': "Asterisk Technologies LLC",
    'category': 'Mongolian Modules',
    'description': """
        Model Data Export
    """,
    'data': [
            'security/ir.model.access.csv',
            'views/model_data_export_views.xml',
    ],
    'license': 'GPL-3',
    'installable': True,
    'auto_install': False,
}