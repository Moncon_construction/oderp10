# -*- coding: utf-8 -*-
from io import BytesIO
import base64
import time
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from operator import itemgetter
import xlrd
from odoo import api, fields, models, _
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.exceptions import UserError, ValidationError

class ModelDataExport(models.Model):
    _name = 'model.data.export' 
    _description = "Model Data Export"
    
    @api.multi
    def export_model_data(self):
        context = dict(self._context or {})
        models = self.env['ir.model'].browse(context.get('active_ids'))
        #create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        #create name
        report_name = _('Data Export')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)

        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('data_export'), form_title=file_name).create({})

        for model in models:
            rowx = 0
            model_name = model.model.replace('.','_')
            self._cr.execute("select * from information_schema.columns where table_name = '"  + model_name + "'")
            is_table = self._cr.dictfetchall()
            #Сонгосон model-ийн нэртэй өгөгдлийн сангийн хүснэгт байгаа эсэхийг шалгаж байна.
            if not is_table:
                raise UserError(_('There is no table with %s name') % model_name)
            else:
                self._cr.execute("SELECT * FROM " + model_name)
                datas = self._cr.dictfetchall()
                # create sheet
                sheet = book.add_worksheet(model_name)
                sheet.set_landscape()
                #sheet.set_page_view()
                sheet.set_paper(9)  # A4
                sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
                sheet.fit_to_pages(1, 0)
                sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})
                colx_number = 0
                sheet.set_default_row(15)
                #Тухайн өгөгдлийн сангийн хүснэгтээс баганын нэр болон баганын өгөгдлийн төрлийг авна.
                self._cr.execute("select column_name, data_type from information_schema.columns where table_name = '"  + model_name + "'")
                fields = self._cr.fetchall()
                for field in fields:
                    #Тухайн баганын өгөгдлийн төрөл нь тайланд хэвлэхэд тохиромжтой эсэхийг шалгана. Мөн илүү өгөгдлийг авахгүй.
                    if field[1] in ['integer', 'boolean', 'text', 'character varying', 'date', 'timestamp without time zone', 'float', 'monetary', 'selection'] and field[0] not in ['create_uid', 'write_uid', 'create_date', 'write_date']:
                        sheet.write(rowx, colx_number, field[0], format_title)
                        sheet.set_column(colx_number,colx_number+1, 20)
                        for data in datas:
                            rowx += 1
                            sheet.write(rowx, colx_number, data[field[0]], format_content_text)
                        colx_number += 1
                    rowx = 0
                rowx += 1
                
        book.close()

        # set file data
        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        
        return report_excel_output_obj.export_report()