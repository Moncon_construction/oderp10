# -*- coding: utf-8 -*-


{
    'name': 'Web Freeze Table Header',
    'version': '1.0',
    'author': 'Asterisk Technologies LLC',
    'website' : 'https://www.asterisk-tech.mn',
    'category': 'web',
    'sequence': 6,
    'summary': 'To freeze table header',
    'description': """
        To freeze table header
""",
    'depends': ['web', 'base'],
    'data': [
        'views/web_freeze_table_header_view.xml'
    ],
    'installable': True,
    'auto_install': False,
    'license': 'OEEL-1',
}
