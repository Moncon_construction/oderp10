# -*- coding: utf-8 -*-

from odoo.http import request
from odoo.addons.website.models.website import slug
from odoo.addons.website.controllers.main import QueryURL
from odoo import http
from odoo import _

class MyController(http.Controller):

    @http.route(['/check_freeze'], type='json', auth="public", methods=['POST'], website=True)
    def check_freeze(self):
        result = []
        return result