odoo.define('web_freeze_table_header', function (require) {
'use strict';

    var ListView = require('web.ListView');
    var ajax = require('web.ajax');

    ListView.include({
        load_list: function () {
            var self = this;
            return this._super.apply(this, arguments).done(function () {
                var form_field_length = self.$el.parents('.o_form_field').length;
                var form_table_responsive = self.$el.width();
                var scrollArea = $(".o_content")[0];
                function do_freeze () {
                	//o_list_view table table-condensed table-striped
                	//o_list_view table table-condensed table-striped o_list_view_grouped
                	var sheet_len = $('div.o_form_sheet').length;
                    if (sheet_len == 0) {
                        self.$el.find('table.o_list_view').each(function () {
                            $(this).stickyTableHeaders({scrollableArea: scrollArea, fixedOffset: 0.1, leftOffset:scrollArea, width:self.$el.width(), model:self.model});
                            var $div = $(this).parent();
                            var $thead = $div.find('thead.tableFloatingHeaderOriginal');
                            $div.scroll(function() {
                              var tb_leftPos = $div.scrollLeft();
                              $thead.animate({
                                    scrollLeft: tb_leftPos
                                }, 0);
                            });

                            $thead.scroll(function() {
                              var th_leftPos = $thead.scrollLeft();
                              $div.animate({
                                    scrollLeft: th_leftPos
                                }, 0);
                            });
                        });
                    }
                }
                do_freeze();
                $(window).unbind('resize', do_freeze).bind('resize', do_freeze);
            });
        },
    })

    ListView.Groups.include({
        render_groups: function () {
            var self = this;
            var placeholder = this._super.apply(this, arguments);
            var grouping_freezer = document.createElement("script");
            grouping_freezer.innerText = "$('.o_group_header').click(function () {"
                + " setTimeout(function () { $(window).resize(); }, 400); })";

            placeholder.appendChild(grouping_freezer);
            return placeholder;
        },
    });
});
