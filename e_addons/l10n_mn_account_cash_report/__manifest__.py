# -*- coding: utf-8 -*-
{
    'name': 'Accounting Cash Report',
    'summary': 'View and create reports',
    'category': 'Accounting',
    'author': "Asterisk Technologies LLC",
    'description': """
Accounting Reports
    """,
    'depends': ['account',
                'account_reports',
                'l10n_mn_account_bank_statement_report',
                'web_enterprise'
                ],
    'data': [
        'views/account_bank_statement_report_view.xml',
        'views/cash_report_view.xml',
        'views/report_account_cash_view.xml'
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True
}
