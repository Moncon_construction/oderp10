# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models
try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    # TODO saas-17: remove the try/except to directly import from misc
    import xlsxwriter
from datetime import datetime
import StringIO
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class AccountReportContextCommon(models.TransientModel):
    _inherit = "account.report.context.common"

    # Тайлангийн нэрээр уг тайлангийн моделийг буцаах функц
    # Дахин тодорхойлсон
    # Шалтгаан нь статикаар буцаадаг тул нэмсэн тайлангаа оруулж өгөх шаардлагатай болсон
    def _report_name_to_report_model(self):
        return {
            'financial_report': 'account.financial.html.report',
            'generic_tax_report': 'account.generic.tax.report',
            'followup_report': 'account.followup.report',
            'bank_reconciliation': 'account.bank.reconciliation.report',
            'general_ledger': 'account.general.ledger',
            'aged_receivable': 'account.aged.receivable',
            'aged_payable': 'account.aged.payable',
            'coa': 'account.coa.report',
            'l10n_be_partner_vat_listing': 'l10n.be.report.partner.vat.listing',
            'l10n_be_partner_vat_intra': 'l10n.be.report.partner.vat.intra',
            'partner_ledger': 'account.partner.ledger',
            'cash_report': 'account.cash.report'
        }

    # Тайлангийн моделийн нэрээр context моделийг буцаах функц
    # Дахин тодорхойлсон
    # Шалтгаан нь статикаар буцаадаг тул нэмсэн тайлангаа оруулж өгөх шаардлагатай болсон
    def _report_model_to_report_context(self):
        return {
            'account.financial.html.report': 'account.financial.html.report.context',
            'account.generic.tax.report': 'account.report.context.tax',
            'account.followup.report': 'account.report.context.followup',
            'account.bank.reconciliation.report': 'account.report.context.bank.rec',
            'account.general.ledger': 'account.context.general.ledger',
            'account.aged.receivable': 'account.context.aged.receivable',
            'account.aged.payable': 'account.context.aged.payable',
            'account.coa.report': 'account.context.coa',
            'l10n.be.report.partner.vat.listing': 'l10n.be.partner.vat.listing.context',
            'l10n.be.report.partner.vat.intra': 'l10n.be.partner.vat.intra.context',
            'account.partner.ledger': 'account.partner.ledger.context',
            'account.cash.report': 'account.cash.report.context'
        }

    # Дахин тодорхойлсон
    # Тайлангийн эксэл харагдац засав
    def get_xlsx(self, response):
        output = StringIO.StringIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        report_id = self.get_report_obj()
        sheet = workbook.add_worksheet(report_id.get_title())

        def_style = workbook.add_format({'font_name': 'Arial'})
        title_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2})
        level_0_style = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'pattern': 1, 'font_color': '#FFFFFF'})
        level_0_style_left = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'left': 2, 'pattern': 1,
             'font_color': '#FFFFFF'})
        level_0_style_right = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'right': 2, 'pattern': 1,
             'font_color': '#FFFFFF'})
        level_1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2})
        level_1_style_left = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'left': 2})
        level_1_style_right = workbook.add_format(
            {'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'right': 2})
        level_2_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'top': 2})
        level_2_style_left = workbook.add_format({'font_name': 'Arial', 'bold': True, 'top': 2, 'left': 2})
        level_2_style_right = workbook.add_format({'font_name': 'Arial', 'bold': True, 'top': 2, 'right': 2})
        level_3_style = def_style
        level_3_style_left = workbook.add_format({'font_name': 'Arial', 'left': 2})
        level_3_style_right = workbook.add_format({'font_name': 'Arial', 'right': 2})
        domain_style = workbook.add_format({'font_name': 'Arial', 'italic': True})
        domain_style_left = workbook.add_format({'font_name': 'Arial', 'italic': True, 'left': 2})
        domain_style_right = workbook.add_format({'font_name': 'Arial', 'italic': True, 'right': 2})
        upper_line_style = workbook.add_format({'font_name': 'Arial', 'top': 2})

        if self.get_report_obj().get_name() in ['aged_receivable', 'aged_payable']:
            format_title = {
                'font_name': 'Times New Roman',
                'font_size': 14,
                'bold': True,
                'align': 'center',
                'valign': 'vcenter',
                'border': 1,
                'text_wrap': 1,
            }
            format_title = workbook.add_format(format_title)
            # Баримтын толгой
            format_filter = workbook.add_format(ReportExcelCellStyles.format_filter)
            # Хүснэгтийн толгой
            format_group = {
                'font_name': 'Times New Roman',
                'font_size': 11,
                'bold': True,
                'align': 'center',
                'valign': 'vcenter',
                'border': 1,
                'bg_color': '#83CAFF',
                'text_wrap': 1,
            }
            format_group = workbook.add_format(format_group)
            format_content_float = workbook.add_format(ReportExcelCellStyles.format_content_float)
            format_content_left = workbook.add_format(ReportExcelCellStyles.format_content_left)
            format_group_float = workbook.add_format(ReportExcelCellStyles.format_group_float)
            format_group_right = workbook.add_format(ReportExcelCellStyles.format_group_right)

            format_content_float_color = workbook.add_format(ReportExcelCellStyles.format_content_float_color)
            format_content_text_wrap = {
                'font_name': 'Times New Roman',
                'font_size': 9,
                'align': 'center',
                'valign': 'vcenter',
                'border': 1,
                'text_wrap': 1,
            }
            format_content_text_wrap = workbook.add_format(format_content_text_wrap)
            y_offset = 0
            sheet.set_column(0, 0, 5)
            sheet.set_column(1, 9, 15)
            sheet.write(0, 0, u'Байгууллага: %s' % self.env.user.company_id.name, format_filter)
            y_offset += 2
            if self.get_report_obj().get_name() == 'aged_payable':
                sheet.merge_range(y_offset, 0, y_offset, 9, u'ӨГЛӨГИЙН НАСЖИЛТЫН ТАЙЛАН', format_title)
            else:
                sheet.merge_range(y_offset, 0, y_offset, 9, u'АВЛАГЫН НАСЖИЛТЫН ТАЙЛАН', format_title)
            y_offset += 2
            sheet.write(y_offset, 0, u'Хэвлэсэн огноо: %s' % (datetime.now().strftime('%Y-%m-%d')), format_filter)
            y_offset += 1

            columns = [u'№', u'Харилцагчийн регистр', u'Харилцагч']
            [columns.append(column.replace('<br/>', ' ').replace('&nbsp;', ' ')) for column in
             self.with_context(is_xls=True).get_columns_names()]
            if self.get_report_obj().get_name() == 'aged_payable':
                columns[3] = u'%s хүртлэх хэвийн хугацаандаа яваа өглөг' % columns[3][13:]
            else:
                columns[3] = u'%s хүртлэх хэвийн хугацаандаа яваа авлага' % columns[3][13:]

            sheet.set_row(y_offset, 50)
            x = 0
            for column in columns:
                sheet.write(y_offset, x, column, format_group)
                x += 1
            y_offset += 1
            seq = 1
            lines = report_id.with_context(no_format=True, print_mode=True).get_lines(self)

            for y in range(0, len(lines)):
                if lines[y].get('type') == 'partner_id':
                    partner_id = self.env['res.partner'].search([('id', '=', lines[y].get('id'))])
                    sheet.write(y_offset, 0, seq, format_content_text_wrap)
                    sheet.write(y_offset, 1, partner_id.register or '-  ', format_content_text_wrap)
                    sheet.write(y_offset, 2, partner_id.name, format_content_left)
                    for i in range(0, len(lines[y]['columns'])):
                        val = lines[y]['columns'][i] if lines[y]['columns'][i] else '-   '
                        sheet.write(y_offset, 3 + i, val, format_content_float)
                    seq += 1
                    y_offset += 1
                elif lines[y].get('level') == 0 and lines[y].get('name') == 'Total':
                    sheet.write(y_offset, 0, '', format_group_right)
                    sheet.write(y_offset, 1, '', format_group_right)
                    sheet.write(y_offset, 2, u'Нийт: ', format_group_right)
                    for i in range(0, len(lines[y]['columns'])):
                        val = lines[y]['columns'][i] if lines[y]['columns'][i] else '-   '
                        sheet.write(y_offset, 3 + i, val, format_group_float)
                    y_offset += 1
        elif self.get_report_obj().get_name() == 'cash_report':
            format_title = {
                'font_name': 'Times New Roman',
                'font_size': 16,
                'bold': True,
                'align': 'center',
                'valign': 'vcenter',
                'border': 0,
                'text_wrap': 1,
            }
            format_title_company = {
                'font_name': 'Times New Roman',
                'font_size': 10,
                'bold': True,
                'align': 'left',
                'valign': 'vcenter',
                'border': 0,
                'text_wrap': 1,
            }
            format_title_date = {
                'font_name': 'Calibri',
                'font_size': 8,
                'bold': False,
                'align': 'right',
                'valign': 'vcenter',
                'border': 0,
                'text_wrap': 1,
            }
            format_total_amount = {
                'font_name': 'Times New Roman',
                'font_size': 9,
                'bold': True,
                'align': 'right',
                'valign': 'vcenter',
                'border': 1,
                'text_wrap': 1,
            }
            format_total_amount = workbook.add_format(format_total_amount)
            format_title_company = workbook.add_format(format_title_company)
            format_title_date = workbook.add_format(format_title_date)
            format_title = workbook.add_format(format_title)
            # Баримтын толгой
            format_account_header = {
                'font_name': 'Times New Roman',
                'font_size': 9,
                'bold': True,
                'align': 'left',
                'valign': 'vcenter',
                'bg_color': '#83CAFF',
                'border': 1,
                'text_wrap': 1,
            }
            format_account_header = workbook.add_format(format_account_header)
            # Хүснэгтийн толгой
            format_group = {
                'font_name': 'Times New Roman',
                'font_size': 12,
                'bold': True,
                'align': 'center',
                'valign': 'vcenter',
                'border': 1,
                'bg_color': '#83CAFF',
                'text_wrap': 1,
            }
            format_group = workbook.add_format(format_group)
            format_move_line = {
                'font_name': 'Times New Roman',
                'font_size': 9,
                'align': 'left',
                'valign': 'vcenter',
                'border': 1,
                'text_wrap': 1,
            }
            format_move_line = workbook.add_format(format_move_line)
            format_move_line_seq = {
                'font_name': 'Times New Roman',
                'font_size': 9,
                'align': 'center',
                'valign': 'vcenter',
                'border': 1,
                'text_wrap': 1,
            }
            format_move_line_seq = workbook.add_format(format_move_line_seq)
            format_group_float = ReportExcelCellStyles.format_group_float
            format_group_float['bold'] = False
            format_group_float['bg_color'] = '#FFFFFF'
            format_group_float = workbook.add_format(format_group_float)
            format_group_float_bg_blue = ReportExcelCellStyles.format_group_float
            format_group_float_bg_blue['bg_color'] = '#83CAFF'
            format_group_float_bg_blue['bold'] = True
            format_group_float_bg_blue = workbook.add_format(format_group_float_bg_blue)
            format_group_float_bold = ReportExcelCellStyles.format_group_float
            format_group_float_bold['bold'] = True
            format_group_float_bold['bg_color'] = '#FFFFFF'
            format_group_float_bold = workbook.add_format(format_group_float_bold)
            y_offset = 1
            sheet.set_column(0, 0, 5)
            sheet.set_column(1, 9, 15)
            sheet.merge_range(y_offset, 0, y_offset, 2, u'Компани: %s' % self.env.user.company_id.name, format_title_company)
            y_offset += 1
            sheet.merge_range(y_offset, 0, y_offset, 9, u'Мөнгөн хөрөнгийн тайлан', format_title)
            y_offset += 1
            sheet.merge_range(y_offset, 6, y_offset, 9, u'Тайлант хугацаа: %s' % (datetime.now().strftime('%Y-%m-%d')), format_title_date)
            y_offset += 2
            columns = [u'№', u'Огноо', u'Баримтын №', u'Харьцсан данс', u'Харилцагч', u'Гүйлгээний утга', u'Валют',
                       u'Орлого', u'Зарлага', u'Үлдэгдэл']
            # [columns.append(column.replace('<br/>', ' ').replace('&nbsp;', ' ')) for column in
            #  self.with_context(is_xls=True).get_columns_names()]
            sheet.set_row(y_offset, 50)
            x = 0
            debit = 0
            credit = 0
            for column in columns:
                sheet.write(y_offset, x, column, format_group)
                x += 1
            y_offset += 1
            seq = 1
            lines = report_id.with_context(no_format=True, print_mode=True).get_lines(self)
            for y in range(0, len(lines)):
                if lines[y].get('type') == 'line' and lines[y].get('level') == 2:
                    seq = 1
                    sheet.merge_range(y_offset, 0, y_offset, 4, '')
                    if lines[y]['unfolded']:
                        sheet.write(y_offset, 0, 'Данс: ' + lines[y]['name'], format_account_header)
                        sheet.write(y_offset, 5, 'Эхний үлдэгдэл', format_account_header)
                        for i in range(0, len(lines[y]['columns'])):
                            val = lines[y]['columns'][i] if lines[y]['columns'][i] else '-   '
                            sheet.write(y_offset, 6 + i, val, format_account_header)
                    else:
                        sheet.write(y_offset, 0, lines[y]['name'], format_account_header)
                        sheet.write(y_offset, 5, 'Эхний үлдэгдэл', format_account_header)
                        for i in range(0, len(lines[y]['columns'])):
                            val = lines[y]['columns'][i] if lines[y]['columns'][i] else '-   '
                            sheet.write(y_offset, 6 + i, val, format_group_float_bg_blue)
                    y_offset += 1
                if lines[y].get('type') == 'initial_balance' and lines[y].get('level') == 1:
                    y_offset -= 1
                    for i in range(0, len(lines[y]['columns'])):
                        val = lines[y]['columns'][i] if lines[y]['columns'][i] else '-   '
                        if i > 4:
                            sheet.write(y_offset, 1 + i, val, format_group_float_bg_blue)
                    y_offset += 1
                if lines[y].get('type') == 'move_line_id' and lines[y].get('level') == 1:
                    for i in range(0, len(lines[y]['columns'])):
                        sheet.write(y_offset, 0, seq, format_move_line_seq)
                        val = lines[y]['columns'][i] if lines[y]['columns'][i] else '-   '
                        if i == 6 and lines[y]['columns'][i]:
                            debit += 1
                        elif i == 6:
                            credit += 1
                        if i > 4:
                            sheet.write(y_offset, 1 + i, val, format_group_float)
                        else:
                            sheet.write(y_offset, 1 + i, val, format_move_line)
                    seq += 1
                    y_offset += 1
                if lines[y].get('type') == 'o_account_reports_domain_total':
                    for i in range(0, len(lines[y]['columns'])):
                        val = lines[y]['columns'][i] if lines[y]['columns'][i] else '-   '
                        sheet.write(y_offset, 1 + i, val, format_group_float_bold)
                    sheet.merge_range(y_offset, 0, y_offset, 5, 'Дансны дүн', format_total_amount)
                    seq += 1
                    y_offset += 1
            y_offset += 1
            format_title_company = {
                'font_name': 'Times New Roman',
                'font_size': 10,
                'bold': True,
                'align': 'center',
                'valign': 'vcenter',
                'border': 0,
                'text_wrap': 1,
            }
            format_title_company = workbook.add_format(format_title_company)
            sheet.merge_range(y_offset, 7, y_offset, 9, 'Орлогын гүйлгээ %s'%debit, format_title_company)
            y_offset += 1
            sheet.merge_range(y_offset, 7, y_offset, 9, 'Зарлагын гүйлгээ %s'%credit, format_title_company)
            y_offset += 1
            sheet.merge_range(y_offset, 8, y_offset, 9, 'Нийт %s' % str(debit + credit), format_title_company)
            y_offset += 2
            format_title_company = {'font_name': 'Times New Roman', 'font_size': 10, 'bold': True, 'align': 'left',
                                    'valign': 'vcenter', 'border': 0, 'text_wrap': 1}
            format_title_company = workbook.add_format(format_title_company)
            sheet.merge_range(y_offset, 3, y_offset, 8, 'Бэлэн мөнгөний нярав: ........................................... /                          /', format_title_company)
            y_offset += 1
            sheet.merge_range(y_offset, 3, y_offset, 8, 'Нягтлан бодогч: ........................................... /                          /', format_title_company)


        else:
            sheet.set_column(0, 0, 15)  # Set the first column width to 15
            sheet.write(0, 0, '', title_style)
            y_offset = 0
            if self.get_report_obj().get_name() == 'coa' and self.get_special_date_line_names():
                sheet.write(y_offset, 0, '', title_style)
                sheet.write(y_offset, 1, '', title_style)
                x = 2
                for column in self.with_context(is_xls=True).get_special_date_line_names():
                    sheet.write(y_offset, x, column, title_style)
                    sheet.write(y_offset, x + 1, '', title_style)
                    x += 2
                sheet.write(y_offset, x, '', title_style)
                y_offset += 1

            x = 1
            for column in self.with_context(is_xls=True).get_columns_names():
                sheet.write(y_offset, x, column.replace('<br/>', ' ').replace('&nbsp;', ' '), title_style)
                x += 1
            y_offset += 1

            lines = report_id.with_context(no_format=True, print_mode=True).get_lines(self)

            if lines:
                max_width = max([len(l['columns']) for l in lines])

            for y in range(0, len(lines)):
                if lines[y].get('level') == 0 and lines[y].get('type') == 'line':
                    for x in range(0, len(lines[y]['columns']) + 1):
                        sheet.write(y + y_offset, x, None, upper_line_style)
                    y_offset += 1
                    style_left = level_0_style_left
                    style_right = level_0_style_right
                    style = level_0_style
                elif lines[y].get('level') == 1 and lines[y].get('type') == 'line':
                    for x in range(0, len(lines[y]['columns']) + 1):
                        sheet.write(y + y_offset, x, None, upper_line_style)
                    y_offset += 1
                    style_left = level_1_style_left
                    style_right = level_1_style_right
                    style = level_1_style
                elif lines[y].get('level') == 2:
                    style_left = level_2_style_left
                    style_right = level_2_style_right
                    style = level_2_style
                elif lines[y].get('level') == 3:
                    style_left = level_3_style_left
                    style_right = level_3_style_right
                    style = level_3_style
                elif lines[y].get('type') != 'line':
                    style_left = domain_style_left
                    style_right = domain_style_right
                    style = domain_style
                else:
                    style = def_style
                    style_left = def_style
                    style_right = def_style
                sheet.write(y + y_offset, 0, lines[y]['name'], style_left)
                for x in xrange(1, max_width - len(lines[y]['columns']) + 1):
                    sheet.write(y + y_offset, x, None, style)
                for x in xrange(1, len(lines[y]['columns']) + 1):
                    if isinstance(lines[y]['columns'][x - 1], tuple):
                        lines[y]['columns'][x - 1] = lines[y]['columns'][x - 1][0]
                    if x < len(lines[y]['columns']):
                        sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, lines[y]['columns'][x - 1], style)
                    else:
                        sheet.write(y + y_offset, x + lines[y].get('colspan', 1) - 1, lines[y]['columns'][x - 1],
                                    style_right)
                if lines[y]['type'] == 'total' or lines[y].get('level') == 0:
                    for x in xrange(0, len(lines[0]['columns']) + 1):
                        sheet.write(y + 1 + y_offset, x, None, upper_line_style)
                    y_offset += 1
            if lines:
                for x in xrange(0, max_width + 1):
                    sheet.write(len(lines) + y_offset, x, None, upper_line_style)

        workbook.close()
        output.seek(0)
        response.stream.write(output.read())
        output.close()

