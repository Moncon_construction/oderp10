from odoo import models, api, _


class ReportAccountCash(models.TransientModel):
    _inherit = 'report.account.cash'

    @api.multi
    def preview(self):
        context = self.env['account.cash.report.context'].search([], order='id DESC', limit=1)
        if context:
            context.date_from = self.date_from
            context.date_to = self.date_to
            context.all_entries = True if self.include_draft_statement else False
            if self.journal_ids:
                context.write({
                    'journal_ids': [(5, _, _)]
                })
                context.write({
                    'journal_ids': [(4, self.journal_ids.ids)]
                })
            else:
                context.write({
                    'journal_ids': [(5, _, _)]
                })
        res = {
            'type': 'ir.actions.client',
            'name': _('Cashflow Statement'),
            'tag': 'account_report_generic',
            'context': {'url': '/l10n_mn_account_cash_report/output_format/cash_report/1', 'model': 'account.cash.report'}
        }
        return res