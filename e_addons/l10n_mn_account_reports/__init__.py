# -*- coding: utf-8 -*-
import models

def init_save_base_data(cr):
    # account_reports модулын data файлын үндсэн өгөгдлүүд account_reports модулыг updgrade хийхэд
    # устаад байсан тул  account_reports-с удамшсан l10n_mn_account_reports модульд дараахи query-г ажлуулав.
    cr.execute("""
        UPDATE ir_model_data SET noupdate = True 
            WHERE module = 'account_reports' AND model IN ('account.report.type', 'account.financial.html.report', 'account.financial.html.report.line');
    """)
    return {}
