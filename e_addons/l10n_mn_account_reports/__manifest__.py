# -*- coding: utf-8 -*-
{
    'name': 'Mongolian Accounting Reports',
    'version': '1.1',
    'depends': ['account_reports',
                'l10n_mn_account_base_report'],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian Modules',
    'description': """

    """,
    'data': [
        'data/account_partner_ledger_report_data.xml',
        'views/account_ledger_report_view.xml',
        'views/report_account_transaction_balance_wizard.xml'
    ],
    'installable': True,
    'auto_install': True,
    'pre_init_hook': 'init_save_base_data',
}
