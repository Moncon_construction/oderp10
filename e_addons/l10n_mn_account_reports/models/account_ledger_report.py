# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2019 Asterisk Technologies LLC Co.,ltd (<http://www.asterisk-tech.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################
from odoo.exceptions import UserError

from odoo import fields, models, api
from odoo import _
import time
from datetime import datetime, timedelta
from io import BytesIO
import base64
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
import math
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT

class PartnerLedgerReport(models.TransientModel):
    """
         Partner Ledger Report Wizard
    """
    _name = "partner.ledger.report"
    _description = "Partner Ledger Report"

    account_type = fields.Selection([('receivable', 'Receivable'), ('payable', 'Payable'), ('receivable_payable', 'Receivable & Payable')], string='Account Type', required=True, default='receivable_payable')
    date_from = fields.Date(string='Date start', required=True, help="")
    date_to = fields.Date(string='Date end', required=True, help="")
    partners = fields.Many2many('res.partner', 'partner_ledger_to_res_partner_rel', 'report_id', 'partner_id', string='Partners')
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)

    @api.onchange('date_from', 'date_to')
    def validate_date_range_start(self):
        if self.date_from and self.date_to:
            if self.date_from > self.date_to:
                raise UserError('Invalid date range, Please enter a date start less than or equal to date end')

    def show(self):
        # Зөвхөн тухайн шүүлтүүрийн огноонд гүйлгээ хийсэн харилцагчдыг шүүдэг байсныг өөрчилж
        # Тухайн шүүлтүүрийн огноонд гүйлгээ хийсэн болон account_move_line-н debit, credit-тэй буюу ямар нэг тооцоотой хэрэглэгчдийг мөн шүүж гаргадаг болгов.  
        all_partners = self.partners
         
        if not self.partners:
            self._cr.execute("""
                SELECT DISTINCT(partner_id) AS id FROM account_move_line WHERE partner_id IS NOT NULL
            """)
            partner_ids = self._cr.dictfetchall()
            if partner_ids and len(partner_ids) > 0:
                ids = [partner_id['id'] for partner_id in partner_ids]
                all_partners = self.env['res.partner'].browse(ids) 
                
        ctx = self.env.context.copy()
        ctx.update({
            'url': '/account_reports/output_format/partner_ledger/1', 
            'model': 'account.partner.ledger', 
            'date_from': self.date_from,
            'date_to': self.date_to,
            'partners': [(6, 0, all_partners.ids)],
            'company_id': self.env.user.company_id.id, # Олон компаны дүрэмтэй үед буруу ажиллаж байсан тул зөвхөн нэвтэрсэн хэрэглэгчийн идэвхтэй компанийг дамжуулав
            'account_type': self.account_type
        })
        return {
            'name': _("Partner Ledger"),
            'type': 'ir.actions.client',
            'tag': 'account_ledger_report',
            'context': ctx,
        }
        
        
class ReportPartnerLedger(models.AbstractModel):
    _inherit = "account.partner.ledger"

    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
        
    @api.model
    def get_lines(self, context_id, line_id=None):
        if type(context_id) == int:
            context_id = self.env['account.partner.ledger.context'].search([['id', '=', context_id]])
        new_context = dict(self.env.context)
        account_types = []
        if 'receivable' in context_id.account_type:
            account_types.append('receivable')
        if 'payable' in context_id.account_type:
            account_types.append('payable')
        # add partners to context !!!!!
        new_context.update({
            'date_from': context_id.date_from,
            'date_to': context_id.date_to,
            'state': context_id.all_entries and 'all' or 'posted',
            'cash_basis': context_id.cash_basis,
            'context_id': context_id,
            'company_ids': [self.env.user.company_id.id],  # Олон компаны дүрэмтэй үед буруу ажиллаж байсан тул зөвхөн нэвтэрсэн хэрэглэгчийн идэвхтэй компанийг дамжуулав
            'partners': context_id.partners,
            'account_types': account_types
        })
        # Тайлангын нэг мөр дээр дармагц уг get_lines функцыг дахин дууддаг тул context_id-н partners-аа цэвэрлэх хэрэгтэй. 
        # Зөвхөн уг тайланг эхний удаа нээхэд partners-г дамжуулахад хангалттай.
        context_id.partners = [(5, _, _)]
        return self.with_context(new_context)._lines(line_id)  
        
    # add partners to context !!!!!
    def do_query(self, line_id, partners):
        # add partners to context !!!!!
        ids = ','.join(str(id) for id in partners.ids) 
        if line_id:
            if len(ids) > 0:
                ids += "," + str(line_id)
            else:
                ids = str(line_id)
        
        select = ',COALESCE(SUM(\"account_move_line\".debit-\"account_move_line\".credit), 0),SUM(\"account_move_line\".debit),SUM(\"account_move_line\".credit)'
        if self.env.context.get('cash_basis'):
            select = select.replace('debit', 'debit_cash_basis').replace('credit', 'credit_cash_basis')
        sql = "SELECT \"account_move_line\".partner_id%s FROM %s WHERE %s %s AND \"account_move_line\".partner_id IS NOT NULL GROUP BY \"account_move_line\".partner_id"
        tables, where_clause, where_params = self.env['account.move.line']._query_get([('account_id.internal_type', 'in', self.env.context['account_types'])])
        
        # add partners to context !!!!!
        line_clause = ' AND \"account_move_line\".partner_id in (' + str(ids) + ")" if len(ids) > 0 else ''
        
        query = sql % (select, tables, where_clause, line_clause)
        self.env.cr.execute(query, where_params)
        results = self.env.cr.fetchall()
        results = dict([(k[0], {'balance': k[1], 'debit': k[2], 'credit': k[3]}) for k in results])
        return results
    
    def get_results(self, is_initial, date_from, date_to, acc_type_qry, partner_qry, company_qry, move_state_qry):
        # Тухайн харилцагч дээр бүртгэгдсэн date_from-с date_to хугацаан дахь журналын мөрүүдийг буцаах функц.
        check_case = ""
        if is_initial:
            # Хэрвээ эхний үлдэгдлийг авах бол эхний огнооноос өмнөх мөрүүдийг шүүнэ
            check_case = """CASE WHEN "account_move_line"."date" <= '%s'""" %(date_from.strftime(DEFAULT_SERVER_DATE_FORMAT)) 
        else:
            # Эхний үлдэгдэл биш бол тухайн шүүлтүүрийн огноон дахь мөрүүдийг шүүнэ
            check_case = """CASE WHEN "account_move_line"."date" >= '%s' 
                            and "account_move_line"."date" < '%s'""" %(date_from.strftime(DEFAULT_SERVER_DATE_FORMAT), date_to.strftime(DEFAULT_SERVER_DATE_FORMAT)) 
        
        res_query = """
            SELECT "aml".partner_id,
                COALESCE(SUM("aml".debit-"aml".credit), 0),
                SUM("aml".debit),
                SUM("aml".credit) 
            FROM
            (
            SELECT                                      
                "account_move_line".partner_id,
                %s
                THEN 
                    "account_move_line".debit
                ELSE 0 END AS "debit",
                %s
                THEN 
                    "account_move_line".credit
                ELSE 0 END AS "credit"
            FROM "account_move" as "account_move_line__move_id","account_move_line", "account_account" as "acc"
            WHERE ("account_move_line"."move_id"="account_move_line__move_id"."id") AND "acc"."id" = "account_move_line"."account_id"
            %s
            %s
            %s
            %s
            AND  "account_move_line".partner_id IS NOT NULL 
            ) "aml"
            GROUP BY "aml".partner_id
        """ %(check_case, check_case,
              acc_type_qry, partner_qry, company_qry, move_state_qry)
          
        self._cr.execute(res_query)
        results = self._cr.fetchall()
        results = dict([(k[0], {'balance': k[1], 'debit': k[2], 'credit': k[3]}) for k in results])
            
        return results
    
    # add partners to context !!!!!
    def group_by_partner_id(self, line_id, context_partners):
        partners = {}
        # add partners to context !!!!!
        context = self.env.context
#         results = self.do_query(line_id, context_partners)
        results = False
        initial_bal_results = False
        initial_bal_date_from = datetime.strptime(context['date_from_aml'], DEFAULT_SERVER_DATE_FORMAT) + timedelta(days=-1)
        date_to = datetime.strptime(context['date_to'], DEFAULT_SERVER_DATE_FORMAT) + timedelta(days=1)
        date_from = datetime.strptime(context['date_from_aml'], DEFAULT_SERVER_DATE_FORMAT) 
        
        # Тайлангийн тухайн нэг мөр дэхь харилцагч түүний эхний үлдэгдэл болон шүүлтүүрийн хугацаанд хийсэн нийт журналын мөрүүдийг 
        # авахад доорхи шалгалтыг ашиглана. core функцаар нь дуудахад dynamic-р бичигдэж буй query нь буруу ажиллаж байсан тул manual-р query бичив.
        if context_partners:
            partner_ids = [partner.id for partner in context_partners]
            partner_qry = " AND account_move_line.partner_id IN (" + str(partner_ids)[1:len(str(partner_ids))-1] + ") " if str(partner_ids) != "[]" else ""
            acc_type_qry = " AND acc.internal_type IN (" + str(context['account_types'])[1:len(str(context['account_types']))-1] + ") " if 'account_types' in context.keys() and str(context['account_types']) != "[]" else ""
            company_qry = " AND account_move_line.company_id IN (" + str(context['company_ids'])[1:len(str(context['company_ids']))-1] + ") " if 'company_ids' in context.keys() and str(context['company_ids']) != "[]" else ""
            move_state_qry = """ AND ("account_move_line__move_id"."state" = 'posted')""" if 'state' in context.keys() and context['state'] == 'posted' else ""
            # Сонгосон харилцагчын эхний үлдэгдлийг олж аваад dictionary бүхий жагсаалтад хадгалж байна.
            initial_bal_results = self.get_results(True, initial_bal_date_from, date_to, acc_type_qry, partner_qry, company_qry, move_state_qry)
            # Сонгосон харилцагчын шүүлтүүрийн огноон дахь журналын мөрүүд, эцсийн үлдэгдлийг dictionary бүхий жагсаалтад хадгалж байна.
            results = self.get_results(False, date_from, date_to, acc_type_qry, partner_qry, company_qry, move_state_qry)
        else:
            initial_bal_results = self.with_context(is_initial_line=True, date_to=initial_bal_date_from.strftime(DEFAULT_SERVER_DATE_FORMAT)).do_query(line_id, context_partners)
            results = self.do_query(line_id, context_partners)
            
        base_domain = [('date', '<=', context['date_to']), ('company_id', 'in', context['company_ids']), ('account_id.internal_type', 'in', context['account_types'])]
        if context['date_from_aml']:
            base_domain.append(('date', '>=', context['date_from_aml']))
        if context['state'] == 'posted':
            base_domain.append(('move_id.state', '=', 'posted'))
        
        # Доорхи шалгалтаар дээр бэлдсэн query-нээс авсан мэдээллээ partner бүрээр group-лэж dictionary бэлдэж байна.
        if initial_bal_results and ((not (results and results.items() and len(results.items()) > 0)) or (results and initial_bal_results and len(initial_bal_results) > len(results))):
            for partner_id, result in initial_bal_results.items():
                have_not_aml = True
                all_result = result
                domain = list(base_domain)  # copying the base domain
                domain.append(('partner_id', '=', partner_id))
                lines = self.env['account.move.line'].search(domain, order='date')
                
                for res_partner_id, result1 in results.items():
                    if res_partner_id == partner_id:
                        if not (result1['balance'] == result1['debit'] == result1['credit'] == 0):
                            have_not_aml = False
                        all_result = result1
                        break
                    
                if have_not_aml and len(lines) == 0 and result['balance'] == result['debit'] == result['credit'] == 0:
                    continue
                
                partner = self.env['res.partner'].browse(partner_id)
                partners[partner] = all_result
                partners[partner]['lines'] = lines
                partners[partner]['initial_bal'] = initial_bal_results.get(partner.id, {'balance': 0, 'debit': 0, 'credit': 0})
        else:
            for partner_id, result in results.items():
                domain = list(base_domain)  # copying the base domain
                domain.append(('partner_id', '=', partner_id))
                partner = self.env['res.partner'].browse(partner_id)
                partners[partner] = result
                partners[partner]['initial_bal'] = initial_bal_results.get(partner.id, {'balance': 0, 'debit': 0, 'credit': 0}) if initial_bal_results else {'balance': 0, 'debit': 0, 'credit': 0}
                partners[partner]['lines'] = self.env['account.move.line'].search(domain, order='date')
                
        return partners
    
    @api.model
    def _lines(self, line_id=None):
        lines = []
        context = self.env.context
        if context.get('company_id'):
            if type(context.get('company_id')) is int:
                company_id = self.env['res.company'].browse(context.get('company_id'))
            else:
                company_id = context.get('company_id')
        else:
            company_id = self.env.user.company_id
        # add partners to context !!!!!
        grouped_partners = self.with_context(date_from_aml=context['date_from'], date_from=context['date_from'] and company_id.compute_fiscalyear_dates(datetime.strptime(context['date_from'], DEFAULT_SERVER_DATE_FORMAT))['date_from'] or None).group_by_partner_id(line_id, context['partners'])  # Aml go back to the beginning of the user chosen range but the amount on the partner line should go back to either the beginning of the fy or the beginning of times depending on the partner
        sorted_partners = sorted(grouped_partners, key=lambda p: p.name)
        unfold_all = context.get('print_mode') and not context['context_id']['unfolded_partners']

        for partner in sorted_partners:
            debit = grouped_partners[partner]['debit']
            credit = grouped_partners[partner]['credit']
            balance = grouped_partners[partner]['balance']
            
            unfold_partners = context['context_id'].with_context(active_test=False)['unfolded_partners']
            domain_lines = []
            amls = amls_all = grouped_partners[partner]['lines']
            too_many = False
            if len(amls) > 80 and not context.get('print_mode'):
                amls = amls[-80:]
                too_many = True
                
            # Эхний үлдэгдлийн мөрийн багануудын утгыг авч байна.            /debit     credit    balance/
            initial_debit = grouped_partners[partner]['initial_bal']['debit']
            initial_credit = grouped_partners[partner]['initial_bal']['credit']
            initial_balance = grouped_partners[partner]['initial_bal']['balance']
            # Эхний үлдэгдлийн мөрөөс бусад тайлангийн үндсэн мөрүүдэд 'Эхний үлдэгдэл' баганын утга онооход ашиглах талбарын анхны утга нь
            # Тухайн тайлангийн эхний үлдэгдлийн мөрийн 'Эцсийн үлдэгдэл' баганын утга байна.
            progress = initial_balance
            
            for line in amls:
                if self.env.context['cash_basis']:
                    line_debit = line.debit_cash_basis
                    line_credit = line.credit_cash_basis
                else:
                    line_debit = line.debit
                    line_credit = line.credit
                progress = progress + line_debit - line_credit
#                 balance = progress
                
                if partner in unfold_partners or unfold_all:
                    name = []
                    name = '-'.join(
                        (line.move_id.name not in ['', '/'] and [line.move_id.name] or []) +
                        (line.ref not in ['', '/', False] and [line.ref] or []) +
                        (line.name not in ['', '/'] and [line.name] or [])
                    )
                    if len(name) > 35 and not self.env.context.get('no_format'):
                        name = name[:32] + "..."
                        
                    domain_lines.append({
                        'id': line.id,
                        'type': 'move_line_id',
                        'move_id': line.move_id.id,
                        'action': line.get_model_id_and_name(),
                        'name': line.date,
                        'footnotes': self.env.context['context_id']._get_footnotes('move_line_id', line.id),
                        'columns': [line.journal_id.code, line.account_id.code, name, line.full_reconcile_id.name,
                                    line_debit != 0 and self._format(line_debit) or '',
                                    line_credit != 0 and self._format(line_credit) or '',
                                    self._format(progress)],
                        'level': 1,
                    })
                
            if partner in unfold_partners or unfold_all:
                domain_lines[:0] = [{
                    'id': partner.id,
                    'type': 'initial_balance',
                    'name': _('Initial Balance'),
                    'footnotes': self.env.context['context_id']._get_footnotes('initial_balance', partner.id),
                    'columns': ['', '', '', '', self._format(initial_debit), self._format(initial_credit), self._format(initial_balance)],
                    'level': 1,
                }]
                domain_lines.append({
                    'id': partner.id,
                    'type': 'o_account_reports_domain_total',
                    'name': _('Total') + ' ' + (partner.name or ''),
                    'footnotes': self.env.context['context_id']._get_footnotes('o_account_reports_domain_total', partner.id),
                    'columns': ['', '', '', '', self._format(debit), self._format(credit), self._format(balance+initial_balance)],
                    'level': 1,
                })
                if too_many:
                    domain_lines.append({
                        'id': partner.id,
                        'domain': "[('id', 'in', %s)]" % amls_all.ids,
                        'type': 'too_many_partners',
                        'name': _('There are more than 80 items in this list, click here to see all of them'),
                        'footnotes': {},
                        'colspan': 8,
                        'columns': [],
                        'level': 3,
                    })
                    
            domain_lines[:0] = [{
                'id': partner.id,
                'type': 'line',
                'name': partner.name,
                'footnotes': self.env.context['context_id']._get_footnotes('line', partner.id),
                'columns': [self._format(debit), self._format(credit), self._format(balance)],
                'level': 2,
                'unfoldable': True,
                'unfolded': partner in context['context_id']['unfolded_partners'] or unfold_all,
                'colspan': 5,
            }]
            lines += domain_lines
            
        return lines
    
class ReportPartnerLedgerContext(models.TransientModel):
    _inherit = "account.partner.ledger.context"

    partners = fields.Many2many('res.partner', 'partner_ledger_filter_to_partners', string='Partners')

    def get_columns_names(self):
        return [_('JRNL'), _('Account'), _('Ref'), _('Matching Number'), _('Debit'), _('Credit'), _('Balance')]

    @api.multi
    def get_columns_types(self):
        return ["text", "text", "text", "text", "number", "number", "number"]
        
    @api.multi
    def get_html(self, given_context=None):
        if given_context is None:
            given_context = {}
        context_obj = self.env['account.report.context.followup']
        report_obj = self.env['account.followup.report']
        reports = []
        emails_not_sent = context_obj.browse()
        processed_partners = False  # The partners that have been processed and for who the context should be deleted
        if 'partner_skipped' in given_context:
            self.skip_partner(self.env['res.partner'].browse(int(given_context['partner_skipped'])))
        partners = self._get_html_get_partners()
        if 'partner_filter' in given_context:
            self.write({'partner_filter': given_context['partner_filter']})
        if 'partner_done' in given_context and 'partner_filter' not in given_context:
            try:
                self.write({'skipped_partners_ids': [(4, int(given_context['partner_done']))]})
            except ValueError:
                pass
            [partners, emails_not_sent, processed_partners] = self._get_html_partner_done(given_context, partners)
        if self.valuemax != self.valuenow + len(partners):
            self.write({'valuemax': self.valuenow + len(partners)})
        if self.partner_filter == 'all':
            partners = self.env['res.partner'].get_partners_in_need_of_action(overdue_only=True)
        partners = partners.sorted(key=lambda x: x.name)
        for partner in partners[((given_context['page'] - 1) * self.PAGER_SIZE):(given_context['page'] * self.PAGER_SIZE)]:
            context_id = context_obj.search([('partner_id', '=', partner.id), ('create_uid', '=', self.env.uid)], limit=1)
            if not context_id:
                context_id = self._get_html_create_context(partner)
            lines = report_obj.with_context(lang=partner.lang).get_lines(context_id)
            reports.append({
                'context': context_id.with_context(lang=partner.lang),
                'lines': lines,
            })
        rcontext = self._get_html_build_rcontext(reports, emails_not_sent, given_context)
        res = self.env['ir.model.data'].xmlid_to_object('account_reports.report_followup_all').render(rcontext)
        if processed_partners:
            self.env['account.report.context.followup'].search([('partner_id', 'in', processed_partners.ids)]).unlink()
        return res
