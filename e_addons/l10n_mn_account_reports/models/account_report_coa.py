# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import datetime


class ReportAccountCoa(models.AbstractModel):
    _inherit = "account.coa.report"

    # Тайлангийн мөрүүдийг буцаах функц
    # Дахин тодорхойлсон
    # Класс болгоны дор нийт хөл дүн болон нийт классын дор нийт хөл дүнг оруулах өөрчлөлт хийгдсэн
    @api.model
    def _lines(self, line_id=None):
        lines = []
        context = self.env.context
        maxlevel = context['context_id'].hierarchy_3 and 3 or 1
        company_id = context.get('company_id') or self.env.user.company_id
        grouped_accounts = {}
        period_number = 0
        initial_balances = {}
        context['periods'].reverse()
        for period in context['periods']:
            res = self.with_context(date_from_aml=period[0], date_to=period[1], date_from=period[0] and company_id.compute_fiscalyear_dates(datetime.strptime(period[0], "%Y-%m-%d"))['date_from'] or None).group_by_account_id(line_id)  # Aml go back to the beginning of the user chosen range but the amount on the account line should go back to either the beginning of the fy or the beginning of times depending on the account
            ################################################################
            # Данс эсвэл/болон дансны төрөл сонгогдсон байвал сонгогдоогүй дансуудыг res ээс гаргана
            accounts_to_pop = []
            if context['context_id'].accounts:
                for k, v in res.iteritems():
                    if k.id not in context['context_id'].accounts.ids:
                        if k not in accounts_to_pop:
                            accounts_to_pop.append(k)
            if context['context_id'].account_types:
                for k, v in res.iteritems():
                    if k.user_type_id.id not in context['context_id'].account_types.ids:
                        if k not in accounts_to_pop:
                            accounts_to_pop.append(k)
            for account in accounts_to_pop:
                res.pop(account)
            ################################################################
            if period_number == 0:
                initial_balances = dict([(k, res[k]['initial_bal']['balance']) for k in res])
            for account in res:
                if account not in grouped_accounts.keys():
                    grouped_accounts[account] = [{'balance': 0, 'debit': 0, 'credit': 0} for p in context['periods']]
                grouped_accounts[account][period_number]['balance'] = res[account]['balance'] - res[account]['initial_bal']['balance']
                grouped_accounts[account][period_number]['debit'] = res[account]['debit'] - res[account]['initial_bal']['debit']
                grouped_accounts[account][period_number]['credit'] = res[account]['credit'] - res[account]['initial_bal']['credit']
            period_number += 1
        sorted_accounts = sorted(grouped_accounts, key=lambda a: a.code)
        title_index = ''
        
        # Override-р атрибут нэмэж зарлаж байна
        before_line_title_index = ''        # Класс болгоны хөлд Классын индексийг ялгаж бичихэд ашиглагдана 
        current_summary_filled = False      # Хөл дүнг бичих боломжтой эсэхийг шалгахад ашиглана. 
                                            # Бичих боломжтой эсэх нь сүүлд бичигдсэн классын нийт хөл бүртгэгдсэн эсэхээс хамаарна.
        current_summary = [0, 0, 0, 0]      # Сүүлд бичигдсэн классын нийт хөлийг тооцоолоход ашиглагдана
        total_summary = [0, 0, 0, 0]        # Нийт хөл дүнг тооцоолоход ашиглагдана
        # Override-д ашиглагдах атрибутуудыг зарлаад дууслаа
        
        for account in sorted_accounts:
            non_zero = False
            for p in xrange(len(context['periods'])):
                if (grouped_accounts[account][p]['debit'] or grouped_accounts[account][p]['credit']) or\
                    not company_id.currency_id.is_zero(initial_balances.get(account, 0)):
                    non_zero = True
            if not non_zero:
                continue
            for level in range(maxlevel):
                if (account.code[:level+1] > title_index[:level+1]):
                    title_index = account.code[:level+1]
                    total = map(lambda x: 0.00, xrange(len(context['periods'])))
                    if maxlevel>1:
                        for account_sum in sorted_accounts:
                            if account_sum.code[:level+1] == title_index:
                                for p in xrange(len(context['periods'])):
                                    total[p] += grouped_accounts[account_sum][p]['balance']
                            if account_sum.code[:level+1] > title_index:
                                break
                        total2 = ['']
                        for p in total:
                            total2.append(p >= 0 and self._format(p) or '')
                            total2.append(p < 0 and self._format(-p) or '')
                        total2.append('')
                    else:
                        total2 = [''] + ['' for p in xrange(len(context['periods']))]*2 + ['']

                    # Override-д хамгийн сүүлчийн классын хөл дүнгийн нийлбэр мөрийг нэмэх функц эхэлж байна.
                    """
                        @note: Belown check used for draw one class's total init, debit, credit & last amount
                    """
                    if current_summary_filled:
                        current_summary_with_format = [self._format(current_summary[0]),
                                                       self._format(current_summary[1]),
                                                       self._format(current_summary[2]),
                                                       self._format(current_summary[3])]
                        current_summary = [0, 0, 0, 0] 
                        lines.append({
                            'id': before_line_title_index,
                            'type': 'footer_line',
                            'name': _(u'Class %s Total') % before_line_title_index,
                            'footnotes': {},
                            'columns': current_summary_with_format,
                            'level': 1,
                            'unfoldable': False,
                            'unfolded': True,
                        })
                    
                    current_summary_filled = True
                    # Override-д хамгийн сүүлчийн классын хөл дүнгийн нийлбэр мөрийг нэмэх функц дуусч байна.
                    
                    lines.append({
                        'id': title_index,
                        'type': 'line',
                        'name': level and title_index or (_("Class %s") % title_index),
                        'footnotes': {},
                        'columns': ['', '', '', ''],
                        'level': level+1,
                        'unfoldable': False,
                        'unfolded': True,
                    })
                    
                    # Override-р хамгийн сүүлчийн классын индексийг хадгалж авч байна.
                    before_line_title_index = title_index
                     
            lines.append({
                'id': account.id,
                'type': 'account_id',
                'name': account.code + " " + account.name,
                'footnotes': self.env.context['context_id']._get_footnotes('account_id', account.id),
                'columns': [account in initial_balances and self._format(initial_balances[account]) or self._format(0.0)] +
                            sum([
                                    [
                                        grouped_accounts[account][p]['debit'] > 0 and self._format(grouped_accounts[account][p]['debit']) or '',
                                        grouped_accounts[account][p]['credit'] > 0 and self._format(grouped_accounts[account][p]['credit']) or ''
                                    ] for p in xrange(len(context['periods']))
                                ], []) +
                            [self._format((account in initial_balances and initial_balances[account] or 0.0) + sum([grouped_accounts[account][p]['balance'] for p in xrange(len(context['periods']))]))],
                'level': 1,
                'unfoldable': False,
            })
            
            if account in initial_balances:
                current_summary[0] += initial_balances[account]
                total_summary[0] += initial_balances[account]
                
                debit, credit = 0, 0
                for p in range(len(context['periods'])):
                   debit += grouped_accounts[account][p]['debit']
                   credit += grouped_accounts[account][p]['credit']
                   
                current_summary[1] += debit
                current_summary[2] += credit
                current_summary[3] += (initial_balances[account] + debit - credit)
                
                total_summary[1] += debit
                total_summary[2] += credit
                total_summary[3] += (initial_balances[account] + debit - credit)
              
            # Override-р хамгийн сүүлчийн классын хөл дүн болон нийт классуудын хөл дүнг агуулсан мөрүүдийг нэмэх функц эхэлж байна.
            """
                @note: Belown check used for:
                            First: Draw last class's total init, debit, credit & last amount
                            Second: Draw all class's total init, debit, credit & last amount
            """
            if sorted_accounts[-1] == account:
                if current_summary_filled:
                    current_summary_with_format = [self._format(current_summary[0]),
                                                   self._format(current_summary[1]),
                                                   self._format(current_summary[2]),
                                                   self._format(current_summary[3])]
                    lines.append({
                        'id': before_line_title_index,
                        'type': 'footer_line',
                        'name': _(u'Class %s Total') % before_line_title_index,
                        'footnotes': {},
                        'columns': current_summary_with_format,
                        'level': 1,
                        'unfoldable': False,
                        'unfolded': True,
                    })
                    
                    total_summary_with_format = [self._format(total_summary[0]),
                                                   self._format(total_summary[1]),
                                                   self._format(total_summary[2]),
                                                   self._format(total_summary[3])]
                    lines.append({
                        'id': 'Total',
                        'type': 'line',
                        'name': _(u'Total'),
                        'footnotes': {},
                        'columns': total_summary_with_format,
                        'level': 1,
                        'unfoldable': False,
                        'unfolded': True,
                    })
            # Override-р хамгийн сүүлчийн классын хөл дүн болон нийт классуудын хөл дүнг агуулсан мөрүүдийг нэмэх функц дуусч байна.
                
        return lines

