# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _, osv
try:
    from odoo.tools.misc import xlsxwriter
except ImportError:
    # TODO saas-17: remove the try/except to directly import from misc
    import xlsxwriter
from odoo.exceptions import Warning
from datetime import timedelta, datetime
import babel
import calendar
import json
import StringIO
from odoo.tools import config, posix_to_ldml
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles

class AccountReportContextCommon(models.TransientModel):
    _inherit = "account.report.context.common"
    
    # Харилцагчийн дэвтэр тайлан дахь МӨРИЙГ эхний үлдэгдлийн МӨР эсэхийг ялгахад ашиглах талбар
    is_initial_line = fields.Boolean(default=False)

    def get_xlsx(self, response):
        output = StringIO.StringIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
        report_id = self.get_report_obj()
        sheet = workbook.add_worksheet(report_id.get_title())

        def_style = workbook.add_format({'font_name': 'Arial'})
        title_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2})
        level_0_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'pattern': 1, 'font_color': '#FFFFFF'})
        level_0_style_left = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'left': 2, 'pattern': 1, 'font_color': '#FFFFFF'})
        level_0_style_right = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'right': 2, 'pattern': 1, 'font_color': '#FFFFFF'})
        level_1_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2})
        level_1_style_left = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'left': 2})
        level_1_style_right = workbook.add_format({'font_name': 'Arial', 'bold': True, 'bottom': 2, 'top': 2, 'right': 2})
        level_2_style = workbook.add_format({'font_name': 'Arial', 'bold': True, 'top': 2})
        level_2_style_left = workbook.add_format({'font_name': 'Arial', 'bold': True, 'top': 2, 'left': 2})
        level_2_style_right = workbook.add_format({'font_name': 'Arial', 'bold': True, 'top': 2, 'right': 2})
        level_3_style = def_style
        level_3_style_left = workbook.add_format({'font_name': 'Arial', 'left': 2})
        level_3_style_right = workbook.add_format({'font_name': 'Arial', 'right': 2})
        domain_style = workbook.add_format({'font_name': 'Arial', 'italic': True})
        domain_style_left = workbook.add_format({'font_name': 'Arial', 'italic': True, 'left': 2})
        domain_style_right = workbook.add_format({'font_name': 'Arial', 'italic': True, 'right': 2})
        upper_line_style = workbook.add_format({'font_name': 'Arial', 'top': 2})

        if self.get_report_obj().get_name() in ['aged_receivable', 'aged_payable']:
            format_title = {
                'font_name': 'Times New Roman',
                'font_size': 14,
                'bold': True,
                'align': 'center',
                'valign': 'vcenter',
                'border': 1,
                'text_wrap': 1,
            }
            format_title = workbook.add_format(format_title)
            # Баримтын толгой
            format_filter = workbook.add_format(ReportExcelCellStyles.format_filter)
            # Хүснэгтийн толгой
            format_group = {
                'font_name': 'Times New Roman',
                'font_size': 11,
                'bold': True,
                'align': 'center',
                'valign': 'vcenter',
                'border': 1,
                'bg_color': '#83CAFF',
                'text_wrap': 1,
            }
            format_group = workbook.add_format(format_group)
            format_content_float = workbook.add_format(ReportExcelCellStyles.format_content_float)
            format_content_left = workbook.add_format(ReportExcelCellStyles.format_content_left)
            format_group_float = workbook.add_format(ReportExcelCellStyles.format_group_float)
            format_group_right = workbook.add_format(ReportExcelCellStyles.format_group_right)

            format_content_float_color = workbook.add_format(ReportExcelCellStyles.format_content_float_color)
            format_content_text_wrap = {
                'font_name': 'Times New Roman',
                'font_size': 9,
                'align': 'center',
                'valign': 'vcenter',
                'border': 1,
                'text_wrap': 1,
            }
            format_content_text_wrap = workbook.add_format(format_content_text_wrap)
            y_offset = 0
            sheet.set_column(0, 0, 5)
            sheet.set_column(1, 9, 15)
            sheet.write(0, 0, u'Байгууллага: %s' % self.env.user.company_id.name, format_filter)
            y_offset += 2
            if self.get_report_obj().get_name() == 'aged_payable':
                sheet.merge_range(y_offset, 0, y_offset, 9, u'ӨГЛӨГИЙН НАСЖИЛТЫН ТАЙЛАН', format_title)
            else:
                sheet.merge_range(y_offset, 0, y_offset, 9, u'АВЛАГЫН НАСЖИЛТЫН ТАЙЛАН', format_title)
            y_offset += 2
            sheet.write(y_offset, 0, u'Хэвлэсэн огноо: %s' % (datetime.now().strftime('%Y-%m-%d')), format_filter)
            y_offset += 1

            columns = [u'№', u'Харилцагчийн регистр', u'Харилцагч']
            [columns.append(column.replace('<br/>', ' ').replace('&nbsp;', ' ')) for column in self.with_context(is_xls=True).get_columns_names()]
            if self.get_report_obj().get_name() == 'aged_payable':
                columns[3] = u'%s хүртлэх хэвийн хугацаандаа яваа өглөг' % columns[3][13:]
            else:
                columns[3] = u'%s хүртлэх хэвийн хугацаандаа яваа авлага' % columns[3][13:]

            sheet.set_row(y_offset, 50)
            x = 0
            for column in columns:
                sheet.write(y_offset, x, column, format_group)
                x += 1
            y_offset += 1
            seq = 1
            lines = report_id.with_context(no_format=True, print_mode=True).get_lines(self)

            for y in range(0, len(lines)):
                if lines[y].get('type') == 'partner_id':
                    partner_id = self.env['res.partner'].search([('id','=',lines[y].get('id'))])
                    sheet.write(y_offset, 0, seq, format_content_text_wrap)
                    sheet.write(y_offset, 1, partner_id.register or '-  ', format_content_text_wrap)
                    sheet.write(y_offset, 2, partner_id.name, format_content_left)
                    for i in range(0, len(lines[y]['columns'])):
                        val = lines[y]['columns'][i] if lines[y]['columns'][i] else '-   '
                        sheet.write(y_offset, 3+i, val, format_content_float)
                    seq += 1
                    y_offset += 1
                elif lines[y].get('level') == 0 and lines[y].get('name') == 'Total':
                    sheet.write(y_offset, 0, '', format_group_right)
                    sheet.write(y_offset, 1, '', format_group_right)
                    sheet.write(y_offset, 2, u'Нийт: ', format_group_right)
                    for i in range(0, len(lines[y]['columns'])):
                        val = lines[y]['columns'][i] if lines[y]['columns'][i] else '-   '
                        sheet.write(y_offset, 3 + i, val, format_group_float)
                    y_offset += 1

        else:
            sheet.set_column(0, 0, 15) #  Set the first column width to 15
    
            sheet.write(0, 0, '', title_style)
    
            y_offset = 0
            if self.get_report_obj().get_name() == 'coa' and self.get_special_date_line_names():
                sheet.write(y_offset, 0, '', title_style)
                sheet.write(y_offset, 1, '', title_style)
                x = 2
                for column in self.with_context(is_xls=True).get_special_date_line_names():
                    sheet.write(y_offset, x, column, title_style)
                    sheet.write(y_offset, x+1, '', title_style)
                    x += 2
                sheet.write(y_offset, x, '', title_style)
                y_offset += 1
    
            x = 1
            for column in self.with_context(is_xls=True).get_columns_names():
                sheet.write(y_offset, x, column.replace('<br/>', ' ').replace('&nbsp;',' '), title_style)
                x += 1
            y_offset += 1
    
            lines = report_id.with_context(no_format=True, print_mode=True).get_lines(self)
    
            if lines:
                max_width = max([len(l['columns']) for l in lines])
    
            for y in range(0, len(lines)):
                if lines[y].get('level') == 0 and lines[y].get('type') == 'line':
                    for x in range(0, len(lines[y]['columns']) + 1):
                        sheet.write(y + y_offset, x, None, upper_line_style)
                    y_offset += 1
                    style_left = level_0_style_left
                    style_right = level_0_style_right
                    style = level_0_style
                elif lines[y].get('level') == 1 and lines[y].get('type') == 'line':
                    for x in range(0, len(lines[y]['columns']) + 1):
                        sheet.write(y + y_offset, x, None, upper_line_style)
                    y_offset += 1
                    style_left = level_1_style_left
                    style_right = level_1_style_right
                    style = level_1_style
                elif lines[y].get('level') == 2:
                    style_left = level_2_style_left
                    style_right = level_2_style_right
                    style = level_2_style
                elif lines[y].get('level') == 3:
                    style_left = level_3_style_left
                    style_right = level_3_style_right
                    style = level_3_style
                elif lines[y].get('type') != 'line':
                    style_left = domain_style_left
                    style_right = domain_style_right
                    style = domain_style
                else:
                    style = def_style
                    style_left = def_style
                    style_right = def_style
                sheet.write(y + y_offset, 0, lines[y]['name'], style_left)
                for x in xrange(1, max_width - len(lines[y]['columns']) + 1):
                    sheet.write(y + y_offset, x, None, style)
                for x in xrange(1, len(lines[y]['columns']) + 1):
                    if isinstance(lines[y]['columns'][x - 1], tuple):
                        lines[y]['columns'][x - 1] = lines[y]['columns'][x - 1][0]
                    if x < len(lines[y]['columns']):
                        sheet.write(y + y_offset, x+lines[y].get('colspan', 1)-1, lines[y]['columns'][x - 1], style)
                    else:
                        sheet.write(y + y_offset, x+lines[y].get('colspan', 1)-1, lines[y]['columns'][x - 1], style_right)
                if lines[y]['type'] == 'total' or lines[y].get('level') == 0:
                    for x in xrange(0, len(lines[0]['columns']) + 1):
                        sheet.write(y + 1 + y_offset, x, None, upper_line_style)
                    y_offset += 1
            if lines:
                for x in xrange(0, max_width+1):
                    sheet.write(len(lines) + y_offset, x, None, upper_line_style)

        workbook.close()
        output.seek(0)
        response.stream.write(output.read())
        output.close()

