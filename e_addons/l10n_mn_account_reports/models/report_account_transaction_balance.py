from odoo import models, api, fields, _


class ReportAccountTransactionBalance(models.TransientModel):
    _inherit = 'report.account.transaction.balance'

    @api.multi
    def preview(self):
        context = self.env['account.context.coa'].search([], order='id DESC', limit=1)
        if context:
            context.date_from = self.date_from
            context.date_to = self.date_to
            context.all_entries = True if self.target_move == 'all' else False
            if self.account_ids:
                context.write({
                    'accounts': [(5, _, _)]
                })
                context.write({
                    'accounts': [(4, self.account_ids.ids)]
                })
            else:
                context.write({
                    'accounts': [(5, _, _)]
                })
            if self.account_type_filter:
                context.write({
                    'account_types': [(5, _, _)]
                })
                context.write({
                    'account_types': [(4, self.account_type_filter.ids)]
                })
            else:
                context.write({
                    'account_types': [(5, _, _)]
                })
        res = {
            'type': 'ir.actions.client',
            'name': 'Trial Balance',
            'tag': 'account_report_generic',
            'context': {'url': '/account_reports/output_format/coa/1',
                        'model': 'account.coa.report'}
        }
        return res


class AccountContextCoa(models.TransientModel):
    _inherit = "account.context.coa"

    accounts = fields.Many2many('account.account', 'account_coa_report_account')
    account_types = fields.Many2many('account.account.type', 'account_coa_report_account_type')
