# -*- coding: utf-8 -*-
{
    'name': 'Accounting Reports - General ledger - Analytic account',
    'summary': 'Adds analytic account to general ledger',
    'category': 'Accounting',
    'author': "Asterisk Technologies LLC",
	'description': """
Adds analytic account to general ledger
====================
    """,
    'depends': [
        'account_reports',
        'analytic'
    ],
    'data': [
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
}
