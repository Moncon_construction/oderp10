# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Project Task Gantt',
    'category': 'Project',
    'author': "Asterisk Technologies LLC",
    'description': """
OpenERP Web Gantt chart view.
=============================

""",
    'version': '1.0',
    'depends': ['l10n_mn_project','web_gantt'],
    'data' : [
        'views/project_task_gantt_view.xml',
    ],
    'auto_install': True,
    'license': 'OEEL-1',
}