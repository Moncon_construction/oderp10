# -*- coding: utf-8 -*-

import os
import logging
import sqlite3

from odoo.tools import config

from .collector import Collector, Metric
from .constants import DEFAULT_METRICS_DB

from . import patches
from . import controllers
from . import collectors

LOGGER = logging.getLogger(__name__)

def on_start():
    metrics_db = config.get_misc("prometheus", "metrics_db", DEFAULT_METRICS_DB)
    if not os.path.exists(metrics_db):
        LOGGER.info("Creating new metrics database on %s", metrics_db)
        db = sqlite3.connect(metrics_db)
        cr = db.cursor()
        cr.execute("CREATE TABLE metrics (name text PRIMARY KEY, tx_value text, n_value real)")
        cr.close()
        db.close()

on_start()
