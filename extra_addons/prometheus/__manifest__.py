# -*- coding: utf-8 -*-
{
    "name": "Prometheus Exporter",
    "summary": """Prometheus exporter to gather metrics of Odoo instance""",
    "description": """
        Prometheus exporter to gather metrics of Odoo instance
    """,
    "author": "Hugo Rodrigues",
    "website": "https://hugorodrigues.net",
    "license": "OPL-1",
    "price": 100,
    "currency": "EUR",
    "support": "me@hugorodrigues.net",
    "category": "Administration",
    "version": "13.0.2.0.0",
    "images": ["static/description/cover.svg"]
}
