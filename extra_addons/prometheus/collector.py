# -*- coding: UTF-8 -*-
"""Collectors helpers"""

import logging
import contextlib
import sqlite3

from odoo.tools import config, convert
from odoo.sql_db import db_connect
from odoo.service import db
from odoo import api, registry, SUPERUSER_ID

from .constants import DEFAULT_METRICS_DB

LOGGER = logging.getLogger(__name__)

class Metric(object):

    def __init__(self, name, mhelp, mtype):
        self.name = name
        self.help = mhelp
        self.type = mtype
        self.labels = []

        assert self.type in ("counter", "gauge", "histogram", "summary", "untyped")

    def add_label(self, value, **kw):
        self.labels.append(([(x, y) for x, y in kw.items()], value))

class MetaCollector(type):
    """
    Meta collector to register collectors
    """

    _collectors = {}

    def __init__(cls, name, bases, attrs):
        """Register collectors"""
        super(MetaCollector, cls).__init__(name, bases, attrs)
        collector_name = getattr(cls, "_name", None)
        if collector_name and collector_name not in cls._collectors:
            cls._collectors[collector_name] = cls
            LOGGER.debug("Registered Prometheus collector %s", collector_name)

class Collector(MetaCollector("DummyCollector", (object,), {})):
    """
    Class used to create collectors
    """
    _name = None  # Name used to identify the collector

    @property
    def databases(self):
        """Get available databases"""
        force = config.get_misc("prometheus", "db_force", False)
        if isinstance(force, str):
            force = convert.str2bool(force)
        return db.list_dbs(force)

    @contextlib.contextmanager
    def db_connect(self, database):
        """Connect to a database"""
        db = db_connect(database)
        with contextlib.closing(db.cursor()) as cr:
            yield cr
            cr.commit()

    @contextlib.contextmanager
    def metrics_db(self):
        """Connect to the metrics database"""
        with sqlite3.connect(config.get_misc("prometheus", "metrics_db", DEFAULT_METRICS_DB)) as db:
            cr = db.cursor()
            yield cr
            cr.close()

    @contextlib.contextmanager
    def environment(self, database):
        """Get a environment of the database"""
        with api.Environment.manage():
            with registry(database).cursor() as cr:
                yield api.Environment(cr, SUPERUSER_ID, {})

    def get_parameter(self, cr, parameter):
        """Get ir.config_parameter"""
        cr.execute("SELECT value FROM ir_config_parameter WHERE key = %s", (parameter,))
        value = cr.fetchone()
        return value[0] if value else None

    def collect(self):
        raise NotImplementedError("collect method should be implemented on each collector")

    @staticmethod
    def gather():
        """Fetch information from all enabled collectors"""

        collected = []
        for collector in config.get_misc("prometheus", "collectors", ",".join(MetaCollector._collectors.keys())).split(","):
            collector = MetaCollector._collectors.get(collector.strip(), None)
            if not collector:
                continue
            metrics = collector().collect()
            if isinstance(metrics, list):
                assert all([isinstance(x, Metric) for x in metrics])
                collected.extend(metrics)
            elif isinstance(metrics, Metric):
                collected.append(metrics)

        return collected
