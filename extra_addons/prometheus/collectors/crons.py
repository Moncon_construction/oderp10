# -*- coding: utf-8 -*-

import datetime

from odoo.tools import misc

from odoo.addons.prometheus import Collector, Metric


class CronCollector(Collector):
    _name = "cron"

    def collect(self):
        metric = Metric("odoo_missed_crons", "Number of missed cron execution", "gauge")

        now_str = datetime.datetime.now().strftime(misc.DEFAULT_SERVER_DATETIME_FORMAT)

        for database in self.databases:
            with self.db_connect(database) as cr:
                cr.execute("SELECT COUNT(*) FROM ir_cron WHERE active=true AND nextcall < %s AND numbercall != 0", (now_str,))

                metric.add_label(cr.fetchone()[0], database=database)

        return metric
