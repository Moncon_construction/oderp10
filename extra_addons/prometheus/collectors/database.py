# -*- coding: utf-8 -*-

import os
import scandir
from odoo.tools import config

from odoo.addons.prometheus import Collector, Metric


def get_path_size(path):
    total_size = os.stat(path).st_size
    for entry in scandir.scandir(path):
        if entry.is_file():
            total_size += entry.stat().st_size
        elif entry.is_dir():
            total_size += get_path_size(entry.path)
    return total_size


class DatabaseCollector(Collector):
    _name = "database"

    def collect(self):
        metric = Metric("odoo_database_size", "Size of a Odoo database", "gauge")
        metric_uuid = Metric("odoo_database_uuid", "database UUID", "gauge")
        metric_version = Metric("odoo_database_version", "Odoo database version", "gauge")
        metric_connections = Metric("odoo_database_connections", "Number of database connections", "gauge")

        databases = self.databases
        databases_placeholder = ", ".join("%s" for _ in databases)

        # Count DB connections
        with self.db_connect("postgres") as cr:
            # Get connection user. Using SQL to prevent any null value from python config
            cr.execute("SELECT user")
            db_user = cr.fetchall()[0][0]

            # Get number of connections
            dbs_with_conns = set()
            # cr.execute("SELECT datname,count(*) FROM pg_stat_activity where usename=%%s AND datname IN (%s) group by datname" % databases_placeholder, (db_user, databases))
            # for db_conn in cr.fetchall():
            #     dbs_with_conns.add(db_conn[0])
            #     metric_connections.add_label(db_conn[1], database=db_conn[0])

            for database in databases:
                if database not in dbs_with_conns:
                    metric_connections.add_label(0, database=database)

                # Count database size
                total_size = 0
                cr.execute("SELECT pg_database_size(%s)", (database,))
                db_size = cr.fetchall()[0][0]
                total_size += db_size
                metric.add_label(db_size, database=database, component="database")

                # Count file store size
                fs_path = config.filestore(database)
                fs_size = get_path_size(fs_path)
                total_size += fs_size
                metric.add_label(fs_size, database=database, component="filestore")

                # Total size
                metric.add_label(total_size, database=database, component="total")

                with self.db_connect(database) as cr2:
                    # UUID
                    metric_uuid.add_label(1, database=database, uuid=self.get_parameter(cr2, "database.uuid"))

                    # Version
                    cr2.execute("SELECT imm.latest_version FROM ir_module_module AS imm JOIN ir_model_data AS imd ON imd.res_id = imm.id WHERE imd.module='base' AND imd.name='module_base';")

                    version = float(".".join(cr2.fetchall()[0][0].split(".")[0:2]))
                    metric_version.add_label(version, database=database)

        return [metric, metric_uuid, metric_version, metric_connections]
