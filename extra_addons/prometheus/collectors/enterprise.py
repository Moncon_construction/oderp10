# -*- coding: utf-8 -*-

import datetime

from odoo.tools import misc
from odoo import release

from odoo.addons.prometheus import Collector, Metric


class EnterpriseCollector(Collector):
    _name = "enterprise"

    def collect(self):

        # Detect if we are running the enterprise version
        if release.version_info[-1] != "e":
            # Not running enterprise version, ignoring
            return []

        metric = Metric("odoo_enterprise_expiration", "Odoo Enterprise Expiration Days", "gauge")

        now = datetime.datetime.now()

        for database in self.databases:
            with self.db_connect(database) as cr:
                expiration_date = self.get_parameter(cr, "database.expiration_date")
                if expiration_date:
                    expiration_date = datetime.datetime.strptime(expiration_date, misc.DEFAULT_SERVER_DATETIME_FORMAT)
                    exp_days = (expiration_date - now).days
                else:
                    exp_days = 0

                metric.add_label(exp_days, database=database)

        return metric
