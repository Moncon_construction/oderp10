# -*- coding: utf-8 -*-


from odoo.addons.prometheus import Collector, Metric


class HttpCollector(Collector):
    _name = "http"

    def collect(self):

        res = []
        metrics = {
            "odoo_json_request_time": "Average time for JSON requests",
            "odoo_http_request_time": "Average time for HTTP requests"
        }

        with self.metrics_db() as cr:

            for metric, description in metrics.items():
                m = Metric(metric, description, "gauge")
                # cr.execute("SELECT AVG(n_value) FROM metrics WHERE name LIKE ?", ("%s%%" % metric,))
                # m.add_label(cr.fetchall()[0][0] or 0)
                # cr.execute("DELETE FROM metrics WHERE name LIKE ?", ("%s%%" % metric,))
                res.append(m)

        return res
