# -*- coding: utf-8 -*-

from odoo.addons.prometheus import Collector, Metric


class MailCollector(Collector):
    _name = "mail"

    def collect(self):

        metric = Metric("odoo_mail", "Number of emails", "gauge")

        for database in self.databases:
            with self.db_connect(database) as cr:
                failed = sent = received = 0

                # Only get the metrics if mail is installed
                cr.execute("SELECT imm.state FROM ir_module_module AS imm JOIN ir_model_data AS imd ON imd.res_id = imm.id WHERE imd.module='base' AND imd.name='module_mail'")
                module_state = cr.fetchone()
                if module_state:
                    module_state = module_state[0]
                else:
                    module_state = "na"
                if module_state == "installed":

                    cr.execute("SELECT COUNT(*) FROM mail_mail WHERE state = 'exception'")
                    failed = int(cr.fetchone()[0])
                    cr.execute("SELECT COUNT(*) FROM mail_mail WHERE state IN ('sent', 'received')")
                    sent = int(cr.fetchone()[0])
                    cr.execute("SELECT COUNT(*) FROM mail_mail WHERE state = 'received'")
                    received = int(cr.fetchone()[0])

                metric.add_label(failed, database=database, state="failed")
                metric.add_label(sent, database=database, state="sent")
                metric.add_label(received, database=database, state="received")

        return metric
