# -*- coding: utf-8 -*-

import sys
import datetime
import threading
import os

import psutil

from odoo.tools import misc
from odoo.service.server import memory_info

from odoo.addons.prometheus import Collector, Metric


class PythonCollector(Collector):
    _name = "python"

    def collect(self):
        python_info = Metric("python_info", "Information about python enviroment", "gauge")
        python_threads = Metric("python_threads", "Number of active threads", "gauge")
        python_mem = Metric("python_memory", "Python used memory", "gauge")

        python_info.add_label(1, version="%d.%d.%d" % (sys.version_info.major, sys.version_info.minor, sys.version_info.micro))

        python_threads.add_label(threading.active_count())

        python_mem.add_label(memory_info(psutil.Process(os.getpid()))[0])

        return [python_info, python_threads, python_mem]
