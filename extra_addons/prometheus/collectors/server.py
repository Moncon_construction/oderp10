# -*- coding: utf-8 -*-
import psutil
from odoo.addons.prometheus import Collector, Metric


class ServerCollector(Collector):
    _name = "server"

    def collect(self):

        res = []
        metrics = {
            "odoo_soft_memory_reset": "Number of times that a worker exceeded the allowed soft limit",
            "odoo_virtual_real_time_limit": "Number of requests that exceeded the allowed real time",
            "odoo_cpu_time_limit_reset": "Number of requests that exceeded the allowed CPU time",
            "odoo_worker_max_requests": "Number of workers that exceeded the maximum number of requests"
        }

        sql_placeholder = ",".join(["?" for _ in metrics])

        with self.metrics_db() as cr:
            cr.execute("SELECT name, n_value from metrics WHERE name IN (%s)" % sql_placeholder, tuple(metrics.keys()))
            m_values = {x[0]: x[1] for x in cr.fetchall()}
            cr.execute("DELETE FROM metrics WHERE name IN (%s)" % sql_placeholder, tuple(metrics.keys()))

        for metric, description in metrics.items():
            m = Metric(metric, description, "gauge")
            m.add_label(int(m_values.get(metric, 0)))
            res.append(m)

        hard_disk_total = Metric("hard_disk_total", "Total disk size", "gauge")
        hard_disk_used = Metric("hard_disk_used", "Used disk size", "gauge")
        hard_disk_free = Metric("hard_disk_free", "Free disk size", "gauge")
        hdd = psutil.disk_usage('/')
        hard_disk_total.add_label(hdd.total / (2**30))
        hard_disk_used.add_label(hdd.used / (2**30))
        hard_disk_free.add_label(hdd.free / (2**30))
        res.append(hard_disk_total)
        res.append(hard_disk_used)
        res.append(hard_disk_free)

        virtual_memory_total = Metric("virtual_memory_total", "Total memory", "gauge")
        virtual_memory_available = Metric("virtual_memory_available", "Available memory", "gauge")
        virtual_memory_percent = Metric("virtual_memory_percent", "Percent memory", "gauge")
        virtual_memory_active = Metric("virtual_memory_active", "Active memory", "gauge")
        virtual_memory_inactive = Metric("virtual_memory_inactive", "Inactive memory", "gauge")
        virtual_memory_wired = Metric("virtual_memory_wired", "Wired memory", "gauge")
        virtual_memory_used = Metric("virtual_memory_used", "Used memory", "gauge")
        virtual_memory_free = Metric("virtual_memory_free", "Free memory", "gauge")
        memeory_dict = psutil.virtual_memory()._asdict()
        if 'total' in memeory_dict:
            virtual_memory_total.add_label(memeory_dict['total'] / (2**30))
            res.append(virtual_memory_total)
        if 'available' in memeory_dict:
            virtual_memory_available.add_label(memeory_dict['available'] / (2**30))
            res.append(virtual_memory_available)
        if 'percent' in memeory_dict:
            virtual_memory_percent.add_label(memeory_dict['percent'])
            res.append(virtual_memory_percent)
        if 'active' in memeory_dict:
            virtual_memory_active.add_label(memeory_dict['active'] / (2**30))
            res.append(virtual_memory_active)
        if 'inactive' in memeory_dict:
            virtual_memory_inactive.add_label(memeory_dict['inactive'] / (2**30))
            res.append(virtual_memory_inactive)
        if 'wired' in memeory_dict:
            virtual_memory_wired.add_label(memeory_dict['wired'] / (2**30))
            res.append(virtual_memory_wired)
        if 'used' in memeory_dict:
            virtual_memory_used.add_label(memeory_dict['used'] / (2**30))
            res.append(virtual_memory_used)
        if 'free' in memeory_dict:
            virtual_memory_free.add_label(memeory_dict['free'] / (2**30))
            res.append(virtual_memory_free)
        return res
