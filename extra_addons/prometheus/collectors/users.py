# -*- coding: utf-8 -*-

import datetime

from odoo.tools import misc

from odoo.addons.prometheus import Collector, Metric


class UserCollector(Collector):
    _name = "user"

    def collect(self):
        metric = Metric("odoo_users", "Number of users", "gauge")
        metric_active = Metric("odoo_users_active", "Number of active users", "gauge")

        # Date used to detect active users
        limit_date = datetime.datetime.now() - datetime.timedelta(15)
        limit_date_str = limit_date.strftime(misc.DEFAULT_SERVER_DATETIME_FORMAT)

        for database in self.databases:
            with self.environment(database) as env:
                Users = env["res.users"]

                # Count internal + internal and active users
                metric.add_label(Users.search_count([("active", "=", True)]), database=database, share=False)
                metric_active.add_label(Users.search_count([("active", "=", True), ("login_date", ">=", limit_date_str)]), database=database, share=False)

                # Count external + external and active users
                external_users = 0
                external_active_users = 0
                if "share" in Users._fields:
                    external_users = Users.search_count([("active", "=", True), ("share", "=", True)])
                    external_active_users = Users.search_count([("active", "=", True), ("share", "=", True), ("login_date", ">=", limit_date_str)])
                metric.add_label(external_users, database=database, share=True)
                metric_active.add_label(external_active_users, database=database, share=True)

        return [metric, metric_active]
