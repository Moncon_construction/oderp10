# -*- coding: UTF-8 -*-
"""Constants used across the module"""

import tempfile
import os

DEFAULT_METRICS_DB = os.path.join(tempfile.gettempdir(), "odoo_prometheus_metrics.db")
