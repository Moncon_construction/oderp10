# -*- coding: utf-8 -*-
from odoo import http
from odoo.tools import config
from odoo.addons.prometheus import collector


class Prometheus(http.Controller):
    @http.route(["/metrics", "/metrics/<string:auth>"], auth="none", type="http")
    def metrics(self, auth=None):

        # If set, check the authentication key set on configuration file
        auth_key = config.get_misc("prometheus", "auth")

        if auth_key and auth != auth_key:
            return http.request.not_found()

        collected_data = collector.Collector.gather()

        result = ""

        for data in collected_data:
            result += """
# HELP {name} {help}
# TYPE {name} {type}
""".format(name=data.name, help=data.help, type=data.type)
            for label in data.labels:
                labels = ", ".join(['%s="%s"' % (x, y) for x, y in label[0]])
                if labels:
                    labels = "{%s}" % labels
                result += "{name}{labels} {value}\n".format(name=data.name, labels=labels, value=label[1])

        response = http.Response(result[1:])  # Removes first blank line
        response.headers.set("Content-Type", "text/plain; version=0.0.4; charset=UTF-8")
        return response
