=========================
Odoo Prometheus Exporter
=========================

Installing the exporter
==========================

The module doesn't need to installed on any database. To install, add the module to your module path and set it as a server wide module.

via CLI::

     --load web,prometheus,base

or configuration file::

    [options]
    server_wide_modules = prometheus,web,base

That's it, the exporter is installed.


Configuring the exporter
========================

The exporter has optional configurations. All of them are set on the prometheus section of the configuration file::

    [prometheus]
    auth = authkey
    collectors = python,database,enterprise
    db_force = true
    metrics_db = /tmp/odoo_prometheus_metrics.db
    
**auth** sets the required authentication key. This is useful if Odoo is publicly accessible.

**collectors** allows you to only enable a subset of collectors. If not set, all of them are active.

**db_force** is required to be true if you have blocked the list of databases (list_db option)

**metrics_db** sets a custom path for a sqlite database used to store some metrics across threads/workers. Default is /tmp/odoo_prometheus_metrics.db

Consuming
---------
To consume the exporter, you just need to point Prometheus to Odoo URL

With authentication
^^^^^^^^^^^^^^^^^^^^
To use authentication, you need to set a custom endpoint in Prometheus.

Inside the configuration, on the job section, set the **metrics_path** option to */metrics/<auth key>*

Examples
^^^^^^^^

- Basic::

    - job_name: odoo
      - targets:
        - 10.0.10.10:8069

- With auth::

    - job_name: odoo
      metrics_path: /metrics/ZmZhOTNhZGZmZDM2M2FkYzUwMzY4ZTE2
      static_configs:
      - targets:
        - 10.0.10.10:8069


Built in collectors
====================
.. list-table::
   :widths: 20 40 40 100 15
   :header-rows: 1

   * - Collector
     - Metrics
     - Labels
     - Description
     - Type
   * - python
     - python_info
     - version
     - Python environment information
     - gauge
   * - python
     - python_threads
     - 
     - Number of active threads
     - gauge
   * - python
     - python_memory
     - 
     - Python used memory in bytes
     - gauge
   * - server
     - odoo_soft_memory_reset
     -
     - Number of times that a worker exceeded the allowed soft limit
     - gauge
   * - server
     - odoo_virtual_real_time_limit
     -
     - Number of requests that exceeded the allowed real time
     - gauge
   * - server
     - odoo_cpu_time_limit_reset
     -
     - Number of requests that exceeded the allowed CPU time
     - gauge
   * - server
     - odoo_worker_max_requests
     -
     - Number of workers that exceeded the maximum number of requests
     - gauge
   * - http
     - odoo_json_request_time
     -
     - Average time for JSON requests
     - gauge
   * - http
     - odoo_http_request_time
     -
     - Average time for HTTP requests
     - gauge
   * - database
     - odoo_database_size
     - database, component
     - Size of each database. The database is identified by **database** and **component** tells if is the database, filestore or both (total)
     - gauge
   * - database
     - odoo_database_uuid
     - database, uuid
     - UUID of each database. The database is identified by **database** and the UUID **uuid**
     - gauge
   * - database
     - odoo_database_version
     - 
     - Version of Odoo database. The version is fetched from the base module. The database is identified by **database**
     - gauge
   * - database
     - odoo_database_connections
     - database
     - Number of database connections. The database is identified by **database**
     - gauge
   * - enterprise
     - odoo_enterprise_expiration
     - database
     - Time left until the database expires. The database is identified by **database**. Only available on Enterprise Edition
     - gauge
   * - cron
     - odoo_missed_crons
     - database
     - Number scheduled tasks that missed execution. The database is identified by **database**
     - gauge
   * - mail
     - odoo_mail
     - database,state
     - Number emails by state. The database is identified by **database** and **state** holds the email state (sent, received, failed)
     - gauge
   * - user
     - odoo_users
     - database,share
     - Number users inside the database. The database is identified by **database** and **share** tells you if the user is internal (False) or public (True)
     - gauge
   * - user
     - odoo_users_active
     - database,share
     - Same as *odoo_users*, but only shows users that have logged in on the last 15 days
     - gauge

Build custom collectors
=======================
The exporter was build in mind to support custom collectors.

Any module can implement a collector, to do that it should extend the class **odoo.addons.prometheus.Collector** and implement the method **collect**. This method must return either a single **odoo.addons.prometheus.Metric** or a list of them.

Example::

    # -*- coding: utf-8 -*-
    from odoo.addons.prometheus import Collector, Metric

    class ExampleCollector(Collector):
        _name = "example"

        def collect(self):
            metric = Metric("odoo_example_metric", "Example metric N1", "gauge")
            metric2 = Metric("odoo_example_metric2", "Example metric N2", "counter")

            # Use self.databases to fetch all available databases
            # Use self.environment with a database name to have a valid environment. This should be used in a context
            for database in self.databases:
                with self.environment(database) as env:
                    Partner = env["res.partner"]

                    pcount = Partner.search_count([("customer", "=", True)])

                    metric.add_label(pcount, database=database) # Named arguments are labels
                    metric2.add_label(123) # Metric without labels



            return [metric, metric2]

Install custom collectors
-------------------------
Custom collectors can be installed in two ways

1. All databases (recommended): Same way as the exporter. Check the Installing the exporter section


2. Subset of databases: Install as a regular module inside the desired databases.
