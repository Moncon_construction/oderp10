# -*- coding: UTF-8 -*-
"""Patches applied to Odoo code base to implement http related metrics"""

import uuid
import logging
import sqlite3
import time

from odoo.tools import config
from odoo import http

from odoo.addons.prometheus import DEFAULT_METRICS_DB

LOGGER = logging.getLogger(__name__)


# def update_metric(metric, time):
#     metric_name = "%s_%s" % (metric, str(uuid.uuid1()))
#     with sqlite3.connect(config.get_misc("prometheus", "metrics_db", DEFAULT_METRICS_DB)) as db:
#         cr = db.cursor()
#         cr.execute("INSERT INTO metrics(name, n_value) VALUES (?, ?)", (metric_name, time))
#         cr.close()


upstream_jsonrequest_dispatch = http.JsonRequest.dispatch


def jsonrequest_dispatch(self):
    LOGGER.debug("Running patched version of odoo.http.JsonRequest.dispatch")
    start = time.time()
    res = upstream_jsonrequest_dispatch(self)
    # update_metric("odoo_json_request_time", time.time() - start)
    return res


upstream_httprequest_dispatch = http.HttpRequest.dispatch


def httprequest_dispatch(self):
    LOGGER.debug("Running patched version of odoo.http.HttpRequest.dispatch")
    start = time.time()
    res = upstream_httprequest_dispatch(self)
    # update_metric("odoo_http_request_time", time.time() - start)
    return res


http.JsonRequest.dispatch = jsonrequest_dispatch
http.HttpRequest.dispatch = httprequest_dispatch

LOGGER.debug("Patched odoo.http")
