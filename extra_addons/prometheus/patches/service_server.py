# -*- coding: UTF-8 -*-
"""Patches applied to Odoo code base to implement server related metrics"""

import logging
import sqlite3
import os
import threading
import time
import signal

import psutil

from odoo.tools import config
from odoo.service import server

from odoo.addons.prometheus import DEFAULT_METRICS_DB

LOGGER = logging.getLogger(__name__)

def update_metric(metric):
    with sqlite3.connect(config.get_misc("prometheus", "metrics_db", DEFAULT_METRICS_DB)) as db:
        cr = db.cursor()
        cr.execute("INSERT INTO metrics(name, n_value) VALUES (?, 1) ON CONFLICT(name) DO UPDATE SET n_value=n_value+1", (metric,))
        cr.close()

# In this case we use the entire function to prevent the execution of duplicated code, mainly the thread loop
def threadedserver_process_limit(self):
    LOGGER.debug("Running patched version of odoo.service.server.ThreadedServer.check_limits")
    memory = server.memory_info(psutil.Process(os.getpid()))
    if config['limit_memory_soft'] and memory > config['limit_memory_soft']:
        LOGGER.warning('Server memory limit (%s) reached.', memory)
        self.limits_reached_threads.add(threading.currentThread())
        update_metric("odoo_soft_memory_reset")

    for thread in threading.enumerate():
        if not thread.daemon or getattr(thread, 'type', None) == 'cron':
            # We apply the limits on cron threads and HTTP requests,
            # longpolling requests excluded.
            if getattr(thread, 'start_time', None):
                thread_execution_time = time.time() - thread.start_time
                thread_limit_time_real = config['limit_time_real']
                if (getattr(thread, 'type', None) == 'cron' and
                        config['limit_time_real_cron'] and config['limit_time_real_cron'] > 0):
                    thread_limit_time_real = config['limit_time_real_cron']
                if thread_limit_time_real and thread_execution_time > thread_limit_time_real:
                    LOGGER.warning(
                        'Thread %s virtual real time limit (%d/%ds) reached.',
                        thread, thread_execution_time, thread_limit_time_real)
                    self.limits_reached_threads.add(thread)
                    update_metric("odoo_virtual_real_time_limit")

    # Clean-up threads that are no longer alive
    # e.g. threads that exceeded their real time,
    # but which finished before the server could restart.
    for thread in list(self.limits_reached_threads):
        if not thread.isAlive():
            self.limits_reached_threads.remove(thread)
    if self.limits_reached_threads:
        self.limit_reached_time = self.limit_reached_time or time.time()
    else:
        self.limit_reached_time = None

upstream_geventserver_process_limits = server.GeventServer.process_limits
def geventserver_process_limits(self):
    LOGGER.debug("Running patched version of odoo.service.server.GeventServer.process_limits")
    memory = memory_info(psutil.Process(self.pid))
    if config['limit_memory_soft'] and memory > config['limit_memory_soft']:
        update_metric("odoo_soft_memory_reset")
    return upstream_geventserver_process_limits(self)


upstream_threadedserver_signal_handler = server.ThreadedServer.signal_handler
def threadedserver_signal_handler(self, sig, frame):
    if hasattr(signal, 'SIGXCPU') and sig == signal.SIGXCPU:
        update_metric("odoo_cpu_time_limit_reset")
    return upstream_threadedserver_signal_handler(self, sig, frame)


# upstream_worker_signal_time_expired_handler = server.Worker.signal_time_expired_handler
# def worker_signal_time_expired_handler(self, n, stack):
#     update_metric("odoo_cpu_time_limit_reset")
#     upstream_worker_signal_time_expired_handler(self, n, stack)


# upstream_worker_check_limits = server.Worker.check_limits
# def worker_check_limits(self):
#     LOGGER.debug("Running patched version of odoo.service.server.Worker.check_limits")
#     if self.request_count >= self.request_max:
#         update_metric("odoo_worker_max_requests")
#     memory = memory_info(psutil.Process(os.getpid()))
#     if config['limit_memory_soft'] and memory > config['limit_memory_soft']:
#         update_metric("odoo_soft_memory_reset")
#     return upstream_worker_check_limits(self)

server.ThreadedServer.process_limit = threadedserver_process_limit
server.ThreadedServer.signal_handler = threadedserver_signal_handler
server.GeventServer.process_limits = geventserver_process_limits
# server.Worker.check_limits = worker_check_limits;
# server.Worker.signal_time_expired_handler = worker_signal_time_expired_handler

LOGGER.debug("Patched odoo.service.server")
