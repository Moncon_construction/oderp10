# -*- coding: utf-8 -*-

from odoo.tests.common import HttpCase
try:
    from prometheus_client import parser
except ImportError:
    parser = None


class PrometheusCase(HttpCase):

    def get_metrics(self):
        assert parser is not None
        res = self.url_open("/metrics")
        return parser.text_string_to_metric_families(res.text)


    def setUp(self):
        super(PrometheusCase, self).setUp()
        self.metrics = self.get_metrics()
