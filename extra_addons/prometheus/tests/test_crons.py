# -*- coding: utf-8 -*-
import datetime

from odoo.tools import misc

from odoo.tests.common import tagged
from odoo.addons.prometheus.tests.prometheus_case import PrometheusCase

@tagged("prometheus")
class TestCron(PrometheusCase):

    def test_cron(self):
        now_str = datetime.datetime.now().strftime(misc.DEFAULT_SERVER_DATETIME_FORMAT)
        cron_count = self.env["ir.cron"].search_count([("nextcall", "<", now_str), ("numbercall", "!=", 0)])
        for metric in self.metrics:
            if metric.name == "odoo_missed_crons":
                for sample in metric.samples:
                    self.assertEqual(sample.value, cron_count)
                    break
                break

