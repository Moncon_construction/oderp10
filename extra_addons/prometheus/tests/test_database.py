# -*- coding: utf-8 -*-

from odoo.tools import config
from odoo.tests.common import tagged

from odoo.addons.prometheus.tests.prometheus_case import PrometheusCase
from odoo.addons.prometheus.collectors.database import get_path_size

@tagged("prometheus")
class TestDatabase(PrometheusCase):

    def test_db_uuid(self):
        for metric in self.metrics:
            if metric.name == "odoo_database_uuid":
                for sample in metric.samples:
                    self.assertEqual(sample.labels["uuid"], self.env["ir.config_parameter"].get_param("database.uuid"))
                    break
                break

    def test_db_version(self):
        i_version = self.env.ref("base.module_base").installed_version.split(".")
        version = float("%s.%s" % (i_version[0], i_version[1]))
        for metric in self.metrics:
            if metric.name == "odoo_database_version":
                for sample in metric.samples:
                    self.assertEqual(sample.value, version)
                    break
                break

    def test_db_size(self):
        self.env.cr.execute("SELECT pg_database_size(%s)", (self.env.cr.dbname,))
        dbsize = self.env.cr.fetchone()[0]
        fssize = get_path_size(config.filestore(self.env.cr.dbname))
        for metric in self.metrics:
            if metric.name == "odoo_database_size":
                for sample in metric.samples:
                    if sample.labels["component"] == "database":
                        self.assertEqual(sample.value, dbsize)
                    elif sample.labels["component"] == "filestore":
                        self.assertEqual(sample.value, fssize)
                    else:
                        self.assertEqual(sample.value, fssize + dbsize)
                    break
                break

    def test_db_connections(self):
        for metric in self.metrics:
            if metric.name == "odoo_database_connections":
                for sample in metric.samples:
                    self.assertIsInstance(sample.value, float)
                    self.assertNotEqual(sample.value, 0)
                    break
                break
