# -*- coding: utf-8 -*-

from odoo import release
from odoo.tests.common import tagged
from odoo.addons.prometheus.tests.prometheus_case import PrometheusCase

@tagged("prometheus")
class TestEnterprise(PrometheusCase):

    def test_enterprise_expiration(self):
        # Detect if we are running the enterprise version
        if release.version_info[-1] != "e":
            # Not running enterprise version, ignoring
            self.skipTest("Ignoring enterprise test since we are running community")

        for metric in self.get_metrics():
            if metric.name == "odoo_enterprise_expiration":
                for sample in metric.samples:
                    self.assertIsInstance(sample.value, float)
                    self.assertTrue(sample.value >= 0)
                    break
                break
