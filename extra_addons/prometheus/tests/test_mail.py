# -*- coding: utf-8 -*-

import contextlib

from odoo.sql_db import db_connect

from odoo.tests.common import tagged
from odoo.addons.prometheus.tests.prometheus_case import PrometheusCase

@tagged("prometheus")
class TestMail(PrometheusCase):

    def setUp(self):
        super(TestMail, self).setUp()
        self.skip = False

        # Ensure mail is installed
        if self.env.ref("base.module_mail").state != "installed":
            self.skip = True


    def create_mail(self, state):
        """
        Since we don't hard depend on mail, we need to use SQL
        
        We also use another cursor to ensure that the emails are in the database when the metrics are fetched
        """
        db = db_connect(self.env.cr.dbname)
        with contextlib.closing(db.cursor()) as cr:
            cr.execute("INSERT INTO mail_message (message_type) VALUES ('email') RETURNING id")
            mm_id = cr.fetchone()[0]
            cr.execute("INSERT INTO mail_mail (state, mail_message_id) VALUES (%s, %s)", (state, mm_id))
            cr.commit()


    def test_failed_mails(self):
        if self.skip:
            self.skipTest("Unable to test mail metric since the mail modules isn't installed")

        self.create_mail("exception")
        for metric in self.get_metrics():
            if metric.name == "odoo_mail":
                for sample in metric.samples:
                    if sample.labels["state"] == "failed":
                        self.assertIsInstance(sample.value, float)
                        self.assertNotEqual(sample.value, 0)
                break

    def test_sent_mails(self):
        if self.skip:
            self.skipTest("Unable to test mail metric since the mail modules isn't installed")
        self.create_mail("sent")
        for metric in self.get_metrics():
            if metric.name == "odoo_mail":
                for sample in metric.samples:
                    if sample.labels["state"] == "sent":
                        self.assertIsInstance(sample.value, float)
                        self.assertNotEqual(sample.value, 0)
                break

    def test_received_mails(self):
        if self.skip:
            self.skipTest("Unable to test mail metric since the mail modules isn't installed")
        self.create_mail("received")
        for metric in self.get_metrics():
            if metric.name == "odoo_mail":
                for sample in metric.samples:
                    if sample.labels["state"] == "received":
                        self.assertIsInstance(sample.value, float)
                        self.assertNotEqual(sample.value, 0)
                break
