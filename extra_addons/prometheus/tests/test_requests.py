# -*- coding: utf-8 -*-

from odoo.tests.common import tagged
from odoo.addons.prometheus.tests.prometheus_case import PrometheusCase

@tagged("prometheus")
class TestRequests(PrometheusCase):

    def test_http_time(self):
        for metric in self.metrics:
            if metric.name == "odoo_http_request_time":
                for sample in metric.samples:
                    self.assertIsInstance(sample.value, float)
                    break
                break


    def test_json_time(self):
        for metric in self.metrics:
            if metric.name == "odoo_json_request_time":
                for sample in metric.samples:
                    self.assertIsInstance(sample.value, float)
                    break
                break
