# -*- coding: utf-8 -*-


import datetime

from odoo.tools import misc, convert

from odoo.tests.common import tagged
from odoo.addons.prometheus.tests.prometheus_case import PrometheusCase

@tagged("prometheus")
class TestUser(PrometheusCase):

    def test_internal_users(self):
        Users = self.env["res.users"]

        allusers = Users.search_count([("active", "=", True)])
        for metric in self.metrics:
            if metric.name == "odoo_users":
                for sample in metric.samples:
                    if not convert.str2bool(sample.labels["share"]):
                        self.assertEqual(sample.value, allusers)
                        break
                break

    def test_external_users(self):
        Users = self.env["res.users"]

        external_users = Users.search_count([("active", "=", True), ("share", "=", True)])
        for metric in self.metrics:
            if metric.name == "odoo_users":
                for sample in metric.samples:
                    if convert.str2bool(sample.labels["share"]):
                        self.assertEqual(sample.value, external_users)
                        break
                break

    def test_active_users(self):
        limit_date = datetime.datetime.now() - datetime.timedelta(15)
        limit_date_str = limit_date.strftime(misc.DEFAULT_SERVER_DATETIME_FORMAT)
        
        Users = self.env["res.users"]
        
        allusers = Users.search_count([("active", "=", True), ("login_date", ">=", limit_date_str)])
        for metric in self.metrics:
            if metric.name == "odoo_users_active":
                for sample in metric.samples:
                    if not convert.str2bool(sample.labels["share"]):
                        self.assertEqual(sample.value, allusers)
                        break
                break


    def test_active_external_users(self):
        limit_date = datetime.datetime.now() - datetime.timedelta(15)
        limit_date_str = limit_date.strftime(misc.DEFAULT_SERVER_DATETIME_FORMAT)
        
        Users = self.env["res.users"]
        
        external_users = Users.search_count([("active", "=", True), ("share", "=", True), ("login_date", ">=", limit_date_str)])
        for metric in self.metrics:
            if metric.name == "odoo_users_active":
                for sample in metric.samples:
                    if convert.str2bool(sample.labels["share"]):
                        self.assertEqual(sample.value, external_users)
                        break
                break
